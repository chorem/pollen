package org.chorem.pollen.votecounting;

/*-
 * #%L
 * Pollen :: VoteCounting :: Instant Runoff
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class InstantRunoffRound implements Serializable {

    private static final long serialVersionUID = -1027997064615605718L;

    protected List<InstantRunoffRoundChoice> roundChoices;

    protected List<String> choiceIdsExclude;

    public List<InstantRunoffRoundChoice> getRoundChoices() {
        if (roundChoices == null) {
            roundChoices = Lists.newLinkedList();
        }
        return roundChoices;
    }

    public void setRoundChoices(List<InstantRunoffRoundChoice> roundChoices) {
        this.roundChoices = roundChoices;
    }

    public List<String> getChoiceIdsExclude() {
        if (choiceIdsExclude == null) {
            choiceIdsExclude = Lists.newLinkedList();
        }
        return choiceIdsExclude;
    }

    public void setChoiceIdsExclude(List<String> choiceIdsExclude) {
        this.choiceIdsExclude = choiceIdsExclude;
    }
}
