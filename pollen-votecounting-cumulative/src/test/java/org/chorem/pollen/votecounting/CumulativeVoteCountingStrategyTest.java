/*
 * #%L
 * Pollen :: VoteCounting :: Percentage
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.ListOfVoter;
import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.SimpleVoter;
import org.chorem.pollen.votecounting.model.SimpleVoterBuilder;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the {@link CumulativeVoteCountingStrategy}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class CumulativeVoteCountingStrategyTest {

    public static final String CHOICE_A = "a";

    public static final String CHOICE_B = "b";

    public static final String CHOICE_C = "c";

    protected static CumulativeVoteCounting voteCounting;

    protected CumulativeVoteCountingStrategy strategy;

    @BeforeClass
    public static void beforeClass() throws Exception {
        VoteCountingFactory factory = new VoteCountingFactory();
        voteCounting = CumulativeVoteCounting.class.cast(factory.getVoteCounting(CumulativeVoteCounting.VOTECOUNTING_ID));
    }

    @Before
    public void setUp() throws Exception {
        strategy = voteCounting.newStrategy();
        CumulativeConfig config = new CumulativeConfig();
        config.setPoints(100);
        strategy.setConfig(config);
    }

    @Test
    public void simpleVotecount() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=100  b=null c=null)
        // 2      (a=null b=100  c=null)
        // 3      (a=50   b=50   c=null)
        // Result (a=50   b=50   c=null)

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 100.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 100.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 50.)
                .addVoteForChoice(CHOICE_B, 50.)
                .addVoteForChoice(CHOICE_C, null)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(50.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(50.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(0.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount2() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=20    b=30    c=50)
        // 2      (a=50    b=20    c=30)
        // 3      (a=10    b=50    c=40)
        // Result (a=26.67 b=33.33 c=40)
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 20.)
                .addVoteForChoice(CHOICE_B, 30.)
                .addVoteForChoice(CHOICE_C, 50.)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 50.)
                .addVoteForChoice(CHOICE_B, 20.)
                .addVoteForChoice(CHOICE_C, 30.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 10.)
                .addVoteForChoice(CHOICE_B, 50.)
                .addVoteForChoice(CHOICE_C, 40.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(26.7),2),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(33.3),1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(40.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount3() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=50  b=50  c=null)
        // 2      (a=50  b=50  c=null)
        // 3      (a=50  b=50  c=null)
        // Result (a=50  b=50  c=null)
        Set<Voter> voters = new SimpleVoterBuilder()
            .newVoter("1", 1.)
            .addVoteForChoice(CHOICE_A, 50.)
            .addVoteForChoice(CHOICE_B, 50.)
            .addVoteForChoice(CHOICE_C, null)
            .newVoter("2", 1.)
            .addVoteForChoice(CHOICE_A, 50.)
            .addVoteForChoice(CHOICE_B, 50.)
            .addVoteForChoice(CHOICE_C, null)
            .newVoter("3", 1.)
            .addVoteForChoice(CHOICE_A, 50.)
            .addVoteForChoice(CHOICE_B, 50.)
            .addVoteForChoice(CHOICE_C, null)
            .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(50.0),0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(50.0),0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(0.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount1() throws Exception {

        // poll with weighted vote
        // 1 (x2) (a=100  b=null c=null)
        // 2 (x1) (a=null b=100  c=null)
        // 3 (x1) (a=null b=50   c=50)
        // Result (a=50   b=37.5 c=12.5)
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 100.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 100.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 50.)
                .addVoteForChoice(CHOICE_C, 50.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(50.0),0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(37.5),1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(12.5), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount2() throws Exception {

        // poll with weighted vote
        // 1 (x2) (a=100  b=null c=null)
        // 2 (x1) (a=null b=100  c=null)
        // 3 (x3) (a=null b=50   c=50)
        // Result (a=33.3  b=41.7  c=25)
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 100.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 100.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 3.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 50.)
                .addVoteForChoice(CHOICE_C, 50.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(33.3),1),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(41.7),0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(25.0), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void listVotecount() throws Exception {

        // Group poll (all weight to 1)

        // G1U1      a => 60%     b => 30%     c => 10%   * 2
        // G1U2      a => 50%     b => 25%     c => 25%
        // G1U3      a => 00%     b => 10%     c => 90%
        // Result G1 a => 42.50%  b => 23.75%  c => 33.75%
        // Vote G1   a => 42.50%  b => 23.75%  c => 33.75%

        // G2U1      a => 60%     b => 30%     c => 10%    * 2
        // G2U2      a => 10%     b => 30%     c => 60%
        // G2U3      a => 20%     b => 50%     c => 30%
        // Result G2 a => 37.50%  b => 35.00%  c => 27.50%
        // Vote G1   a => 37.50%  b => 35.00%  c => 27.50%  * 2

        // Result    a => 39.17%  b => 31.25%  c => 29.58%

        ListOfVoter voters = ListOfVoter.newVoter(null, 1, Sets.newHashSet(
                ListOfVoter.newVoter("G1", 1, Sets.newHashSet(
                        SimpleVoter.newVoter("G1U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 60d),
                                VoteForChoice.newVote(CHOICE_B, 30d),
                                VoteForChoice.newVote(CHOICE_C, 10d))),
                        SimpleVoter.newVoter("G1U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 50d),
                                VoteForChoice.newVote(CHOICE_B, 25d),
                                VoteForChoice.newVote(CHOICE_C, 25d))),
                        SimpleVoter.newVoter("G1U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 00d),
                                VoteForChoice.newVote(CHOICE_B, 10d),
                                VoteForChoice.newVote(CHOICE_C, 90d))))),
                ListOfVoter.newVoter("G2", 2, Sets.newHashSet(
                        SimpleVoter.newVoter("G2U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 60d),
                                VoteForChoice.newVote(CHOICE_B, 30d),
                                VoteForChoice.newVote(CHOICE_C, 10d))),
                        SimpleVoter.newVoter("G2U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 10d),
                                VoteForChoice.newVote(CHOICE_B, 30d),
                                VoteForChoice.newVote(CHOICE_C, 60d))),
                        SimpleVoter.newVoter("G2U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 20d),
                                VoteForChoice.newVote(CHOICE_B, 50d),
                                VoteForChoice.newVote(CHOICE_C, 30d)))))
        ));

        ListVoteCountingResult result = strategy.votecount(voters);

        assertThat(result)
                .isNotNull();
        assertThat(result.getMainResult()).isNotNull();
        assertThat(result.getMainResult().getScores()).isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(39.2), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(31.3), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(29.6), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

}
