package org.chorem.pollen.votecounting;

/*
 * #%L
 * Pollen :: VoteCounting :: Percentage
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import org.chorem.pollen.votecounting.model.ChoiceToVoteRenderType;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.Locale;
import java.util.Objects;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.n;

/**
 * Percentage vote counting entry point.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6
 */
public class CumulativeVoteCounting extends AbstractVoteCounting<CumulativeVoteCountingStrategy, CumulativeConfig> {

    public static final int VOTECOUNTING_ID = 2;

    public CumulativeVoteCounting() {
        super(VOTECOUNTING_ID,
                CumulativeVoteCountingStrategy.class,
                CumulativeConfig.class,
                n("pollen.voteCountingType.cumulative"),
                n("pollen.voteCountingType.cumulative.shortHelp"),
                n("pollen.voteCountingType.cumulative.help")
        );
    }

    @Override
    public ChoiceToVoteRenderType getVoteValueEditorType() {
        return ChoiceToVoteRenderType.TEXTFIELD;
    }

    @Override
    public Double getMinimumValue() {
        return 0d;
    }

    @Override
    public Multimap<String, String> checkVoteForChoice(VoteForChoice voteForChoice, CumulativeConfig config, Locale locale) {
        Multimap<String, String> errorMap = super.checkVoteForChoice(voteForChoice, config, locale);

        Double voteValue = voteForChoice.getVoteValue();
        if (voteValue != null) {
            if (voteValue < 0) {
                errorMap.put(
                        VoteForChoice.PROPERTY_VOTE_VALUE,
                        l(locale, "pollen.voteCountingType.cumulative.voteValue.error.positive", voteValue));

            } else if (voteValue > config.getPoints()) {
                errorMap.put(
                        VoteForChoice.PROPERTY_VOTE_VALUE,
                        l(locale, "pollen.voteCountingType.cumulative.voteValue.error.max", voteValue, config.getPoints()));
            }
        }

        return errorMap;

    }

    @Override
    public Multimap<String, String> checkVote(Voter vote, CumulativeConfig config, Locale locale) {
        Multimap<String, String> errorMap = super.checkVote(vote, config, locale);

        double totalValues = vote.getVoteForChoices()
                .stream()
                .mapToDouble(VoteForChoice::getVoteValue)
                .filter(Objects::nonNull)
                .sum();
        if (totalValues != config.getPoints()) {
            errorMap.put(
                    "totalValues",
                    l(locale, "pollen.voteCountingType.cumulative.totalValue.error.equal", totalValues, config.getPoints()));
        }

        return errorMap;

    }
}
