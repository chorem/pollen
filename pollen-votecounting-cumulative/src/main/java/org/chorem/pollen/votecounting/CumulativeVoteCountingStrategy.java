/*
 * #%L
 * Pollen :: VoteCounting :: Percentage
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

/**
 * Percentage strategy.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class CumulativeVoteCountingStrategy extends AbstractVoteCountingStrategy<CumulativeConfig> {

    @Override
    public VoteCountingResult votecount(Set<Voter> voters) {

        // get empty result by choice
        Map<String, ChoiceScore> scores = newEmptyChoiceScoreMap(voters);

        BigDecimal weightSum = BigDecimal.ZERO;
        for (Voter voter : voters) {

            weightSum = weightSum.add(BigDecimal.valueOf(voter.getWeight()));
            // add this voter votes to result
            addVoterChoices(voter, scores);
        }

        for (ChoiceScore choiceScore : scores.values()) {
            BigDecimal scoreValue = choiceScore.getScoreValue();
            if (scoreValue != null) {
                choiceScore.setScoreValue(scoreValue.divide(weightSum, BigDecimal.ROUND_HALF_UP));
            }
        }

        // order scores (using their value) and return result
        return orderByValues(scores.values(), null);
    }

    protected void addVoterChoices(Voter voter,
                                   Map<String, ChoiceScore> scores) {

        double voterWeight = voter.getWeight();

        for (VoteForChoice voteForChoice : voter.getVoteForChoices()) {
            Double voteValue = voteForChoice.getVoteValue();
            if (voteValue != null) {

                // get score for choice
                String choiceId = voteForChoice.getChoiceId();
                ChoiceScore choiceScore = scores.get(choiceId);

                // compute score to add
                double scoreToAdd = voteValue * voterWeight;

                // add to score this weighted vote
                choiceScore.addScoreValue(scoreToAdd);
            }
        }
    }

    @Override
    public Set<VoteForChoice> toVoteForChoices(VoteCountingResult voteCountingResult) {
        Set<VoteForChoice> voteForChoices = Sets.newHashSet();

        for (ChoiceScore choiceScore : voteCountingResult.getScores()) {

            double score = choiceScore.getScoreValue().doubleValue();
            VoteForChoice voteForChoice = VoteForChoice.newVote(
                    choiceScore.getChoiceId(),
                    score);
            voteForChoices.add(voteForChoice);
        }
        return voteForChoices;
    }


}
