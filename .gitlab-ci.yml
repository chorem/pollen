include:
  - project: codelutin/ci
    file:
      - ci.yml
      - build_base.yml
  - project: codelutin/swarmpages
    file: swarmpages_base.yml

stages:
- swarmpages-build
- swarmpages
- build
- dockerbuild
- dockertest
- deploy-demo
- release
- sonar
- sonar-ui

variables:
  SP_ADDRESSES: doc.pollen.cl

build-java:
  image: maven:3.9-eclipse-temurin-21-alpine
  stage: build
  script:
    - mvn clean package
    - |
      if [ -n "$CI_COMMIT_TAG" ]; then
        echo "ToDo: send to Nexus"
      fi
  artifacts:
    paths:
    - pollen-rest-api/target/pollen-rest-api-*.war
    expire_in: 4 weeks
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_MESSAGE =~ /Merge branch/
      when: never
    - if: $CI_PIPELINE_SOURCE != "schedule"

build-js:
  image: node:16-alpine
  stage: build
  script:
    - cd pollen-ui-riot-js; npm install
    - ./node_modules/.bin/eslint --ext .js,.tag.html src/main/
    - npm run-script package
    - |
      if [ -n "$CI_COMMIT_TAG" ]; then
        echo "ToDo: send to Nexus"
      fi
  artifacts:
    paths:
    - pollen-ui-riot-js/target/dist/
    expire_in: 4 weeks
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_MESSAGE =~ /Merge branch/
      when: never
    - if: $CI_PIPELINE_SOURCE != "schedule"

release:
  image: maven:3.9-eclipse-temurin-21-alpine
  stage: release
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^release\/.*$/'
      when: manual
  variables:
    # Les tags ne sont pas nettoyés par GitLab par défaut, et posent problème
    # lorsqu'on relance des jobs maven-release. Pour les nettoyer, on rajoute
    # '--prune-tags'.
    # Cf. https://stackoverflow.com/questions/1841341/remove-local-git-tags-that-are-no-longer-on-the-remote-repository/54297675#54297675
    # et https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-fetch-extra-flags
    GIT_FETCH_EXTRA_FLAGS: --prune --prune-tags
  script:
    # Install dependencies (via our package cache)
    # jq required for pollen-ui-riot-js version update
    - sed -i -E 's#https?://dl-cdn.alpinelinux.org#https://pcache.cloud.codelutin.com/dl-cdn.alpinelinux.org#' /etc/apk/repositories
    - apk add git jq
    - VERSION="${CI_COMMIT_REF_NAME#release/}"
    - echo "create release for $VERSION"
    # Replace remote by SSH remote. This uses the private key of the
    # maven-release GitLab account, which is accessible by the maven-release
    # runner
    - git remote remove origin
    - git remote add origin "git@gitlab.nuiton.org:chorem/pollen.git"
    - git fetch origin develop
    - git fetch origin master
    # ce job ajoute des commits sur la branche, et peut échouer. Dans ce
    # cas, on doit pouvoir relancer le job en ignorant lesdits commits. On
    # utilise donc `-B` pour réinitialiser la branche si elle existe, en
    # utilisant comme point de départ le commit pour lequel le job a été créé
    - git checkout -B "$CI_COMMIT_BRANCH" "$CI_COMMIT_SHA"
    - mvn org.codehaus.mojo:versions-maven-plugin:set -DnewVersion=$VERSION
    - mvn org.codehaus.mojo:versions-maven-plugin:display-dependency-updates
      | sed -n '/The following dependencies in Dependencies have newer versions/,/--------------------------------------------------/p'
    - mvn org.codehaus.mojo:versions-maven-plugin:use-releases
    - mvn org.codehaus.mojo:license-maven-plugin:1.20:check-file-header
      -Dlicense.failOnMissingHeader=true
      -DfailOnNotUptodateHeader=true
      -Dlicense.roots=src/main
      -Dlicense.excludes='**/i18n/*.properties,**/THIRD-PARTY.properties,**/*.sh,**/*.sql,**/*.json,**/*.svg'
      || (echo "Veullez mettre a jour vos header sur la branche $RELEASE avec la commande 'mvn -Pupdate-file-header'"; exit 2)
    # Update pollen-ui-riot-js version (same as 'npm version', avoid installing npm just for this)
    - |
      (
      set -xeu
      cd pollen-ui-riot-js
      mv package.json package.json.old
      jq ".version = \"$VERSION\"" package.json.old > package.json
      mv package-lock.json package-lock.json.old
      jq ".version = \"$VERSION\"" package-lock.json.old > package-lock.json
      )
    - git commit -a -m "release $VERSION success [skip ci]"
    # Replace 'git flow release finish -m "release $VERSION success"' by manual
    # actions. This gives more clarity and control.
    - |
      (
      set -xeu
      git checkout master
      git merge --no-ff -m "Merge branch '$CI_COMMIT_REF_NAME'" $CI_COMMIT_REF_NAME
      git tag "$VERSION"
      git checkout develop
      git merge --no-ff -m "[skip-ci] Merge tag '$VERSION'" "$VERSION"
      mvn org.codehaus.mojo:versions-maven-plugin:set -DnextSnapshot
      git commit -am "[skip-ci] Update version"
      git push --verbose --branches
      git push --verbose --tags
      git push -d origin $CI_COMMIT_REF_NAME # delete remote release branch
      )
  tags:
    - maven-release

sonar_job:
  image: maven:3.9-eclipse-temurin-21-alpine
  stage: sonar
  script:
    - mvn -e -X clean
    - mvn -e -U clean verify org.sonarsource.scanner.maven:sonar-maven-plugin:$MAVEN_SONAR_PLUGIN_VERSION:sonar -Dsonar.projectKey=org.chorem:pollen -Dsonar.exclusions='**/js/libs/*','**/target/*','**/generated*/*'
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

sonar_job_ui:
  image: sonarsource/sonar-scanner-cli:11.0
  stage: sonar-ui
  script:
    - cd pollen-ui-riot-js; npm install
    - sonar-scanner -Dsonar.host.url="$SONAR_INSTANCE_URL" -Dsonar.projectKey=org.chorem:pollen-ui -Dsonar.projectName=Pollen-UI
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

build-docker:
  extends: .build
  before_script:
    # Remove 'release/', replace other '/' with '_'
    - export IMAGE_TAG="$(echo "$CI_COMMIT_REF_NAME" | sed 's#release/##; s#/#_#;')"
    # Parse pom.xml to get the Tomcat version used during tests
    - TOMCAT_VERSION="$(grep '<tomcatEmbedVersion>.*</tomcatEmbedVersion>' pom.xml | grep -oE "[0-9]+\.[0-9]+\.[0-9]+")"
    - EXTRA_BUILD_OPTIONS="--build-arg TOMCAT_VERSION=$TOMCAT_VERSION --build-arg REVISION=$CI_COMMIT_SHA"
  when: on_success # Override 'when: manual' from template
  dependencies:
    - build-java
    - build-js
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_MESSAGE =~ /Merge branch/
      when: never
    - if: $CI_PIPELINE_SOURCE != "schedule"
  tags:
    - h3n1r1 # Runner with BuildKit

docker-test:
  image: docker:25-cli
  stage: dockertest
  script:
    - |
      set -euo pipefail

      echo "Start container"

      # The container where this job is run may not be in the same network as
      # the pollen-test container, so instead of contacting it directly via it's
      # IP, we expose the 8080 port on pollen-test and we make requests to
      # $GATEWAY_IP:8080.
      GATEWAY_IP="$(ip route | grep default | cut -d ' ' -f 3)"

      # IMAGE_DIGEST is exported by the codelutin/ci/build_base.yml template
      (
        set +e
        set -x
        docker run --rm -d --name pollen-test -p 8080:8080 "$IMAGE_DIGEST"
        if [ $? -eq 125 ]; then
          echo "Another container may be using port 8080"
          sleep 60
          docker run --rm -d --name pollen-test -p 8080:8080 "$IMAGE_DIGEST"
        fi
        sleep 30
      )

      # Save logs to var while printing them
      DOCKER_LOGS="$(docker logs pollen-test 2>&1 | tee /dev/stderr)"
    - |
      echo "Test 1/3: check if all listeners started correctly"
      if [[ "$DOCKER_LOGS" =~ 'One or more listeners failed to start' ]]; then exit 1; fi
    - |
      echo "Test 2/3: check if front responds"
      (set -x; wget -qO- "$GATEWAY_IP:8080") | if grep -E --color "HTTP [4-5][0-9]{2} "; then exit 1; fi
    - |
      echo "Test 3/3: check if back responds"
      (set -x; wget -qO- "$GATEWAY_IP:8080/pollen-rest-api/v1/configuration") | grep userConnectedRequired | grep -q usersCanCreatePoll
  after_script:
    - docker rm -f pollen-test
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_MESSAGE =~ /Merge branch/
      when: never
    - if: $CI_PIPELINE_SOURCE != "schedule"
  tags:
    - docker

# deploy-demo:
#   image: alpine
#   stage: deploy-demo
#   script:
#     - wget -qO- 'https://webhooks.demo.codelutin.com/deploy?target_dir=/var/local/demo4/pollen-latest'
#   rules:
#     - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"

swarmpages-build:
  image: python:3.11-slim-bullseye
  stage: swarmpages-build
  script:
    # https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes
    - pip3 install mkdocs~=1.5.3 mkdocs-material~=9.4
    - mkdocs build
    - mv site public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
      changes:
        - docs/**/*
        - mkdocs.yml
  artifacts:
    paths:
      - public

swarmpages-build-docker:
  extends: .swarmpages-build-docker
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
      changes:
        - docs/**/*
        - mkdocs.yml

swarmpages-deploy:
  extends: .swarmpages-deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
      changes:
        - docs/**/*
        - mkdocs.yml
