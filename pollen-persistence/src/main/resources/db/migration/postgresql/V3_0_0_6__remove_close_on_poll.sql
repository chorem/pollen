-- remove close in poll
update poll set enddate = NOW() where enddate is null and closed;
alter table poll drop column closed;