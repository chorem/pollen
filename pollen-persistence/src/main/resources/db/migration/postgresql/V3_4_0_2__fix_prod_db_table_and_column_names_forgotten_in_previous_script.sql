    --
-- Corrige la base de production
--

CREATE OR REPLACE FUNCTION rename_column_if_exists(
    table_name TEXT,
    old_column_name TEXT,
    new_column_name TEXT
)
RETURNS VOID AS $$
DECLARE
    column_exists BOOLEAN;
BEGIN
    -- Verify whether the column is present in the table
    EXECUTE format('SELECT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_name = %L
        AND column_name = %L
    )', table_name, old_column_name)
    INTO column_exists;

    -- If the column exists, rename it
    IF column_exists THEN
        EXECUTE format('ALTER TABLE %I RENAME COLUMN %I TO %I', table_name, old_column_name, new_column_name);
    END IF;
END;
$$ LANGUAGE plpgsql;

SELECT rename_column_if_exists('favoriteList', 'pollenuser', 'pollenUser');
SELECT rename_column_if_exists('favoriteList', 'parentlists', 'parentLists');

DROP FUNCTION rename_column_if_exists;