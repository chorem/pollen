-- add invitation sent in VoterListMember
alter table voterlistmember add invitationsent boolean;
update voterlistmember set invitationsent = true;