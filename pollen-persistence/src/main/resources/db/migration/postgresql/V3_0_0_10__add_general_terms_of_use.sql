-- add general terms of use in poll and user
alter table poll add gtuValidationDate TIMESTAMP;
alter table pollenUser add gtuValidationDate TIMESTAMP;
