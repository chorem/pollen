--
-- Création de la table Question
--

CREATE TABLE question (
    topiaid VARCHAR(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate timestamp without time zone,
    title VARCHAR(255),
    description TEXT,
    beginchoicedate timestamp without time zone,
    endchoicedate timestamp without time zone,
    choiceaddallowed boolean,
    votecountingtype INTEGER,
    votecountingconfig TEXT,
    questionorder INTEGER,
    poll VARCHAR(255)
);


ALTER TABLE ONLY question
    ADD CONSTRAINT question_pkey PRIMARY KEY (topiaid);

ALTER TABLE ONLY question
    ADD CONSTRAINT fksmy9lue6u4dk2tpvn608rjsww FOREIGN KEY (poll) REFERENCES poll(topiaid);

--
-- Ajout dans la table Choice
--
ALTER TABLE choice ADD question VARCHAR(255);

ALTER TABLE ONLY choice
    ADD CONSTRAINT fk44vwej9g109rxo3lxpaqorn86 FOREIGN KEY (question) REFERENCES question(topiaid);

--
-- Ajout dans la table Choice
--
ALTER TABLE "comment" ADD question VARCHAR(255);

ALTER TABLE ONLY "comment"
    ADD CONSTRAINT fkmnu16n1e6icu727us4i5rl29p FOREIGN KEY (question) REFERENCES question(topiaid);

--
-- Ajout dans la table Vote
--
ALTER TABLE vote ADD question VARCHAR(255);

ALTER TABLE ONLY vote
    ADD CONSTRAINT fkf2x0c7yxch6w2vyqjm7pdcsov FOREIGN KEY (question) REFERENCES question(topiaid);

--
-- Migration des données de poll vers question
--

INSERT INTO question
    SELECT replace(topiaId, 'Poll', 'Question'),
    1,
    current_timestamp,
    title,
    description,
    beginchoicedate,
    endchoicedate,
    choiceaddallowed,
    votecountingtype,
    votecountingconfig,
    0,
    topiaid
    FROM poll;


--
-- remplacement des pollId dans les choix
--

UPDATE choice c set question = (select q.topiaid from question q where q.poll = c.poll);

--
-- remplacement des pollId dans les votes
--

UPDATE vote v set question = (select q.topiaid from question q where q.poll = v.poll);

--
-- supression des colonnes des sondages
--

ALTER TABLE poll
DROP COLUMN IF EXISTS beginchoicedate,
DROP COLUMN IF EXISTS endchoicedate,
DROP COLUMN IF EXISTS maxchoicenumber,
DROP COLUMN IF EXISTS choiceaddallowed,
DROP COLUMN IF EXISTS votecountingtype,
DROP COLUMN IF EXISTS participants,
DROP COLUMN IF EXISTS withme,
DROP COLUMN IF EXISTS votecountingconfig;

--
-- Supression du poll des choix
--

ALTER TABLE choice DROP COLUMN IF EXISTS poll;

--
-- Supression du poll des votes
--

ALTER TABLE vote DROP COLUMN IF EXISTS poll;
