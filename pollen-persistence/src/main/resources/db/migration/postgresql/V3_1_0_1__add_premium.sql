-- add premium in poll and user
alter table poll add premium boolean;
update poll set premium = false;

alter table poll add notificationMaxVoterSend boolean;
update poll set notificationMaxVoterSend = false;

alter table pollenUser add premiumTo TIMESTAMP;
