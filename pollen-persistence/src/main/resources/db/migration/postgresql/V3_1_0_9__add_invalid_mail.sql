-- add invalid email in PollenPrincipal
alter table pollenprincipal add invalid boolean;
update pollenprincipal set invalid = false;