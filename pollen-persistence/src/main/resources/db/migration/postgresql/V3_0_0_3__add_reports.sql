-- reporting

CREATE TABLE report (
    topiaid character varying(255) NOT NULL PRIMARY KEY,
    topiaversion bigint NOT NULL,
    topiacreatedate timestamp without time zone,
    level integer,
    email character varying(255),
    ignore boolean,
    targetid character varying(255)
);
