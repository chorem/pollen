--
-- Corrige la base de production
--

CREATE OR REPLACE FUNCTION rename_column_if_exists(
    table_name TEXT,
    old_column_name TEXT,
    new_column_name TEXT
)
RETURNS VOID AS $$
DECLARE
    column_exists BOOLEAN;
BEGIN
    -- Verify whether the column is present in the table
    EXECUTE format('SELECT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_name = %L
        AND column_name = %L
    )', table_name, old_column_name)
    INTO column_exists;

    -- If the column exists, rename it
    IF column_exists THEN
        EXECUTE format('ALTER TABLE %I RENAME COLUMN %I TO %I', table_name, old_column_name, new_column_name);
    END IF;
END;
$$ LANGUAGE plpgsql;

ALTER TABLE IF EXISTS public."childfavoritelist" RENAME TO "childFavoriteList";
SELECT rename_column_if_exists('childFavoriteList', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('childFavoriteList', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('childFavoriteList', 'topiacreatedate', 'topiaCreateDate');

SELECT rename_column_if_exists('choice', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('choice', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('choice', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('choice', 'choicevalue', 'choiceValue');
SELECT rename_column_if_exists('choice', 'choiceorder', 'choiceOrder');
SELECT rename_column_if_exists('choice', 'choicetype', 'choiceType');

SELECT rename_column_if_exists('comment', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('comment', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('comment', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('comment', 'postdate', 'postDate');

ALTER TABLE IF EXISTS public."emailtoresend" RENAME TO "emailToResend";
SELECT rename_column_if_exists('emailToResend', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('emailToResend', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('emailToResend', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('emailToResend', 'adrfrom', 'adrFrom');
SELECT rename_column_if_exists('emailToResend', 'replyto', 'replyTo');

ALTER TABLE IF EXISTS public."favoritelist" RENAME TO "favoriteList";
SELECT rename_column_if_exists('favoriteList', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('favoriteList', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('favoriteList', 'topiacreatedate', 'topiaCreateDate');

ALTER TABLE IF EXISTS public."favoritelistmember" RENAME TO "favoriteListMember";
SELECT rename_column_if_exists('favoriteListMember', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('favoriteListMember', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('favoriteListMember', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('favoriteListMember', 'favoritelist', 'favoriteList');

ALTER TABLE IF EXISTS public."loginprovider" RENAME TO "loginProvider";
SELECT rename_column_if_exists('loginProvider', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('loginProvider', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('loginProvider', 'topiacreatedate', 'topiaCreateDate');

SELECT rename_column_if_exists('poll', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('poll', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('poll', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('poll', 'begindate', 'beginDate');
SELECT rename_column_if_exists('poll', 'enddate', 'endDate');
SELECT rename_column_if_exists('poll', 'anonymousvoteallowed', 'anonymousVoteAllowed');
SELECT rename_column_if_exists('poll', 'continuousresults', 'continuousResults');
SELECT rename_column_if_exists('poll', 'notifymehoursbeforepollends', 'notifyMeHoursBeforePollEnds');
SELECT rename_column_if_exists('poll', 'pollendremindersent', 'pollEndReminderSent');
SELECT rename_column_if_exists('poll', 'notificationlocale', 'notificationLocale');
SELECT rename_column_if_exists('poll', 'votenotification', 'voteNotification');
SELECT rename_column_if_exists('poll', 'feedcontent', 'feedContent');
SELECT rename_column_if_exists('poll', 'commentnotification', 'commentNotification');
SELECT rename_column_if_exists('poll', 'newchoicenotification', 'newChoiceNotification');
SELECT rename_column_if_exists('poll', 'gtuvalidationdate', 'gtuValidationDate');
SELECT rename_column_if_exists('poll', 'notificationmaxvotersend', 'notificationMaxVoterSend');
SELECT rename_column_if_exists('poll', 'emailaddresssuffixes', 'emailAddressSuffixes');
SELECT rename_column_if_exists('poll', 'polltype', 'pollType');
SELECT rename_column_if_exists('poll', 'votevisibility', 'voteVisibility');
SELECT rename_column_if_exists('poll', 'commentvisibility', 'commentVisibility');
SELECT rename_column_if_exists('poll', 'resultvisibility', 'resultVisibility');

ALTER TABLE IF EXISTS public."pollenprincipal" RENAME TO "pollenPrincipal";
SELECT rename_column_if_exists('pollenPrincipal', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('pollenPrincipal', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('pollenPrincipal', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('pollenPrincipal', 'pollenuser', 'pollenUser');

ALTER TABLE IF EXISTS public."pollenresource" RENAME TO "pollenResource";
SELECT rename_column_if_exists('pollenResource', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('pollenResource', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('pollenResource', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('pollenResource', 'resourcecontent', 'resourceContent');
SELECT rename_column_if_exists('pollenResource', 'contenttype', 'contentType');
SELECT rename_column_if_exists('pollenResource', 'resourcetype', 'resourceType');

ALTER TABLE IF EXISTS public."pollentoken" RENAME TO "pollenToken";
SELECT rename_column_if_exists('pollenToken', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('pollenToken', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('pollenToken', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('pollenToken', 'creationdate', 'creationDate');
SELECT rename_column_if_exists('pollenToken', 'enddate', 'endDate');

ALTER TABLE IF EXISTS public."pollenuser" RENAME TO "pollenUser";
SELECT rename_column_if_exists('pollenUser', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('pollenUser', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('pollenUser', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('pollenUser', 'gtuvalidationdate', 'gtuValidationDate');
SELECT rename_column_if_exists('pollenUser', 'premiumto', 'premiumTo');
SELECT rename_column_if_exists('pollenUser', 'cancreatepoll', 'canCreatePoll');
SELECT rename_column_if_exists('pollenUser', 'defaultemailaddress', 'defaultEmailAddress');

ALTER TABLE IF EXISTS public."pollenuseremailaddress" RENAME TO "pollenUserEmailAddress";
SELECT rename_column_if_exists('pollenUserEmailAddress', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('pollenUserEmailAddress', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('pollenUserEmailAddress', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('pollenUserEmailAddress', 'emailaddress', 'emailAddress');
SELECT rename_column_if_exists('pollenUserEmailAddress', 'pgppublickey', 'pgpPublicKey');
SELECT rename_column_if_exists('pollenUserEmailAddress', 'pollenuser', 'pollenUser');

SELECT rename_column_if_exists('question', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('question', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('question', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('question', 'beginchoicedate', 'beginChoiceDate');
SELECT rename_column_if_exists('question', 'endchoicedate', 'endChoiceDate');
SELECT rename_column_if_exists('question', 'choiceaddallowed', 'choiceAddAllowed');
SELECT rename_column_if_exists('question', 'votecountingtype', 'voteCountingType');
SELECT rename_column_if_exists('question', 'votecountingconfig', 'voteCountingConfig');
SELECT rename_column_if_exists('question', 'questionorder', 'questionOrder');

SELECT rename_column_if_exists('report', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('report', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('report', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('report', 'targetid', 'targetId');

ALTER TABLE IF EXISTS public."sessiontoken" RENAME TO "sessionToken";
SELECT rename_column_if_exists('sessionToken', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('sessionToken', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('sessionToken', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('sessionToken', 'pollenuser', 'pollenUser');
SELECT rename_column_if_exists('sessionToken', 'pollentoken', 'pollenToken');

ALTER TABLE IF EXISTS public."usercredential" RENAME TO "userCredential";
SELECT rename_column_if_exists('userCredential', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('userCredential', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('userCredential', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('userCredential', 'userid', 'userId');
SELECT rename_column_if_exists('userCredential', 'username', 'userName');
SELECT rename_column_if_exists('userCredential', 'pollenuser', 'pollenUser');

SELECT rename_column_if_exists('vote', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('vote', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('vote', 'topiacreatedate', 'topiaCreateDate');

ALTER TABLE IF EXISTS public."votetochoice" RENAME TO "voteToChoice";
SELECT rename_column_if_exists('voteToChoice', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('voteToChoice', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('voteToChoice', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('voteToChoice', 'votevalue', 'voteValue');

SELECT rename_column_if_exists('voter', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('voter', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('voter', 'topiacreatedate', 'topiaCreateDate');

ALTER TABLE IF EXISTS public."voterlist" RENAME TO "voterList";
SELECT rename_column_if_exists('voterList', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('voterList', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('voterList', 'topiacreatedate', 'topiaCreateDate');

ALTER TABLE IF EXISTS public."voterlistmember" RENAME TO "voterListMember";
SELECT rename_column_if_exists('voterListMember', 'topiaid', 'topiaId');
SELECT rename_column_if_exists('voterListMember', 'topiaversion', 'topiaVersion');
SELECT rename_column_if_exists('voterListMember', 'topiacreatedate', 'topiaCreateDate');
SELECT rename_column_if_exists('voterListMember', 'invitationsent', 'invitationSent');
SELECT rename_column_if_exists('voterListMember', 'voterlist', 'voterList');

DROP FUNCTION rename_column_if_exists;