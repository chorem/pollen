--
-- Corrige la base de production
--

ALTER TABLE IF EXISTS public."childfavoritelist" RENAME TO "childFavoriteList";
ALTER TABLE IF EXISTS public."childFavoriteList" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."childFavoriteList" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."childFavoriteList" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";

ALTER TABLE IF EXISTS public."choice" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."choice" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."choice" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."choice" ALTER COLUMN IF EXISTS choicevalue RENAME TO "choiceValue";
ALTER TABLE IF EXISTS public."choice" ALTER COLUMN IF EXISTS choiceorder RENAME TO "choiceOrder";
ALTER TABLE IF EXISTS public."choice" ALTER COLUMN IF EXISTS choicetype RENAME TO "choiceType";

ALTER TABLE IF EXISTS public."comment" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."comment" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."comment" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."comment" ALTER COLUMN IF EXISTS postdate RENAME TO "postDate";

ALTER TABLE IF EXISTS public."emailtoresend" RENAME TO "emailToResend";
ALTER TABLE IF EXISTS public."emailToResend" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."emailToResend" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."emailToResend" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."emailToResend" ALTER COLUMN IF EXISTS adrfrom RENAME TO "adrFrom";
ALTER TABLE IF EXISTS public."emailToResend" ALTER COLUMN IF EXISTS replyto RENAME TO "replyTo";

ALTER TABLE IF EXISTS public."favoritelist" RENAME TO "favoriteList";
ALTER TABLE IF EXISTS public."favoriteList" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."favoriteList" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."favoriteList" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";

ALTER TABLE IF EXISTS public."favoritelistmember" RENAME TO "favoriteListMember";
ALTER TABLE IF EXISTS public."favoriteListMember" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."favoriteListMember" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."favoriteListMember" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."favoriteListMember" ALTER COLUMN IF EXISTS favoritelist RENAME TO "favoriteList";

ALTER TABLE IF EXISTS public."loginprovider" RENAME TO "loginProvider";
ALTER TABLE IF EXISTS public."loginProvider" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."loginProvider" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."loginProvider" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";

ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS begindate RENAME TO "beginDate";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS enddate RENAME TO "endDate";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS anonymousvoteallowed RENAME TO "anonymousVoteAllowed";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS continuousresults RENAME TO "continuousResults";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS notifymehoursbeforepollends RENAME TO "notifyMeHoursBeforePollEnds";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS pollendremindersent RENAME TO "pollEndReminderSent";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS notificationlocale RENAME TO "notificationLocale";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS votenotification RENAME TO "voteNotification";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS feedcontent RENAME TO "feedContent";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS commentnotification RENAME TO "commentNotification";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS newchoicenotification RENAME TO "newChoiceNotification";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS gtuvalidationdate RENAME TO "gtuValidationDate";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS notificationmaxvotersend RENAME TO "notificationMaxVoterSend";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS emailaddresssuffixes RENAME TO "emailAddressSuffixes";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS polltype RENAME TO "pollType";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS votevisibility RENAME TO "voteVisibility";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS commentvisibility RENAME TO "commentVisibility";
ALTER TABLE IF EXISTS public."poll" ALTER COLUMN IF EXISTS resultvisibility RENAME TO "resultVisibility";

ALTER TABLE IF EXISTS public."pollenprincipal" RENAME TO "pollenPrincipal";
ALTER TABLE IF EXISTS public."pollenPrincipal" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."pollenPrincipal" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."pollenPrincipal" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."pollenPrincipal" ALTER COLUMN IF EXISTS pollenuser RENAME TO "pollenUser";

ALTER TABLE IF EXISTS public."pollenresource" RENAME TO "pollenResource";
ALTER TABLE IF EXISTS public."pollenResource" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."pollenResource" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."pollenResource" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."pollenResource" ALTER COLUMN IF EXISTS resourcecontent RENAME TO "resourceContent";
ALTER TABLE IF EXISTS public."pollenResource" ALTER COLUMN IF EXISTS contenttype RENAME TO "contentType";
ALTER TABLE IF EXISTS public."pollenResource" ALTER COLUMN IF EXISTS resourcetype RENAME TO "resourceType";

ALTER TABLE IF EXISTS public."pollentoken" RENAME TO "pollenToken";
ALTER TABLE IF EXISTS public."pollenToken" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."pollenToken" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."pollenToken" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."pollenToken" ALTER COLUMN IF EXISTS creationdate RENAME TO "creationDate";
ALTER TABLE IF EXISTS public."pollenToken" ALTER COLUMN IF EXISTS enddate RENAME TO "endDate";

ALTER TABLE IF EXISTS public."pollenuser" RENAME TO "pollenUser";
ALTER TABLE IF EXISTS public."pollenUser" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."pollenUser" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."pollenUser" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."pollenUser" ALTER COLUMN IF EXISTS gtuvalidationdate RENAME TO "gtuValidationDate";
ALTER TABLE IF EXISTS public."pollenUser" ALTER COLUMN IF EXISTS premiumto RENAME TO "premiumTo";
ALTER TABLE IF EXISTS public."pollenUser" ALTER COLUMN IF EXISTS cancreatepoll RENAME TO "canCreatePoll";
ALTER TABLE IF EXISTS public."pollenUser" ALTER COLUMN IF EXISTS defaultemailaddress RENAME TO "defaultEmailAddress";

ALTER TABLE IF EXISTS public."pollenuseremailaddress" RENAME TO "pollenUserEmailAddress";
ALTER TABLE IF EXISTS public."pollenUserEmailAddress" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."pollenUserEmailAddress" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."pollenUserEmailAddress" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."pollenUserEmailAddress" ALTER COLUMN IF EXISTS emailaddress RENAME TO "emailAddress";
ALTER TABLE IF EXISTS public."pollenUserEmailAddress" ALTER COLUMN IF EXISTS pgppublickey RENAME TO "pgpPublicKey";
ALTER TABLE IF EXISTS public."pollenUserEmailAddress" ALTER COLUMN IF EXISTS pollenuser RENAME TO "pollenUser";

ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS beginchoicedate RENAME TO "beginChoiceDate";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS endchoicedate RENAME TO "endChoiceDate";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS choiceaddallowed RENAME TO "choiceAddAllowed";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS votecountingtype RENAME TO "voteCountingType";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS votecountingconfig RENAME TO "voteCountingConfig";
ALTER TABLE IF EXISTS public."question" ALTER COLUMN IF EXISTS questionorder RENAME TO "questionOrder";

ALTER TABLE IF EXISTS public."report" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."report" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."report" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."report" ALTER COLUMN IF EXISTS targetid RENAME TO "targetId";

ALTER TABLE IF EXISTS public."sessiontoken" RENAME TO "sessionToken";
ALTER TABLE IF EXISTS public."sessionToken" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."sessionToken" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."sessionToken" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."sessionToken" ALTER COLUMN IF EXISTS pollenuser RENAME TO "pollenUser";
ALTER TABLE IF EXISTS public."sessionToken" ALTER COLUMN IF EXISTS pollentoken RENAME TO "pollenToken";

ALTER TABLE IF EXISTS public."usercredential" RENAME TO "userCredential";
ALTER TABLE IF EXISTS public."userCredential" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."userCredential" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."userCredential" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."userCredential" ALTER COLUMN IF EXISTS userid RENAME TO "userId";
ALTER TABLE IF EXISTS public."userCredential" ALTER COLUMN IF EXISTS username RENAME TO "userName";
ALTER TABLE IF EXISTS public."userCredential" ALTER COLUMN IF EXISTS pollenuser RENAME TO "pollenUser";

ALTER TABLE IF EXISTS public."vote" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."vote" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."vote" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";

ALTER TABLE IF EXISTS public."votetochoice" RENAME TO "voteToChoice";
ALTER TABLE IF EXISTS public."voteToChoice" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."voteToChoice" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."voteToChoice" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."voteToChoice" ALTER COLUMN IF EXISTS votevalue RENAME TO "voteValue";

ALTER TABLE IF EXISTS public."voter" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."voter" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."voter" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";

ALTER TABLE IF EXISTS public."voterlist" RENAME TO "voterList";
ALTER TABLE IF EXISTS public."voterList" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."voterList" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."voterList" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";

ALTER TABLE IF EXISTS public."voterlistmember" RENAME TO "voterListMember";
ALTER TABLE IF EXISTS public."voterListMember" ALTER COLUMN IF EXISTS topiaid RENAME TO "topiaId";
ALTER TABLE IF EXISTS public."voterListMember" ALTER COLUMN IF EXISTS topiaversion RENAME TO "topiaVersion";
ALTER TABLE IF EXISTS public."voterListMember" ALTER COLUMN IF EXISTS topiacreatedate RENAME TO "topiaCreateDate";
ALTER TABLE IF EXISTS public."voterListMember" ALTER COLUMN IF EXISTS invitationsent RENAME TO "invitationSent";
ALTER TABLE IF EXISTS public."voterListMember" ALTER COLUMN IF EXISTS voterlist RENAME TO "voterList";
