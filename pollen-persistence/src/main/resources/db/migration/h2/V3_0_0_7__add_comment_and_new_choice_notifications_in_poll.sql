-- add notification types in poll
alter table poll add commentNotification boolean;
alter table poll add newChoiceNotification boolean;

update poll set commentNotification = true, newChoiceNotification = true where voteNotification;
update poll set commentNotification = false, newChoiceNotification = false where not voteNotification;