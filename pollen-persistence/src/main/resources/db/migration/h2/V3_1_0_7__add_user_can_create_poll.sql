-- add canCreatePoll in user
alter table pollenuser add cancreatepoll boolean;
update pollenuser set cancreatepoll = false;