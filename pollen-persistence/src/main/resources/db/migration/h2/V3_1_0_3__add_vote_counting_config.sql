-- add vote counting type configuration
alter table poll add votecountingconfig longvarchar;

-- 1 Normal     ;
UPDATE poll set votecountingconfig = concat('{maxChoiceNumber: ', maxChoiceNumber, '}') where votecountingtype = 1;

-- 2 Pourcentage
UPDATE poll set votecountingconfig = '{weight: 100}' where votecountingtype = 2;

-- 3 Condorcet
UPDATE poll set votecountingconfig = '{}' where votecountingtype = 3;

-- 4 Number
UPDATE poll set votecountingconfig = concat('{maxChoiceNumber: ', maxChoiceNumber, '}') where votecountingtype = 4;

-- 5 Boda
UPDATE poll set votecountingconfig = '{maxChoiceNumber: 0, pointsByRank: null}' where votecountingtype = 5;

-- 6 Instant runoff
UPDATE poll set votecountingconfig = '{}' where votecountingtype = 6;

-- 6 Coombs
UPDATE poll set votecountingconfig = '{}' where votecountingtype = 7;