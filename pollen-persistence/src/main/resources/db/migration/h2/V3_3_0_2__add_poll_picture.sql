--
-- Ajout de la colonne Picture dans la table Poll
--
ALTER TABLE poll ADD picture VARCHAR(255);
alter table poll ADD FOREIGN KEY (picture) REFERENCES PollenResource(topiaId);
