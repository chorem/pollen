-- use JWT for validate email

-- add validate field
alter table pollenUserEmailaddress add validated boolean;
update pollenUserEmailaddress set validated = activationtoken is null;

-- drop activationtoken field
alter table pollenUserEmailaddress drop activationtoken;

-- delete orphan token
delete from pollentoken where topiaid in (
    select t.topiaid from pollentoken t
    left outer join pollenprincipal p
    on p.permission = t.topiaid
    left outer join sessiontoken s
    on s.pollentoken = t.topiaid
    where p.topiaid is null
    and s.topiaid is null
    );