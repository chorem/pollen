--
-- Corrige la base de production
--

ALTER TABLE IF EXISTS public."favoriteList" ALTER COLUMN IF EXISTS pollenuser RENAME TO "pollenUser";
ALTER TABLE IF EXISTS public."favoriteList" ALTER COLUMN IF EXISTS parentlists RENAME TO "parentLists";
