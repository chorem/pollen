-- add user's avatar
alter table pollenUser add avatar VARCHAR(255);
alter table pollenUser ADD FOREIGN KEY (avatar) REFERENCES PollenResource(topiaId);
