package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.DaoUtils;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenUserTopiaDao extends AbstractPollenUserTopiaDao<PollenUser> {

    public PaginationResult<PollenUser> findAll(PaginationParameter page, String search) {

        Map<String, Object> parameters = new HashMap<>();

        String hql = "SELECT user FROM " + PollenUser.class.getName() + " as user";
        if (StringUtils.isNotBlank(search)) {
            hql += " WHERE " + DaoUtils.getSearchClause("user", parameters, PollenUser.PROPERTY_NAME, search);
        }

        return findPage(hql, parameters, page);

    }

    @Override
    public void delete(PollenUser entity) {

        { // --- remove favorite lists --- //

            FavoriteListTopiaDao dao = topiaDaoSupplier
                    .getDao(FavoriteList.class, FavoriteListTopiaDao.class);
            List<FavoriteList> list = dao.forPollenUserEquals(entity).findAll();
            dao.deleteAll(list);

        }

        super.delete(entity);

    }

    public PollenUser findUserWithCredentialOrNull(String providerId, String userId) {
        Objects.requireNonNull(providerId);
        Objects.requireNonNull(userId);

        Map<String, Object> parameters = new HashMap<>();

        String hql = "SELECT user FROM " + PollenUser.class.getName() + " as user " +
                     "INNER JOIN user." + PollenUser.PROPERTY_USER_CREDENTIAL + " as credentials " +
                     "WHERE credentials." + UserCredential.PROPERTY_PROVIDER + " = :providerId " +
                        "AND credentials." + UserCredential.PROPERTY_USER_ID + " = :userId";
        parameters.put("providerId", providerId);
        parameters.put("userId", userId);

        return findUniqueOrNull(hql, parameters);
    }

    public PollenUser findUserWithEmailAddressOrNull(String emailAddress) {
        Objects.requireNonNull(emailAddress);

        Map<String, Object> parameters = new HashMap<>();

        String hql = "SELECT user FROM " + PollenUser.class.getName() + " as user " +
                     "INNER JOIN user." + PollenUser.PROPERTY_EMAIL_ADDRESSES + " as emailAddresses " +
                     "WHERE emailAddresses." + PollenUserEmailAddress.PROPERTY_EMAIL_ADDRESS + " = :emailAddress";
        parameters.put("emailAddress", emailAddress);

        return findUniqueOrNull(hql, parameters);
    }

}
