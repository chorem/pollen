package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

public class ChoiceTopiaDao extends AbstractChoiceTopiaDao<Choice> {

    public List<Choice> findAll(Question question) {

        return forQuestionEquals(question)
                .setOrderByArguments(Choice.PROPERTY_CHOICE_ORDER)
                .findAll();
    }

    @Override
    public void delete(Choice entity) {

        if (entity.getChoiceType() == ChoiceType.RESOURCE) {

            { // --- remove Resource --- //

                PollenResourceTopiaDao dao = topiaDaoSupplier
                        .getDao(PollenResource.class, PollenResourceTopiaDao.class);
                PollenResource resource = dao
                        .forEquals(PollenResource.PROPERTY_TOPIA_ID, entity.getChoiceValue())
                        .findUnique();

                dao.delete(resource, entity.getTopiaId());

            }

        }

        super.delete(entity);

    }
}
