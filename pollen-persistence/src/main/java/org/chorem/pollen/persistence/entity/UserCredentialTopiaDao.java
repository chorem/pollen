package org.chorem.pollen.persistence.entity;

/*-
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public class UserCredentialTopiaDao extends AbstractUserCredentialTopiaDao<UserCredential> {

    public boolean isCredentialValid(String provider, String credentialUserId, String userTopiaId, String email) {
        String query = "SELECT COUNT(*)"
                       + " FROM " + PollenUser.class.getName() + " AS user"
                            + " LEFT JOIN user." + PollenUser.PROPERTY_USER_CREDENTIAL + " AS credential";
        if (email != null) {
            query += " LEFT JOIN user." + PollenUser.PROPERTY_EMAIL_ADDRESSES + " AS emailAddress";
        }
        query += " WHERE (credential." + UserCredential.PROPERTY_PROVIDER + " = :provider"
                       + " AND credential." + UserCredential.PROPERTY_USER_ID + " = :credentialUserId)";
        if (email != null) {
            query += " OR user." + PollenUser.PROPERTY_TOPIA_ID + " != :userTopiaId"
                        + " AND ("
                            + "credential." + UserCredential.PROPERTY_EMAIL + " = :credentialEmail"
                            + " OR emailAddress." + PollenUserEmailAddress.PROPERTY_EMAIL_ADDRESS + " = :emailAddress)";
        }

        Map<String, Object> params = new HashMap<>();
        params.put("provider", provider);
        params.put("credentialUserId", credentialUserId);
        if (email != null) {
            params.put("userTopiaId", userTopiaId);
            params.put("credentialEmail", email);
            params.put("emailAddress", email);
        }
        return count(query, params) == 0;
    }
}
