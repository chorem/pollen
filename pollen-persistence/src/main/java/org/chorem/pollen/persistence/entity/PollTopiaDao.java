package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.DaoUtils;
import org.nuiton.util.pagination.PaginationOrder;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollTopiaDao extends AbstractPollTopiaDao<Poll> {

    public PaginationResult<Poll> findAll(PaginationParameter page, String search) {

        Map<String, Object> parameters = Maps.newHashMap();

        String hql = "SELECT poll FROM " + Poll.class.getName() + " as poll";
        if (StringUtils.isNotBlank(search)) {
            hql += " WHERE " + DaoUtils.getSearchClause("poll", parameters, Poll.PROPERTY_TITLE, search);
        }

        return findPage(hql, parameters, page);

    }

    public PaginationResult<Poll> findAllUserPolls(PollenUser user, PaginationParameter page, String search, String filter) {

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("user", user);

        String hqlCondition = "";
        if (StringUtils.isNotBlank(filter)) {
            hqlCondition += filterCondition(filter, parameters);
        }

        return findAllUserPollsWithConditions(user, page, search, parameters, hqlCondition);
    }

    public PaginationResult<Poll> findAllCreated(PollenUser user, PaginationParameter page, String search, String filter) {

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("user", user);

        String hql = "SELECT poll FROM " + Poll.class.getName() + " as poll"
                + " WHERE poll." + Poll.PROPERTY_CREATOR + "." + PollenPrincipal.PROPERTY_POLLEN_USER + " = :user";
        if (StringUtils.isNotBlank(search)) {
            hql += " AND " + DaoUtils.getSearchClause("poll", parameters, Poll.PROPERTY_TITLE, search);
        }

        if (StringUtils.isNotBlank(filter)) {
            var strFilter = filterCondition(filter, parameters);
            hql += !strFilter.isEmpty() ? " AND " + strFilter : "";
        }

        return findPage(hql, parameters, page);
    }

    public PaginationResult<Poll> findAllInvited(PollenUser user, PaginationParameter page, String search, String filter) {

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("user", user);

        String hql = "FROM " + VoterListMember.class.getName() + " as mem"
                + " INNER JOIN mem." + VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL + " as poll"
                + " WHERE mem." + VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_POLLEN_USER + " = :user";
        if (StringUtils.isNotBlank(search)) {
            hql += " AND " + DaoUtils.getSearchClause("poll", parameters, Poll.PROPERTY_TITLE, search);
        }

        if (StringUtils.isNotBlank(filter)) {
            var strFilter = filterCondition(filter, parameters);
            hql += !strFilter.isEmpty() ? " AND " + strFilter : "";
        }

        PaginationOrder order = page.getOrderClauses().get(0);

        PaginationParameter page2 = PaginationParameter.of(page.getPageNumber(), page.getPageSize(), "poll." + order.getClause(), order.isDesc());

        List<Poll> polls = find("SELECT poll " + hql, parameters, page2);
        long count = count("SELECT COUNT(poll.topiaId) " + hql, parameters);
        return PaginationResult.of(polls, count, page);

    }

    public PaginationResult<Poll> findAllParticipated(PollenUser user, PaginationParameter page, String search, String filter) {

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("user", user);

        String hql = "FROM " + Vote.class.getName() + " as vote "
                + " INNER JOIN vote." + Vote.PROPERTY_QUESTION + " as question"
                + " INNER JOIN question." + Question.PROPERTY_POLL + " as poll"
                + " WHERE vote." + Vote.PROPERTY_VOTER + "." + PollenPrincipal.PROPERTY_POLLEN_USER + " = :user";
        if (StringUtils.isNotBlank(search)) {
            hql += " AND " + DaoUtils.getSearchClause("poll", parameters, Poll.PROPERTY_TITLE, search);
        }


        if (StringUtils.isNotBlank(filter)) {
            var strFilter = filterCondition(filter, parameters);
            hql += !strFilter.isEmpty() ? " AND " + strFilter : "";
        }

        PaginationOrder order = page.getOrderClauses().get(0);

        PaginationParameter page2 = PaginationParameter.of(page.getPageNumber(), page.getPageSize(), "poll." + order.getClause(), order.isDesc());

        List<Poll> polls = find("SELECT poll " + hql, parameters, page2);
        long count = count("SELECT COUNT(poll.topiaId) " + hql, parameters);
        return PaginationResult.of(polls, count, page);
    }

    private String filterCondition(String filter, Map<String, Object> parameters) {
        switch (filter.toUpperCase()) {
            case "PAST":
                parameters.put("today", new Date());
                return "poll." + Poll.PROPERTY_END_DATE + " IS NOT NULL "
                        + " AND poll." + Poll.PROPERTY_END_DATE + " <= :today ";

            case "CURRENT":
                parameters.put("today", new Date());
                return "(poll." + Poll.PROPERTY_BEGIN_DATE + " IS NULL OR poll." + Poll.PROPERTY_BEGIN_DATE + " <= :today) "
                        + " AND ( poll." + Poll.PROPERTY_END_DATE + " IS NULL OR poll." + Poll.PROPERTY_END_DATE + ">= :today)";

            case "UPCOMING":
                parameters.put("today", new Date());
                return "poll." + Poll.PROPERTY_BEGIN_DATE + " IS NOT NULL "
                        + " AND poll." + Poll.PROPERTY_BEGIN_DATE + " >= :today ";

            default:
                return "";
        }
    }

    private PaginationResult<Poll> findAllUserPollsWithConditions(PollenUser user, PaginationParameter page, String search, Map<String, Object> parameters, String hqlCondition) {


        String hql = "FROM " + Poll.class.getName() + " as poll "
                + " WHERE (poll." + Poll.PROPERTY_CREATOR + "." + PollenPrincipal.PROPERTY_POLLEN_USER + " = :user"
                + " OR EXISTS (SELECT 1 FROM " + VoterListMember.class.getName() + " memb"
                + "             WHERE memb." + VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL + "." + Poll.PROPERTY_TOPIA_ID + " = poll." + Poll.PROPERTY_TOPIA_ID
                + "             AND memb." + VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_POLLEN_USER + " = :user )"
                + " OR EXISTS (SELECT 1 FROM " + Vote.class.getName() + " vote"
                + "             WHERE vote." + Vote.PROPERTY_VOTER + "." + PollenPrincipal.PROPERTY_POLLEN_USER + " = :user "
                + "             AND vote." + Vote.PROPERTY_QUESTION + "." + Question.PROPERTY_POLL + "." + Poll.PROPERTY_TOPIA_ID + " = poll." + Poll.PROPERTY_TOPIA_ID + ")"
                + ")";

        if (StringUtils.isNotBlank(hqlCondition)) {
            hql += " AND " + hqlCondition;
        }

        if (StringUtils.isNotBlank(search)) {
            hql += " AND " + DaoUtils.getSearchClause("poll", parameters, Poll.PROPERTY_TITLE, search);
        }

        PaginationOrder order = page.getOrderClauses().get(0);

        PaginationParameter page2 = PaginationParameter.of(page.getPageNumber(), page.getPageSize(), "poll." + order.getClause(), order.isDesc());

        List<Poll> polls = find("SELECT poll " + hql, parameters, page2);
        long count = count("SELECT COUNT(poll.topiaId) " + hql, parameters);
        return PaginationResult.of(polls, count, page);
    }


    public Set<Poll> findAllFreePolls() {

        List<Poll> polls = forPollTypeEquals(PollType.FREE).findAll();
        return ImmutableSet.copyOf(polls);

    }

    public Set<Poll> findByVoterListMemberPermission(PollenToken token) {

        VoterListMemberTopiaDao voterListMemberDao =
                topiaDaoSupplier.getDao(VoterListMember.class, VoterListMemberTopiaDao.class);

        List<VoterListMember> voterListMembers =
                voterListMemberDao.forEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_PERMISSION + "." + PollenToken.PROPERTY_TOKEN, token.getToken()).findAll();

        VoterListTopiaDao voterListDao = topiaDaoSupplier.getDao(VoterList.class, VoterListTopiaDao.class);

        Set<Poll> polls = new HashSet<>();
        for (VoterListMember member : voterListMembers) {
            polls.add(
                    voterListDao.forEquals(
                            VoterList.PROPERTY_TOPIA_ID,
                            member.getVoterList().getTopiaId()
                    ).findUnique().getPoll()
            );
        }

        return polls;
    }

    @Override
    public void delete(Poll entity) {

        //get all questions
        QuestionTopiaDao questionDao = topiaDaoSupplier
                .getDao(Question.class, QuestionTopiaDao.class);
        List<Question> questions = questionDao.forPollEquals(entity).findAll();

        for (Question question : questions) {
            { // --- remove choices --- //

                ChoiceTopiaDao dao = topiaDaoSupplier
                        .getDao(Choice.class, ChoiceTopiaDao.class);
                List<Choice> list = dao.forQuestionEquals(question).findAll();
                dao.deleteAll(list);

            }

            { // --- remove votes --- //

                VoteTopiaDao dao = topiaDaoSupplier
                        .getDao(Vote.class, VoteTopiaDao.class);
                List<Vote> list = dao.forQuestionEquals(question).findAll();
                dao.deleteAll(list);

            }

            { // --- remove question comments -- //

                CommentTopiaDao dao = topiaDaoSupplier
                        .getDao(Comment.class, CommentTopiaDao.class);
                List<Comment> list = dao.forPollEquals(entity).findAll();
                dao.deleteAll(list);

            }
        }

        { // --- remove poll comments -- //

            CommentTopiaDao dao = topiaDaoSupplier
                    .getDao(Comment.class, CommentTopiaDao.class);
            List<Comment> list = dao.forPollEquals(entity).findAll();
            dao.deleteAll(list);

        }

        { // --- remove voterLists --- //

            VoterListTopiaDao dao = topiaDaoSupplier
                    .getDao(VoterList.class, VoterListTopiaDao.class);
            dao.forPollEquals(entity)
                    .addNull(VoterList.PROPERTY_PARENT)
                    .tryFindUnique()
                    .toJavaUtil()
                    .ifPresent(dao::delete);

        }

        super.delete(entity);

    }

    public Collection<Poll> findPollsWithReminderNeeded() {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();

        return forHql("FROM " + Poll.class.getCanonicalName() + " p" +
                " WHERE p." + Poll.PROPERTY_END_DATE + " IS NOT NULL" +
                " AND p." + Poll.PROPERTY_POLL_END_REMINDER_SENT + " = false" +
                " AND p." + Poll.PROPERTY_NOTIFY_ME_HOURS_BEFORE_POLL_ENDS + " > 0"
        ).findAll().stream()
                .filter(poll -> {
                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTime(poll.getEndDate());
                    calendar2.add(Calendar.HOUR, -1 * poll.getNotifyMeHoursBeforePollEnds());
                    Date notifyDate = calendar2.getTime();

                    return notifyDate.before(now) && now.before(poll.getEndDate());
                })
                .collect(Collectors.toList());
    }

    //TODO JC181002 - Per question ?
    public boolean setFlagNotificationMaxVoterSend(Poll poll) {

        int nbModif = topiaJpaSupport.execute("UPDATE " + Poll.class.getCanonicalName() +
                        " SET " + Poll.PROPERTY_NOTIFICATION_MAX_VOTER_SEND + " = true" +
                        " WHERE " + Poll.PROPERTY_TOPIA_ID + " = :" + Poll.PROPERTY_TOPIA_ID +
                        " AND " + Poll.PROPERTY_NOTIFICATION_MAX_VOTER_SEND + " = false",
                Collections.singletonMap(Poll.PROPERTY_TOPIA_ID, poll.getTopiaId()));

        return nbModif > 0;
    }

}
