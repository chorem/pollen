package org.chorem.pollen.persistence.entity;

/*-
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.DaoUtils;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;


public class FavoriteListMemberTopiaDao extends AbstractFavoriteListMemberTopiaDao<FavoriteListMember> {

    public PaginationResult<FavoriteListMember> search(FavoriteList parent, String search, PaginationParameter page) {
        HqlAndParametersBuilder<FavoriteListMember> builder = getSearchBuilder(parent, search);

        return findPage(builder.getHql(), builder.getHqlParameters(), page);

    }

    private HqlAndParametersBuilder<FavoriteListMember> getSearchBuilder(FavoriteList parent, String search) {
        HqlAndParametersBuilder<FavoriteListMember> builder = newHqlAndParametersBuilder();
        builder.addEquals(FavoriteListMember.PROPERTY_FAVORITE_LIST, parent);
        builder.addWhereClause(DaoUtils.getSearchClause(
                builder.getAlias(),
                builder.getHqlParameters(), FavoriteListMember.PROPERTY_NAME,
                search));
        return builder;
    }

    public List<FavoriteListMember> search(FavoriteList parent, String search, String order, int startIndex, int endIndex) {
        HqlAndParametersBuilder<FavoriteListMember> builder = getSearchBuilder(parent, search);
        builder.setOrderByArguments(order);

        return find(builder.getHql(), builder.getHqlParameters(), startIndex, endIndex);
    }

    public long countSearch(FavoriteList parent, String search) {
        HqlAndParametersBuilder<FavoriteListMember> builder = getSearchBuilder(parent, search);
        builder.setSelectClause("select count(" + builder.getAlias() + ".topiaId)");
        return count(builder.getHql(), builder.getHqlParameters());
    }
}
