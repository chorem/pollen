package org.chorem.pollen.persistence;

/*-
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.nuiton.topia.flyway.TopiaFlywayServiceImpl;
import org.nuiton.topia.persistence.TopiaApplicationContext;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollenFlywayServiceImpl extends TopiaFlywayServiceImpl {

    @Override
    protected void setLocations(ClassicConfiguration flywayConfiguration, TopiaApplicationContext topiaApplicationContext) {
        List<String> locations = Lists.newArrayList("db/migration/common");
        String jdbcUrl = topiaApplicationContext.getConfiguration().getJdbcConnectionUrl();

        if (jdbcUrl.startsWith("jdbc:postgresql")) {
            locations.add("db/migration/postgresql");
        } else if (jdbcUrl.startsWith("jdbc:h2")) {
            locations.add("db/migration/h2");
        }

        String[] locationsArray = locations.toArray(new String[locations.size()]);
        flywayConfiguration.setLocationsAsStrings(locationsArray);
    }

    @Override
    protected void doExtraConfiguration(ClassicConfiguration flywayConfiguration, TopiaApplicationContext topiaApplicationContext) {
        // Since flyway 6, default table name have changed so for backward compatibility with old pollen instance
        // we fallback to the old table name to store migration state
        flywayConfiguration.setTable("schema_version");
    }
}
