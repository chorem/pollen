package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.DaoUtils;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;

public class FavoriteListTopiaDao extends AbstractFavoriteListTopiaDao<FavoriteList> {

    public PaginationResult<FavoriteList> search(PollenUser user, String search, PaginationParameter page) {

        HqlAndParametersBuilder<FavoriteList> builder = newHqlAndParametersBuilder();
        builder.addEquals(FavoriteList.PROPERTY_POLLEN_USER, user);
        builder.addWhereClause(DaoUtils.getSearchClause(builder.getAlias(), builder.getHqlParameters(), FavoriteList.PROPERTY_NAME, search));

        return findPage(builder.getHql(), builder.getHqlParameters(), page);
    }


    @Override
    public void delete(FavoriteList entity) {

        // --- Delete members --- //

        FavoriteListMemberTopiaDao memberDao = topiaDaoSupplier
                .getDao(FavoriteListMember.class, FavoriteListMemberTopiaDao.class);
        List<FavoriteListMember> members = memberDao.forFavoriteListEquals(entity).findAll();
        memberDao.deleteAll(members);

        ChildFavoriteListTopiaDao childDao = topiaDaoSupplier
                .getDao(ChildFavoriteList.class, ChildFavoriteListTopiaDao.class);
        List<ChildFavoriteList> children = childDao.forChildEquals(entity).findAll();
        childDao.deleteAll(children);

        List<ChildFavoriteList> parents = childDao.forParentEquals(entity).findAll();
        childDao.deleteAll(parents);

        super.delete(entity);

    }
}
