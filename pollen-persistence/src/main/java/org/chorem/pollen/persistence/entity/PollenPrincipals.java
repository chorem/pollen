package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Created on 5/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenPrincipals {

    public static <E extends TopiaEntity> PollenPrincipal getPrincipal(E entity) {

        PollenPrincipal principal = null;

        if (entity instanceof Poll) {

            principal = ((Poll) entity).getCreator();

        } else if (entity instanceof Choice) {

            principal = ((Choice) entity).getCreator();

        } else if (entity instanceof Comment) {

            principal = ((Comment) entity).getAuthor();

        } else if (entity instanceof Vote) {

            principal = ((Vote) entity).getVoter();

        } else if (entity instanceof FavoriteList) {

            principal = ((FavoriteList) entity).getOwner();

        }

        return principal;

    }
}
