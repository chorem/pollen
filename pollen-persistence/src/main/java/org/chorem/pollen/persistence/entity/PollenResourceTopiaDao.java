package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 24/07/14.
 *
 * @author dralagen
 */
public class PollenResourceTopiaDao extends AbstractPollenResourceTopiaDao<PollenResource> {

    @Override
    public void delete(PollenResource entity) {

        // check if resource is used
        ChoiceTopiaDao dao = topiaDaoSupplier
                .getDao(Choice.class, ChoiceTopiaDao.class);

        boolean isUsed = dao.forEquals(Choice.PROPERTY_CHOICE_VALUE, entity.getTopiaId())
                            .addEquals(Choice.PROPERTY_CHOICE_TYPE, ChoiceType.RESOURCE)
                            .exists();

        if (!isUsed) {
            super.delete(entity);
        }
    }

    public void delete(PollenResource entity, String choiceId) {

        ChoiceTopiaDao dao = topiaDaoSupplier
                .getDao(Choice.class, ChoiceTopiaDao.class);

        boolean isUsed = dao.forEquals(Choice.PROPERTY_CHOICE_TYPE, ChoiceType.RESOURCE)
                            .addEquals(Choice.PROPERTY_CHOICE_VALUE, entity.getTopiaId())
                            .addNotEquals(Choice.PROPERTY_TOPIA_ID, choiceId)
                            .exists();

        if (!isUsed) {
            super.delete(entity);
        }

    }

    public PollenResource findAvatarForUser(String userId) {
        String hql = "SELECT " + PollenUser.PROPERTY_AVATAR +
                     " FROM " + PollenUser.class.getName() + " user " +
                     " WHERE user." + PollenUser.PROPERTY_TOPIA_ID + " = :userId";
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        return findUniqueOrNull(hql, params);
    }
}
