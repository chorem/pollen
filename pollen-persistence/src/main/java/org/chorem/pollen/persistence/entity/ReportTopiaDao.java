package org.chorem.pollen.persistence.entity;

/*-
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;

import java.util.Map;

public class ReportTopiaDao extends AbstractReportTopiaDao<Report> {

    public static final String REPORT_REQUEST = "SELECT new "+ ReportResume.class.getCanonicalName() +"(" +
            "sum(" + Report.PROPERTY_LEVEL + "), " +
            "count(*), " +
            "sum(CASE WHEN " + Report.PROPERTY_IGNORE + " = true THEN 1 ELSE 0 END)) " +
            "FROM " + Report.class.getCanonicalName() + " " +
            "WHERE " + Report.PROPERTY_TARGET_ID + " = :" + Report.PROPERTY_TARGET_ID + " ";

    public ReportResume getReportResume(String targetId) {

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put(Report.PROPERTY_TARGET_ID, targetId);

        ReportResume report = findAnyOrNull(REPORT_REQUEST, parameters);

        return report;
    }

} //ReportingTopiaDao
