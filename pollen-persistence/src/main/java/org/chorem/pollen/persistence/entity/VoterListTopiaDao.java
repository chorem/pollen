package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

public class VoterListTopiaDao extends AbstractVoterListTopiaDao<VoterList> {

    @Override
    public void delete(VoterList entity) {

        // --- Delete members --- //

        VoterListMemberTopiaDao dao = topiaDaoSupplier
                .getDao(VoterListMember.class, VoterListMemberTopiaDao.class);
        List<VoterListMember> list = dao.forVoterListEquals(entity).findAll();
        dao.deleteAll(list);

        List<VoterList> children = forParentEquals(entity).findAll();
        deleteAll(children);

        super.delete(entity);

    }

    public List<VoterListMember> getAllMembers(VoterList entity) {

        VoterListMemberTopiaDao dao = topiaDaoSupplier
                .getDao(VoterListMember.class, VoterListMemberTopiaDao.class);
        List<VoterListMember> members = dao.forVoterListEquals(entity).findAll();

        for (VoterList subVoterList : forParentEquals(entity).findAll()) {
            members.addAll(getAllMembers(subVoterList));
        }

        return members;
    }
}
