package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class SessionTokenTopiaDao extends AbstractSessionTokenTopiaDao<SessionToken> {

    public SessionToken findUniqueOrNullByToken(String token) {

        return forEquals(SessionToken.PROPERTY_POLLEN_TOKEN + "." + PollenToken.PROPERTY_TOKEN, token).findUniqueOrNull();

    }

    public Set<SessionToken> findAllBeforeEndDate(Date now) {

        HqlAndParametersBuilder<SessionToken> builder = newHqlAndParametersBuilder();
        builder.addLowerThan(SessionToken.PROPERTY_POLLEN_TOKEN + "." + PollenToken.PROPERTY_END_DATE, now);

        List<SessionToken> sessionTokens = findAll(builder.getHql(), builder.getHqlParameters());
        return ImmutableSet.copyOf(sessionTokens);

    }
}
