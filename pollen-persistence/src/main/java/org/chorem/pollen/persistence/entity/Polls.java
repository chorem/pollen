package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Date;
import java.util.Objects;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class Polls {

    public static boolean isPollFree(Poll poll) {
        return Objects.equals(PollType.FREE, poll.getPollType());
    }

    public static boolean isPollRestricted(Poll poll) {
        return Objects.equals(PollType.RESTRICTED, poll.getPollType());
    }

    public static boolean isPollRegistered(Poll poll) {
        return Objects.equals(PollType.REGISTERED, poll.getPollType());
    }

    public static boolean isStarted(Poll poll, Date currentDate) {
        Date beginDate = poll.getBeginDate();
        return beginDate == null || beginDate.before(currentDate);
    }

    public static boolean isRunning(Poll poll, Date currentDate) {
        return isStarted(poll, currentDate) &&
                !isFinished(poll, currentDate);
    }

    public static boolean isFinished(Poll poll, Date currentDate) {
        Date endDate = poll.getEndDate();
        return endDate != null && currentDate.after(endDate);
    }

    public static boolean isAddChoiceStarted(Question question, Date currentDate) {
        Date beginChoiceDate = question.getBeginChoiceDate();
        return question.isChoiceAddAllowed() &&
                (beginChoiceDate == null || beginChoiceDate.before(currentDate));
    }

    public static boolean isAddChoiceRunning(Question question, Date currentDate) {
        return question.isChoiceAddAllowed() &&
                !isFinished(question.getPoll(), currentDate) &&
                isAddChoiceStarted(question, currentDate) &&
                !isAddChoiceFinished(question, currentDate);
    }

    public static boolean isAddChoiceFinished(Question question, Date currentDate) {
        Date endChoiceDate = question.getEndChoiceDate();
        return !question.isChoiceAddAllowed() ||
                (endChoiceDate != null && endChoiceDate.before(currentDate));
    }
}
