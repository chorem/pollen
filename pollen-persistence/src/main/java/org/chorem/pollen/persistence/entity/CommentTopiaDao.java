package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

public class CommentTopiaDao extends AbstractCommentTopiaDao<Comment> {

    public PaginationResult<Comment> find(Poll poll, PaginationParameter paginationParameter) {

        return forPollEquals(poll)
                .findPage(paginationParameter);

    }

    public PaginationResult<Comment> find(Question question, PaginationParameter paginationParameter) {

        return forQuestionEquals(question)
                .findPage(paginationParameter);

    }
}
