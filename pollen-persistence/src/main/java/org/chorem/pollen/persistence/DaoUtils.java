package org.chorem.pollen.persistence;

/*-
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DaoUtils {

    protected static final String LIKE =
            "TRANSLATE(LOWER( %s ),"
                    + "'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş',"
                    + "'aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')"
                    + "like LOWER( %s )";

    /**
     * Generate sql like operator case and accent insensitive.
     *
     * @param field1 entity field to search into
     * @param field2 value field (must be accent escaped)
     * @return sql string
     */
    public static String getFieldLikeInsensitive(String field1, String field2) {
        String query = String.format(LIKE, field1, field2);
        return query;
    }

    public static String addQueryAttribute(Map<String, Object> args, String entityAttributeName, Object value) {
        String baseAttributeName = entityAttributeName.replaceAll("[.]", "_");

        int index = 0;
        String queryAttributeName;
        do {
            queryAttributeName = baseAttributeName + index;
            index++;
        } while (args.containsKey(queryAttributeName));

        args.put(queryAttributeName, value);
        return queryAttributeName;
    }


    public static String getSearchClause(String alias, Map<String, Object> parameters, String entityAttributeName, String search) {
        String queryAttributeName = addQueryAttribute(parameters, entityAttributeName, "%" + StringUtils.stripAccents(search) + "%");
        return getFieldLikeInsensitive(alias + "." + entityAttributeName,":" + queryAttributeName);
    }
}
