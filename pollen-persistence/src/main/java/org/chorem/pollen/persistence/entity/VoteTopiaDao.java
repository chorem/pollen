package org.chorem.pollen.persistence.entity;

/*
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.Date;
import java.util.List;

public class VoteTopiaDao extends AbstractVoteTopiaDao<Vote> {

    public static final String ANONYMOUS_NAME = "?";

    public List<Vote> findAll(Question question) {

        return forQuestionEquals(question)
                .setOrderByArguments(Vote.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();
    }

    public long getVoteIndex(Vote vote) {

        HqlAndParametersBuilder<Vote> builder = newHqlAndParametersBuilder();
        builder.addEquals(Vote.PROPERTY_QUESTION, vote.getQuestion());
        builder.addLowerOrEquals(Vote.PROPERTY_TOPIA_CREATE_DATE, vote.getTopiaCreateDate());
        builder.setSelectClause("select count(" + Vote.PROPERTY_TOPIA_ID + ")");
        return count(builder.getHql(), builder.getHqlParameters());
    }

    public List<Vote> findAllSince(Question question, Date since) {
        if (since == null) {
            return findAll(question);
        }
        return forHql("FROM " + Vote.class.getCanonicalName() +" v" +
                      " WHERE v." + Vote.PROPERTY_QUESTION + " = :" + Vote.PROPERTY_QUESTION +
                      " AND v." + Vote.PROPERTY_TOPIA_CREATE_DATE + " >= :" + Vote.PROPERTY_TOPIA_CREATE_DATE +
                      " ORDER BY v." + Vote.PROPERTY_TOPIA_CREATE_DATE,
                      Vote.PROPERTY_QUESTION, question,
                      Vote.PROPERTY_TOPIA_CREATE_DATE, since).findAll();
    }

    public List<Vote> findAllOldVotes(Date until) {
        HqlAndParametersBuilder<Vote> builder = newHqlAndParametersBuilder();
        builder.addLowerThan(Vote.PROPERTY_TOPIA_CREATE_DATE, until);
        builder.addNotNull(Vote.PROPERTY_VOTER+"."+ PollenPrincipal.PROPERTY_POLLEN_USER);
        builder.addNotEquals(Vote.PROPERTY_VOTER+"."+PollenPrincipal.PROPERTY_POLLEN_USER+"."+ PollenUser.PROPERTY_NAME, ANONYMOUS_NAME);
        List<Vote> oldVotes = findAll(builder.getHql(), builder.getHqlParameters());
        return oldVotes;
    }

}
