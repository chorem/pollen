/*
 * Copyright (c) 2005, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */



/** XXX Remove this when we stop supporting java 8, hack for java 11 compilation (should use javax.annotation.processing.Generated) **/
package javax.annotation;

/*-
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.lang.annotation.*;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * The Generated annotation is used to mark source code that has been generated.
 * It can also be used to differentiate user written code from generated code in
 * a single file.
 *
 * <h3>Examples:</h3>
 * <pre>
 *   &#064;Generated("com.example.Generator")
 * </pre>
 * <pre>
 *   &#064;Generated(value="com.example.Generator", date= "2017-07-04T12:08:56.235-0700")
 * </pre>
 * <pre>
 *   &#064;Generated(value="com.example.Generator", date= "2017-07-04T12:08:56.235-0700",
 *      comments= "comment 1")
 * </pre>
 *
 * @since 9
 */
@Documented
@Retention(SOURCE)
@Target({PACKAGE, TYPE, METHOD, CONSTRUCTOR, FIELD,
        LOCAL_VARIABLE, PARAMETER})
public @interface Generated {

    /**
     * The value element MUST have the name of the code generator. The
     * name is the fully qualified name of the code generator.
     *
     * @return The name of the code generator
     */
    String[] value();

    /**
     * Date when the source was generated. The date element must follow the ISO
     * 8601 standard. For example the date element would have the following
     * value 2017-07-04T12:08:56.235-0700 which represents 2017-07-04 12:08:56
     * local time in the U.S. Pacific Time time zone.
     *
     * @return The date the source was generated
     */
    String date() default "";

    /**
     * A place holder for any comments that the code generator may want to
     * include in the generated code.
     *
     * @return Comments that the code generated included
     */
    String comments() default "";
}
