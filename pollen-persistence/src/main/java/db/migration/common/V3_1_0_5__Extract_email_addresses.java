package db.migration.common;

/*-
 * #%L
 * Pollen :: Persistence
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.internal.ShortTopiaIdFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class V3_1_0_5__Extract_email_addresses extends BaseJavaMigration {

    /** Logger. */
    private static final Log log = LogFactory.getLog(V3_1_0_5__Extract_email_addresses.class);

    @Override
    public void migrate(Context context) throws SQLException {

        Connection connection = context.getConnection();

        PreparedStatement insertStatement = null;
        PreparedStatement updateStatement = null;
        ResultSet resultSet = null;

        TopiaIdFactory shortTopiaIdFactory = new ShortTopiaIdFactory();

        try {
            connection.setAutoCommit(false);

            insertStatement = connection.prepareStatement("INSERT INTO POLLENUSEREMAILADDRESS " +
                                                          "(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, EMAILADDRESS, ACTIVATIONTOKEN, POLLENUSER) " +
                                                          "VALUES (?, ?, ?, ?, ?, ?)");
            updateStatement =
                    connection.prepareStatement("UPDATE pollenuser SET DEFAULTEMAILADDRESS = ? WHERE topiaid = ?");

            resultSet = connection.createStatement()
                    .executeQuery("SELECT u.topiaId, u.email, u.emailactivationtoken FROM pollenuser u");

            while (resultSet.next()) {

                String userTopiaId = resultSet.getString(1);
                String emailAddress = resultSet.getString(2);
                String emailTokenActivation = resultSet.getString(3);
                String addressTopiaId = shortTopiaIdFactory.newTopiaId(PollenUserEmailAddress.class, (TopiaEntity) null);

                insertStatement.setString(1, addressTopiaId);
                insertStatement.setInt(2, 1);
                insertStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                insertStatement.setString(4,emailAddress);
                insertStatement.setString(5, emailTokenActivation);
                insertStatement.setString(6, userTopiaId);
                insertStatement.addBatch();

                updateStatement.setString(1, addressTopiaId);
                updateStatement.setString(2, userTopiaId);
                updateStatement.addBatch();
            }

            resultSet.close();

            resultSet = connection.createStatement()
                    .executeQuery("SELECT uc.pollenuser, uc.email FROM usercredential uc");

            while (resultSet.next()) {

                String userTopiaId = resultSet.getString(1);
                String emailAddress = resultSet.getString(2);

                if (StringUtils.isNotBlank(emailAddress) && !emailAddress.equals("null")) {
                    String addressTopiaId = shortTopiaIdFactory.newTopiaId(PollenUserEmailAddress.class, (TopiaEntity) null);
                    insertStatement.setString(1, addressTopiaId);
                    insertStatement.setInt(2, 1);
                    insertStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                    insertStatement.setString(4, emailAddress);
                    insertStatement.setNull(5, Types.VARCHAR);
                    insertStatement.setString(6, userTopiaId);
                    insertStatement.addBatch();
                }
            }

            resultSet.close();

            insertStatement.executeBatch();
            updateStatement.executeBatch();

            connection.commit();

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (insertStatement != null) {
                insertStatement.close();
            }
            if (updateStatement != null) {
                updateStatement.close();
            }
        }

        log.info("done !");
    }
}
