/*
 * #%L
 * Pollen :: VoteCounting :: Condorcet
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.EmptyVoteCountingConfig;
import org.chorem.pollen.votecounting.model.ListOfVoter;
import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.SimpleVoter;
import org.chorem.pollen.votecounting.model.SimpleVoterBuilder;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the {@link CondorcetVoteCountingStrategy}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class CondorcetVoteCountingStrategyTest {

    public static final String CHOICE_A = "a";

    public static final String CHOICE_B = "b";

    public static final String CHOICE_C = "c";

    protected static CondorcetVoteCounting voteCounting;

    protected CondorcetVoteCountingStrategy strategy;

    @BeforeClass
    public static void beforeClass() throws Exception {
        VoteCountingFactory factory = new VoteCountingFactory();
        voteCounting = CondorcetVoteCounting.class.cast(factory.getVoteCounting(CondorcetVoteCounting.VOTECOUNTING_ID));
    }

    @Before
    public void setUp() throws Exception {
        strategy = voteCounting.newStrategy();
        strategy.setConfig(new EmptyVoteCountingConfig());
    }

    @Test
    public void simpleVotecount() throws Exception {

        // Simple poll (all weight to 1)
        //          a   b   c
        // 1        1   2
        // 2        1   3   2
        // 3        1       2
        // combats
        //       a  X  -3  -3
        //       b  3   X   1
        //       c  3  -1   X
        //--------------------
        // Result   2  -2   0

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 3.)
                .addVoteForChoice(CHOICE_C, 2.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(-2.0), 2),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(0.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount0() throws Exception {

        // Simple poll (all weight to 1)
        //          a   b   c
        // 1        1   2
        // 2        3   2   1
        // 3        1       2
        // combats
        //       a  X  -1  -1
        //       b  1   X   1
        //       c  1  -1   X
        //--------------------
        // Result   2  -2   0

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 3.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(-2.0), 2),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(0.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount2() throws Exception {

        // Simple poll (all weight to 1)
        //          a   b   c
        // 1        1   2
        // 2        1   2   1
        // 3                2
        // combats  a   b   c
        //       a  X  -2   0
        //       b  2   X   1
        //       c  0  -1   X
        //--------------------
        // Result   1  -2   1

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(1.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(-2.0), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(1.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount3() throws Exception {

        // Simple poll (all weight to 1)
        //          a   b   c
        // 1        1
        // 2            1
        // 3                1
        // combats
        //       a  X   0   0
        //       b  0   X   0
        //       c  0   0   X
        //--------------------
        // Result   0   0   0

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(0.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(0.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(0.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount1() throws Exception {

        // poll with weighted vote
        //          a   b   c
        // 1        1           x 2
        // 2            1
        // 3            1   2
        // combats
        //       a  X   0  -1
        //       b  0   X  -2
        //       c  1   2   X
        //--------------------
        // Result   1   1  -2

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(1.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(1.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(-2.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount2() throws Exception {

        // poll with weighted vote
        //          a   b   c
        // 1        1           x 2
        // 2            1
        // 3            2   1   x 3
        // combats
        //       a  X   2   1
        //       b -2   X   2
        //       c -1  -2   X
        //--------------------
        // Result  -2   0   2

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 3.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(-2.0), 2),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(0.0), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(2.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void listVotecount() throws Exception {
        //           a   b   c
        // G1U1      1   2   3  * 2
        // G1U2      1   2   2
        // G1U3      3   2   1
        //--------------------
        // combats
        //       a   X  -2  -2
        //       b   2   X  -1
        //       c   2   1   X
        //--------------------
        // Result G1 2   0  -2
        // Vote G1   1   2   3

        // G2U1      1   2   3  * 2
        // G2U2      3   2   1
        // G2U3      3   1   2
        //--------------------
        // combats
        //       A   X   0   0
        //       B   0   X  -2
        //       C   0   2   X
        //--------------------
        // Result G2 0   1  -1
        // Vote   G2 2   1   3 * 2

        //--------------------
        // combats
        //       A   X   1  -3
        //       B  -1   X  -3
        //       C   3   3   X
        //--------------------
        // Result    0   2  -2

        ListOfVoter voters = ListOfVoter.newVoter(null, 1, Sets.newHashSet(
                ListOfVoter.newVoter("G1", 1, Sets.newHashSet(
                        SimpleVoter.newVoter("G1U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 3d))),
                        SimpleVoter.newVoter("G1U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 2d))),
                        SimpleVoter.newVoter("G1U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 3d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 1d))))),
                ListOfVoter.newVoter("G2", 2, Sets.newHashSet(
                        SimpleVoter.newVoter("G2U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 3d))),
                        SimpleVoter.newVoter("G2U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 3d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 1d))),
                        SimpleVoter.newVoter("G2U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 3d),
                                VoteForChoice.newVote(CHOICE_B, 1d),
                                VoteForChoice.newVote(CHOICE_C, 2d)))))
        ));

        ListVoteCountingResult result = strategy.votecount(voters);

        assertThat(result)
                .isNotNull();
        assertThat(result.getMainResult()).isNotNull();
        assertThat(result.getMainResult().getScores()).isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(0.0), 1),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(-2.0), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount4() throws Exception {

        // Simple poll (all weight to 1)    see issue #140
        //          a   b   c
        // 1        1   2   3
        // 2        1   2   3
        // 3        1   2   3
        // 4        3   1   2
        // 4        3   1   2
        // combats
        //       a  X  -1  -1
        //       b  1   X  -5
        //       c  1   5   X
        //--------------------
        // Result   2   0  -2

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 3.)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 3.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 3.)
                .newVoter("4", 1.)
                .addVoteForChoice(CHOICE_A, 3.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .newVoter("5", 1.)
                .addVoteForChoice(CHOICE_A, 3.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(0.0), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(-2.0), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

}
