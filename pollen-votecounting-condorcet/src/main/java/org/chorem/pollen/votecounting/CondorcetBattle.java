package org.chorem.pollen.votecounting;

/*-
 * #%L
 * Pollen :: VoteCounting :: Condorcet
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CondorcetBattle implements Serializable {

    private static final long serialVersionUID = -9068912900242060104L;
    
    protected String opponentId;

    protected String runnerId;

    protected BigDecimal score;

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public String getRunnerId() {
        return runnerId;
    }

    public void setRunnerId(String runnerId) {
        this.runnerId = runnerId;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public void addScoreValue(double scoreToAdd) {
        BigDecimal newScoreValue;
        if (score == null) {

            newScoreValue = BigDecimal.valueOf(scoreToAdd);
        } else {
            newScoreValue = score.add(BigDecimal.valueOf(scoreToAdd));
        }
        setScore(newScoreValue);
    }
}
