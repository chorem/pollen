package org.chorem.pollen.votecounting;

/*-
 * #%L
 * Pollen :: VoteCounting :: Condorcet
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.votecounting.model.VoteCountingDetailResult;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CondorcetDetailResult implements VoteCountingDetailResult {

    private static final long serialVersionUID = -1586948510368173100L;

    protected List<CondorcetBattle> battles;

    public List<CondorcetBattle> getBattles() {
        if (battles == null) {
            battles = Lists.newLinkedList();
        }
        return battles;
    }

    public void setBattles(List<CondorcetBattle> battles) {
        this.battles = battles;
    }
}
