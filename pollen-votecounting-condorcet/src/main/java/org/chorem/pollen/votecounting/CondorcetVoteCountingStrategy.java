/*
 * #%L
 * Pollen :: VoteCounting :: Condorcet
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.EmptyVoteCountingConfig;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Condorcet.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class CondorcetVoteCountingStrategy extends AbstractVoteCountingStrategy<EmptyVoteCountingConfig> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CondorcetVoteCountingStrategy.class);

    @Override
    public VoteCountingResult votecount(Set<Voter> voters) {

        // get empty result by choice
        Map<String, ChoiceScore> scores = newEmptyChoiceScoreMap(voters);
        CondorcetDetailResult detailResult = new CondorcetDetailResult();

        for (Voter voter : voters) {

            // add this voter votes to result
            addVoterChoices(voter, detailResult);
        }

        resolveBattles(detailResult, scores);

        // order scores (using their value) and return result
        return orderByValues(scores.values(), detailResult);
    }

    @Override
    public Set<VoteForChoice> toVoteForChoices(VoteCountingResult voteCountingResult) {
        Set<VoteForChoice> voteForChoices = Sets.newHashSet();

        for (ChoiceScore choiceScore : voteCountingResult.getScores()) {

            double score = choiceScore.getScoreOrder();
            VoteForChoice voteForChoice = VoteForChoice.newVote(
                    choiceScore.getChoiceId(),
                    score);
            voteForChoices.add(voteForChoice);
        }
        return voteForChoices;
    }

    protected void addVoterChoices(Voter voter, CondorcetDetailResult detailResult) {

        if (log.isDebugEnabled()) {
            log.debug("Start count for voter " + voter.getVoterId());
        }
        double voterWeight = voter.getWeight();

        for (VoteForChoice voteForChoiceX : voter.getVoteForChoices()) {

            String choiceIdX = voteForChoiceX.getChoiceId();

            for (VoteForChoice voteForChoiceY : voter.getVoteForChoices()) {

                String choiceIdY = voteForChoiceY.getChoiceId();
                if (choiceIdX.equals(choiceIdY)) {
                    // no battle for same choice
                    continue;
                }
                int compare = voteValueComparator.compare(voteForChoiceX,
                                                          voteForChoiceY);

                if (compare < 0) {

                    // X wins over Y;
                    addBattle(detailResult, choiceIdX, choiceIdY, voterWeight);
                    addBattle(detailResult, choiceIdY, choiceIdX, -voterWeight);
                }
            }
        }
    }

    protected void addBattle(CondorcetDetailResult detailResult, String opponentId, String runnerId, double score) {
        Optional<CondorcetBattle> battleOptional = detailResult.getBattles().stream()
                .filter(battle -> battle.getOpponentId().equals(opponentId)
                        && battle.getRunnerId().equals(runnerId))
                .findFirst();
        CondorcetBattle battle;
        if (battleOptional.isPresent()) {
            battle = battleOptional.get();
        } else {
            battle = new CondorcetBattle();
            battle.setOpponentId(opponentId);
            battle.setRunnerId(runnerId);
            detailResult.getBattles().add(battle);
        }
        battle.addScoreValue(score);
    }

    protected void resolveBattles(CondorcetDetailResult detailResult, Map<String, ChoiceScore> scores) {

        for (CondorcetBattle battle : detailResult.getBattles()) {

            ChoiceScore choiceScore = scores.get(battle.getOpponentId());
            choiceScore.addScoreValue(battle.getScore().signum());

        }

    }

}
