/* eslint strict: 0 */
"use strict";
var webpack = require("webpack");
var CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: {
        pollen: "./src/main/web"
    },

    output: {
        filename: "[name].js",
        path: __dirname + "/target/dist/",
        sourceMapFilename: "[name].js.map"
    },

    plugins: [

        new webpack.DefinePlugin({
            POLLEN_VERSION: JSON.stringify(require("./package.json").version)
        }),
        new CopyWebpackPlugin([
            {from: "src/main/web/conf.js",
                transform: function(content) {
                    return content.toString().replace("POLLEN_API_URL", JSON.stringify(process.env.POLLEN_SERVER_CONTEXT || "/pollen-rest-api"));
                }},
            {from: "src/main/web/index.html"},
            {from: "src/main/web/help", to: "help"},
            {from: "src/main/web/customData", to: "customData"},
            {from: "src/main/web/robots.txt"},
            {from: "src/main/web/img", to: "img"},
            {from: "src/main/web/css", to: "css"},
            {from: "./node_modules/font-awesome/css", to: "css"},
            {from: "./node_modules/font-awesome/fonts", to: "fonts"},
            {from: "./node_modules/nprogress/nprogress.css", to: "css",
                transform: function(content) {
                    return content.toString().replace(/#29d/g, "var(--main)");
                }
            }
        ])
    ],

    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.tag.html$/,
                exclude: /node_modules/,
                use: [{
                    loader: "riot-tag-loader",
                    options: {
                        hot: true,
                        type: "es6"
                    }
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            [
                                "@babel/preset-env",
                                {
                                    useBuiltIns: 'usage',
                                    corejs: {"version": 2, "proposals": true},
                                    targets:  {
                                        ie: 11
                                    }
                                }
                            ]
                        ]

                    }
                }
            }
        ]
    }
};
