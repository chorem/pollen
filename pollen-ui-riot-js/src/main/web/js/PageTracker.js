/*-
 * #%L
 * Pollen :: UI RiotJs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";

class PageTracker {

    constructor() {
        window._paq = window._paq || [];
        if (window.pollenConf.piwikUrl && window.pollenConf.piwikSiteId) {
            window._paq.push([function() {
                let _this = this;
                function getOriginalVisitorCookieTimeout() {
                    var now = new Date(),
                        nowTs = Math.round(now.getTime() / 1000),
                        visitorInfo = _this.getVisitorInfo();
                    var createTs = parseInt(visitorInfo[2], 10);
                    var cookieTimeout = 33696000; // 13 mois en secondes
                    var originalTimeout = createTs + cookieTimeout - nowTs;
                    return originalTimeout;
                }
                this.setVisitorCookieTimeout(getOriginalVisitorCookieTimeout());
            }]);
            window._paq.push(["trackPageView"]);
            window._paq.push(["enableLinkTracking"]);
            var u = window.pollenConf.piwikUrl;
            window._paq.push(["setTrackerUrl", u + "piwik.php"]);
            window._paq.push(["setSiteId", window.pollenConf.piwikSiteId]);
            var d = document,
                g = d.createElement("script");
            g.type = "text/javascript";
            g.async = true;
            g.defer = true;
            g.src = u + "piwik.js";
            document.head.appendChild(g);
        }
    }

    _track(url) {
        if (window._paq) {
            window._paq.push(["setDocumentTitle", url]);
            window._paq.push(["setCustomUrl", "/" + url]);
            window._paq.push(["trackPageView"]);
        }
    }

    trackLogin() {
        this._track("login");
    }

    trackPoll() {
        this._track("poll");
    }

    trackVote() {
        this._track("vote");
    }

    trackResults() {
        this._track("results");
    }

    trackComments() {
        this._track("comments");
    }

    trackAddComment() {
        this._track("newComment");
    }

    trackCreatePoll(step) {
        this._track("newPoll" + step);
    }

    trackPollCreated() {
        this._track("pollCreated");
    }
}

export default singleton(PageTracker);
