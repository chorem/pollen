/*-
 * #%L
 * Pollen :: UI RiotJs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export default {
    log(message) {
        // eslint-disable-next-line no-console
        console.log(message);
    },

    info(message) {
        // eslint-disable-next-line no-console
        console.info(message);
    },

    warn(message) {
        // eslint-disable-next-line no-console
        console.warn(message);
    },

    error(message) {
        // eslint-disable-next-line no-console
        console.error(message);
    },

    getHistory() {
        // eslint-disable-next-line no-console
        let history = console.history.map(l => {
            let log = Object.assign({}, l);
            if (log.type === "info" || log.type === "log") {
                delete log.stack;
            }
            log.arguments = Array.from(log.arguments).map(argument => {
                return argument
                    && argument.DATETIME_INPUT_PATTERN
                    && argument.logger ? "TAG : <" + argument.root.localName + ">" : argument;
            });
            return log;
        });
        return history;
    }
};
