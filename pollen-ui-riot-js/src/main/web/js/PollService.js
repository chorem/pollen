/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2019 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";

class PollService extends FetchService {

    _getUrlPrefix(pollId, questionId) {
        let url = "/v1/polls";
        if (pollId) {
            url += "/" + pollId;
        }
        if (questionId) {
            url += "/questions/" + questionId;
        }
        return url;
    }

    empty(choiceType) {
        let url = this._getUrlPrefix("new");
        return this.get(url, {choiceType: choiceType});
    }

    create(poll, voterLists, voterListMembers) {
        let poll2 = Object.assign({}, poll);
        delete poll2.participant;
        delete poll2.votePeriod;
        delete poll2.questions[0].addChoices;
        delete poll2.questions[0].voteVisibility;
        delete poll2.questions[0].anonymousVote;
        delete poll2.questions[0].resultVisibility;
        delete poll2.questions[0].continuousResults;
        delete poll2.questions[0].commentVisibility;
        delete poll2.questions[0].voteCountingConfig.pointsByRankDefault;
        delete poll2.voteCountingType;
        delete poll2.alreadyParticipants;
        delete poll2.limitChoices;
        let url = this._getUrlPrefix();
        return this.post(url, {poll: poll2, voterLists: voterLists, voterListMembers: voterListMembers});
    }

    save(poll) {
        let poll2 = Object.assign({}, poll);
        delete poll2.participant;
        delete poll2.votePeriod;
        delete poll2.questions[0].addChoices;
        delete poll2.questions[0].voteVisibility;
        delete poll2.questions[0].anonymousVote;
        delete poll2.questions[0].resultVisibility;
        delete poll2.questions[0].continuousResults;
        delete poll2.questions[0].commentVisibility;
        delete poll2.questions[0].voteCountingConfig.pointsByRankDefault;
        delete poll2.voteCountingType;
        delete poll2.alreadyParticipants;
        delete poll2.limitChoices;
        let url = this._getUrlPrefix(poll.id);
        return this.post(url, poll2, {permission: poll.permission});
    }

    _getPolls(pagination, use, search) {
        let params = Object.assign({}, pagination);
        params.search = search || "";
        params.use = use;
        let url = this._getUrlPrefix();
        return this.get(url, params);
    }

    createdPolls(pagination, search) {
        return this._getPolls(pagination, "created", search);
    }

    invitedPolls(pagination, search) {
        return this._getPolls(pagination, "invited", search);
    }

    participatedPolls(pagination, search) {
        return this._getPolls(pagination, "participated", search);
    }

    polls(pagination, search) {
        return this._getPolls(pagination, "", search);
    }

    deletePoll(pollId, permission) {
        let url = this._getUrlPrefix(pollId);
        return this.doDelete(url, {permission: permission});
    }

    getPoll(pollId, permission) {
        let url = this._getUrlPrefix(pollId);
        return this.get(url, {permission: permission});
    }

    closePoll(pollId, permission) {
        let url = this._getUrlPrefix(pollId) + "/close";
        return this.put(url, null, {permission: permission});
    }

    reopenPoll(pollId, permission) {
        let url = this._getUrlPrefix(pollId) + "/reopen";
        return this.put(url, null, {permission: permission});
    }

    assignPoll(pollId, permission) {
        let url = this._getUrlPrefix(pollId) + "/assign";
        return this.put(url, null, {permission: permission});
    }

    addReport(pollId, targetId, report, permission) {
        let url = this._getUrlPrefix(pollId) + "/reports";
        return this.post(url, {report: report}, {permission: permission});
    }

    getReports(pollId, targetId, permission) {
        let url = this._getUrlPrefix(pollId) + "/reports";
        return this.get(url, {permission: permission});
    }

    saveReport(pollId, targetId, report, permission) {
        let url = this._getUrlPrefix(pollId) + "/reports/" + report.id;
        return this.post(url, {report: report}, {permission: permission});
    }

    getInvalidEmails(pollId, permission) {
        let url = this._getUrlPrefix(pollId) + "/invalidEmails";
        return this.get(url, {permission: permission});
    }

    getParticipants(pollId, pagination, permission) {
        let params = Object.assign({}, pagination);
        if (permission) {
            params.permission = permission;
        }
        let url = this._getUrlPrefix(pollId) + "/participants";
        return this.get(url, params);
    }

    getQuestions(pollId, pagination, permission) {
        let params = Object.assign({}, pagination);
        if (permission) {
            params.permission = permission;
        }
        let url = this._getUrlPrefix(pollId);
        return this.get(url, params);
    }

    getVotes(pollId, questionId, pagination, permission) {
        let params = Object.assign({}, pagination);
        if (permission) {
            params.permission = permission;
        }
        let url = this._getUrlPrefix(pollId, questionId);
        return this.get(url, params);
    }
}

export default singleton(PollService);
