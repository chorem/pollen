/*-
 * #%L
 * Pollen :: UI RiotJs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import session from "./Session.js";
import moment from "moment";
import bus from "./PollenBus.js";
import momentTZ from "moment-timezone";

export default {
    getWindowDimensions() {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName("body")[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight || g.clientHeight;
        return {width: x, height: y};
    },

    positionDropdown(selector, visible) {
        const w = this.getWindowDimensions();
        const m = this.root.querySelector(selector);
        if (!m) {
            return;
        }
        if (!visible) {
            // Reset position
            m.style.marginTop = "";
            m.style.marginLeft = "";
            return;
        }
        const pos = m.getBoundingClientRect();
        if (w.width < pos.left + pos.width) {
            // menu is off the right hand of the page
            m.style.marginLeft = (w.width - (pos.left + pos.width) - 20) + "px";
        }
        if (pos.left < 0) {
            // menu is off the right hand of the page
            m.style.marginLeft = "20px";
        }
        if (w.height < pos.top + pos.height) {
            // Popup is off the bottom of the page
            m.style.marginTop = (w.height - (pos.top + pos.height) - 20) + "px";
        }
    },

    formatDate(date, format) {
        moment.locale(session.locale);
        if (date) {
            let dateUtc = momentTZ(date);
            return dateUtc.format(format || "LLLL");
        }
        return "";
    },

    formatDateTimeForInput(date) {
        return this.formatDate(date, "YYYY-MM-DDTHH:mm");
    },

    formatDateTimeForSubmit(date) {
        return this.formatDate(date, "YYYY-MM-DDTHH:mm:ss.SSS");
    },

    bus: bus,

    listen(events, callback) {
        this.bus.on(events, callback);
        this.on("before-unmount", () => {
            this.bus.off(events, callback);
        });
    },

    checkSize(resource) {
        if (resource && resource.size && resource.size > window.pollenConf.resourceMaxSize) {
            return this.info(
                this._t.fileSizeMax_title,
                this._l("fileSizeMax_message", resource.name, this.formatSize(resource.size), this.formatSize(window.pollenConf.resourceMaxSize)),
                undefined,
                "error").then(() => false);
        }
        return Promise.resolve(true);
    },

    formatSize(size) {
        let unitsSI = ["o", "ko", "Mo", "Go", "To", "Po", "Eo", "Zo", "Yo"];
        let index = Math.min(Math.floor(Math.log10(size) / 3), unitsSI.length - 1);
        let value = (size / Math.pow(10, index * 3)).toFixed(2);
        return value + " " + unitsSI[index];
    },

    DATETIME_INPUT_PATTERN: "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}"
};
