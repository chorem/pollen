/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";

class FavoriteListService extends FetchService {

    favoriteLists(pagination, search) {
        let params = Object.assign({}, pagination);
        params.search = search || "";
        return this.get("/v1/favoriteLists", params);
    }

    favoriteList(favoriteListId) {
        return this.get("/v1/favoriteLists/" + favoriteListId);
    }

    importFavoriteLists(importFile) {
        return this.form("/v1/favoriteLists/imports", {importFile: importFile}, true);
    }

    importCsv(favoriteListId, csvFile) {
        return this.form("/v1/favoriteLists/" + favoriteListId + "/importCsv", {csvFile: csvFile}, true);
    }

    importLdap(favoriteListId, ldap) {
        return this.post("/v1/favoriteLists/" + favoriteListId + "/importLdap", ldap);
    }

    createFavoriteList(favoriteList) {
        return this.post("/v1/favoriteLists", favoriteList);
    }

    saveFavoriteList(favoriteList) {
        return this.post("/v1/favoriteLists/" + favoriteList.id, favoriteList);
    }

    deleteFavoriteList(favoriteListId) {
        return this.doDelete("/v1/favoriteLists/" + favoriteListId);
    }

    members(favoriteListId, search, pagination) {
        let params = Object.assign({}, pagination);
        params.search = search || "";
        return this.get("/v1/favoriteLists/" + favoriteListId + "/members", params);
    }

    member(favoriteListId, memberId) {
        return this.get("/v1/favoriteLists/" + favoriteListId + "/members/" + memberId);
    }

    addMember(favoriteListId, member) {
        return this.post("/v1/favoriteLists/" + favoriteListId + "/members", member);
    }

    saveMember(favoriteListId, member) {
        return this.post("/v1/favoriteLists/" + favoriteListId + "/members/" + member.id, member);
    }

    deleteMember(favoriteListId, memberId) {
        return this.doDelete("/v1/favoriteLists/" + favoriteListId + "/members/" + memberId);
    }

    childrenLists(favoriteListId, search, pagination) {
        let params = Object.assign({}, pagination);
        params.search = search || "";
        return this.get("/v1/favoriteLists/" + favoriteListId + "/lists", params);
    }

    childList(favoriteListId, childListId) {
        return this.get("/v1/favoriteLists/" + favoriteListId + "/lists/" + childListId);
    }

    addChildList(favoriteListId, childList) {
        return this.post("/v1/favoriteLists/" + favoriteListId + "/lists", childList);
    }

    saveChildList(favoriteListId, childList) {
        return this.post("/v1/favoriteLists/" + favoriteListId + "/lists/" + childList.id, childList);
    }

    deleteChildList(favoriteListId, childListId) {
        return this.doDelete("/v1/favoriteLists/" + favoriteListId + "/lists/" + childListId);
    }

    importFromVoterList(voterListId) {
        return this.post("/v1/favoriteLists/importVoterList", voterListId);
    }

    allChildren(favoriteListId, pagination, search) {
        let params = Object.assign({}, pagination);
        params.search = search || "";
        return this.get("/v1/favoriteLists/" + favoriteListId + "/all", params);
    }

}

export default singleton(FavoriteListService);
