/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";

class ResourceService extends FetchService {

    _getUrlPrefix(resourceId) {
        let url = "/v1/resources";
        if (resourceId) {
            url += "/" + resourceId;
        }
        return url;
    }

    create(resource, resourceType) {
        let url = this._getUrlPrefix();
        let file = new File([resource], encodeURIComponent(resource.name), {type: resource.type});
        return this.form(url, {resource: file, resourceType: resourceType}, true);
    }

    create64(resource64) {
        let url = "/v1/resources64";
        return this.post(url, resource64);
    }

    getResource(resourceId) {
        let url = this._getUrlPrefix(resourceId);
        return this.get(url);
    }

    getPreview(resourceId, maxDimension) {
        let url = this._getUrlPrefix(resourceId) + "/preview";
        if (maxDimension) {
            url += "?maxDimension=true";
        }
        return this.get(url);
    }

    getPreviewUrl(resourceId, maxDimension) {
        let url = this.endPoint + this._getUrlPrefix(resourceId) + "/preview";
        if (maxDimension) {
            url += "?maxDimension=true";
        }
        return url;
    }

    getMeta(resourceId) {
        let url = this._getUrlPrefix(resourceId) + "/meta";
        return this.get(url);
    }

    delete(resourceId) {
        let url = this._getUrlPrefix(resourceId);
        return this.doDelete(url);
    }

    gtus() {
        let url = "/v1/gtus";
        return this.get(url);
    }

    isGtu() {
        let url = "/v1/gtu/define";
        return this.get(url);
    }
}

export default singleton(ResourceService);
