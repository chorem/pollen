/*-
/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import authService from "./AuthService";
import resourceService from "./ResourceService";
import configurationService from "./ConfigurationService";
import bus from "./PollenBus.js";
import logger from "./Logger";
import pageTracker from "./PageTracker";
import riot from "riot";
import fr from "../i18n/fr.json";
import en from "../i18n/en.json";

class Session {

    constructor() {

        riot.observable(this);

        // pour contenir la locale à utiliser
        this.locale = null;
        // pour contenir la configuration
        this.configuration = window.pollenConf;
        if (this.configuration.endPoint.startsWith("/")) {
            this.configuration.endPoint = window.location.origin + this.configuration.endPoint;
        }
        // pour contenir les données à envoyer au serveur à chaque appel
        this.pollenUIContext = {
            uiEndPoint: window.location.origin,
            userValidateUrl: window.location.origin + "/#signcheck/{userId}/{token}",
            pollVoteUrl: window.location.origin + "/#poll/{pollId}/vote/{token}",
            pollVoteEditUrl: window.location.origin + "/#poll/{pollId}/vote/{voteId}/{token}",
            pollEditUrl: window.location.origin + "/#poll/{pollId}/summary/{token}",
            resourceUrl: this.configuration.endPoint + "/v1/resources/{resourceId}",
            resourceDownloadUrl: this.configuration.endPoint + "/v1/resources/{resourceId}/download",
            profileUrl: window.location.origin + "/#user/profile",
            offersUrl: window.location.origin + "/#"
        };
        // pour contenir les traductions
        this.i18n = {
            fr: fr,
            en: en
        };
        // pour contenir l"utillisateur connecté
        this.user = null;

        let lang = localStorage && localStorage.userLanguage || navigator.language || navigator.userLanguage;
        if (lang.indexOf("en") === 0) {
            this.locale = "en";
        } else {
            this.locale = "fr";
        }

        let input = document.createElement("input");
        let notADateValue = "not-a-date-nor-time";

        input.setAttribute("type", "date");
        input.setAttribute("value", notADateValue);
        this.dateInputSupported = (input.value !== notADateValue);

        input.setAttribute("type", "time");
        input.setAttribute("value", notADateValue);
        this.timeInputSupported = (input.value !== notADateValue);

        input.setAttribute("type", "datetime");
        input.setAttribute("value", notADateValue);
        this.datetimeInputSupported = (input.value !== notADateValue);
        logger.info("support input date : " + this.dateInputSupported + ", time : " + this.timeInputSupported + ", datetime : " + this.datetimeInputSupported);

        bus.on("unauthorize", () => {
            if (this.user !== null) {
                var oldUser = this.user;
                this.user = null;
                bus.trigger("user", this.user, oldUser);
            }
        });
        this.isGtu = false;
    }

    start() {
        this.updateConfigurationServer();
        this.updateUser();
        this.userPromise.catch(() => { /* prevent uncatched promise */ });
        this.updateGtu();
    }

    updateConfigurationServer() {
        this.configurationServerPromise = configurationService.getConfiguration();
        this.configurationServerPromise.then(result => {
            this.configurationServer = result;
            return this;
        });
    }

    updateUser() {
        if (this.isConnected()) {
            this.userPromise = this.connect();
            this.userPromise.then(user => {
                var oldUser = this.user;
                this.user = user;
                bus.trigger("user", user, oldUser);
            }, () => {
                bus.trigger("userNoMoreConnected");
            });
        } else {
            this.userPromise = Promise.reject("user not connected");
        }
    }

    updateGtu() {
        resourceService.isGtu().then(result => {
            this.isGtu = result;
            bus.trigger("isGtu", this.isGtu);
        });
    }

    getUser() {
        return this.user;
    }

    onLocaleChanged(fn) {
        this.on("localeChanged", fn);
    }

    changeLocale(locale) {
        this.locale = locale;
        localStorage.userLanguage = locale;
        bus.trigger("locale", this.locale);
    }

    getUserSetting(key, defaultValue) {
        return localStorage[key] || defaultValue;
    }

    setUserSetting(key, value) {
        localStorage[key] = value;
    }

    isConnected() {
        return document.cookie.indexOf("pollen-auth=") !== -1;
    }

    isAdminConnected() {
        return this.userPromise.then((user) => {
            return Promise.resolve(user.administrator);
        });
    }

    connect() {
        logger.info("Connect::");
        return authService.connectedUserPromise().then((user) => {
            if (!user) {
                logger.info("Connect error");
                return Promise.reject();
            }
            logger.info("Connect user::");
            logger.info(user);
            return Promise.resolve(user);
        }, () => {
            logger.info("Connect error");
            return Promise.reject();
        });
    }

    signIn(login, password) {
        return authService.signIn(login, password).then(auth => this.updateConnection(auth, this));
    }

    signInProvider(query) {
        return authService.signInProvider(query)
            .then(auth => this.updateConnection(auth, this));
    }

    updateConnection(auth, session) {
        logger.info("SignIn::");
        logger.info(auth);
        this.userPromise = authService.userPromise(auth);
        return this.userPromise.then((user) => {
            if (!user) {
                logger.info("SignIn error");
                let oldUser = session.user;
                session.user = null;
                bus.trigger("user", session.user, oldUser);
                return Promise.reject();
            }
            logger.info("SignIn user::");
            logger.info(user);
            pageTracker.trackLogin();
            let oldUser = session.user;
            session.user = user;
            bus.trigger("user", session.user, oldUser);

            return session.user;
        });
    }

    signOut() {
        return authService.signOut().then(() => {
            var oldUser = this.user;
            this.user = null;
            this.userPromise = Promise.reject();
            bus.trigger("user", this.user, oldUser);
            return this.user;
        });
    }

    getProviderRedirectionUrl(provider) {
        return this.pollenUIContext.uiEndPoint + "/?loginProvider=" + provider + "&action=signin";
    }

    isCanCreatePoll() {
        return this.configurationServer
            && (!this.configurationServer.userConnectedRequired || this.user)
            && (this.configurationServer.usersCanCreatePoll === "ALL_USERS"
                || (this.configurationServer.usersCanCreatePoll === "USERS_CONNECTED" && this.user)
                || (this.configurationServer.usersCanCreatePoll === "USERS_SELECTED" && this.user && this.user.canCreatePoll));
    }

    getCanCreatePollPromise() {
        return this.configurationServerPromise.then(config => {
            if (config.usersCanCreatePoll === "ALL_USERS") {
                return true;
            }
            return this.userPromise.then(user => {
                return config.usersCanCreatePoll === "USERS_CONNECTED" || user.canCreatePoll;
            }, () => {
                return false;
            });
        });
    }

}

export default singleton(Session);
