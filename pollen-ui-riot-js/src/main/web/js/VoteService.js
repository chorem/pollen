/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";

class VoteService extends FetchService {

    _getUrlPrefix(pollId, questionId, voteId) {
        let url = "/v1/polls/" + pollId + "/questions/" + questionId + "/votes";
        if (voteId) {
            url += "/" + voteId;
        }
        return url;
    }

    getVotes(pollId, questionId, pagination, permission) {
        let params = Object.assign({}, pagination);
        if (permission) {
            params.permission = permission;
        }
        let url = this._getUrlPrefix(pollId, questionId);
        return this.get(url, params);
    }

    addVote(pollId, questionId, form, permission) {
        let url = this._getUrlPrefix(pollId, questionId);
        return this.post(url, form, {permission: permission});
    }

    getNewVote(pollId, questionId, permission) {
        let url = this._getUrlPrefix(pollId, questionId, "new");
        return this.get(url, {permission: permission});
    }

    updateVote(pollId, questionId, form, permission) {
        let url = this._getUrlPrefix(pollId, questionId, form.id);
        return this.post(url, form, {permission: permission});
    }

    deleteVote(pollId, questionId, voteId, permission) {
        let url = this._getUrlPrefix(pollId, questionId, voteId);
        return this.doDelete(url, {permission: permission});
    }
}

export default singleton(VoteService);
