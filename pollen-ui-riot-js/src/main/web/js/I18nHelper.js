/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
export default {

    installBundle(session, value, callback) {
        this.bundle = session.i18n;
        this.debug = session.configuration.debugI18n;
        this.generateBundle(session.locale, value);
        let onLocalChange = (locale) => {
            this.generateBundle(locale, value);
            if (callback) {
                callback(locale);
            }
            try {
                this.update();
            } catch (e) {
                this.logger.error("Error in generateBundle for " + value, e);
            }
        };
        this.listen("locale", onLocalChange);
    },

    generateBundle(locale, value) {

        let bundle = this.bundle[locale];
        this._t = {};
        Object.keys(bundle).forEach((key) => {
            if (key.startsWith(value + "_")) {

                let realKey = key.substring(value.length + 1);
                this._t[realKey] = bundle[key];
                if (this.debug) {
                    this.logger.debug(realKey + " -> " + this._t[realKey]);
                }
            }
        });
    },

    _l(key, ...params) {
        return this.i18nformat(this._t[key], params) || key;
    },

    translateErrors(errors) {
        let errorsTranslate = {};
        Object.keys(errors).forEach((key) => {
            errorsTranslate[key] = errors[key].map(this._l);
        });
        return errorsTranslate;
    },

    i18nformat(value, params) {
        if (!params) {
            return value;
        }

        if (!value) {
            return "";
        }

        return value.replace(/{(\d+)}/g, (match, number) => {
            return params[number] || "";
        });
    }
};
