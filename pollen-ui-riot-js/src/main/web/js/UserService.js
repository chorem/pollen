/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";

class UserService extends FetchService {

    _getUsersUrlPrefix(userId) {
        let url = "/v1/users";
        if (userId) {
            url += "/" + userId;
        }
        return url;
    }

    _getUserUrlPrefix() {
        return "/v1/user";
    }

    users(pagination, search) {
        let params = Object.assign({}, pagination);
        params.search = search || "";
        let url = this._getUsersUrlPrefix();
        return this.get(url, params);
    }

    user(userId, permission) {
        let url = this._getUsersUrlPrefix(userId);
        return this.get(url, {permission: permission});
    }

    deleteUser(userId) {
        let url = this._getUsersUrlPrefix(userId);
        return this.doDelete(url, {anonymize: true});
    }

    saveUser(user) {
        let url = this._getUsersUrlPrefix(user.id);
        return this.post(url, user);
    }

    addEmailAddressToUser(emailAddress, userId) {
        let url;
        if (userId) {
            url = this._getUsersUrlPrefix(userId);
        } else {
            url = this._getUserUrlPrefix();
        }
        url += "/email";
        return this.post(url, emailAddress);
    }

    deleteEmailAddress(emailAddressId, userId) {
        let url;
        if (userId) {
            url = this._getUsersUrlPrefix(userId);
        } else {
            url = this._getUserUrlPrefix();
        }
        url += "/email/" + emailAddressId;
        return this.doDelete(url);
    }

    setDefaultEmailAddress(emailAddressId, userId) {
        let url;
        if (userId) {
            url = this._getUsersUrlPrefix(userId);
        } else {
            url = this._getUserUrlPrefix();
        }
        url += "/email/default?emailAddressId=" + emailAddressId;
        return this.put(url);
    }

    changePassword(oldPassword, newPassword) {
        let url = this._getUserUrlPrefix() + "/password";
        let body = {
            oldPassword: oldPassword,
            newPassword: newPassword
        };
        return this.post(url, body);
    }

    linkProvider(query) {
        let url = this._getUserUrlPrefix() + "/credentials/" + query.loginProvider;
        let body = JSON.stringify(query);
        return this.post(url, body);
    }

    unlinkProvider(credentialId) {
        let url = this._getUserUrlPrefix() + "/credentials/" + credentialId;
        return this.doDelete(url);
    }

    gtuValidate() {
        let url = "/v1/gtu/validate";
        return this.post(url);
    }

    setProviderAvatar(query) {
        let url = this._getUserUrlPrefix() + "/avatar/" + query.loginProvider;
        let body = JSON.stringify(query);
        return this.post(url, body);
    }

    getUserAvatarUrl(userId) {
        return window.pollenConf.endPoint + this._getUsersUrlPrefix(userId) + "/avatar";
    }

    setUserAvatar(avatar) {
        let url = this._getUserUrlPrefix() + "/avatar";
        return this.form(url, {avatar: avatar}, true);
    }

    deleteAvatar(userId) {
        let url;
        if (userId) {
            url = this._getUsersUrlPrefix(userId);
        } else {
            url = this._getUserUrlPrefix();
        }
        return this.doDelete(url + "/avatar");
    }

    saveEmailAdress(emailAddress, userId) {
        let url;
        if (userId) {
            url = this._getUsersUrlPrefix(userId);
        } else {
            url = this._getUserUrlPrefix();
        }
        url += "/email/" + emailAddress.id;
        return this.post(url, emailAddress);
    }
}

export default singleton(UserService);
