/*-
 * #%L
 * Pollen :: UI RiotJs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import pollService from "./PollService";
import voteService from "./VoteService";
import Choice from "./Choice";
import choiceService from "./ChoiceService";
import voteCountingTypeService from "./VoteCountingTypeService";
import resultService from "./ResultService";
import commentService from "./CommentService";
import resourceService from "./ResourceService";
import bus from "./PollenBus.js";
import pageTracker from "./PageTracker";

class Poll {

    constructor() {
        this.questions = [];
        this.comments = [];
    }

    init(pollId, voteId, permission) {
        this._initPromise = pollService.getPoll(pollId, permission).then(result => {
            delete this.id;
            delete this.permission;
            delete this.voteId;
            delete this.votePermission;
            Object.assign(this, result);
            this.voteId = voteId;
            this.votePermission = permission;
            this.comments = undefined;
            this.results = undefined;
            bus.trigger("poll", this);
            return Promise.resolve(this);
        });

        return this._initPromise;
    }

    getPermission() {
        return this.votePermission || this.permission;
    }

    reloadPoll() {
        if (this.id) {
            return pollService.getPoll(this.id, this.getPermission()).then(result => {
                Object.assign(this, result);
                bus.trigger("poll", this);
                return Promise.resolve(this);
            });
        }
        return Promise.reject("Init poll after reload poll");
    }

    delete() {
        if (this.id) {
            return pollService.deletePoll(this.id, this.permission).then(() => {
                delete this.id;
                delete this.permission;
                delete this._initPromise;
                this.comments = [];
                bus.trigger("poll", this);
            });
        }
        return Promise.reject("Init poll after delete poll");
    }

    initChoice(choice) {
        choice = choice || new Choice();
        choice.id = undefined;
        if (this.choices && this.choices.length > 0) {
            let lastChoice = this.choices[this.choices.length - 1];
            choice.choiceType = lastChoice.choiceType;
            if (lastChoice.choiceType.startsWith("DATE")) {
                choice.choiceValue = lastChoice.choiceValue;
            } else {
                choice.choiceValue = undefined;
            }
        } else {
            choice.choiceType = this.choiceType || "TEXT";
            choice.choiceValue = undefined;
            choice.description = undefined;
        }
        choice.description = undefined;
        return choice;
    }

    addChoice(questionId, choice) {
        if (this.id && questionId) {
            let promise;
            if (choice.choiceType === "RESOURCE" && choice.choiceValue) {
                promise = resourceService.create(choice.choiceValue, "CHOICE").then((result) => {
                    choice.choiceValue = result.id;
                    return Promise.resolve(choice);
                });
            } else {
                promise = Promise.resolve(choice);
            }
            return promise.then(choice2 => {
                return choiceService.addChoice(this.id, questionId, choice2, this.getPermission()).then(() => {
                    return Promise.all([this.reloadPoll()]);
                });
            });
        }
        return Promise.reject("Init poll after add choice");
    }

    loadLazyVotes(questionId, pagination) {
        if (this._initPromise) {
            return voteService.getVotes(this.id, questionId, pagination, this.getPermission()).then((result) => {
                if (this.voteIsVisible) {
                    //TODO JC190110-Should use questionId
                    this.questions[0].voteCount = result.pagination.count;
                }
                return result;
            });
        }
        return Promise.reject("Init poll after load votes");
    }

    loadLazyParticipants(questionId, pagination) {
        if (this._initPromise) {
            return this._initPromise.then(() => {
                return pollService.getParticipants(this.id, pagination, this.getPermission());
            });
        }
        return Promise.reject("Init poll after load participants");
    }

    loadForVotes(questionId) {
        if (this._initPromise) {
            return this._initPromise.then(() => {
                if (questionId) {
                    var promises = [
                        choiceService.getChoices(this.id, questionId, this.getPermission()),
                        voteCountingTypeService.getVoteCountingType(this.voteCountingType)
                    ];
                    if (this.resultIsVisible) {
                        promises.push(resultService.getResults(this.id, questionId, this.getPermission()));
                    }
                    if (this.canVote) {
                        promises.push(voteService.getNewVote(this.id, questionId, this.getPermission()));
                    }
                    return Promise.all(promises).then(resultsArray => {
                        let indexResult = 0;
                        this.choices = resultsArray[indexResult++];
                        this.choiceCount = this.choices.length;

                        this.voteCountingTypeValue = resultsArray[indexResult++];

                        if (this.resultIsVisible) {
                            this.results = resultsArray[indexResult++];
                            this.choices.forEach(choice => {
                                choice.score = this.results.scores.find(score => score.choiceId === choice.id);
                            });
                        }

                        if (this.canVote) {
                            this.voterName = resultsArray[indexResult++].voterName;
                        }

                        pageTracker.trackPoll();

                        bus.trigger("poll", this);
                        return Promise.resolve(this);
                    });
                }
                return Promise.reject("Init poll after load questions");
            });
        }
        return Promise.reject("Init poll after load votes");
    }

    reloadVoteCountingTypeValue(question) {
        if (this._initPromise) {
            return voteCountingTypeService.getVoteCountingType(question.voteCountingType).then(result => {
                question.voteCountingTypeValue = result;
                return Promise.resolve(this);
            });
        }
        return Promise.reject("Init poll after reload vote counting type");
    }

    getVoteChoice(vote, choice) {
        return vote.choice.find(c => c.choiceId === choice.id);
    }

    getVoteValue(vote, choice) {
        let voteChoice = this.getVoteChoice(vote, choice);
        let value = voteChoice && voteChoice.voteValue;
        if (this.voteCountingType === 8) {
            value = this.voteCountingConfig.grades[value];
        }
        return value;
    }

    loadResults(questionId) {
        if (this._initPromise) {
            return this._initPromise.then(() => {
                if (this.resultIsVisible && questionId) {
                    return Promise.all([
                        choiceService.getChoices(this.id, questionId, this.getPermission()),
                        resultService.getResults(this.id, questionId, this.getPermission())])
                        .then(resultsArray => {
                            this.choices = resultsArray[0];
                            this.choiceCount = this.choices.length;
                            this.questions[0].results = resultsArray[1];
                            this.questions[0].choices.forEach(choice => {
                                choice.score = this.questions[0].results.scores.find(score => score.choiceId === choice.id);
                            });

                            pageTracker.trackResults();

                            bus.trigger("poll", this);
                            return Promise.resolve(this);
                        });
                }
                return Promise.resolve(this);
            });
        }
        return Promise.reject("Init poll after load results");
    }

    getPodium(questionId) {
        let podium = {};
        let question = this.getQuestion(questionId);
        if (this.id && this.resultIsVisible && question.results && question.results.nbVotants > 0) {
            let choiceCount = 0;
            let index = 0;

            let getChoicesByScoreOrder = (order) => question.results.scores
                .filter(score => score.scoreOrder === order)
                .map(score => question.choices.find(choice => choice.id === score.choiceId));

            while (choiceCount < 3 && index < 3) {
                let choices = getChoicesByScoreOrder(index);
                podium["step" + (choiceCount + 1)] = choices;
                index++;
                choiceCount += choices.length;
            }
        }
        return podium;
    }

    //TODO Deal with question and poll comments
    loadCommentAuthorName() {
        if (this._initPromise) {
            return this._initPromise.then(() => {
                return commentService.getNewComment(this.id, this.getPermission()).then((result) => {
                    this.authorName = result.authorName;
                    pageTracker.trackComments();
                    bus.trigger("poll", this);
                    return Promise.resolve(this);
                });
            });
        }
        return Promise.reject("Init poll after load comments");
    }

    //TODO Deal with question and poll comments
    loadLazyComments(pagination) {
        if (this._initPromise) {
            return this._initPromise.then(() => {
                return commentService.getComments(this.id, pagination, this.getPermission()).then((result) => {
                    this.commentCount = result.pagination.count;
                    bus.trigger("poll", this);
                    return result;
                });
            });
        }
        return Promise.reject("Init poll after load comments");
    }

    addVote(questionId, vote) {
        if (this.id && questionId) {
            return voteService.addVote(this.id, questionId, vote, this.getPermission()).then((result) => {
                pageTracker.trackVote();

                this.voteId = result.id;
                this.votePermission = result.permission;
                return this.reloadPoll();
            });
        }
        return Promise.reject("Init poll after add vote");
    }

    updateVote(questionId, vote) {
        if (this.id && questionId) {
            return voteService.updateVote(this.id, questionId, vote, this.getPermission() || vote.permission || "").then(() => {
                return this.reloadPoll();
            });
        }
        return Promise.reject("Init poll after update vote");
    }

    deleteVote(questionId, vote) {
        if (this.id && questionId) {
            return voteService.deleteVote(this.id, questionId, vote.id, this.getPermission() || vote.permission || "").then(() => {
                return this.reloadPoll();
            });
        }
        return Promise.reject("Init poll after delete vote");
    }

    getChoice(questionId, choiceId) {
        let question = this.getQuestion(questionId);
        return question.choices && question.choices.find(choice => choice.id === choiceId);
    }

    getQuestion(questionId) {
        return this.questions && this.questions.find(question => question.id === questionId);
    }
    //TODO Deal with question and poll comments
    addComment(authorName, text) {
        if (this.id) {
            let form = {
                authorName: authorName,
                text: text
            };
            return commentService.createComment(this.id, form, this.getPermission()).then((comment) => {
                pageTracker.trackAddComment();
                return comment;
            });
        }
        return Promise.reject("Init poll after add comment");
    }
    //TODO Deal with question and poll comments
    updateComment(comment) {
        if (this.id) {
            return commentService.updateComment(this.id, comment, this.getPermission() || "");
        }
        return Promise.reject("Init poll after update comment");
    }
    //TODO Deal with question and poll comments
    deleteComment(comment) {
        if (this.id) {
            return commentService.deleteComment(this.id, comment.id, this.getPermission() || "");
        }
        return Promise.reject("Init poll after delete comment");
    }

    isPoll(target) {
        return this === target;
    }
    //TODO Deal with question and poll comments
    isComment(target) {
        return this.comments && this.comments.indexOf(target) >= 0;
    }

    //TODO Add question management
    isChoice(target) {
        return this.choices && this.choices.indexOf(target) >= 0;
    }


    //TODO API reports a changé ?
    _getService(target) {
        let service;
        if (this.isPoll(target)) {
            service = pollService;
        } else if (this.isComment(target)) {
            service = commentService;
        } else if (this.isChoice(target)) {
            service = choiceService;
        }
        return service;
    }

    addReport(target, report) {
        if (this.id) {
            let service = this._getService(target);
            return service.addReport(this.id, target.id, report, this.permission || "");
        }
        return Promise.reject("Init poll after add report");
    }

    getReports(target) {
        if (this.id) {
            let service = this._getService(target);
            return service.getReports(this.id, target.id, this.permission || "");
        }
        return Promise.reject("Init poll after load reports");
    }

    saveReport(target, report) {
        if (this.id) {
            let service = this._getService(target);
            return service.saveReport(this.id, target.id, report, this.permission || "");
        }
        return Promise.reject("Init poll after ignore report");
    }

    loadVoteCountingType() {
        voteCountingTypeService.getVoteCountingType(this.questions[0].voteCountingType).then((voteCountingTypeValue) => {
            this.questions[0].voteCountingTypeValue = voteCountingTypeValue;
            bus.trigger("poll", this);
        });
    }

    getInvalidEmails() {
        if (this.id) {
            return pollService.getInvalidEmails(this.id, this.permission);
        }
        return Promise.reject("Init poll after get invalid emails");
    }
}

export default singleton(Poll);
