/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import values from "object.values";
if (!Object.values) {
    values.shim();
}

class Message {

    constructor(e, type, timeout) {
        if (e instanceof Message) {
            this.content = e.content;
            this.type = e.type;
            this.timeout = e.timeout;
        } else if (typeof e === "string" || e instanceof String) {
            this.content = e;
        } else {
            this.content = Object.values(e).reduce((messages, messagesfield) => messages.concat(messagesfield), []);
        }
        if (!this.timeout) {
            this.timeout = timeout;
        }
        if (!this.type) {
            this.type = type;
        }
    }
}

export default Message;
