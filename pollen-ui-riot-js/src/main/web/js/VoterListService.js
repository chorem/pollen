/*-
 * #%L
 * Pollen :: UI RiotJs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";
import values from "object.values";
import FavoriteListService from "./FavoriteListService";
if (!Object.values) {
    values.shim();
}

class VoterListService extends FetchService {

    constructor() {
        super();
        this.tempPrefix = "TEMP";
        this.tempId = 1;
        this.voterListsById = {};
        this.voterListMembersById = {};
        this.voterListsToDelete = [];
        this.voterListMembersToDelete = [];
    }

    _newVoterList(parent, name, weight) {
        return {
            id: this.tempPrefix + this.tempId++,
            parentId: parent && parent.id,
            countSubLists: 0,
            subLists: [],
            countMembers: 0,
            members: [],
            allEmails: [],
            name: name,
            weight: weight || 1,
            loadded: true,
            temp: true
        };
    }

    _newMember(voterList, name, email, weight) {
        return {
            id: this.tempPrefix + this.tempId++,
            voterListId: voterList.id,
            name: name,
            email: email,
            weight: weight || 1,
            temp: true
        };
    }

    _getUrlPrefix(pollId, voterListId) {
        let url = "/v1/polls/" + pollId + "/voterLists";
        if (voterListId) {
            url += "/" + voterListId;
        }
        return url;
    }

    init(pollForm) {
        this.tempId = 1;
        this.voterListsById = {};
        this.voterListMembersById = {};
        this.voterListsToDelete = [];
        this.voterListMembersToDelete = [];
        this.pollForm = pollForm;
        let mainVoterListPromise = Promise.resolve();
        if (this.pollForm.model && this.pollForm.model.id) {
            let url = this._getUrlPrefix(pollForm.model.id, "main");
            mainVoterListPromise = this.get(url, {permission: pollForm.model.permission});
        }
        mainVoterListPromise = mainVoterListPromise.then(list => {
            let mainList = list || this._newVoterList();
            this.voterListsById[mainList.id] = mainList;
            return mainList;
        });
        return mainVoterListPromise;
    }

    addMeIfEmpty() {
        let mainList = this.pollForm.mainVoterList;
        let name = this.pollForm.model.creatorName;
        let email = this.pollForm.model.creatorEmail;
        if (mainList.countMembers === 0 && mainList.countSubLists === 0 && email) {
            let me = this._newMember(mainList, name, email, 1);
            mainList.members.push(me);
            this.voterListMembersById[me.id] = me;
            mainList.countMembers = mainList.members.length;
        }
    }

    loadList(voterList) {
        let voterListPromise;
        if (!voterList.loadded) {
            voterListPromise = Promise.resolve([[], []]);
            if (this.pollForm.model && this.pollForm.model.id && !voterList.subLists && !voterList.id.startsWith(this.tempPrefix)) {
                let url = this._getUrlPrefix(this.pollForm.model.id, voterList.id);
                voterListPromise = Promise.all([
                    this.get(url + "/lists", {permission: this.pollForm.model.permission}),
                    this.get(url + "/members", {permission: this.pollForm.model.permission})]);
            }
            voterListPromise.then(result => {
                voterList.subLists = result[0];
                voterList.members = result[1];
                voterList.subLists.forEach(list => {this.voterListsById[list.id] = list;});
                voterList.members.forEach(member => {this.voterListMembersById[member.id] = member;});
                voterList.loadded = true;
                return voterList;
            });
        } else {
            voterListPromise = Promise.resolve(voterList);
        }
        return voterListPromise;
    }

    _checkVoterList(parent, name, weight, id) {
        let errors = {};
        let sameName = parent.subLists.find(m => m.id !== id && m.name === name);
        if (sameName) {
            errors.name = ["name_error_alreadyExist"];
        }
        return errors;
    }

    addVoterList(parent, name, weight) {
        let errors = this._checkVoterList(parent, name, weight);
        if (Object.keys(errors).length > 0) {
            return Promise.reject(errors);
        }
        let voterList = this._newVoterList(parent, name, weight);
        parent.subLists.push(voterList);
        this.voterListsById[voterList.id] = voterList;
        parent.countSubLists = parent.subLists.length;
        return Promise.resolve(voterList);
    }

    saveVoterList(id, name, weight) {
        let voterList = this.voterListsById[id];
        let parent = this.voterListsById[voterList.parentId];
        let errors = this._checkVoterList(parent, name, weight, id);
        if (Object.keys(errors).length > 0) {
            return Promise.reject(errors);
        }
        voterList.name = name;
        voterList.weight = weight;
        return Promise.resolve(voterList);
    }

    deleteVoterList(voterList) {
        let parent = this.voterListsById[voterList.parentId];
        let index = parent.subLists.indexOf(voterList);
        parent.subLists.splice(index, 1);

        voterList.subLists && voterList.subLists.forEach(subList => {
            this._deleteVoterList2(subList);
        });

        voterList.members && voterList.members.forEach(member => {
            delete this.voterListMembersById[member.id];
        });

        delete this.voterListsById[voterList.id];

        if (!voterList.id.startsWith(this.tempPrefix)) {
            this.voterListsToDelete.push(voterList);
        }
        parent.countSubLists = parent.subLists.length;
    }

    _deleteVoterList2(voterList) {
        voterList.subLists.forEach(subList => {
            this._deleteVoterList2(subList);
        });
        delete this.voterListsById[voterList.id];
    }


    _checkMember(parent, name, email, weight, id) {
        let errors = {};
        let sameName = parent.members.find(m => m.id !== id && m.name === name);
        if (sameName) {
            errors.name = ["name_error_alreadyExist"];
        }
        let sameEmail = parent.members.find(m => m.id !== id && m.email === email);
        if (sameEmail) {
            errors.email = ["email_error_alreadyExist"];
        }
        return errors;
    }

    addMember(parent, name, email, weight) {
        let errors = this._checkMember(parent, name, email, weight);
        if (Object.keys(errors).length > 0) {
            return Promise.reject(errors);
        }
        let member = this._newMember(parent, name, email, weight);
        parent.members.push(member);
        this.voterListMembersById[member.id] = member;
        parent.countMembers = parent.members.length;
        return Promise.resolve(member);
    }

    saveMember(id, name, email, weight) {
        let member = this.voterListMembersById[id];
        let parent = this.voterListsById[member.voterListId];
        let errors = this._checkMember(parent, name, email, weight, id);
        if (Object.keys(errors).length > 0) {
            return Promise.reject(errors);
        }
        member.name = name;
        member.email = email;
        member.weight = weight;
        return Promise.resolve(member);
    }

    countAllEmails(voterList) {
        let emails = this.getAllEmails(voterList);
        return emails.size;
    }

    getAllEmails(voterList) {
        let emails = new Set();
        if (voterList.loadded) {
            voterList.members
                .map(m => m.email)
                .forEach(email => emails.add(email));
            voterList.subLists
                .map(subList => this.getAllEmails(subList))
                .forEach(subEmails => subEmails.forEach(email => emails.add(email)));
        } else {
            voterList.allEmails.forEach(email => emails.add(email));
        }
        return emails;
    }

    deleteMember(member) {
        let voterList = this.voterListsById[member.voterListId];
        let index = voterList.members.indexOf(member);
        voterList.members.splice(index, 1);
        delete this.voterListMembersById[member.id];

        if (!member.id.startsWith(this.tempPrefix)) {
            this.voterListMembersToDelete.push(member);
        }
        voterList.countMembers = voterList.members.length;
    }

    getVoterLists() {
        return Object.values(this.voterListsById).map(voterlist => {
            return {
                id: voterlist.id,
                parentId: voterlist.parentId,
                name: voterlist.name,
                weight: voterlist.weight,
                countMembers: voterlist.countMembers
            };
        });
    }

    getVoterListMembers() {
        return Object.values(this.voterListMembersById).map(voterlistmember => {
            return {
                id: voterlistmember.id,
                voterListId: voterlistmember.voterListId,
                name: voterlistmember.name,
                email: voterlistmember.email,
                weight: voterlistmember.weight};
        });
    }

    getVoterListIdsToDelete() {
        return this.voterListsToDelete.map(voterList => voterList.id);
    }

    getVoterListMemberIdsToDelete() {
        return this.voterListMembersToDelete.map(member => member.id);
    }

    save() {
        let url = this._getUrlPrefix(this.pollForm.model.id);
        return this.put(url,
            {
                pollType: this.pollForm.model.pollType,
                listsToSave: this.getVoterLists(),
                membersToSave: this.getVoterListMembers(),
                listsToDelete: this.voterListsToDelete,
                membersToDelete: this.voterListMembersToDelete
            },
            {permission: this.pollForm.model.permission})
            .then(() => {
                this.voterListsToDelete = [];
                this.voterListMembersToDelete = [];
            });
    }

    importFavoriteList(voterList, favoriteListId) {
        let pagination = {
            order: "name",
            desc: false,
            pageSize: -1,
            pageNumber: 0
        };
        let importPromises = [];
        importPromises.push(FavoriteListService.members(favoriteListId, "", pagination).then(result => {
            result.elements.forEach(member => {
                this.addMember(voterList, member.name, member.email, member.weight);
            });
            return Promise.resolve();
        }));
        let paginationChild = {
            order: "child.name",
            desc: false,
            pageSize: -1,
            pageNumber: 0
        };
        importPromises.push(FavoriteListService.childrenLists(favoriteListId, "", paginationChild).then(result => {
            let subListImportPromises = result.elements.map(childList => {
                return this.addVoterList(voterList, childList.child.name, childList.weight).then(subVoterList => {
                    return this.importFavoriteList(subVoterList, childList.child.id);
                });
            });
            return Promise.all(subListImportPromises);
        }));
        return Promise.all(importPromises);
    }

    sendInvitationList(voterList) {
        if (!voterList.temp) {
            let url = this._getUrlPrefix(this.pollForm.model.id, voterList.id) + "/send";
            return this.get(url, {permission: this.pollForm.model.permission});
        }
        return Promise.reject();
    }

    sendInvitationMember(member) {
        if (!member.temp) {
            let url = this._getUrlPrefix(this.pollForm.model.id, member.voterListId) + "/members/" + member.id + "/send";
            return this.get(url, {permission: this.pollForm.model.permission});
        }
        return Promise.reject();
    }

    resendInvitationList(voterList) {
        if (!voterList.temp) {
            let url = this._getUrlPrefix(this.pollForm.model.id, voterList.id) + "/resend";
            return this.get(url, {permission: this.pollForm.model.permission});
        }
        return Promise.reject();
    }

    resendInvitationMember(member) {
        if (!member.temp) {
            let url = this._getUrlPrefix(this.pollForm.model.id, member.voterListId) + "/members/" + member.id + "/resend";
            return this.get(url, {permission: this.pollForm.model.permission});
        }
        return Promise.reject();
    }
}

export default singleton(VoterListService);
