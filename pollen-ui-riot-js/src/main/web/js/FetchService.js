/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import bus from "./PollenBus.js";
import session from "./Session";

class FetchService {

    constructor() {
        this.endPoint = window.pollenConf.endPoint;
    }

    fetch(url, method, headers, body) {
        headers = headers || {};
        if (!(body instanceof FormData)) {
            headers["Content-Type"] = "application/json;charset=UTF-8";
        }

        headers["X-Pollen-UI-context"] = JSON.stringify(session.pollenUIContext);
        headers["Content-Language"] = session.locale;
        let loadEvent = {};
        bus.trigger("loading", loadEvent);
        return fetch(
            this.endPoint + url, {
                headers,
                method,
                credentials: "include",
                body: (body instanceof FormData || typeof body === "string") ? body : body && JSON.stringify(body)
            })
            .then((response) => {
                bus.trigger("loaded", loadEvent);
                if (response.status === 204) {
                    return null;
                }
                if (response.status === 200) {
                    let responseCopy = response.clone();
                    return responseCopy.json().catch(() => {
                        return response.text();
                    });
                }
                if (response.status === 400) {
                    return response.json().then(json => Promise.reject(json));
                }
                if (response.status === 401) {
                    bus.trigger("unauthorize");
                    return Promise.reject(response);
                }
                if (response.status === 503) {
                    bus.trigger("unauthorize");
                    return Promise.reject(response);
                }
                return Promise.reject(response);
            },
            (error) => {
                bus.trigger("loaded", loadEvent);

                return Promise.reject(error);
            });
    }

    get(url, params) {
        url = this._addParamsToUrl(url, params);
        return this.fetch(url, "GET");
    }

    _addParamsToUrl(url, params) {
        if (params) {
            let query = "?";
            let keys = Object.keys(params);
            keys.forEach((key) => {
                let value = params[key];
                if (value) {
                    if (typeof value === "object") {
                        value = JSON.stringify(value);
                    }
                    if (query.length > 1) {
                        query += "&";
                    }
                    query += key + "=" + value;
                }
            });
            return url + query;
        }
        return url;
    }

    post(url, body, params) {
        url = this._addParamsToUrl(url, params);
        return this.fetch(url, "POST", null, body);
    }

    put(url, body, params) {
        url = this._addParamsToUrl(url, params);
        return this.fetch(url, "PUT", null, body);
    }

    doDelete(url, params) {
        url = this._addParamsToUrl(url, params);
        return this.fetch(url, "DELETE");
    }

    form(url, data, doNotStringify, params) {
        let formData = null;
        if (data) {
            formData = new FormData();

            let keys = Object.keys(data);
            keys.forEach((key) => {
                let value = data[key];
                if (!doNotStringify && (typeof value === "object")) {
                    formData.set(key, JSON.stringify(value));
                } else {
                    formData.set(key, value);
                }
            });
        }
        url = this._addParamsToUrl(url, params);
        return this.fetch(url, "POST", null, formData);
    }
}

export default FetchService;
