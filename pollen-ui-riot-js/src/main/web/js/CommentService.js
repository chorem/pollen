/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";

class CommentService extends FetchService {

    _getUrlPrefix(pollId, commentId) {
        let url = "/v1/polls/" + pollId + "/comments";
        if (commentId) {
            url += "/" + commentId;
        }
        return url;
    }

    getComments(pollId, pagination, permission) {
        let params = Object.assign({}, pagination);
        if (permission) {
            params.permission = permission;
        }
        let url = this._getUrlPrefix(pollId);
        return this.get(url, params);
    }

    getNewComment(pollId, permission) {
        let url = this._getUrlPrefix(pollId) + "/new";
        return this.get(url, {permission: permission});
    }

    createComment(pollId, form, permission) {
        let url = this._getUrlPrefix(pollId);
        return this.post(url, form, {permission: permission});
    }

    updateComment(pollId, form, permission) {
        let url = this._getUrlPrefix(pollId, form.id);
        return this.post(url, form, {permission: permission});
    }

    deleteComment(pollId, commentId, permission) {
        let url = this._getUrlPrefix(pollId, commentId);
        return this.doDelete(url, {permission: permission});
    }

    addReport(pollId, commentId, report, permission) {
        let url = this._getUrlPrefix(pollId, commentId) + "/reports";
        return this.post(url, report, {permission: permission});
    }

    getReports(pollId, commentId, permission) {
        let url = this._getUrlPrefix(pollId, commentId) + "/reports";
        return this.get(url, {permission: permission});
    }

    saveReport(pollId, commentId, report, permission) {
        let url = this._getUrlPrefix(pollId, commentId) + "/reports/" + report.id;
        return this.post(url, report, {permission: permission});
    }
}

export default singleton(CommentService);
