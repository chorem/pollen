/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import Choice from "./Choice";
import voteCountingTypeService from "./VoteCountingTypeService";
import resourceService from "./ResourceService";
import pollService from "./PollService";
import voterListService from "./VoterListService";
import bus from "./PollenBus";
import logger from "./Logger";
import pageTracker from "./PageTracker";

class PollForm {

    constructor() {
        this.steps = [
            "general",
            "choices",
            "options",
            "voters"
        ];
        this.types = ["FREE", "RESTRICTED", "REGISTERED"];

        this.pollService = pollService;
        this.pageTracker = pageTracker;
        this.step = -1;
        this.maxStepReached = -1;
        this.isInit = false;
        this.creation = false;
        this.showOptions = false;
        this.model = {};
        this.mainVoterList = {};
        this.currentVoterList = {};
    }

    _loadVoteCountingTypes() {
        if (!this.voteCountingTypes) {
            return this.reloadVoteCountingTypes();
        }
        return Promise.resolve();
    }

    reloadVoteCountingTypes() {
        return voteCountingTypeService.getVoteCountingTypes().then(result => {
            this.voteCountingTypes = result;
            return Promise.resolve();
        });
    }

    loadPoll(pollId, permission, clone) {
        return Promise.all([
            pollService.getPoll(pollId, permission),
            voterListService.init(pollId, permission),
            this._loadVoteCountingTypes()
        ]).then(results => {
            Object.assign(this.model, results[0]);
            this.showOptions = true;
            this.creation = !!clone;

            //TODO check if everything is still on the model
            if (clone) {
                this.model.id = undefined;
                this.model.commentCount = 0;
                this.model.createDate = undefined;
                this.model.canVote = false;
                this.model.closed = false;
                this.model.alreadyParticipants = undefined;
                this.model.permission = undefined;
                this.model.status = undefined;
                this.hasVotes = false;
                this.model.questions.forEach((question) => {
                    question.id = undefined;
                });

            } else {
                this.hasVotes = false;
                this.model.questions.forEach((question) => {
                    if (question.voteCount > 0) {
                        this.hasVotes = true;
                    }
                });
                this.model.alreadyParticipants = this.model.participants;
            }

            if (clone) {
                this.model.questions.forEach((question) => {
                    question.choices.forEach((choice) => {
                        choice.id = undefined;
                        choice.permission = undefined;

                    });
                });
            }

            voterListService.init(this).then(voterList => {
                this.mainVoterList = voterList;
                this.setCurrentVoterList(voterList);
            });

            this._updateSteps();
            this.step = 0;

            return Promise.resolve(this);
        });
    }

    init(choiceType, user) {
        this.showOptions = false;
        this.hasVotes = false;
        this.creation = true;

        return Promise.all([
            pollService.empty(this.choiceType),
            this._loadVoteCountingTypes()
        ]).then((results) => {
            this.model = results[0];

            if (user) {
                this.model.creatorName = user.name;
                this.model.creatorEmail = user.defaultEmailAddress.emailAddress;
            }
            this.model.participant = [];
            this.model.questions[0].choices = [new Choice("TEXT"), new Choice("TEXT"), new Choice("TEXT")];
            this.model.questions[0].choices.forEach((c, index) => {c.choiceOrder = index;});

            voterListService.init(this).then(voterList => {
                this.mainVoterList = voterList;
                this.setCurrentVoterList(voterList);
            });

            this.isInit = true;
            this._updateSteps();
            this.setStep(0);
        });
    }

    create() {
        logger.info("form before create");
        logger.info(this.form);

        // if the choice is of type resource, then upload the file and set its id as choice value
        let fileUploadPromises = [];

        this.model.questions.forEach((question) => {
            question.choices.forEach((choice) => {
                if (choice.choiceType === "RESOURCE" && choice.choiceValue.name) {
                    let promise = resourceService.create(choice.choiceValue, "CHOICE").then((result) => {
                        choice.choiceValue = result.id;
                        return result.id;
                    });
                    fileUploadPromises.push(promise);
                }
            });
        });

        return Promise.all(fileUploadPromises).then(() => {
            return pollService.create(this.model, voterListService.getVoterLists(), voterListService.getVoterListMembers()).then((result) => {
                logger.info("Poll created");
                logger.info(result);
                this.model.id = result.id;
                this.model.permission = result.permission;
                this.pageTracker.trackPollCreated();
                return Promise.resolve();
            },
            (error) => {
                logger.error("Could not create poll");
                logger.error(error);
                return Promise.reject(error);
            });
        });
    }

    save() {
        let fileUploadPromises = [];

        this.model.questions.forEach((question) => {
            question.choices.forEach((choice) => {
                // choice.choiceValue: if not a RESOURCE it is a String
                // if it's a resource and there is a name attribute it means that it's a file that has not
                // been uploaded yet! It's a new file
                if (choice.choiceType === "RESOURCE" && choice.choiceValue.name) {
                    let promise = resourceService.create(choice.choiceValue, "CHOICE").then((result) => {
                        // the file has been uploaded and server return the Id
                        // we replace choiceValue by the id
                        choice.choiceValue = result.id;
                        return result.id;
                    });
                    fileUploadPromises.push(promise);
                }
            });
        });

        return Promise.all(fileUploadPromises).then(() => {
            let promises = [pollService.save(this.model)];

            if (this.model.pollType !== "FREE") {
                promises.push(voterListService.save());
            }
            return Promise.all(promises)
                .then(() => this.loadPoll(this.model.id, this.model.permissions));
        });
    }

    previousStep() {
        this.setStep(this.step - 1);
    }

    nextStep() {
        this.setStep(this.step + 1);
    }

    addNewChoice(questionId) {
        this.model.questions.forEach((question) => {
            if (question.id === questionId || question.id === null) {
                let choice;
                if (question.choices.length > 0) {
                    let lastChoice = question.choices[question.choices.length - 1];
                    if (lastChoice.choiceType.startsWith("DATE")) {
                        choice = new Choice(lastChoice.choiceType, lastChoice.choiceValue);
                    } else {
                        choice = new Choice(lastChoice.choiceType);
                    }
                } else {
                    choice = new Choice(question.choiceType);
                }
                choice.choiceOrder = question.choices.length;
                question.choices.push(choice);
            }
        });
    }

    removeChoice(questionId, index) {
        this.model.questions.forEach((question) => {
            if (question.id === questionId || question.id === null) {
                question.choices.splice(index, 1);
            }
        });
    }

    setStep(step) {
        logger.info("setStep:: " + step);
        this.step = Math.min(this.steps.length, Math.max(0, step));
        if (this.step > this.maxStepReached) {
            this.pageTracker.trackCreatePoll(this.step);
            this.maxStepReached = this.step;
        }
        // step 2 = Option, user can choise how to expose the vote
        if (this.creation && this.step === 2) {
            voterListService.addMeIfEmpty();
        }
    }

    setSettingsDefault() {
        this.model.beginChoiceDate = undefined;
        this.model.endChoiceDate = undefined;

        this.model.beginDate = undefined;
        this.model.endDate = undefined;
        this.model.commentVisibility = "EVERYBODY";

        this.model.questions.forEach((question) => {
            this.setQuestionDefaultSettings(question);
        });
    }

    setQuestionDefaultSettings(question) {
        question.addChoices = false;
        question.voteCountingType = 1;
        question.voteCountingConfig = {};

        question.voteVisibility = "EVERYBODY";
        question.anonymousVote = false;
        question.resultVisibility = "EVERYBODY";
        question.continuousResults = true;
        question.commentVisibility = "EVERYBODY";
    }

    setPollType(type) {
        if (this.types.indexOf(type) >= 0 && this.model.pollType !== type) {
            this.model.pollType = type;
            this._updateSteps();
            bus.trigger("pollStepsChange", this.steps);
        }
    }

    _updateSteps() {
        let steps = ["general", "choices", "options"];
        if (this.model.pollType === "RESTRICTED") {
            steps.push("voters");
        }
        this.steps = steps;
    }

    setCurrentVoterList(list) {
        return voterListService.loadList(list).then(() => {
            this.currentVoterList = list;
            return list;
        });
    }

    close() {
        if (this.model.id) {
            return pollService.closePoll(this.model.id, this.model.permission).then((result) => {
                this.model.closed = true;
                this.model.canVote = false;
                this.model.endDate = result;
                return Promise.resolve(this);
            });
        }
        return Promise.reject("Init poll after close");
    }

    reopen() {
        if (this.model.id) {
            return pollService.reopenPoll(this.model.id, this.model.permission).then(() => {
                this.model.closed = false;
                this.model.canVote = true;
                this.model.endDate = null;
                return Promise.resolve(this);
            });
        }
        return Promise.reject("Init poll after reopen");
    }

    getInvalidEmails() {
        if (this.model.id) {
            return pollService.getInvalidEmails(this.model.id, this.model.permission);
        }
        return Promise.reject("Init poll after get invalid emails");
    }

    sendInvitations() {
        if (this.model.id) {
            if (this.model.pollType === "RESTRICTED" && this.mainVoterList) {
                return voterListService.sendInvitationList(this.mainVoterList)
                    .then(nbInvitation => {
                        return pollService.getPoll(this.model.id, this.model.permission)
                            .then(poll => {
                                Object.assign(this.model, poll);
                                return nbInvitation;
                            });
                    });
            }
            return Promise.reject("send invitation only for restricted poll");
        }
        return Promise.reject("Init poll after get invalid emails");
    }

}

export default singleton(PollForm);
