/*-
 * #%L
 * Pollen :: UI (Riot Js)
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import singleton from "./Singleton";
import FetchService from "./FetchService";

class AuthService extends FetchService {

    constructor() {
        super();
        this.providerIcons = {
            amazon: "amazon",
            facebook: "facebook-official",
            flickr: "flickr",
            foursquare: "foursquare",
            github: "github",
            googleplus: "google",
            instagram: "instagram",
            linkedin: "linkedin",
            linkedin2: "linkedin",
            stackexchange: "stack-exchange",
            twitter: "twitter",
            yahoo: "yahoo"
        };
    }

    signIn(login, password) {
        return this.fetch(
            "/v1/login",
            "POST",
            {Authorization: "Basic " + btoa(unescape(encodeURIComponent(login + ":" + password)))},
            null);
    }

    signUp(user) {
        let user1 = {
            name: user.name,
            defaultEmailAddress: {emailAddress: user.email},
            password: user.password,
            gtuValidated: user.gtuValidated
        };
        return this.post("/v1/users", user1);
    }

    signOut() {
        return this.doDelete("/v1/logout");
    }

    userPromise(auth) {
        return this.get("/v1/users/" + auth.id);
    }

    connectedUserPromise() {
        return this.get("/v1/user");
    }

    validateEmail(userId, token) {
        return this.put("/v1/users/" + userId + "?token=" + token);
    }

    validateEmailByAdmin(userId, emailAddressId) {
        return this.put("/v1/users/" + userId + "/email/" + emailAddressId);
    }

    newPassword(email) {
        return this.post("/v1/lostpassword", email);
    }

    resendValidation(email) {
        return this.post("/v1/resendValidation", email);
    }

    getLoginProviderUrl(providerId, redirection) {
        return this.get("/v1/login/" + providerId, {providerRedirection: redirection});
    }

    signInProvider(query) {
        return this.post(
            "/v1/login/" + query.loginProvider,
            JSON.stringify(query));
    }

    getAllLoginProviders() {
        return this.get("/v1/loginproviders");
    }

    getAvailableLoginProviders() {
        return this.get("/v1/loginproviders/available");
    }

    getActiveLoginProviders() {
        return this.get("/v1/loginproviders/active");
    }

    saveLoginProvider(loginProvider) {
        return this.post("/v1/loginproviders/" + (loginProvider.id || ""), loginProvider);
    }

    deleteLoginProvider(loginProvider) {
        return this.doDelete("/v1/loginproviders/" + loginProvider.id);
    }
}

export default singleton(AuthService);
