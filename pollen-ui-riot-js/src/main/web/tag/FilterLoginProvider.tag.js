/*-
 * #%L
 * Pollen :: UI RiotJs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import riot from "riot";
import route from "riot-route/lib/tag";
import session from "../js/Session";
import userService from "../js/UserService";

riot.tag("filterloginProvider", false, function() {

    let query = route.query();
    if (query.loginProvider != null) {
        if (query.action === "signin") {
            session.signInProvider(query).then(() => {
                let currentPage = localStorage.getItem("currentPage");
                localStorage.removeItem("currentPage");
                location.replace(session.pollenUIContext.uiEndPoint + "/" + currentPage);
            }, (e) => {
                let currentPage = localStorage.getItem("currentPage");
                localStorage.removeItem("currentPage");
                e.text().then(label => {
                    location.replace(session.pollenUIContext.uiEndPoint + "/" + currentPage + "?error=" + label);
                });
            });

        } else if (query.action === "link" && session.isConnected()) {
            session.userPromise.then(() => {
                userService.linkProvider(query).then(() => {
                    location.replace(session.pollenUIContext.uiEndPoint + "/#user/profile");
                }, (e) => {
                    e.text().then(label => {
                        location.replace(session.pollenUIContext.uiEndPoint + "/#user/profile?error=" + label);
                    });
                });
            });

        } else if (query.action === "avatar" && session.isConnected()) {
            session.userPromise.then(() => {
                userService.setProviderAvatar(query).then(() => {
                    location.replace(session.pollenUIContext.uiEndPoint + "/#user/profile");
                }, (e) => {
                    e.text().then(label => {
                        location.replace(session.pollenUIContext.uiEndPoint + "/#user/profile?error=" + label);
                    });
                });
            });
        }
    }
});
