/*-
 * #%L
 * Pollen :: UI RiotJs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import riot from "riot";
import route from "riot-route/lib/tag"; // a voir pourquoi le default doit être mi ;

riot.tag("signinaction", false, function() {

    let query = route.query();
    this.bus.trigger("signIn");
    this.listen("signInClosed", () => {
        this.logger.info("route to " + (query.hash || ""));
        route(query.hash || "");
    });
});
