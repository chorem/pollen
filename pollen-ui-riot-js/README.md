POLLEN
------

Pollen est une application Web de sondages en ligne qui permet de créer et de 
gérer des sondages avec différents types de choix (texte, date, image).
Les votants peuvent participer au sondage en suivant un lien qui identifie le
sondage.

