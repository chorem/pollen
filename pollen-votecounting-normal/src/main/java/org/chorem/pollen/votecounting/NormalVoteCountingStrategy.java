/*
 * #%L
 * Pollen :: VoteCounting :: Normal
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.MinMaxChoicesNumberConfig;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Vote counting strategy for {@link NormalVoteCounting}.
 * <p/>
 * This is a very basic vote counting:
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class NormalVoteCountingStrategy extends AbstractVoteCountingStrategy<MinMaxChoicesNumberConfig> {

    @Override
    public VoteCountingResult votecount(Set<Voter> voters) {

        // get empty result by choice
        Map<String, ChoiceScore> scores = newEmptyChoiceScoreMap(voters, ZERO_D);

        for (Voter voter : voters) {

            // add this voter votes to result
            addVoterChoices(voter, scores);
        }

        // order scores (using their value) and return result
        return orderByValues(scores.values(), null);
    }

    protected void addVoterChoices(Voter voter,
                                   Map<String, ChoiceScore> scores) {

        double voterWeight = voter.getWeight();

        for (VoteForChoice voteForChoice : voter.getVoteForChoices()) {
            Double voteValue = voteForChoice.getVoteValue();
            if (voteValue != null && voteValue != 0) {

                // get score for choice
                ChoiceScore score = scores.get(voteForChoice.getChoiceId());

                // add to score this weighted vote
                score.addScoreValue(voterWeight);
            }
        }
    }

    @Override
    public Set<VoteForChoice> toVoteForChoices(VoteCountingResult voteCountingResult) {
        Set<VoteForChoice> voteForChoices = Sets.newHashSet();

        Collection<ChoiceScore> winners = voteCountingResult.getTopRanking();
        for (ChoiceScore choiceScore : voteCountingResult.getScores()) {

            double score = winners.contains(choiceScore) ? 1d : 0d;
            VoteForChoice voteForChoice = VoteForChoice.newVote(
                    choiceScore.getChoiceId(),
                    score);
            voteForChoices.add(voteForChoice);
        }
        return voteForChoices;
    }

}
