/*
 * #%L
 * Pollen :: VoteCounting :: Normal
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.ListOfVoter;
import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.MinMaxChoicesNumberConfig;
import org.chorem.pollen.votecounting.model.SimpleVoter;
import org.chorem.pollen.votecounting.model.SimpleVoterBuilder;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the {@link NormalVoteCountingStrategy}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class NormalVoteCountingStrategyTest {

    public static final String CHOICE_A = "a";

    public static final String CHOICE_B = "b";

    public static final String CHOICE_C = "c";

    protected static NormalVoteCounting voteCounting;

    protected NormalVoteCountingStrategy strategy;

    @BeforeClass
    public static void beforeClass() throws Exception {
        VoteCountingFactory factory = new VoteCountingFactory();
        voteCounting = NormalVoteCounting.class.cast(factory.getVoteCounting(NormalVoteCounting.VOTECOUNTING_ID));
    }

    @Before
    public void setUp() throws Exception {
        strategy = voteCounting.newStrategy();
        strategy.setConfig(new MinMaxChoicesNumberConfig());
    }

    @Test
    public void simpleVotecount() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=1    b=null c=null)
        // 2      (a=null b=1    c=null)
        // 3      (a=null b=1    c=1)
        // Result (a=1    b=2    c=1)

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(1.0), 1),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(1.0), 1))
        .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void listVotecount() throws Exception {

        // Group poll (all weight to 1)

        // G1U1      a * 2
        // G1U2      a
        // G1U3      b
        // Result G1 a => 2    b => 1    c => 0
        // Vote G1   a

        // G2U1      a * 2
        // G2U2      c
        // G2U3      c
        // Result G2 a => 2    b => 0    c => 2
        // Vote G1   a, c * 2

        // Result    a => 3    b => 1    c => 2

        ListOfVoter voters = ListOfVoter.newVoter(null, 1, Sets.newHashSet(
                ListOfVoter.newVoter("G1", 1, Sets.newHashSet(
                        SimpleVoter.newVoter("G1U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 0d),
                                VoteForChoice.newVote(CHOICE_C, 0d))),
                        SimpleVoter.newVoter("G1U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 0d),
                                VoteForChoice.newVote(CHOICE_C, 0d))),
                        SimpleVoter.newVoter("G1U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 0d),
                                VoteForChoice.newVote(CHOICE_B, 1d),
                                VoteForChoice.newVote(CHOICE_C, 0d))))),
                ListOfVoter.newVoter("G2", 2, Sets.newHashSet(
                        SimpleVoter.newVoter("G2U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 0d),
                                VoteForChoice.newVote(CHOICE_C, 0d))),
                        SimpleVoter.newVoter("G2U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 0d),
                                VoteForChoice.newVote(CHOICE_B, 0d),
                                VoteForChoice.newVote(CHOICE_C, 1d))),
                        SimpleVoter.newVoter("G2U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 0d),
                                VoteForChoice.newVote(CHOICE_B, 0d),
                                VoteForChoice.newVote(CHOICE_C, 1d)))))
        ));

        ListVoteCountingResult result = strategy.votecount(voters);

        assertThat(result)
                .isNotNull();
        assertThat(result.getMainResult()).isNotNull();
        assertThat(result.getMainResult().getScores()).isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(3.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(0.0), 2),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(2.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount2() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=1    b=null c=null)
        // 2      (a=1    b=1    c=null)
        // 3      (a=1    b=1    c=null)
        // Result (a=3    b=2    c=null)
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(3.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(2.0), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(0.0), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);

    }

    @Test
    public void simpleVotecount3() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=1    b=1    c=null)
        // 2      (a=1    b=1    c=null)
        // 3      (a=1    b=1    c=null)
        // Result (a=3    b=3    c=null)
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(3.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(3.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(0.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount1() throws Exception {

        // poll with weighted vote
        // 1 (x2) (a=1    b=null c=null)
        // 2 (x1) (a=null b=1    c=null)
        // 3 (x1) (a=null b=1    c=1)
        // Result (a=2    b=2    c=1)
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(1.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount2() throws Exception {

        // poll with weighted vote
        // 1 (x2) (a=1    b=null c=null)
        // 2 (x1) (a=null b=1    c=null)
        // 3 (x3) (a=null b=1    c=1)
        // Result (a=2    b=4    c=3)
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 3.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 2),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(4.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(3.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

}
