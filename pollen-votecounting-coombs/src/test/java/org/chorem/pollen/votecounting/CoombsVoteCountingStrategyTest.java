/*
 * #%L
 * Pollen :: VoteCounting :: Coombs
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.EmptyVoteCountingConfig;
import org.chorem.pollen.votecounting.model.SimpleVoterBuilder;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.Voter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the {@link CoombsVoteCountingStrategy}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class CoombsVoteCountingStrategyTest {

    public static final String CHOICE_A = "Ville A";

    public static final String CHOICE_B = "Ville B";

    public static final String CHOICE_C = "Ville C";

    public static final String CHOICE_D = "Ville D";

    protected static CoombsVoteCounting voteCounting;

    protected CoombsVoteCountingStrategy strategy;

    @BeforeClass
    public static void beforeClass() throws Exception {
        VoteCountingFactory factory = new VoteCountingFactory();
        voteCounting = CoombsVoteCounting.class.cast(factory.getVoteCounting(CoombsVoteCounting.VOTECOUNTING_ID));
    }

    @Before
    public void setUp() throws Exception {
        strategy = voteCounting.newStrategy();
        strategy.setConfig(new EmptyVoteCountingConfig());
    }

    @Test
    public void simpleVotecount() throws Exception {

        // see http://fr.wikipedia.org/wiki/M%C3%A9thode_de_Coombs

        // Ville	1re	2e	3e	4e	R1 R2
        // A	    42	0	0	58	42 -
        // B	    26	42	32	0	26 26+42
        // C	    15	43	42	0	15 15
        // D	    17	15	26	42	17 17

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("Ville A", 42.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 3.)
                .addVoteForChoice(CHOICE_D, 4.)
                .newVoter("Ville B", 26.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .addVoteForChoice(CHOICE_D, 3.)
                .addVoteForChoice(CHOICE_A, 4.)
                .newVoter("Ville C", 15.)
                .addVoteForChoice(CHOICE_C, 1.)
                .addVoteForChoice(CHOICE_D, 2.)
                .addVoteForChoice(CHOICE_B, 3.)
                .addVoteForChoice(CHOICE_A, 4.)
                .newVoter("Ville D", 17.)
                .addVoteForChoice(CHOICE_D, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .addVoteForChoice(CHOICE_B, 3.)
                .addVoteForChoice(CHOICE_A, 4.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(-4), 3),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(68.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(15.0), 2),
                        ChoiceScore.newScore(CHOICE_D, BigDecimal.valueOf(17.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount0() throws Exception {

        // Simple poll (all weight to 1)    R1
        // 1      (a=1 b=2    c=null)    a  2
        // 2      (a=3 b=2    c=1)       b  1
        // 3      (a=1 b=null c=2)       c  -

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 3.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(0.0), 2),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(1.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount2() throws Exception {

        // Simple poll (all weight to 1)     R1
        // 1      (a=1    b=2    c=null)  a  2
        // 2      (a=1    b=2    c=1)     b  -
        // 3      (a=null b=null c=2)     c  2

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(0.0), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(2.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount3() throws Exception {

        // Simple poll (all weight to 1)    R1
        // 1      (a=1    b=null c=null)  a 1
        // 2      (a=null b=1    c=null)  b 1
        // 3      (a=null b=null c=1)     c 1

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(1.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(1.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(1.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount1() throws Exception {

        // poll with weighted vote         R1
        // 1 (x2) (a=1    b=null c=null) a 2
        // 2 (x1) (a=null b=1    c=null) b 2
        // 3 (x1) (a=null b=1    c=2)    c -

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(2.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(-3), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount2() throws Exception {

        // poll with weighted vote         R1
        //          A   B   C
        // 1 (x2)   1
        // 2 (x1)       1
        // 3 (x3)       2   1
        // 1 tour----------------
        // 1er      2   1   3
        // Der      4   2   3
        // elimination de A
        // 2 tour----------------
        // 1er      X   3   5
        // gagnant C
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 3.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(-3), 2),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(3.0), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(5.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    public static void assertChoiceScore(ChoiceScore choiceScore,
                                         String choiceId,
                                         BigDecimal choiceResult) {
        Assert.assertNotNull(choiceScore);
        Assert.assertEquals(choiceId, choiceScore.getChoiceId());
        Assert.assertEquals(choiceResult, choiceScore.getScoreValue());
    }

    @Test
    public void majorityOnFirstRoundTest() throws Exception {

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 2.)
                .addVoteForChoice(CHOICE_B, 4.)
                .addVoteForChoice(CHOICE_C, 3.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .contains(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(1.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);

        // We have only one vote, so we have an absolute majority on first round, should not have multiple rounds
        CoombsDetailResult detailResult = (CoombsDetailResult) result.getDetailResult();
        Assert.assertEquals(1, detailResult.getRounds().size());
    }

}
