package org.chorem.pollen.votecounting;

/*-
 * #%L
 * Pollen :: VoteCounting :: Coombs
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CoombsRoundChoice implements Serializable {

    private static final long serialVersionUID = -4609190485828274256L;

    protected String choiceId;

    protected BigDecimal firstScore;

    protected BigDecimal lastScore;

    public String getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(String choiceId) {
        this.choiceId = choiceId;
    }

    public BigDecimal getFirstScore() {
        return firstScore;
    }

    public void setFirstScore(BigDecimal firstScore) {
        this.firstScore = firstScore;
    }

    public BigDecimal getLastScore() {
        return lastScore;
    }

    public void setLastScore(BigDecimal lastScore) {
        this.lastScore = lastScore;
    }

    public void addFirstScore(double scoreToAdd) {
        BigDecimal newScoreValue;
        if (firstScore == null) {

            newScoreValue = BigDecimal.valueOf(scoreToAdd);
        } else {
            newScoreValue = firstScore.add(BigDecimal.valueOf(scoreToAdd));
        }
        setFirstScore(newScoreValue);
    }

    public void addLastScore(double scoreToAdd) {
        BigDecimal newScoreValue;
        if (lastScore == null) {

            newScoreValue = BigDecimal.valueOf(scoreToAdd);
        } else {
            newScoreValue = lastScore.add(BigDecimal.valueOf(scoreToAdd));
        }
        setLastScore(newScoreValue);
    }
}
