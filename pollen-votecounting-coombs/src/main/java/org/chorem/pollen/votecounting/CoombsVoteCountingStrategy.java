/*
 * #%L
 * Pollen :: VoteCounting :: Coombs
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.EmptyVoteCountingConfig;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Coombs vote counting strategy.
 *
 * use negative score value for indicate elimination round
 * from -1 * nb choices
 *
 * exemple : for 5 choices, score value for first elimination is -5,  for next -4, ...
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class CoombsVoteCountingStrategy extends AbstractVoteCountingStrategy<EmptyVoteCountingConfig> {

    @Override
    public VoteCountingResult votecount(Set<Voter> voters) {

        // get empty result by choice
        Map<String, ChoiceScore> scores = newEmptyChoiceScoreMap(voters);
        CoombsDetailResult detailResult = new CoombsDetailResult();

        // calcul du score minimum pour atteindre la majorité absolue
        double totalWeight = voters.stream().mapToDouble(Voter::getWeight).sum() / 2;

        // calcul pour chaque votant de ses choix préférés
        Map<Voter, List<Set<String>>> topRankChoices = buildVoterSortedChoices(voters);

        Set<String> choiceIdsToKeep = Sets.newHashSet(scores.keySet());

        round(topRankChoices,
                choiceIdsToKeep,
                scores,
                totalWeight,
                detailResult,
                - scores.keySet().size());

        // order scores (using their value) and return result
        return orderByValues(scores.values(), detailResult);
    }

    @Override
    public Set<VoteForChoice> toVoteForChoices(VoteCountingResult voteCountingResult) {
        Set<VoteForChoice> voteForChoices = Sets.newHashSet();

        for (ChoiceScore choiceScore : voteCountingResult.getScores()) {

            double score = choiceScore.getScoreOrder();
            VoteForChoice voteForChoice = VoteForChoice.newVote(
                    choiceScore.getChoiceId(),
                    score);
            voteForChoices.add(voteForChoice);
        }
        return voteForChoices;
    }

    protected void round(Map<Voter, List<Set<String>>> topRankChoices,
                         Set<String> idsEnabled,
                         Map<String, ChoiceScore> resultByChoice,
                         double totalWeight,
                         CoombsDetailResult detailResult,
                         int roundIndex) {

        CoombsRound round = new CoombsRound();

        detailResult.getRounds().add(round);

        List<ChoiceScore> results = applyScores(
                topRankChoices,
                idsEnabled,
                resultByChoice,
                round);

        if (!results.isEmpty()) {

            // get best score
            BigDecimal scoreValue = results.get(results.size() - 1).getScoreValue();
            double max = scoreValue == null ? 0 : scoreValue.doubleValue();

            if (max < totalWeight || max == totalWeight && idsEnabled.size() > 2) {

                // pas de majorité absolue, il faut éliminer le(s) choix les plus mauvais
                Set<String> idsToExclude = guessChoiceIdsToRemove(idsEnabled, topRankChoices, round);

                // on ne veux plus utiliser ces choix dans le tour suivant
                idsEnabled.removeAll(idsToExclude);

                topRankChoices.values()
                        .stream()
                        .flatMap(Collection::stream)
                        .forEach(choiceIds -> choiceIds.removeAll(idsToExclude));

                topRankChoices.values().forEach(list ->
                        list.removeIf(Collection::isEmpty)
                );


                if (CollectionUtils.isNotEmpty(idsEnabled)) {

                    idsToExclude.stream()
                            .map(resultByChoice::get)
                            .forEach(score -> score.setScoreValue(BigDecimal.valueOf(roundIndex)));


                    // nouveau tour
                    round(topRankChoices,
                            idsEnabled,
                            resultByChoice,
                            totalWeight,
                            detailResult,
                            roundIndex + 1);
                }

//          } else {
                // majorité absolue trouvée plus rien à faire en fait :)
                // ou 2 choix égaux
            }
        }
    }

    protected List<ChoiceScore> applyScores(Map<Voter, List<Set<String>>> topRankChoices,
                                            Set<String> idsEnabled,
                                            Map<String, ChoiceScore> resultByChoice,
                                            CoombsRound round) {

        // on remet à zero les scores pour les ids a conserver
        for (String id : idsEnabled) {
            resultByChoice.get(id).setScoreValue(ZERO_D);
            addRoundChoiceFirst(round, id, 0);
        }

        // on calcule les scores à partir des classements

        for (Map.Entry<Voter, List<Set<String>>> entry : topRankChoices.entrySet()) {
            List<Set<String>> idsByLevel = entry.getValue();
            if (!idsByLevel.isEmpty()) {

                Set<String> winnerIds = idsByLevel.get(0);
                Voter voter = entry.getKey();
                double voterWeight = voter.getWeight();
                for (String id : winnerIds) {
                    ChoiceScore choiceScore = resultByChoice.get(id);
                    choiceScore.addScoreValue(voterWeight);
                    addRoundChoiceFirst(round, id, voterWeight);
                }
            }
        }

        // recopy choices to run rounds on it and eliminates choices until
        // there is a choice > 50
        List<ChoiceScore> results = idsEnabled.stream()
                .map(resultByChoice::get)
                .sorted(Comparator.comparing(ChoiceScore::getScoreValue, Comparator.nullsFirst(Comparator.naturalOrder())))
                .collect(Collectors.toList());

        return results;
    }

    protected Set<String> guessChoiceIdsToRemove(Set<String> idsToInclude,
                                          Map<Voter, List<Set<String>>> results,
                                          CoombsRound round) {

        // pour chaque choix restant on calcule son score lorsqu'il est le dernier

        Map<String, ChoiceScore> badScores = Maps.newTreeMap();
        for (String id : idsToInclude) {
            badScores.put(id, ChoiceScore.newScore(id, BigDecimal.ZERO));
        }

        for (Map.Entry<Voter, List<Set<String>>> entry : results.entrySet()) {
            Voter voter = entry.getKey();
            List<Set<String>> idsChoices = entry.getValue();
            if (!idsChoices.isEmpty()) {

                Set<String> lastIds = idsChoices.get(idsChoices.size() - 1);

                double voterWeight = voter.getWeight();

                for (String lastId : lastIds) {
                    badScores.get(lastId).addScoreValue(voterWeight);
                    addRoundChoiceLast(round, lastId, voterWeight);
                }
            }
        }

        // on construit alors la liste des scores
        BigDecimal maxScore = badScores.values()
                .stream()
                .map(ChoiceScore::getScoreValue)
                .max(BigDecimal::compareTo)
                .orElse(ZERO_D);

        Set<String> idsToExclude = badScores.values()
                .stream()
                .filter(score -> maxScore.equals(score.getScoreValue()))
                .map(ChoiceScore::getChoiceId)
                .collect(Collectors.toSet());


        round.getChoiceIdsExclude().addAll(idsToExclude);
        return idsToExclude;
    }

    protected void addRoundChoiceFirst(CoombsRound round, String choiceId, double score) {
        CoombsRoundChoice roundChoice = getRoundChoice(round, choiceId);
        roundChoice.addFirstScore(score);
    }

    protected void addRoundChoiceLast(CoombsRound round, String choiceId, double score) {
        CoombsRoundChoice roundChoice = getRoundChoice(round, choiceId);
        roundChoice.addLastScore(score);
    }

    protected CoombsRoundChoice getRoundChoice(CoombsRound round, String choiceId) {
        Optional<CoombsRoundChoice> roundChoiceOptional = round.getRoundChoices().stream()
                .filter(rc -> rc.getChoiceId().equals(choiceId))
                .findFirst();

        CoombsRoundChoice result;
        if (roundChoiceOptional.isPresent()) {
            result = roundChoiceOptional.get();
        } else {
            result = new CoombsRoundChoice();
            result.setChoiceId(choiceId);
            round.getRoundChoices().add(result);
        }
        return result;
    }
}
