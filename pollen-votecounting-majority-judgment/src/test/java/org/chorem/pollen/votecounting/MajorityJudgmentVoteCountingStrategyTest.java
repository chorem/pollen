/*
 * #%L
 * Pollen :: VoteCounting :: Instant Runoff
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.SimpleVoterBuilder;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.Voter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MajorityJudgmentVoteCountingStrategyTest {

    public static final String CHOICE_A = "A";

    public static final String CHOICE_B = "B";

    public static final String CHOICE_C = "C";

    public static final String CHOICE_D = "D";

    public static final double POOR = 0;

    public static final double FAIR = 1;

    public static final double GOOD = 2;

    public static final BigDecimal POOR_BD = BigDecimal.valueOf(0);

    public static final BigDecimal FAIR_BD = BigDecimal.valueOf(1);

    public static final BigDecimal GOOD_BD = BigDecimal.valueOf(2);

    public static final ImmutableList<String> GRADES_3 = ImmutableList.of(
            "Poor", "Fair", "Good");

    public static final ImmutableList<Character> GRADES_5 = ImmutableList.of(
            '0', '1', '2', '3', '4');

    protected static MajorityJudgmentVoteCounting voteCounting;


    protected MajorityJudgmentVoteCountingStrategy strategy;

    @BeforeClass
    public static void beforeClass() throws Exception {
        VoteCountingFactory factory = new VoteCountingFactory();
        voteCounting = MajorityJudgmentVoteCounting.class.cast(factory.getVoteCounting(MajorityJudgmentVoteCounting.VOTECOUNTING_ID));
    }

    @Before
    public void setUp() throws Exception {
        strategy = voteCounting.newStrategy();
        MajorityJudgmentConfig config = new MajorityJudgmentConfig();
        config.setGrades(GRADES_3);
        strategy.setConfig(config);
    }

    @Test
    public void simpleVotecount1() throws Exception {

        // Ville	A   B   C   D
        //-----------------------
        // voter 1  G   F   G   P
        // voter 2  G   P   P   G
        // voter 3  P   F   P   F
        // voter 4  F   P   F   G
        // voter 5  G   P   G   F
        //------------------------
        // Poor     1   3   2   1
        // Fair     1   2   1   2
        // Good     3   0   2   2
        //------------------------
        // median   G   P   F   F
        // order    0   3   2   1


        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("Voter 1", 1.)
                .addVoteForChoice(CHOICE_A, GOOD)
                .addVoteForChoice(CHOICE_B, FAIR)
                .addVoteForChoice(CHOICE_C, GOOD)
                .addVoteForChoice(CHOICE_D, POOR)
                .newVoter("Voter 2", 1.)
                .addVoteForChoice(CHOICE_A, GOOD)
                .addVoteForChoice(CHOICE_B, POOR)
                .addVoteForChoice(CHOICE_C, POOR)
                .addVoteForChoice(CHOICE_D, GOOD)
                .newVoter("Voter 3", 1.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, FAIR)
                .addVoteForChoice(CHOICE_C, POOR)
                .addVoteForChoice(CHOICE_D, FAIR)
                .newVoter("Voter 4", 1.)
                .addVoteForChoice(CHOICE_A, FAIR)
                .addVoteForChoice(CHOICE_B, POOR)
                .addVoteForChoice(CHOICE_C, FAIR)
                .addVoteForChoice(CHOICE_D, GOOD)
                .newVoter("Voter 5", 1.)
                .addVoteForChoice(CHOICE_A, GOOD)
                .addVoteForChoice(CHOICE_B, POOR)
                .addVoteForChoice(CHOICE_C, GOOD)
                .addVoteForChoice(CHOICE_D, FAIR)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, GOOD_BD, 0),
                        ChoiceScore.newScore(CHOICE_B, POOR_BD, 3),
                        ChoiceScore.newScore(CHOICE_C, FAIR_BD, 2),
                        ChoiceScore.newScore(CHOICE_D, FAIR_BD, 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount2() throws Exception {

        // Ville	A   B   C   D
        // voter 1  P   F   G   F   x 2
        // voter 2  P   F   P   P   x 3
        // voter 3  P   G   F   P
        // voter 4  F   P   F   G
        // voter 5  P   P   F   P
        //------------------------
        // Poor     7   2   3   5
        // Fair     1   5   3   2
        // Good     0   1   2   1
        //------------------------
        // median   P   F   F   P
        // order    3   0   1   2

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("Voter 1", 2.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, FAIR)
                .addVoteForChoice(CHOICE_C, GOOD)
                .addVoteForChoice(CHOICE_D, FAIR)
                .newVoter("Voter 2", 3.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, FAIR)
                .addVoteForChoice(CHOICE_C, POOR)
                .addVoteForChoice(CHOICE_D, POOR)
                .newVoter("Voter 3", 1.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, GOOD)
                .addVoteForChoice(CHOICE_C, FAIR)
                .addVoteForChoice(CHOICE_D, POOR)
                .newVoter("Voter 4", 1.)
                .addVoteForChoice(CHOICE_A, FAIR)
                .addVoteForChoice(CHOICE_B, POOR)
                .addVoteForChoice(CHOICE_C, FAIR)
                .addVoteForChoice(CHOICE_D, GOOD)
                .newVoter("Voter 5", 1.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, POOR)
                .addVoteForChoice(CHOICE_C, FAIR)
                .addVoteForChoice(CHOICE_D, POOR)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
//        assertThat(result.getScores())
//                .isNotNull()
//                .containsExactlyInAnyOrder(
//                        ChoiceScore.newScore(CHOICE_A, POOR_BD, 3),
//                        ChoiceScore.newScore(CHOICE_B, FAIR_BD, 0),
//                        ChoiceScore.newScore(CHOICE_C, FAIR_BD, 1),
//                        ChoiceScore.newScore(CHOICE_D, POOR_BD, 2))
//                .isSortedAccordingTo(ChoiceScore::compareTo);

        MajorityJudgmentDetailResult detailResult = MajorityJudgmentDetailResult.class.cast(result.getDetailResult());

        MajorityJudgmentChoiceResult resultA = detailResult.getChoiceResults().stream()
                .filter(c -> CHOICE_A.equals(c.getChoiceId()))
                .findFirst()
                .orElseThrow(() -> new Exception("Not found"));

        MajorityJudgmentChoiceResult resultD = detailResult.getChoiceResults().stream()
                .filter(c -> CHOICE_D.equals(c.getChoiceId()))
                .findFirst()
                .orElseThrow(() -> new Exception("Not found"));

        int compareDtoA = MajorityJudgmentVoteCountingStrategy.CHOICE_RESULT_COMPARATOR.compare(resultD, resultA);
        Assert.assertEquals(1, compareDtoA);
    }

    @Test
    public void simpleVotecount3() throws Exception {

        // Ville	A   B   C   D
        // voter 1  P   F   G
        // voter 2  P   F   P
        // voter 3  P   G   F
        // voter 4  F   P   F
        // voter 5  P   P   F   P
        //------------------------
        // Poor     7   2   3   1
        // Fair     1   5   3   0
        // Good     0   1   2   0
        //------------------------
        // median   P   F   F   P
        // order    3   0   1   1

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("Voter 1", 1.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, FAIR)
                .addVoteForChoice(CHOICE_C, GOOD)
                .newVoter("Voter 2", 1.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, FAIR)
                .addVoteForChoice(CHOICE_C, POOR)
                .newVoter("Voter 3", 1.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, GOOD)
                .addVoteForChoice(CHOICE_C, FAIR)
                .newVoter("Voter 4", 1.)
                .addVoteForChoice(CHOICE_A, FAIR)
                .addVoteForChoice(CHOICE_B, POOR)
                .addVoteForChoice(CHOICE_C, FAIR)
                .newVoter("Voter 5", 1.)
                .addVoteForChoice(CHOICE_A, POOR)
                .addVoteForChoice(CHOICE_B, POOR)
                .addVoteForChoice(CHOICE_C, FAIR)
                .addVoteForChoice(CHOICE_D, POOR)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();

        MajorityJudgmentDetailResult detailResult = MajorityJudgmentDetailResult.class.cast(result.getDetailResult());

        MajorityJudgmentChoiceResult resultA = detailResult.getChoiceResults().stream()
                .filter(c -> CHOICE_A.equals(c.getChoiceId()))
                .findFirst()
                .orElseThrow(() -> new Exception("Not found"));

        MajorityJudgmentChoiceResult resultD = detailResult.getChoiceResults().stream()
                .filter(c -> CHOICE_D.equals(c.getChoiceId()))
                .findFirst()
                .orElseThrow(() -> new Exception("Not found"));

        int compareDtoA = MajorityJudgmentVoteCountingStrategy.CHOICE_RESULT_COMPARATOR.compare(resultD, resultA);
        Assert.assertEquals(-1, compareDtoA);
    }


    @Test
    public void testCompareEqual() {
        // egalité
        testEqualChoice(
                "00000111111122333344",
                "00000111111122333344");
    }

    @Test
    public void testCompareDiferentMedian() {
        testGreaterChoice(
                "00112233333333444444",
                "00000111111122333344");
    }

    @Test
    public void testCompareSameMedianWithoutSameBounds() {

        // médians identique sans egalité
        testGreaterChoice(
                "00011122222233333344",
                "00011112222222333344");

        testGreaterChoice(
                "00111111222222333344",
                "00011111122223333444");

        testGreaterChoice(
                "00111111222222333344",
                "00011111122223333444");
    }

    @Test
    public void testCompareSameMedianWithSameBounds2() {

        // test médians identique avec egalité
        testGreaterChoice(
                "00111111222223333344",
                "00011111222222333444");

        testGreaterChoice(
                "00011112222333334444",
                "00001111122233334444");

        testGreaterChoice(
                "00111122222233333444",
                "00011112222233334444");
    }

    @Test
    public void testCompareSameMedianWithSameBounds3() {

        // test médians identique avec egalité
        testGreaterChoice(
                "00111112222233334444",
                "00111111222233334444");

        testGreaterChoice(
                "00001111222233334444",
                "00001111222223334444");
    }

    @Test
    public void testCompareSameMedianWithSameBounds4() {

        // test médians identique avec egalité
        testGreaterChoice(
                "00111112222233344444",
                "00111112222233334444");
        
    }

    protected void testGreaterChoice(String votesA, String votesB) {
        testCompareChoice(votesA, votesB, 1);
        testCompareChoice(votesB, votesA, -1);
    }

    protected void testEqualChoice(String votesA, String votesB) {
        testCompareChoice(votesA, votesB, 0);
    }

    protected void testCompareChoice(String votesForA, String votesForB, int expected) {
        Preconditions.checkArgument(votesForA.length() == votesForB.length());
        MajorityJudgmentChoiceResult a = stringToMajorityJudgmentChoiceResult(CHOICE_A, votesForA);

        MajorityJudgmentChoiceResult b = stringToMajorityJudgmentChoiceResult(CHOICE_B, votesForB);

        strategy.computeMedian(a);
        strategy.computeMedian(b);

        int compareAtoB = MajorityJudgmentVoteCountingStrategy.CHOICE_RESULT_COMPARATOR.compare(a, b);

        String message = "Votes for A is not " + (expected == 0 ? "equal" : (expected < 0 ? "lower" : "greater")) + " to votes for B " +
                "\n    Votes for A : " + votesForA +
                "\n    Votes for B : " + votesForB;

        Assert.assertEquals(message, expected, compareAtoB);

    }

    protected MajorityJudgmentChoiceResult stringToMajorityJudgmentChoiceResult(String choiceId, String votes) {

        BigDecimal half = BigDecimal.valueOf(votes.length() / 2.);
        BigDecimal sum = BigDecimal.valueOf(votes.length());

        MajorityJudgmentChoiceResult choiceResult = new MajorityJudgmentChoiceResult();
        choiceResult.setChoiceId(choiceId);
        List<BigDecimal> voteByGrad = GRADES_5.stream()
                .map(grad -> CharMatcher.is(grad).countIn(votes))
                .map(BigDecimal::valueOf)
                .collect(Collectors.toList());
        choiceResult.setVoteByGrad(voteByGrad);
        choiceResult.setHalfWeight(half);
        choiceResult.setSumWeight(sum);
        return choiceResult;
    }

}
