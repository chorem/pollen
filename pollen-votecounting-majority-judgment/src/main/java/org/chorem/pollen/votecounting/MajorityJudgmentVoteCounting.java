package org.chorem.pollen.votecounting;

/*
 * #%L
 * Pollen :: VoteCounting :: Instant Runoff
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import org.chorem.pollen.votecounting.model.ChoiceToVoteRenderType;
import org.chorem.pollen.votecounting.model.VoteForChoice;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.n;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MajorityJudgmentVoteCounting extends AbstractVoteCounting<MajorityJudgmentVoteCountingStrategy, MajorityJudgmentConfig> {

    public static final int VOTECOUNTING_ID = 8;

    public MajorityJudgmentVoteCounting() {
        super(VOTECOUNTING_ID,
                MajorityJudgmentVoteCountingStrategy.class,
                MajorityJudgmentConfig.class,
                n("pollen.voteCountingType.majorityJudgment"),
                n("pollen.voteCountingType.majorityJudgment.shortHelp"),
                n("pollen.voteCountingType.majorityJudgment.help")
        );
    }

    @Override
    public ChoiceToVoteRenderType getVoteValueEditorType() {
        return ChoiceToVoteRenderType.SELECT;
    }

    @Override
    public Double getMinimumValue() {
        return 0d;
    }

    @Override
    public Multimap<String, String> checkVoteForChoice(VoteForChoice voteForChoice, MajorityJudgmentConfig config, Locale locale) {
        Multimap<String, String> errorMap = super.checkVoteForChoice(voteForChoice, config, locale);

        Double voteValue = voteForChoice.getVoteValue();
        if (voteValue == null) {
            errorMap.put(
                    VoteForChoice.PROPERTY_VOTE_VALUE,
                    l(locale, "pollen.voteCountingType.majorityJudgment.voteValue.error.gradeRequired"));
        } else if ((voteValue != Math.floor(voteValue)) || voteValue < 0 || voteValue >= config.getGrades().size()) {
            errorMap.put(
                    VoteForChoice.PROPERTY_VOTE_VALUE,
                    l(locale, "pollen.voteCountingType.majorityJudgment.voteValue.error.gradeUnknown", voteValue));
        }

        return errorMap;

    }
}
