/*
 * #%L
 * Pollen :: VoteCounting :: Instant Runoff
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MajorityJudgmentVoteCountingStrategy extends AbstractVoteCountingStrategy<MajorityJudgmentConfig> {

    @Override
    public VoteCountingResult votecount(Set<Voter> voters) {

        MajorityJudgmentDetailResult detailResult = new MajorityJudgmentDetailResult();

        List<MajorityJudgmentChoiceResult> choiceResults = getAllChoiceIds(voters)
                .stream()
                .map(this::newChoiceResult)
                .collect(Collectors.toList());

        detailResult.setChoiceResults(choiceResults);
        detailResult.setSumWeight(BigDecimal.ZERO);

        voters.forEach(voter -> addVoter(detailResult, voter));

        detailResult.setHalfWeight(detailResult.getSumWeight().divide(BigDecimal.valueOf(2), MathContext.DECIMAL32));

        choiceResults.forEach(this::computeMedian);

        choiceResults.sort(CHOICE_RESULT_COMPARATOR.reversed());

        List<ChoiceScore> scores = computeScore(choiceResults);

        return VoteCountingResult.newResult(scores, detailResult);
    }

    @Override
    public Set<VoteForChoice> toVoteForChoices(VoteCountingResult voteCountingResult) {
        Set<VoteForChoice> voteForChoices = Sets.newHashSet();

        for (ChoiceScore choiceScore : voteCountingResult.getScores()) {

            double score = choiceScore.getScoreOrder();
            VoteForChoice voteForChoice = VoteForChoice.newVote(
                    choiceScore.getChoiceId(),
                    score);
            voteForChoices.add(voteForChoice);
        }
        return voteForChoices;
    }

    protected MajorityJudgmentChoiceResult newChoiceResult(String choiceId) {
        MajorityJudgmentChoiceResult choiceResult = new MajorityJudgmentChoiceResult();
        choiceResult.setChoiceId(choiceId);
        List<BigDecimal> voteByGrad = config.getGrades().stream()
                .map(grad -> BigDecimal.ZERO)
                .collect(Collectors.toList());
        choiceResult.setVoteByGrad(voteByGrad);
        return choiceResult;
    }

    protected void addVoter(MajorityJudgmentDetailResult detailResult, Voter voter) {
        BigDecimal weight = BigDecimal.valueOf(voter.getWeight());
        detailResult.setSumWeight(detailResult.getSumWeight().add(weight));
        voter.getVoteForChoices()
                .forEach(voteForChoice -> addVoteForChoice(detailResult, weight, voteForChoice));

    }

    protected void addVoteForChoice(MajorityJudgmentDetailResult detailResult, BigDecimal weight, VoteForChoice voteForChoice) {
        detailResult.getChoiceResults()
                .stream()
                .filter(choiceResult -> voteForChoice.getChoiceId().equals(choiceResult.getChoiceId()))
                .findFirst()
                .ifPresent(choiceResult -> addGrad(choiceResult, voteForChoice.getVoteValue(), weight));
    }

    protected void addGrad(MajorityJudgmentChoiceResult choiceResult, Double voteValue, BigDecimal weight) {
        int grade = voteValue == null ? 0 : voteValue.intValue();
        BigDecimal sum = choiceResult.getSumWeight();
        if (sum == null) {
            sum = new BigDecimal(0);
        }
        choiceResult.setSumWeight(sum.add(weight));
        choiceResult.setHalfWeight(choiceResult.getSumWeight().divide(BigDecimal.valueOf(2), MathContext.DECIMAL32));
        List<BigDecimal> voteByGrad = choiceResult.getVoteByGrad();
        BigDecimal vote = voteByGrad.get(grade).add(weight);
        voteByGrad.set(grade, vote);
    }

    protected void computeMedian(MajorityJudgmentChoiceResult choiceResult) {

        BigDecimal temp = BigDecimal.ZERO;

        BigDecimal demiWeight = choiceResult.getHalfWeight();

        Integer median = null;

        int grad = 0;
        for (BigDecimal voteByGrad : choiceResult.getVoteByGrad()) {

            temp = temp.add(voteByGrad);

            if (temp.compareTo(demiWeight) >= 0) {

                median = grad;
                break;

            }

            grad++;
        }

        Preconditions.checkNotNull(median);
        choiceResult.setMedian(median);

    }

    protected List<ChoiceScore> computeScore(List<MajorityJudgmentChoiceResult> choiceResults) {

        List<ChoiceScore> scores = Lists.newArrayListWithCapacity(choiceResults.size());

        int order = 0;

        for (MajorityJudgmentChoiceResult choiceResult : choiceResults) {

            ChoiceScore choiceScore = new ChoiceScore();

            choiceScore.setChoiceId(choiceResult.choiceId);
            choiceScore.setScoreValue(BigDecimal.valueOf(choiceResult.getMedian()));
            choiceScore.setScoreOrder(order);

            scores.add(choiceScore);
            order++;
        }

        return scores;
    }

    protected static Comparator<MajorityJudgmentChoiceResult> CHOICE_RESULT_COMPARATOR =
            Comparator.comparingInt(MajorityJudgmentChoiceResult::getMedian)
                    .thenComparing(MajorityJudgmentVoteCountingStrategy::compareChoiceResultSameMedian);

    protected static int compareChoiceResultSameMedian(MajorityJudgmentChoiceResult a, MajorityJudgmentChoiceResult b) {
        return compareChoiceResultSameMedian(a, b , 0);
    }

    /**
     * Comparaison de deux {@code MajorityJudgmentChoiceResult} ayant la même mediane
     * et dont les bornes a une distance inférieur a {@code gradAroundMedian} mention sont identique
     *
     * @param a une valeur à comparer
     * @param b l'autre valeur à comparer
     * @param gradAroundMedian l a distance a la médiane où faire la comparaison
     * @return -1, 0 ou 1 si {@code b} est préféré à {@code a}, équivalent ou si {@code a} est préféré à {@code b}
     */
    protected static int compareChoiceResultSameMedian(MajorityJudgmentChoiceResult a, MajorityJudgmentChoiceResult b, int gradAroundMedian) {

        int median = a.getMedian();
        int indexUnder = median - gradAroundMedian;
        int indexUpper = median + gradAroundMedian + 1;

        int nbGrades = a.getVoteByGrad().size();
        if (indexUnder <= 0 && indexUpper >= nbGrades) {
            // si on est en dehors du tableau des votes par mention alors les deux choix sont équivalent
            return 0;
        }

        BigDecimal aUnder, aUpper, bUnder, bUpper;

        // pour les deux choix on calcul le nombre de votant avec une mention strictement inférieur à {median - gradAroundMedian}
        if (indexUnder > 0) {
            aUnder = a.getVoteByGrad().subList(0, indexUnder).stream().reduce(BigDecimal.ZERO, BigDecimal::add);
            bUnder = b.getVoteByGrad().subList(0, indexUnder).stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        } else {
            aUnder = BigDecimal.ZERO;
            bUnder = BigDecimal.ZERO;
        }

        // pour les deux choix on calcul le nombre de votant avec une mention strictement supérieur à {median + gradAroundMedian}
        if (indexUpper < nbGrades) {
            aUpper = a.getVoteByGrad().subList(indexUpper, nbGrades).stream().reduce(BigDecimal.ZERO, BigDecimal::add);
            bUpper = b.getVoteByGrad().subList(indexUpper, nbGrades).stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        } else {
            aUpper = BigDecimal.ZERO;
            bUpper = BigDecimal.ZERO;
        }

        // on fait toutes les comparaisons des borne deux à deux
        int aUnderAUpper = aUnder.compareTo(aUpper);
        int aUnderBUnder = aUnder.compareTo(bUnder);
        int aUnderBUpper = aUnder.compareTo(bUpper);
        int aUpperBUnder = aUpper.compareTo(bUnder);
        int aUpperBUpper = aUpper.compareTo(bUpper);
        int bUnderBUpper = bUnder.compareTo(bUpper);

        int result;

        if (aUnderBUnder < 0 && aUpperBUnder < 0 && bUnderBUpper >  0 // bUnder > aUnder, aUpper, bUpper
                || aUnderAUpper < 0 && aUpperBUnder > 0 && aUpperBUpper > 0 // aUpper > aUnder, bUnder, bUpper
                ) {
            result = 1;

        } else if (aUnderAUpper > 0 && aUnderBUnder > 0 && aUnderBUpper > 0   // aUnder > aUpper, bUnder, bUpper
                || aUnderBUpper < 0 && aUpperBUpper < 0 && bUnderBUpper < 0 // bUpper > aUpper, aUnder, bUnder
                ){
            result = -1;
             // ici on géré des cas rare  : une egalité
        } else if (aUnderAUpper == 0 && aUnderBUnder > 0 && aUnderBUpper > 0 && bUnderBUpper < 0 // aUnder = aUpper > bUpper > bUnder
                || aUnderBUnder == 0 && aUnderAUpper > 0 && aUnderBUpper > 0 && aUpperBUpper > 0 // aUnder = bUnder > bUpper > aUpper
                || aUpperBUnder == 0 && aUnderAUpper < 0 && aUpperBUpper > 0 // aUpper = bUnder > aUnder, bUpper
                || aUpperBUpper == 0 && aUnderAUpper < 0 && aUpperBUnder > 0 && aUnderBUnder < 0 // aUpper = bUpper > bUnder > aUnder
                || bUnderBUpper == 0 && aUnderBUnder < 0 && aUnderBUpper < 0 && aUnderAUpper > 0 // bUnder = bUpper > aUnder > aUpper
                ){
            result = 1;
        } else if (aUnderAUpper == 0 && aUnderBUnder > 0 && aUnderBUpper > 0 && bUnderBUpper > 0 // aUnder = aUpper > bUnder > bUpper
                || aUnderBUnder == 0 && aUnderAUpper > 0 && aUnderBUpper > 0 && aUpperBUpper < 0 // aUnder = bUnder > aUpper > bUpper
                || aUnderBUpper == 0 && aUnderAUpper > 0 && aUnderBUnder > 0 // aUnder = bUpper > bUnder, aUpper
                || aUpperBUpper == 0 && aUnderAUpper < 0 && aUpperBUnder > 0 && aUnderBUnder > 0 // aUpper = bUpper > aUnder > bUnder
                || bUnderBUpper == 0 && aUpperBUnder < 0 && aUnderBUpper < 0 && aUnderAUpper < 0 // bUnder = bUpper > aUpper > aUnder
                ) {
            result = -1;
            // ici on géré des cas encore plus rare : deux egalités
        } else if (aUnderAUpper == 0 && aUnderBUnder == 0 && aUnderBUpper > 0 // aUnder = aUpper = bUnder > bUpper
                || aUpperBUnder == 0 && aUpperBUpper == 0 && aUnderAUpper < 0 // aUpper = bUnder = bUpper > aUnder
                ){
            result = 1;
        } else if (aUnderAUpper == 0 && aUnderBUpper == 0 && aUnderBUnder > 0 // aUnder = aUpper = bUpper > bUnder
                || aUnderBUnder == 0 && aUnderBUpper == 0 && aUnderAUpper > 0 // aUnder = bUnder = bUpper > aUpper
                ) {
            result = -1;
        } else { // strict egalité des bornes donc on fait la comparaison a une distance de plus
            result = compareChoiceResultSameMedian(a, b, gradAroundMedian + 1);
        }

        return result;

    }

}
