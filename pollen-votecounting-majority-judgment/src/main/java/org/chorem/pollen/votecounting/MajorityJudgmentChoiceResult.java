package org.chorem.pollen.votecounting;

/*-
 * #%L
 * Pollen :: VoteCounting :: Majority Judgment
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MajorityJudgmentChoiceResult implements Serializable {

    private static final long serialVersionUID = -5443173721590730351L;

    protected String choiceId;

    protected List<BigDecimal> voteByGrad;

    protected int median;

    protected BigDecimal sumWeight;

    protected BigDecimal halfWeight;

    public BigDecimal getSumWeight() {
        return sumWeight;
    }

    public void setSumWeight(BigDecimal sumWeight) {
        this.sumWeight = sumWeight;
    }

    public BigDecimal getHalfWeight() {
        return halfWeight;
    }

    public void setHalfWeight(BigDecimal halfWeight) {
        this.halfWeight = halfWeight;
    }

    public String getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(String choiceId) {
        this.choiceId = choiceId;
    }

    public List<BigDecimal> getVoteByGrad() {
        return voteByGrad;
    }

    public void setVoteByGrad(List<BigDecimal> voteByGrad) {
        this.voteByGrad = voteByGrad;
    }

    public int getMedian() {
        return median;
    }

    public void setMedian(int median) {
        this.median = median;
    }
}
