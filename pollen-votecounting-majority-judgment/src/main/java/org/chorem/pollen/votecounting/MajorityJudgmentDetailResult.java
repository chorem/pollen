package org.chorem.pollen.votecounting;

/*-
 * #%L
 * Pollen :: VoteCounting :: Instant Runoff
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.votecounting.model.VoteCountingDetailResult;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MajorityJudgmentDetailResult implements VoteCountingDetailResult {

    private static final long serialVersionUID = -6759337206955499369L;

    protected BigDecimal sumWeight;
    
    protected BigDecimal halfWeight;

    protected List<MajorityJudgmentChoiceResult> choiceResults;

    public BigDecimal getSumWeight() {
        return sumWeight;
    }

    public void setSumWeight(BigDecimal sumWeight) {
        this.sumWeight = sumWeight;
    }

    public BigDecimal getHalfWeight() {
        return halfWeight;
    }

    public void setHalfWeight(BigDecimal halfWeight) {
        this.halfWeight = halfWeight;
    }

    public List<MajorityJudgmentChoiceResult> getChoiceResults() {
        return choiceResults;
    }

    public void setChoiceResults(List<MajorityJudgmentChoiceResult> choiceResults) {
        this.choiceResults = choiceResults;
    }
}
