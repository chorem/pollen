/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import org.chorem.pollen.votecounting.model.ListOfVoter;
import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.Set;

/**
 * Strategy of a vote counting.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public interface VoteCountingStrategy<C extends VoteCountingConfig> {

    /**
     * Vote count for the given {@code group} of voters and return the
     * result of it.
     *
     * @param listOfVoter the list of voters and their votes.
     * @return the result of the group vote counting for the given voter.
     */
    ListVoteCountingResult votecount(ListOfVoter listOfVoter);

    /**
     * Vote count for the given {@code voter} and return the result of it.
     *
     * @param voter the voter and their votes.
     * @return the result of the vote counting for the given voter.
     */
    VoteCountingResult votecount(Set<Voter> voter);

    /**
     * transform the result of vote to vote
     *
     * @param voteCountingResult  the result of vote
     * @return the vot for choices
     */
    Set<VoteForChoice> toVoteForChoices(VoteCountingResult voteCountingResult);

    void setConfig(C config);
}
