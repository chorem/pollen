package org.chorem.pollen.votecounting;

/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import org.chorem.pollen.votecounting.model.ChoiceToVoteRenderType;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.Locale;

/**
 * Contract of a vote counting.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6
 */
public interface VoteCounting<S extends VoteCountingStrategy<C>, C extends VoteCountingConfig> {

    /**
     * Obtains a fresh instance of a vote coutning strategy.
     *
     * @return a new instance of vote counting strategy for this type
     * of vote counting.
     */
    S newStrategy();

    /**
     * get vote couting config type
     *
     * @return a vote counting config type .
     */
    Class<C> getConfigType();

    /**
     * Obtains the unique id of this strategy.
     *
     * @return the unique id of this strategy.
     */
    int getId();

    /**
     * Get the vote counting strategy name to display in UI.
     *
     * @param locale the locale used to render the strategy name
     * @return the localized vote counting strategy name
     */
    String getName(Locale locale);

    /**
     * Get the vote counting short help to display in UI.
     *
     * @param locale the locale used to render the strategy name
     * @return the localized vote counting short help
     */
    String getShortHelp(Locale locale);

    /**
     * Get the vote counting strategy help to display in UI.
     *
     * @param locale the locale used to render the strategy name
     * @return the localized vote counting strategy help
     */
    String getHelp(Locale locale);

    /**
     * Gets the type of editor used to render a vote value.
     *
     * @return the type of editor used to render a vote value.
     * @see ChoiceToVoteRenderType
     */
    ChoiceToVoteRenderType getVoteValueEditorType();

    /**
     * Gets the minimum value for the vote value to be valid.
     * If null, then it means no minimum value.
     *
     * @return the minimum value for the vote to be valid.
     */
    Double getMinimumValue();

    /**
     * Check if vote vote is valid
     *
     * @param vote vote to check
     * @param config counting type configuration
     * @param locale the locale used to render the error message
     * @return the errorMaps (field, errors) empty if vote is OK
     */
    Multimap<String, String> checkVote(Voter vote, C config, Locale locale);
}
