/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class ListVoteCountingResult implements Serializable {

    private static final long serialVersionUID = 1L;

    protected ListOfVoter voter;

    protected Map<String, ListOfVoter> lists;

    public static ListVoteCountingResult newResult(ListOfVoter voter,
                                                   Set<ListOfVoter> lists) {
        ListVoteCountingResult result = new ListVoteCountingResult();
        result.setVoter(voter);
        lists.remove(voter);
        result.setLists(lists);
        return result;
    }

    public VoteCountingResult getMainResult() {
        return voter.getResult();
    }

    public Set<String> getGroupIds() {
        return Sets.newHashSet(lists.keySet());
    }

    public VoteCountingResult getListResult(String listId) {
        ListOfVoter groupOfVoter = lists.get(listId);
        return groupOfVoter.getResult();
    }

    public void setVoter(ListOfVoter voter) {
        this.voter = voter;
    }

    public void setLists(Set<ListOfVoter> lists) {
        this.lists = Maps.uniqueIndex(lists, ListOfVoter::getVoterId);
    }
}
