package org.chorem.pollen.votecounting;

/*-
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import org.chorem.pollen.votecounting.model.MinMaxChoicesNumberConfig;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

public abstract class AbstractVoteCountingMinMaxChoice<S extends VoteCountingStrategy<C>, C extends MinMaxChoicesNumberConfig> extends AbstractVoteCounting<S, C> {


    protected AbstractVoteCountingMinMaxChoice(int id, Class<S> strategyType, Class<C> configType, String i18nName, String i18nShortHelp, String i18nHelp) {
        super(id, strategyType, configType, i18nName, i18nShortHelp, i18nHelp);
    }

    @Override
    public Multimap<String, String> checkVote(Voter vote, C config, Locale locale) {
        Multimap<String, String> errorMap = super.checkVote(vote, config, locale);

        //Check max
        if (config.getMaxChoiceNumber() > 0) {
            long nbChoice = vote.getVoteForChoices()
                    .stream()
                    .filter(voteForChoice -> voteForChoice.getVoteValue() != null && voteForChoice.getVoteValue() != 0)
                    .count();
            if (nbChoice > config.getMaxChoiceNumber()) {
                errorMap.put("choices", l(locale, "pollen.voteCountingType.maxChoices.error.overflow"));
            }

        }

        //Check min
        if (config.getMinChoiceNumber() > 0) {
            long nbChoice = vote.getVoteForChoices()
                    .stream()
                    .filter(voteForChoice -> voteForChoice.getVoteValue() != null && voteForChoice.getVoteValue() != 0)
                    .count();
            if (nbChoice < config.getMinChoiceNumber()) {
                errorMap.put("choices", l(locale, "pollen.voteCountingType.minChoices.error.overflow"));
            }

        }

        return errorMap;
    }
}
