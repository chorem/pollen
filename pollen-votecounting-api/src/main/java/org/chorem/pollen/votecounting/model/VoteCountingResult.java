/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimaps;

import java.io.Serializable;
import java.util.List;

/**
 * Contains results for a vote.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class VoteCountingResult implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Results for each choice.
     * <p/>
     * <strong>Note:</strong> Natural order is used to describe choice scores
     * (first is winner,...).
     */
    private List<ChoiceScore> scores;

    private int nbVotants;

    private VoteCountingDetailResult detailResult;

    /**
     * Result for each choice order by their winning rank.
     *
     * @see ChoiceScore#getScoreOrder()
     */
    private transient ArrayListMultimap<Integer, ChoiceScore> scoresByRank;

    public static VoteCountingResult newResult(List<ChoiceScore> scores, VoteCountingDetailResult detailResult) {
        VoteCountingResult result = new VoteCountingResult();
        result.setScores(scores);
        result.setDetailResult(detailResult);
        return result;
    }

    public List<ChoiceScore> getScores() {
        return scores;
    }

    public ChoiceScore getScore(String choiceId) {
        ChoiceScore result = null;
        for (ChoiceScore score : scores) {
            if (choiceId.equals(score.getChoiceId())) {
                result = score;
                break;
            }
        }
        return result;
    }

    public void setScores(List<ChoiceScore> scores) {
        this.scores = Lists.newArrayList(scores);
    }

    public List<ChoiceScore> getTopRanking() {
        return getScoresByRank().get(0);
    }

    public ArrayListMultimap<Integer, ChoiceScore> getScoresByRank() {
        if (scoresByRank == null) {
            scoresByRank = ArrayListMultimap.create(
                    Multimaps.index(scores, ChoiceScore::getScoreOrder));
        }
        return scoresByRank;
    }

    public int getNbVotants() {
        return nbVotants;
    }

    public void setNbVotants(int nbVotants) {
        this.nbVotants = nbVotants;
    }

    public VoteCountingDetailResult getDetailResult() {
        return detailResult;
    }

    public void setDetailResult(VoteCountingDetailResult detailResult) {
        this.detailResult = detailResult;
    }
}
