/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Physical voter.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class SimpleVoter implements Voter {

    private static final long serialVersionUID = 1L;

    /** Id of the voter. */
    private String voterId;

    /** Weight of the voter. */
    private double weight;

    /** All votes for this voter (or group of voter). */
    private Set<VoteForChoice> voteForChoices;

    public static SimpleVoter newVoter(String voterId,
                                       double weight,
                                       Set<VoteForChoice> voteForChoices) {
        SimpleVoter result = new SimpleVoter();
        result.setVoterId(voterId);
        result.setWeight(weight);
        if (voteForChoices != null) {
            for (VoteForChoice voteForChoice : voteForChoices) {
                result.addVoteForChoice(voteForChoice);
            }
        }
        return result;
    }

    @Override
    public String getVoterId() {
        return voterId;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void addVoteForChoice(VoteForChoice voteForChoice) {
        getVoteForChoices().add(voteForChoice);
    }

    @Override
    public Set<VoteForChoice> getVoteForChoices() {
        if (voteForChoices == null) {
            voteForChoices = Sets.newHashSet();
        }
        return voteForChoices;
    }

    @Override
    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

}
