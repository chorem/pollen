package org.chorem.pollen.votecounting;

/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Base abstract implementation of a {@link VoteCounting}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6
 */
public abstract class AbstractVoteCounting<S extends VoteCountingStrategy<C>, C extends VoteCountingConfig> implements VoteCounting<S, C> {

    /** Unique id of the vote counting type. */
    protected final int id;

    /** Type of strategy used to vote count. */
    protected final Class<S> strategyType;

    /** Type of config used to vote count. */
    protected final Class<C> configType;

    /** I18n key for name of this vote counting type. */
    protected final String i18nName;

    /** I18n Key for short help of this vote counting type. */
    protected final String i18nShortHelp;

    /** I18n Key for help of this vote counting type. */
    protected final String i18nHelp;

    protected AbstractVoteCounting(int id,
                                   Class<S> strategyType,
                                   Class<C> configType,
                                   String i18nName,
                                   String i18nShortHelp,
                                   String i18nHelp) {
        this.id = id;
        this.strategyType = strategyType;
        this.configType = configType;
        this.i18nName = i18nName;
        this.i18nShortHelp = i18nShortHelp;
        this.i18nHelp = i18nHelp;
    }

    @Override
    public final S newStrategy() {
        try {
            return strategyType.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Could not instanciate strategy", e);
        }
    }

    @Override
    public final String getName(Locale locale) {
        return l(locale, i18nName);
    }

    @Override
    public final String getShortHelp(Locale locale) {
        String voteName = l(locale, i18nName);
        String voteHelp = l(locale, i18nShortHelp);
        return l(locale, "pollen.voteCountingType.help", voteName, voteHelp);
    }

    @Override
    public final String getHelp(Locale locale) {
        return l(locale, i18nHelp);
    }

    @Override
    public final int getId() {
        return id;
    }

    @Override
    public Class<C> getConfigType() {
        return configType;
    }

    @Override
    public Multimap<String, String> checkVote(Voter vote, C config, Locale locale) {
        Multimap<String, String> errorMap = ArrayListMultimap.create();

        for(VoteForChoice voteForChoice : vote.getVoteForChoices()) {
            Multimap<String, String> choiceError = checkVoteForChoice(voteForChoice, config, locale);
            choiceError.entries().forEach(entry ->
                    errorMap.put("choice[" + voteForChoice.getChoiceId() + "]." + entry.getKey(), entry.getValue()));
        }

        return errorMap;

    }

    public Multimap<String, String> checkVoteForChoice(VoteForChoice voteForChoice, C config, Locale locale) {
        return ArrayListMultimap.create();

    }
}
