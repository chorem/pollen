/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import java.io.Serializable;

/**
 * A vote for a choice.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class VoteForChoice implements ChoiceIdAble, Serializable {

    public static final String PROPERTY_VOTE_VALUE = "voteValue";

    public static final String PROPERTY_CHOICE_ID = "choiceId";

    private static final long serialVersionUID = 1L;


    /** Id of the choice. */
    private String choiceId;

    /** Value of the vote for this choice. */
    private Double voteValue;

    public static VoteForChoice newVote(String choiceId, Double voteValue) {
        VoteForChoice vote = new VoteForChoice();
        vote.setChoiceId(choiceId);
        vote.setVoteValue(voteValue);
        return vote;
    }

    @Override
    public String getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(String choiceId) {
        this.choiceId = choiceId;
    }

    public Double getVoteValue() {
        return voteValue;
    }

    public void setVoteValue(Double voteValue) {
        this.voteValue = voteValue;
    }
}
