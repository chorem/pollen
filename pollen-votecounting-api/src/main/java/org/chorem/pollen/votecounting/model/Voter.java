/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import java.io.Serializable;
import java.util.Set;

/**
 * A voter (can be a physical voter or a group of voters).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public interface Voter extends Serializable {

    /**
     * Gets the voter unique id.
     *
     * @return the unique id of the voter.
     */
    String getVoterId();

    /**
     * Gets the weight of the voter.
     *
     * @return the weight to apply for this voter.
     */
    double getWeight();

    /**
     * Gets all the vote for choices registred for this voter.
     *
     * @return all the vote values registred for this voter
     */
    Set<VoteForChoice> getVoteForChoices();

    /**
     * Register a new vote value for this voter.
     *
     * @param voteForChoice new vote value to register for this voter
     */
    void addVoteForChoice(VoteForChoice voteForChoice);

    /**
     * Sets the voter id.
     *
     * @param voterId the new voter id to set
     */
    void setVoterId(String voterId);

    /**
     * Sets the voter weight.
     *
     * @param weight the new weight to set
     */
    void setWeight(double weight);
}
