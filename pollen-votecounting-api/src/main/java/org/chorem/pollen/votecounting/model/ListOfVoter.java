/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import com.google.common.collect.Sets;

import java.util.Iterator;
import java.util.Set;

/**
 * Group of voters.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class ListOfVoter implements Voter, Iterable<Voter> {

    private static final long serialVersionUID = 1L;

    private final SimpleVoter rootVoter = SimpleVoter.newVoter(null, 1.0, null);

    /** Set of voters for this group. */
    private Set<Voter> voters;

    /** Result for this group. */
    private VoteCountingResult result;

    public static ListOfVoter newVoter(String voterId,
                                       double weight) {
        return newVoter(voterId, weight, Sets.newHashSet());
    }

    public static ListOfVoter newVoter(String voterId,
                                       double weight,
                                       Set<Voter> voters) {
        ListOfVoter result = new ListOfVoter();
        result.setVoterId(voterId);
        result.setWeight(weight);
        result.setVoters(voters);
        return result;
    }

    @Override
    public String getVoterId() {
        return rootVoter.getVoterId();
    }

    @Override
    public double getWeight() {
        return rootVoter.getWeight();
    }

    @Override
    public Set<VoteForChoice> getVoteForChoices() {
        return rootVoter.getVoteForChoices();
    }

    @Override
    public void setVoterId(String voterId) {
        rootVoter.setVoterId(voterId);
    }

    @Override
    public void setWeight(double weight) {
        rootVoter.setWeight(weight);
    }

    @Override
    public void addVoteForChoice(VoteForChoice voteForChoice) {
        getVoteForChoices().add(voteForChoice);
    }

    public Set<Voter> getVoters() {
        if (voters == null) {
            voters = Sets.newHashSet();
        }
        return voters;
    }

    public void addVoter(Voter voter) {
        getVoters().add(voter);
    }

    public VoteCountingResult getResult() {
        return result;
    }

    public void setVoters(Set<Voter> voters) {
        this.voters = voters;
    }

    public void setResult(VoteCountingResult result) {
        this.result = result;
        this.result.setNbVotants(voters.size());
//        Collection<ChoiceScore> winners = result.getTopRanking();
//        for (ChoiceScore choiceScore : result.getScores()) {
//
//            double score = winners.contains(choiceScore) ? 1d : 0d;
//            VoteForChoice voteForChoice = VoteForChoice.newVote(
//                    choiceScore.getChoiceId(),
//                    score);
//            addVoteForChoice(voteForChoice);
//        }
    }

    @Override
    public Iterator<Voter> iterator() {
        return getVoters().iterator();
    }
}
