/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import java.util.function.Function;

/**
 * To mark any object that identify a choice via the {@link #getChoiceId()}
 * method.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public interface ChoiceIdAble {

    String getChoiceId();

    class ChoiceIdAbleById implements Function<ChoiceIdAble, String> {
        @Override
        public String apply(ChoiceIdAble input) {
            return input.getChoiceId();
        }
    }
}
