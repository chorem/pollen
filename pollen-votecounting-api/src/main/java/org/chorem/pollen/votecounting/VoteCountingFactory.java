package org.chorem.pollen.votecounting;

/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * Factory of {@link VoteCounting}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6
 */
public class VoteCountingFactory implements Iterable<VoteCounting> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoteCountingFactory.class);

    /**
     * List of available vote counting detected via the {@link ServiceLoader}
     * mecanism no contract {@link VoteCounting}.
     */
    private final Map<Integer, VoteCounting> voteCountings;

    public VoteCountingFactory() {
        voteCountings = Maps.newHashMap();

        ServiceLoader<VoteCounting> loader =
                ServiceLoader.load(VoteCounting.class);

        Locale l = Locale.getDefault();

        for (VoteCounting strategy : loader) {
            int id = strategy.getId();

            String voteCountingName = strategy.getName(l);
            if (voteCountings.containsKey(id)) {
                throw new IllegalStateException(
                        "Strategy [" +
                                voteCountingName + "] with id " + id +
                                ", can not be used since the id is already used " +
                                "by strategy [" + voteCountings.get(id).getName(l) + "] ");
            }
            if (log.isInfoEnabled()) {
                log.info("Detected strategy [" + id + "-" +
                                 voteCountingName + "] : " +
                                 strategy.getClass().getName());
            }
            voteCountings.put(id, strategy);
        }


    }

    public <S extends VoteCountingStrategy<C>, C extends VoteCountingConfig> VoteCounting<S, C> getVoteCounting(int strategyId) throws VoteCountingNotFound {
        VoteCounting type = voteCountings.get(strategyId);

        if (type == null) {
            throw new VoteCountingNotFound(
                    "Could not find strategy with id " + strategyId);
        }
        return type;
    }

    @Override
    public Iterator<VoteCounting> iterator() {
        return voteCountings.values().iterator();
    }
}
