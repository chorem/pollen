package org.chorem.pollen.votecounting.model;

/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import java.util.Set;

/**
 * {@link SimpleVoter} builder.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.6
 */
public class SimpleVoterBuilder implements VoterBuilder {

    private final Set<Voter> voters = Sets.newHashSet();

    private Voter voter;

    @Override
    public SimpleVoterBuilder newVoter(String voterId, double weight) {
        flush();
        voter = SimpleVoter.newVoter(voterId, weight, null);
        return this;
    }

    @Override
    public SimpleVoterBuilder addVoteForChoice(String choiceId, Double voteValue) {
        Preconditions.checkState(voter != null, "No voter defined, use method newVoter before this one");
        VoteForChoice voteForChoice = VoteForChoice.newVote(choiceId, voteValue);
        voter.addVoteForChoice(voteForChoice);
        return this;
    }

    @Override
    public Set<Voter> getVoters() {
        flush();
        return voters;
    }

    @Override
    public void flush() {
        if (voter != null) {
            voters.add(voter);
            voter = null;
        }
    }
}
