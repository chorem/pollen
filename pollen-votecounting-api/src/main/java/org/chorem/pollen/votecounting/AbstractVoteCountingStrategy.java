/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceIdAble;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.ListOfVoter;
import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;
import org.chorem.pollen.votecounting.model.VoteCountingDetailResult;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Base abstract implementation of a {@link VoteCountingStrategy}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public abstract class AbstractVoteCountingStrategy<C extends VoteCountingConfig> implements VoteCountingStrategy<C> {

    public static final BigDecimal ZERO_D = BigDecimal.valueOf(0.);

    protected final Comparator<VoteForChoice> voteValueComparator =
            Comparator.comparing(VoteForChoice::getVoteValue, Comparator.nullsLast(Comparator.naturalOrder()));

    protected C config;

    @Override
    public void setConfig(C config) {
        this.config = config;
    }

    @Override
    public final ListVoteCountingResult votecount(ListOfVoter listOfVoter) {

        Set<ListOfVoter> lists = Sets.newHashSet();
        recursiveVoteCount(listOfVoter, lists);

        return ListVoteCountingResult.newResult(
                listOfVoter, lists);
    }

    public Map<String, ChoiceScore> newEmptyChoiceScoreMap(Set<Voter> voters) {
        return newEmptyChoiceScoreMap(voters, ZERO_D);
    }

    public Map<String, ChoiceScore> newEmptyChoiceScoreMap(Set<Voter> voters, BigDecimal defaultScore) {
        // get all choice Id
        Set<String> choiceIds = getAllChoiceIds(voters);

        Map<String, ChoiceScore> resultByChoice = Maps.newHashMap();

        // creates all empty result for choice
        for (String choiceId : choiceIds) {
            ChoiceScore choiceScore = ChoiceScore.newScore(choiceId, defaultScore);
            resultByChoice.put(choiceId, choiceScore);
        }
        return resultByChoice;
    }

    protected VoteCountingResult orderByValues(Collection<ChoiceScore> scores, VoteCountingDetailResult detailResult) {

        // get scores by score value
        Multimap<BigDecimal, ChoiceScore> map = Multimaps.index(
                scores, ChoiceScore::getScoreValue
        );

        // get all distinct score values
        List<BigDecimal> values = Lists.newArrayList(map.asMap().keySet());
        Collections.sort(values);
        Collections.reverse(values);

        // compute rank for each scores
        int rank = 0;
        for (BigDecimal value : values) {

            Collection<ChoiceScore> scoresForValue = map.get(value);
            for (ChoiceScore score : scoresForValue) {
                score.setScoreOrder(rank);
            }
            rank++;
        }
        // get all scores
        List<ChoiceScore> orderedScores = Lists.newArrayList(map.values());

        // sort them by their rank
        Collections.sort(orderedScores);

        // transform map of result to list of them (and sort them)
        return VoteCountingResult.newResult(orderedScores, detailResult);
    }

    public Set<String> getAllChoiceIds(Set<Voter> voters) {
        ChoiceIdAble.ChoiceIdAbleById function = new ChoiceIdAble.ChoiceIdAbleById();
        Set<String> result = Sets.newHashSet();
        for (Voter voter : voters) {
            List<String> transform = voter.getVoteForChoices().stream().map(function).collect(Collectors.toList());
            result.addAll(transform);

        }
        return result;
    }

    public Map<Voter, List<Set<String>>> buildVoterSortedChoices(Set<Voter> voters) {

        Map<Voter, List<Set<String>>> voterSortedChoices = Maps.newHashMap();

        for (Voter voter : voters) {

            List<Set<String>> sortedChoices = sortVoteForChoices(voter.getVoteForChoices());
            voterSortedChoices.put(voter, sortedChoices);
        }
        return voterSortedChoices;
    }

    public List<Set<String>> sortVoteForChoices(Set<VoteForChoice> voteForChoices) {
        // get sort vote for choices
        List<VoteForChoice> sortedChoices = Lists.newArrayList(voteForChoices);
        sortedChoices.sort(voteValueComparator);

        // build ranks
        List<Set<String>> result = Lists.newArrayList();

        Set<String> set = Sets.newHashSet();
        result.add(set);
        VoteForChoice lastVoteForChoice = null;
        for (VoteForChoice voteForChoice : sortedChoices) {
            if (lastVoteForChoice != null &&
                    voteValueComparator.compare(lastVoteForChoice, voteForChoice) != 0) {

                // new rank found
                // register it
                result.add(set = Sets.newHashSet());
            }

            set.add(voteForChoice.getChoiceId());
            lastVoteForChoice = voteForChoice;
        }
        return result;
    }

    protected void recursiveVoteCount(ListOfVoter listOfVoter, Set<ListOfVoter> listOfVoters) {

        listOfVoters.add(listOfVoter);

        // all childs of this group
        Set<Voter> voters = listOfVoter.getVoters();

        // treat before all his group childs
        for (Voter voter : voters) {
            if (voter instanceof ListOfVoter) {

                // treat group child before all
                recursiveVoteCount((ListOfVoter) voter, listOfVoters);
            }
        }

        // once here, all childs has been treated, can votecount this group
        VoteCountingResult voteCountingResult = votecount(voters);

        // store the result for this group
        listOfVoter.setResult(voteCountingResult);

        Set<VoteForChoice> voteForChoices = toVoteForChoices(voteCountingResult);

        listOfVoter.getVoteForChoices().addAll(voteForChoices);
    }
}
