/*
 * #%L
 * Pollen :: VoteCounting (Api)
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Score for a given choice.
 * <p/>
 * Rank is given by the field {@link #scoreOrder} and this class is
 * comparable of this data.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class ChoiceScore implements ChoiceIdAble, Comparable<ChoiceScore>, Serializable {

    private static final long serialVersionUID = 1L;

    /** Id of the choice. */
    private String choiceId;

    /**
     * Global score for this choice (in some vote count it does not mean a
     * lot (and it should be improved to take account of the notion of round)).
     */
    private BigDecimal scoreValue;

    /** Order of this score (0 is winner, 1 is second,...). */
    private int scoreOrder;

    public static ChoiceScore newScore(String choiceId, BigDecimal scoreValue) {
        return newScore(choiceId, scoreValue, 0);
    }

    public static ChoiceScore newScore(String choiceId, BigDecimal scoreValue, int scoreOrder) {
        ChoiceScore choiceScore = new ChoiceScore();
        choiceScore.setChoiceId(choiceId);
        choiceScore.setScoreValue(scoreValue);
        choiceScore.setScoreOrder(scoreOrder);
        return choiceScore;
    }

    @Override
    public String getChoiceId() {
        return choiceId;
    }

    public BigDecimal getScoreValue() {
        return scoreValue;
    }

    public int getScoreOrder() {
        return scoreOrder;
    }

    public void setScoreOrder(int scoreOrder) {
        this.scoreOrder = scoreOrder;
    }

    public void setChoiceId(String choiceId) {
        this.choiceId = choiceId;
    }

    public void addScoreValue(double scoreToAdd) {
        BigDecimal newScoreValue;
        if (scoreValue == null) {

            newScoreValue = BigDecimal.valueOf(scoreToAdd);
        } else {
            newScoreValue = scoreValue.add(BigDecimal.valueOf(scoreToAdd));
        }
        setScoreValue(newScoreValue);
    }

    public void setScoreValue(BigDecimal scoreValue) {
        this.scoreValue = scoreValue;
    }

    @Override
    public int compareTo(ChoiceScore o) {
        return scoreOrder - o.scoreOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChoiceScore that = (ChoiceScore) o;
        return scoreOrder == that.scoreOrder &&
                Objects.equals(choiceId, that.choiceId) &&
                Objects.equals(scoreValue, that.scoreValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(choiceId, scoreValue, scoreOrder);
    }

    @Override
    public String toString() {
        return "ChoiceScore{" +
                "choiceId='" + choiceId + '\'' +
                ", scoreValue=" + scoreValue +
                ", scoreOrder=" + scoreOrder +
                '}';
    }
}
