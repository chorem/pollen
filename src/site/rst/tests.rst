.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

~~~~~~~~~~
Tests d'UI
~~~~~~~~~~

Abstract
========

- Tests Pollen by ui.
- Needs a db with
  - 1 admin user *admin/admin*
  - 1 none admin user *user/user*
  - 1 none admin user *user2/user2*
  - 1 poll of each votecounting type (Normal, pourcentage, Condorcet, Nombre) + some votes on it
  - 1 poll with restricted mode

Scenarii
========

Security
--------

- authentication avec mauvais login/password (KO)
- authentication avec bon login/password (OK)

- access aux pages connectés en mode déconnecté (KO)
- access aux pages connectés en mode connecté (OK)

- access aux pages admin en mode déconnecté (KO)
- access aux pages admin avec un user non admin (KO)
- access aux pages admin avec un user admin (OK)

- access aux pages d'un sondage avec un mauvais pollId (KO)
- access aux pages d'un sondage avec un bon pollId (OK)

- access aux pages d'un sondage en mode créateur
  - en mode admin (pas de accountId dans l'url) : avec un user non admin (KO)
  - en mode admin (pas de accountId dans l'url) : avec un admin (OK)
  - en mode créateur (avec mauvais accountId dans l'url) (KO)
  - en mode créateur (avec bon accountId dans l'url) (OK)

- access aux résultats d'un sondage (non restreint) (OK)
- access aux résultats d'un sondage restreint en anonyme (KO)
- access aux résultats d'un sondage restreint en extérieur (mauvais accountId) (KO)
- access aux résultats d'un sondage restreint en extérieur (mauvais utilisateur connecté) (KO)
- access aux résultats d'un sondage restreint avec bon accountId (OK)
- access aux résultats d'un sondage restreint avec bon utilisateur connecté  (OK)
- access aux résultats d'un sondage restreint avec accountId de créateur (OK)
- access aux résultats d'un sondage restreint avec un admin connecté (OK)

Création d'un sondage (free + text)
-----------------------------------

- creation avec champs obligatoires remplis (OK)
- creation avec Titre non rempli (KO)
- creation avec aucun choix proposé (KO)
- creation avec deux choix identiques (KO)
- creation avec Date de fin < Date de début (KO)
- creation avec Date de fin antérieur à maintenant (KO) (TODO)
- creation avec Ajout de choix Date de fin < Date de début (KO)
- creation avec Ajout de choix Date de fin antérieur à maintenant (KO)
- creation avec Ajout de choix cochez et remplit + decochez (les dates doivent être supprimées) (OK) (TODO)
- creation avec email invalide (KO)
- creation avec heure avant la fin de sondage <=0 (KO) (TODO)
- TODO Mettre un warning si date de fin proche... (new feature)
- creation avec nombre de choix <=0 (KO) (TODO : pffff quand on veut voter on a un message Le nombre de choix maximal est de -5....)
- TODO Par défaut mettre au moins 1

- TODO Quand on déchoche alors il faut desactiver les champs concernés (et pas uniquement les cacher)

Création d'un sondage (free + date)
-----------------------------------

- creation avec aucun choix proposé (KO)
- creation avec deux choix identiques (KO)
- creation d'un sondage de Date avec mauvais format (KO)

Création d'un sondage (free + image)
------------------------------------

- creation avec aucun choix proposé (KO)
- creation d'un sondage d'Image avec mauvais mimetype (KO) (TODO)
- creation avec deux images au nom identiques (KO) (TODO)
- TODO Mettre une image avec une croix ROUGE!!! quand pas chargée

Création d'un sondage (restreint + text)
----------------------------------------

- création sans participant (KO)
- création avec deux participants ayant le même nom (KO)
- création avec deux participants ayant le même email (KO)
- création avec un participants ayant juste un email et pas de nom (KO) (TODO : pollenTechincalException)
- création avec un participants ayant juste un nom et pas d'email (KO)
- TODO problème d'enregistrement des plusieurs votants, le dernier est conservé et dupliqué.
- TODO L'email envoyé n'est pas le bon (il manque le token d'account) + la sécurité ne va pas car je ne dois pas avoir accès à ce sondage

Création d'un sondage (group + text)
----------------------------------------

- création sans nom de groupe (KO)
- création avec deux participants ayant le même nom (KO)
- création avec deux participants ayant le même email (KO)
- création avec un participants ayant juste un email et pas de nom (KO) (TODO : pollenTechincalException)
- création avec un participants ayant juste un nom et pas d'email (KO)
- TODO problème d'enregistrement des plusieurs votants, le dernier est conservé et dupliqué.
- TODO Revoir l'enregistrement (le groupe 2 est perdu...)


Edition d'un sondage
---------------------

- edition avec champs titre vidé (OK)
- edition avec modification date de fin antérieure à maintenant (KO)
- edition avec suppression de tous les choix (KO)

Page de résumé d'un sondage
---------------------------

- TODO supprimer les // dans les urls

