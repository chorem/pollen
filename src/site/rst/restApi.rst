.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

~~~~~~~~~~~~~~~
Pollen Rest API
~~~~~~~~~~~~~~~

Abstract
========

- Describe the REST API offers since Pollen 2.0

Doc
===

GET /v1/doc
-----------

Abstract
~~~~~~~~

To Show this documentation page.

Auth
====

PUT /v1/login
-------------

Abstract
~~~~~~~~

To log in.

In
~~

- mandaTory parameter **login**
- mandaTory parameter **password**
- optional parameter **remberberMe** (TODO)

Validation
~~~~~~~~~~

- status 401 if bad authentication

Out
~~~

::

  {
    id: "TokenId",
    permission: "Token"
  }

You must keep the **permission** value and add it in all your REST queries as the parameter named **sessionToken**.

GET /v1/lostpassword/{Token}
----------------------------

Abstract
~~~~~~~~

To ask a new password generation.

In
~~

- mandaTory parameter **login**

Validation
~~~~~~~~~~

- status 404 If login is not found

FIXME Should return nothing (do not inform user the login does not exist).

Out
~~~

None.

GET /v1/logout
--------------

Abstract
~~~~~~~~

To log out from pollen.

In
~~

None.

Validation
~~~~~~~~~~

None.

Out
~~~

None. The **pollenSession** cookie will be removed.

Poll
====

GET /v1/polls/new
-----------------

Abstract
~~~~~~~~

To get a empty poll with default create values.

In
~~

None.

Validation
~~~~~~~~~~

None.

Out
~~~

::

  {
    "id" : null,
    "permission" : null,
    "creaTorName" : null,
    "creaTorEmail" : null,
    "title" : null,
    "description" : null,
    "beginChoiceDate" : null,
    "endChoiceDate" : null,
    "beginDate" : 1388530800000,
    "endDate" : null,
    "maxChoiceNumber" : 0,
    "choiceAddAllowed" : false,
    "anonymousVoteAllowed" : false,
    "continuousResults" : false,
    "voteCountingType" : 1,
    "pollType" : "FREE",
    "voteVisibility" : "EVERYBODY",
    "commentVisibility" : "EVERYBODY",
    "resultVisibility" : "EVERYBODY",
    "persisted" : false
  }


GET /v1/polls
-------------

Abstract
~~~~~~~~

To get all polls (for admin usage only).

In
~~

None.

Validation
~~~~~~~~~~

- must be connected
- must be admin

Out
~~~

::

  [
    {

    }
  ]

GET /v1/polls/created
---------------------

Abstract
~~~~~~~~

Get all the created polls for a connected user.

In
~~

Validation
~~~~~~~~~~

- need To be connected

Out
~~~

A list of polls:

::

  [
    { poll1 },
    ...
  ]

GET /v1/polls/invited
---------------------

Abstract
~~~~~~~~

Get all the invited polls for a connected user.

In
~~

Validation
~~~~~~~~~~

- need To be connected

Out
~~~

A list of polls:

::

  [
    { poll1 },
    ...
  ]

GET /v1/polls/participated
--------------------------

Abstract
~~~~~~~~

Get all the participated polls for a connected user.

In
~~

Validation
~~~~~~~~~~

- need To be connected

Out
~~~

A list of polls:

::

  [
    { poll1 },
    ...
  ]

POST /v1/polls
--------------

Abstract
~~~~~~~~

To create a poll.

In
~~

- mandaTory **poll** parameter with this format

::


  {
    "id" : null,
    "permission" : null,
    "creaTorName" : null,
    "creaTorEmail" : null,
    "title" : null,
    "description" : null,
    "beginChoiceDate" : null,
    "endChoiceDate" : null,
    "beginDate" : 1388530800000,
    "endDate" : null,
    "maxChoiceNumber" : 0,
    "choiceAddAllowed" : false,
    "anonymousVoteAllowed" : false,
    "continuousResults" : false,
    "voteCountingType" : 1,
    "pollType" : "FREE",
    "voteVisibility" : "EVERYBODY",
    "commentVisibility" : "EVERYBODY",
    "resultVisibility" : "EVERYBODY",
    "persisted" : false
  }

- mandaTory **choices** paremeter

::

  [
    {
      name: "",
      choicetype: "",
      description: ""
    },
    ...
  ]

- optional **voterLists** parameter (only for none free polls)

::

  [
    {
        name: "voterList1",
        weight: 1.0,
        member :
        [
          {
            name: "member1",
            email: "member1@pollen.org",
            weight: 1.0
          },
          ...
        ]
    }
  ]

Validation
~~~~~~~~~~

TODO

Out
~~~

Created poll reference:

::

  {
    id: "pollId",
    permission: "Token To be able To edit again the poll"
  }

PUT /v1/polls/{pollId}
----------------------

Abstract
~~~~~~~~

Edit a poll.

In
~~

- mandaTory **poll** parameter with this format

::

  {
    "id" : null,
    "permission" : null,
    "creaTorName" : null,
    "creaTorEmail" : null,
    "title" : null,
    "description" : null,
    "beginChoiceDate" : null,
    "endChoiceDate" : null,
    "beginDate" : 1388530800000,
    "endDate" : null,
    "maxChoiceNumber" : 0,
    "choiceAddAllowed" : false,
    "anonymousVoteAllowed" : false,
    "continuousResults" : false,
    "voteCountingType" : 1,
    "pollType" : "FREE",
    "voteVisibility" : "EVERYBODY",
    "commentVisibility" : "EVERYBODY",
    "resultVisibility" : "EVERYBODY",
    "persisted" : false
  }

- optional parameter **permission** (if user is connected no need)

Validation
~~~~~~~~~~

Out
~~~

GET /v1/polls/{pollId}
----------------------

Abstract
~~~~~~~~

Get the poll of the given id.

In
~~

None.

Validation
~~~~~~~~~~

None.

Out
~~~

A poll:

::

  {
    "id" : null,
    "permission" : null,
    "creaTorName" : null,
    "creaTorEmail" : null,
    "title" : null,
    "description" : null,
    "beginChoiceDate" : null,
    "endChoiceDate" : null,
    "beginDate" : 1388530800000,
    "endDate" : null,
    "maxChoiceNumber" : 0,
    "choiceAddAllowed" : false,
    "anonymousVoteAllowed" : false,
    "continuousResults" : false,
    "voteCountingType" : 1,
    "pollType" : "FREE",
    "voteVisibility" : "EVERYBODY",
    "commentVisibility" : "EVERYBODY",
    "resultVisibility" : "EVERYBODY",
    "persisted" : false
  }

DELETE /v1/polls/{pollId}
-------------------------

Abstract
~~~~~~~~

To delete a poll.

In
~~

None.

Validation
~~~~~~~~~~

None.

Out
~~~

None.

POST /v1/polls/{pollId}
-----------------------

Abstract
~~~~~~~~

To clone a poll.

In
~~

None.

Validation
~~~~~~~~~~

None.

Out
~~~

Reference on cloned poll.

GET /v1/polls/{pollId}/export
-----------------------------

Abstract
~~~~~~~~

To export a poll.

In
~~

None.

Validation
~~~~~~~~~~

None.

Out
~~~

Stream of the file.

PUT /v1/polls/{pollId}/close
----------------------------

Abstract
~~~~~~~~

To close a poll.

In
~~

None.

Validation
~~~~~~~~~~

Out
~~~

None.

Choice
======

GET /v1/polls/{pollId}/choices
------------------------------

Abstract
~~~~~~~~

To get all choices of a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~

POST /v1/polls/{pollId}/choices
-------------------------------

Abstract
~~~~~~~~

To add a new choice on a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~

GET /v1/polls/{pollId}/choices/{choiceId}
-----------------------------------------

Abstract
~~~~~~~~

To get a choice.

In
~~

Validation
~~~~~~~~~~

Out
~~~

PUT /v1/polls/{pollId}/choices/{choiceId}
-----------------------------------------

Abstract
~~~~~~~~

To edit a choice.

In
~~

Validation
~~~~~~~~~~

Out
~~~

DELETE /v1/polls/{pollId}/choices/{choiceId}
--------------------------------------------

Abstract
~~~~~~~~

To delete a choice.

In
~~

Validation
~~~~~~~~~~

Out
~~~

Comment
=======

GET /v1/polls/{pollId}/comments
-------------------------------

Abstract
~~~~~~~~

To get all comments of a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/polls/{pollId}/comments
--------------------------------

Abstract
~~~~~~~~

To add a comment on a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/polls/{pollId}/comments/{commentId}
-------------------------------------------

Abstract
~~~~~~~~

To get a comment.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/polls/{pollId}/comments/{commentId}
-------------------------------------------

Abstract
~~~~~~~~

To edit a comment.

In
~~

Validation
~~~~~~~~~~

Out
~~~


DELETE /v1/polls/{pollId}/comments/{commentId}
----------------------------------------------

Abstract
~~~~~~~~

To delete a comment.

In
~~

Validation
~~~~~~~~~~

Out
~~~


FavoriteList
============

GET /v1/favoriteLists
---------------------

Abstract
~~~~~~~~

To get all favorite lists.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/favoriteLists/{flId}
----------------------------

Abstract
~~~~~~~~

To get a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/favoriteLists/{flId}/importCsv
---------------------------------------

Abstract
~~~~~~~~

To import a favorite list from a csv file.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/favoriteLists/{flId}/importLdap
----------------------------------------

Abstract
~~~~~~~~

To import a favorite list from a ldap.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/favoriteLists
----------------------

Abstract
~~~~~~~~

To create a new favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/favoriteLists/{flId}
----------------------------

Abstract
~~~~~~~~

To edit a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


DELETE /v1/favoriteLists/{flId}
-------------------------------

Abstract
~~~~~~~~

To delete a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/favoriteLists/{flId}/members
------------------------------------

Abstract
~~~~~~~~

To get all members of a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/favoriteLists/{flId}/members/{mId}
------------------------------------------

Abstract
~~~~~~~~

To get a member of a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/favoriteLists/{flId}/members
-------------------------------------

Abstract
~~~~~~~~

To add a member in a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/favoriteLists/{flId}/members/{mId}
------------------------------------------

Abstract
~~~~~~~~

To edit a favorite list member.

In
~~

Validation
~~~~~~~~~~

Out
~~~


DELETE /v1/favoriteLists/{flId}/members/{mId}
---------------------------------------------

Abstract
~~~~~~~~

To delete a member from a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PollenUser
==========

GET /v1/users
-------------

Abstract
~~~~~~~~

To get all users (only for admin).

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/users/{userId}
----------------------

Abstract
~~~~~~~~

To get a user.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/users
--------------

Abstract
~~~~~~~~

To create a user.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/users/{userId}
----------------------

Abstract
~~~~~~~~

To edit a user.

In
~~

Validation
~~~~~~~~~~

Out
~~~


DELETE /v1/users/{userId}
-------------------------

Abstract
~~~~~~~~

To delete a user.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/users/{userId}?Token={}
-------------------------------

Abstract
~~~~~~~~

To validate email of a user.

In
~~

Validation
~~~~~~~~~~

Out
~~~


VoteCounting
============

GET /v1/polls/{pollId}/results
------------------------------

Abstract
~~~~~~~~

To get results of a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~


VoterList
=========

PUT /v1/polls/{pollId}/favoriteLists/{flId}
-------------------------------------------

Abstract
~~~~~~~~

To create a voterList from a favorite list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/polls/{pollId}/voterLists
---------------------------------

Abstract
~~~~~~~~

To get voter lists of a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/polls/{pollId}/voterLists/{vlId}
----------------------------------------

Abstract
~~~~~~~~

To get a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/polls/{pollId}/voterLists
----------------------------------

Abstract
~~~~~~~~

To add a new voter list To a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/polls/{pollId}/voterLists/{vlId}
----------------------------------------

Abstract
~~~~~~~~

To edit a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


DELETE /v1/polls/{pollId}/voterLists/{vlId}
-------------------------------------------

Abstract
~~~~~~~~

To delete a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/polls/{pollId}/voterLists/{vlId}/members
------------------------------------------------

Abstract
~~~~~~~~

To get all members of a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/polls/{pollId}/voterLists/{vlId}/members/{mId}
------------------------------------------------------

Abstract
~~~~~~~~

To get a member of a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


POST /v1/polls/{pollId}/voterLists/{vlId}/members
-------------------------------------------------

Abstract
~~~~~~~~

To add a member To a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/polls/{pollId}/voterLists/{vlId}/members/{mId}
------------------------------------------------------

Abstract
~~~~~~~~

To edit a member of a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


DELETE /v1/polls/{pollId}/voterLists/{vlId}/members/{mId}
---------------------------------------------------------

Abstract
~~~~~~~~

To delete a member from a voter list.

In
~~

Validation
~~~~~~~~~~

Out
~~~


Vote
====

GET /v1/polls/{pollId}/votes
----------------------------

Abstract
~~~~~~~~

To get all votes of a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/polls/{pollId}/votes
----------------------------

Abstract
~~~~~~~~

To add a vote To a poll.

In
~~

Validation
~~~~~~~~~~

Out
~~~


GET /v1/polls/{pollId}/votes/{voteId}
-------------------------------------

Abstract
~~~~~~~~

To get a vote.

In
~~

Validation
~~~~~~~~~~

Out
~~~


PUT /v1/polls/{pollId}/votes/{voteId}
-------------------------------------

Abstract
~~~~~~~~

To edit a vote.

In
~~

Validation
~~~~~~~~~~

Out
~~~


DELETE /v1/polls/{pollId}/votes/{voteId}
----------------------------------------

Abstract
~~~~~~~~

To delete a vote.

In
~~

Validation
~~~~~~~~~~

Out
~~~

