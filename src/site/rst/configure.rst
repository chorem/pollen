.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

Configure Pollen
~~~~~~~~~~~~~~~~

How to configure Pollen
-----------------------

All Pollen configuration is done via a file named **pollen.properties**.

This file can be located in several places on the computer that starts Pollen,
depending on the installation mode:

::

  /etc/pollen.properties
  directoryWherePollenIsLaunched/pollen.properties

Beware, if the file is in different places, the configurations are merged.

More information on the available options is available on `options page`_.

Configure the public address
----------------------------

Pollen public address is the one used in the generated emails.

You can configure it using the following configuration option:

::

  siteUrl=your Pollen instance public address(used to generate links in sent emails)

Configure mail sending
----------------------

So that Pollen can send emails you must fill in the following configuration:

::

  email_host= Smtp server address (ex: localhost, smtp.free.fr, ...)
  email_port= Smtp server port (by default: 25)
  email_from= Mail sender address (ex: mypolleninstance@me.com)

Configure database
------------------

Pollen use a database. By default, Pollen use a H2 database.

To modify the connection information, you can modify the following lines in
the configuration:

::

  jakarta.persistence.jdbc.user=sa
  jakarta.persistence.jdbc.password=

To change the database you want to use, you to modify the following properties:

::

  hibernate.dialect=Database dialect
  jakarta.persistence.jdbc.driver=Database driver
  jakarta.persistence.jdbc.url=Database connection url

Here are the properties for usual databases:

+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+
| Database                | hibernate.dialect                        | jakarta.persistence.jdbc.driver   | jakarta.persistence.jdbc.url             |
+=========================+==========================================+===================================+==========================================+
| H2                      | org.hibernate.dialect.H2Dialect          | org.h2.Driver                     | jdbc:h2:file:emplacementBaseH2/pollendb  |
+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+
| PostgreSql              | org.hibernate.dialect.PostgreSQLDialect  | org.postgresql.Driver             | jdbc:postgresql:pollen                   |
+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+
| MySql                   |  org.hibernate.dialect.MySQLDialect      | com.mysql.jdbc.Driver             | jdbc:mysql:pollen                        |
+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+

.. _options page: ./application-config-report.html
