.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

Pollen's votecounting methods
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pollen allows you to chose between several votecounting methods for
consultations.
Each method has its pros and cons.

Condorcet
---------

The Condorcet method has been created by the marquis de Condorcet in XVIIIth
century and that is aimed to find the best compromise between all the voters.

According to this method, the unique winner, if it exists, is the choice that,
compared to all the other choices one by one, is the most appreciated.

This method has been created to improve the first past the post method that
might not represent the wishes of the voters.

The major inconvenient of this method it that it is possible that no winner can
be designated (this is the Condorcet paradox). In this case, you have to use
another method to solve conflicts.

(Source : Wikipedia_ )

.. _Wikipedia: http://en.wikipedia.org/wiki/Condorcet_method

Percentage
----------

Each voter has 100 points to distribute between the choices (so the percentage).
The choice that has the greater number of points is declared winner.

This votecounting method has the main advantage of inciting sincere vote.

Approval
--------

The approval voting is a one round voting system. Each person select as many
choices as he/she want : none, one, several, all, but never two times the same.
The winner is the choice that has the greater number of votes.

This method is simple and easy. In Pollen, it is possible to limit the number of
choices, this permits to switch from the first-to-post voting (only one choice)
to approval voting (no limit of choice). This invites voters to vote sincerely.

Number
------

The consultation is held by many in one round. Each person
assign a number to each choice. You may also decide not
speak about choice and leave the textbox empty.

The recount shows for each choice:
 - The total number of votes
 - The number of white votes
 - The sum of votes among non-white
 - The average number of votes among non-white

Méthode Coombs
--------------

Coming soon in version 2.5 (http://chorem.org/issues/543)

Méthode instant-runoff
----------------------

Coming soon in version 2.5 (http://chorem.org/issues/541)

Méthode Borda
-------------

Coming soon in version 2.5 (http://chorem.org/issues/542)

