.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

~~~~~~~~~~~~~~~~~~~~~~~~
Security model in Pollen
~~~~~~~~~~~~~~~~~~~~~~~~

Abstract
========

- New way of dealing security in Pollen (1.5 ?)

Roles
=====

Pollen defines different roles for different usages. Some roles can be found
by the stored Pollen session (in servlet session), others can be found by the
accountId in some urls (example http://pollen/action/pollId:accountId).

Anonymous
---------

This is a not loggued user with nothing is url (no accountId) which can
identify himself.

Connected user
--------------

When a user has been registred inside Pollen and is connected in Pollen.

We can find him back by using the pollen user account stored in servlet session.

Admin
-----

Connected user with more privileges to manage everything in Pollen.

We can find him back by using the pollen user account stored in servlet session.

Poll creator
------------

Person which has created a poll. It acts as an admin for his very own poll.

We can find him back by using a accountId in url.

Poll voter
----------

A person which has voted to a poll.

We can find him back by using a accountId in url.

Poll participant
----------------

A person that can vote on a none free poll.

We can find him back by using a accountId in url.

Resources and permissions
=========================

User
----

A user (*UserAccount*) is a Pollen connected user.

- user:modify
- user:delete

VotingList
----------

A voting list (*VotingList*) is a list of person (name + email) to simplify
creation of restricted poll (you can import in one time a such list for a poll).

- votingList:modify
- votingList:delete

Poll
----

A poll (*Poll*) is life in Pollen!

- poll:create
- poll:modify
- poll:accesVote
- poll:vote
- poll:close
- poll:delete
- poll:clone
- poll:export
- poll:votecounting
- poll:comment
- poll:add choice

Choice
------

A choice (*Choice*) is a choice on which to vote in a poll.

- choice:modify
- choice:delete

Comment
-------

A comment (*Comment*) is a comment to be added on a poll.

- comment:modify
- comment:delete

Usage of wildcard
-----------------

To simplify usage of permissions, we will use some wildcard. For example, an
admin can do anything and has all permissions: ::

  user:*:*
  poll:*:* (only if poll is free, result public and continuous,...)
  choice:*:*
  comment:*:*

A poll creator will have all permissions on his poll: ::

  poll:*:pollId    (only if poll is free, result public and continuous,...)
  choice:*:pollId  (only if choice can add
  comment:*:pollId

But these examples are not quite exact. In facts, a admin or a poll creator could have

How to implement this security model
====================================

Based on Shiro (http://shiro.apache.org/):

- using authentication mecanism to find roles (http://shiro.apache.org/authentication-features.html)
- using permission mecanism to find role permissions (http://shiro.apache.org/permissions.html)

Global workflow
---------------

When a user wants to acces a resource (page), find out first the user role,
then compute his.

To tune this we will use a chache of permissions, beacuse it does seem very
realist to recompute at each request the cache for a poll or a connected user.

From this first cache:

- key re userAccount or accountId,
- values are his set of permissions,

we will build one form each poll:

- key is pollId
- values are users (on which permissions where build) involved by this poll.

At each poll modification, we then invalidate poll cache for the pollId entry
and then also simply invalidate the cache of all users involved by this poll.




