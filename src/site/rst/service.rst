.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

Pollen REST Service API
~~~~~~~~~~~~~~~~~~~~~~~

Pollen offers some public REST APIs to access to his data.

APIS
====

Authentication
--------------

- lostpassword GET /lostpassword/email : AuthService.lostPassword(email)
- login POST /login  {login/password} :  AuthService.login(login, password) -> token
- logout GET /logout {login} AuthService.logout(login)

User
----

- signin  PUT /user {...} : UserService.createUser(...) -> User
- get users GET /users UserService.getUsers() -> User[]
- get user profile GET /user[/userId] UserService.getUser(userId) -> User
- edit user profile POST /user {[userId +] properties) UserService.editUser(User) -> User
- validate email GET /validateemail/token UserService.validateEmail(token)

Voting List
-----------

- get user's list of voting list GET /votinglists  VotingListService.getUserVotingLists(userId) -> VotingList[]
- get voting list GET /votinglist/votinglistId VotingListService.getVotingList(vlId) -> VotingList
- create voting list PUT /votinglist {...} VotingListService.createVotingList(userId,...) -> VotingList
- edit voting list POST /votinglist {votinglistId,name, description, ..., but not voter} VotingListService.editVotingList(VotingList) -> VotingList
- delete voting list DELETE /votinglist/vlId VotingListService.deleteVotingList(vlId)
- add voter to voting list PUT /votinglist/voId/voter {...} VotingListService.addVoter(voId, Voter) -> Voter
- remove voter to voting list : DELETE /votinglist/{votinglistId}/voter/voterId  VotingListService.removeVoter(voId, Voter)

Poll
----

- list poll created: GET /user/userId/polls {filter=created}  PollService.getPollCreated(userId) : Poll[]
- list poll participed: GET /user/userId/polls {filter=participated} PollService.getPollParticipated(userId) : PollId[]
- list poll invited: GET /user/userId/polls {filter=invited} PollService.getPollInvited(userId) : PollId[]
- create poll: PUT /poll {...} PollService.createPoll(User,Poll) -> Poll
- delete poll: DELETE /poll/{pollId}   PollService.deletePoll(pollId)
- clone poll : PUT /poll/{fromPollId} ? PollService.clonePoll(fromPollId) -> Poll
- close poll : POST /poll/{pollId} {action=close} PollService.closePoll(pollId)
- export poll : GET /poll/{pollId} {action=export} PollService.exportPoll(pollId) -> File
- edit poll : POST /poll {...} PollService.editPoll(Poll) -> Poll
- import voting list : POST /poll/{pollId}/votingList/{votingListId} PollService.importVotingList(pollId, votingListId)

- get poll's choice: GET /poll/{pollId}/choices  getChoices(pollId) -> Choice[]
- get a choice: GET /poll/{pollId}/choices/{choiceID} getChoice(pollId, choiceId) -> Choice
- add choice in poll: PUT /poll/{pollId}/choices {choice} addChoice(pollId, choice) -> Choice
- edit choice in poll: POST /poll/{pollId}/choices {choice} editChoice(choice) -> Choice
- remove choice in poll: DELETE /poll/{pollId}/choices/{choiceId} deleteChoice(pollId, choiceId)

- get poll's invited: GET /poll/{pollId}/inviteds
- get a poll's invited: GET /poll/{pollId}/inviteds{invitedId}
- add invited:  PUT /poll/{pollId}/inviteds {invited}
- remove invited: DELETE /poll/{pollId}/inviteds/{invitedId}

Comment
-------

- get poll's comment: GET /poll/{pollId}/comments CommentService.getComments(pollId) -> Comment[]
- get a poll's comment: GET /poll/{pollId}/comments/{commentId} CommentService.getComment(commentId) -> Comment
- add comment: PUT /poll/{pollId}/comments {comment} CommentService.addComment(pollId, comment) -> Comment
- edit comment: POST /poll/{pollId}/comments {comment} CommentService.editComment(comment) -> Comment
- remove comment: DELETE /poll/{pollId}/comments/{commentId} CommentService.deleteComment(commentId)

Vote
----

- get poll's votes: GET /poll/{pollId}/votes VoteService.getVotes(pollId) -> Vote[]
- get a poll's vote: GET /poll/{pollId}/votes/{voteId} VoteService.getVote(voteId) -> Vote
- add vote: PUT /poll/{pollId}/votes {vote} VoteService.addVote(pollId, vote) -> Vote
- edit vote: POST /poll/{pollId}/votes {vote} VoteService.editVote(vote) -> Vote
- remove vote: DELETE /poll/{pollId}/votes/{voteId} VoteService.deleteVote(voteId)

Counting
--------

- winners list: GET /polls/{pollId}/result VoteCountingService.getResult(pollId) -> Result

each counting type can have specific result type, how handle it ? (table, image, ...) : dans l'objet de retour, on peut avoir une entrée qui donne le type avant de donner le contenu ?

Security
--------

See how to apply security, previous token way is a good choice (one token for poll, one token for user)


Idées / Questions
-----------------

- ne pas utiliser directement un blob mais un lien vers une table qui gère les blob (BlobEntity)
- dans les urls, faut-il mettre du pluriel ou du singulier ? /user/ ou /users/  ?  (YM : plutot pour le pluriel)

Security
========

We are using a security token for authenticated user given by the login request,
keep it until you want need it...

We should regenerate it at each request, and you should get it back then ?
