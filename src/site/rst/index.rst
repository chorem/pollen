.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

Welcome to Pollen website
~~~~~~~~~~~~~~~~~~~~~~~~~

Pollen is web based poll/vote application. It is aimed to small entities that
want to have a way to manage votes without giving their data to someone.

No need of user account...
--------------------------

- Pollen features textual, date or image vote.
- You can use different `vote counting methods`_ such as Condorcet
- You can vote anonymously.
- You can change your votes (even anonymous ones).
- You can receive notification and/or reminder emails.
- You can add some comments on a poll.
- You can restrict a poll to one or several lists of persons
  (for example, one by company or department).

But an account offers others features...
----------------------------------------

- You can found all polls you've created and participated.
- You can import voters lists from CSV files or LDAP.
- You can import/export votes/polls.

Use Pollen
----------

How to use Pollen?

- Use our `shared instance of Pollen`_, with no install on your own computer.
- `Download Pollen`_ and install it on our own computer following
   the `installation guide`_ .

But also...
-----------

- You can install Pollen on your own server to keep the control on your data
  (confidential vote for example).
- Pollen is Libre so you can modify it, improve it, adapt it to your needs.
- You can report bugs, features request and follow the project on our forge_

.. image:: home.png
   :alt: Pollen screenshot

.. _vote counting methods: ./methods.html
.. _shared instance of Pollen: http://pollen.chorem.org/pollen
.. _Download Pollen: http://www.chorem.org/projects/pollen/files
.. _installation guide: ./install.html
.. _forge: http://www.chorem.org/projects/pollen
