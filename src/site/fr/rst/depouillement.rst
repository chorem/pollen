.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

Les méthodes de dépouillement de Pollen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pollen permet différentes méthodes de dépouillement pour les consultations.
Chaque méthode a ses avantages et ses inconvénients.

Méthode Condorcet
-----------------

La méthode Condorcet est une méthode créée par le marquis de Condorcet au XVIIIe
siècle et qui a pour but de trouver le meilleur compromis entre tous les votants
ou sondés.

D'après cette méthode, l'unique vainqueur, s'il existe, est le choix qui comparé
à tous les autres choix un à un s'avèrerait être le plus apprécié.

Cette méthode a été créée pour améliorer la méthode uninominale à un tour qui
peut très bien ne pas représenter les désirs des électeurs.

L'inconvénient de cette méthode est qu'il est possible qu'aucun vainqueur ne
puisse être désigné (c'est le paradoxe de Condorcet). Dans ce cas, il faut
utiliser une autre méthode de résolution des conflits.

(Source : Wikipedia_ )

.. _Wikipedia: http://fr.wikipedia.org/wiki/M%C3%A9thode_Condorcet#Utilisation_du_vote_de_Condorcet

Méthode par pourcentage
-----------------------

Chaque électeur a 100 points à répartir entre les différents choix (d'où le
pourcentage). Le choix qui a le plus de points est déclaré vainqueur.

Ce dépouillement a pour principal avantage d'inciter le vote sincère, mais peut
se révéler complexe en cas de nombreux choix.

Méthode par assentiment
-----------------------

La consultation par assentiment se déroule en seul tour. Chaque personne
sélectionne autant de choix qu'elle le désire : aucun, un, plusieurs, tous, mais
jamais deux fois le même. Le choix vainqueur est celui qui a le plus de voix.

Cette méthode est simple et pratique. Dans Pollen, il est possible de limiter le
nombre de choix, cela permet de varier entre le scrutin uninominal à un tour et
le scrutin par assentiment, cela invite également les électeurs à voter
sincèrement.

Méthode par nombre
------------------

La consultation par nombre se déroule en seul tour. Chaque personne
attribue un nombre à chaque choix. On peut aussi décider de ne pas
s'exprimer à propos d'un choix et laisser le champ de saisie vide.

Le dépouillement indique pour chaque choix:
 - le nombre total de votes
 - le nombre de votes blanc
 - la somme des nombres parmi les votes non blanc
 - la moyenne des nombres parmi les votes non blanc

Méthode Coombs
--------------

A venir en version 2.5 (http://chorem.org/issues/543)

Méthode instant-runoff
----------------------

A venir en version 2.5 (http://chorem.org/issues/541)

Méthode Borda
-------------

A venir en version 2.5 (http://chorem.org/issues/542)
