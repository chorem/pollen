.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

Bienvenue sur le site de Pollen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pollen est une application de consultation web. Elle est destinée aux petites entités
qui veulent un moyen simple de gérer des consultations tout en restant maître de leurs
données.

Le but de Pollen n'est pas de permettre de faire du vote en ligne et de remplacer
les scrutins existant mais d'introduire de nouveaux usages de
consultation/sondage/vote là où il n'y en a pas. De ce fait aucune réflexion sur
la sécurité des votes a été conduite, car ce n'est pas notre but.

Pas besoin de créer de compte utilisateur...
--------------------------------------------

- Pollen permet de créer des consultations sur des textes, dates ou images.
- Vous pouvez utiliser plusieurs `modes de dépouillement`_ comme le Condorcet.
- Vous pouvez voter anonymement.
- Vous pouvez modifier vos votes (même anonyme).
- Vous pouvez recevoir des e-mails de notification et de rappel.
- Vous pouvez ajouter des commentaires à une consultation.
- Vous pouvez restreindre une consultation à une ou plusieurs listes de personnes
  (par exemple, une liste par entreprise ou département).   .

Mais cela vous offre d'autres possibilités...
---------------------------------------------

- Vous pouvez retrouver les consultations que vous avez créé ou celles
  auxquelles vous avez participé.
- Vous pouvez importer des listes de votants depuis des fichiers de CSV ou des
  annuaires LDAP.
- Vous pouvez importer/exporter des consultations.

Utiliser Pollen
---------------

Comment utiliser pollen ?

- Utiliser notre `instance de Pollen`_, sans rien installer sur votre machine.
- `Télécharger Pollen`_ puis l'installer sur votre machine en suivant le
  `guide d'installation`_ .

Mais encore...
--------------

- Vous pouvez installer Pollen sur votre propre serveur pour garder le contrôle
  de vos données (consultation confidentielle par exemple).
- Pollen est libre, vous pouvez le modifier, l'améliorer ou l'adapter a vos
  besoins.
- Vous pouvez nous remonter les bugs ou demandes d'améliorations et suivre le
  projet sur notre forge_ .

.. image:: home.png
   :alt: Page d'accueil de Pollen

.. _modes de dépouillement: ./depouillement.html
.. _instance de Pollen: http://pollen.chorem.org/pollen
.. _Télécharger Pollen: http://www.chorem.org/projects/pollen/files
.. _guide d'installation: ./install.html
.. _forge: http://www.chorem.org/projects/pollen
