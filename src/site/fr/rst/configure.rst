.. -
.. * #%L
.. * Pollen
.. * %%
.. * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

Configurer Pollen
~~~~~~~~~~~~~~~~~

Comment configurer Pollen ?
---------------------------

Toute la configuration de Pollen se fait via un fichier nommé **pollen.properties**.

Ce fichier peut se trouver à différents endroits sur la machine qui démarre
Pollen suivant le mode d'installation choisi :

::

  /etc/pollen.properties
  repertoireOuEstLancePollen/pollen.properties

A noter que si le fichier est déposé dans plusieurs endroits, alors les
configurations se cumulent.

Pour plus d'information sur les options disponibles,
rendez-vous sur la `page des options`_.

Configurer l'adresse publique
-----------------------------

L'adresse publique de Pollen est l'adresse utilisée dans les emails générés.

On peut la configurer via la propriété suivante de la configuration :

::

  siteUrl=Adresse publique de votre instance de pollen (utilisé pour générer les liens d'accès au sondage dans les emails envoyés)

Configurer l'envoi d'email
--------------------------

Pour que Pollen puisse envoyer des emails vous devez bien renseigner la
configuration:

::

  email_host= Serveur smtp pour envoyer les emails
  email_port= Port du serveur smtp
  email_from= Adresse de l'envoyeur de mail

Configurer la base de données
-----------------------------

Pollen utilise une base de données. Par défaut Pollen utilise une base type
h2.

Pour modifier la configuration de la base de données, on modifie dans le
fichier de configuration les lignes:

::

  jakarta.persistence.jdbc.user=sa
  jakarta.persistence.jdbc.password=

Pour changer de type de base données, il faut modifier modifier
les propriétés suivantes:

::

  hibernate.dialect=Dialect de la base
  jakarta.persistence.jdbc.driver=Driver de la base
  jakarta.persistence.jdbc.url=Url de connexion à la base

Voici un tableau des propriétés pour les bases usuelles:

+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+
| Type de base de données | hibernate.dialect                        | jakarta.persistence.jdbc.driver   | jakarta.persistence.jdbc.url             |
+=========================+==========================================+===================================+==========================================+
| H2                      | org.hibernate.dialect.H2Dialect          | org.h2.Driver                     | jdbc:h2:file:emplacementBaseH2/pollendb  |
+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+
| PostgreSql              | org.hibernate.dialect.PostgreSQLDialect  | org.postgresql.Driver             | jdbc:postgresql:pollen                   |
+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+
| MySql                   |  org.hibernate.dialect.MySQLDialect      | com.mysql.jdbc.Driver             | jdbc:mysql:pollen                        |
+-------------------------+------------------------------------------+-----------------------------------+------------------------------------------+

.. _page des options: ./application-config-report.html
