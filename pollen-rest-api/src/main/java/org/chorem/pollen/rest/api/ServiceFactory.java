package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2024 Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import org.chorem.pollen.services.PollenService;
import org.chorem.pollen.services.PollenServiceContext;
import org.chorem.pollen.services.service.ChoiceService;
import org.chorem.pollen.services.service.CommentService;
import org.chorem.pollen.services.service.CryptoService;
import org.chorem.pollen.services.service.FavoriteListService;
import org.chorem.pollen.services.service.FeedService;
import org.chorem.pollen.services.service.FeedbackService;
import org.chorem.pollen.services.service.GtuService;
import org.chorem.pollen.services.service.NotificationService;
import org.chorem.pollen.services.service.PollService;
import org.chorem.pollen.services.service.PollenResourceService;
import org.chorem.pollen.services.service.PollenUIUrlRenderService;
import org.chorem.pollen.services.service.PollenUserService;
import org.chorem.pollen.services.service.ReportService;
import org.chorem.pollen.services.service.SocialAuthService;
import org.chorem.pollen.services.service.TransverseService;
import org.chorem.pollen.services.service.VoteCountingService;
import org.chorem.pollen.services.service.VoteCountingTypeService;
import org.chorem.pollen.services.service.VoteService;
import org.chorem.pollen.services.service.VoterListService;
import org.chorem.pollen.services.service.mail.EmailService;
import org.chorem.pollen.services.service.mail.MailBoxService;
import org.chorem.pollen.services.service.security.SecurityService;

import java.io.Serializable;

import static org.chorem.pollen.rest.api.PollenRestApiRequestFilter.SERVICE_CONTEXT_PARAMETER;

@RequestScoped
public class ServiceFactory implements Serializable {

    @Inject
    protected HttpServletRequest servletRequest;

    @Produces @RequestScoped
    public ChoiceService getChoiceService() {
        return createService(ChoiceService.class);
    }

    @Produces @RequestScoped
    public CommentService getCommentService() {
        return createService(CommentService.class);
    }

    @Produces @RequestScoped
    public FavoriteListService getFavoriteListService() {
        return createService(FavoriteListService.class);
    }

    @Produces @RequestScoped
    public EmailService getEmailService() {
        return createService(EmailService.class);
    }

    @Produces @RequestScoped
    public NotificationService getNotificationService() {
        return createService(NotificationService.class);
    }

    @Produces @RequestScoped
    public FeedService getFeedService() {
        return createService(FeedService.class);
    }

    @Produces @RequestScoped
    public PollService getPollService() {
        return createService(PollService.class);
    }

    @Produces @RequestScoped
    public SecurityService getSecurityService() {
        return createService(SecurityService.class);
    }

    @Produces @RequestScoped
    public ReportService getReportService() {
        return createService(ReportService.class);
    }

    @Produces @RequestScoped
    public PollenResourceService getPollenResourceService() {
        return createService(PollenResourceService.class);
    }

    @Produces @RequestScoped
    public PollenUIUrlRenderService getPollenUIUrlRenderService() {
        return createService(PollenUIUrlRenderService.class);
    }

    @Produces @RequestScoped
    public PollenUserService getPollenUserService() {
        return createService(PollenUserService.class);
    }

    @Produces @RequestScoped
    public VoteCountingService getVoteCountingService() {
        return createService(VoteCountingService.class);
    }

    @Produces @RequestScoped
    public VoterListService getVoterListService() {
        return createService(VoterListService.class);
    }

    @Produces @RequestScoped
    public VoteCountingTypeService getVoteCountingTypeService() {
        return createService(VoteCountingTypeService.class);
    }

    @Produces @RequestScoped
    public VoteService getVoteService() {
        return createService(VoteService.class);
    }

    @Produces @RequestScoped
    public FeedbackService getFeedbackService() {
        return createService(FeedbackService.class);
    }

    @Produces @RequestScoped
    public SocialAuthService getSocialAuthService() {
        return createService(SocialAuthService.class);
    }

    @Produces @RequestScoped
    public TransverseService getTransverseService() {
        return createService(TransverseService.class);
    }

    @Produces @RequestScoped
    public GtuService getGtuService() {
        return  createService(GtuService.class);
    }

    @Produces @RequestScoped
    public MailBoxService getMailBoxService() {
        return  createService(MailBoxService.class);
    }

    @Produces @RequestScoped
    public CryptoService getCryptoService() {
        return  createService(CryptoService.class);
    }

    protected <S extends PollenService> S createService(Class<S> serviceClass) {
        PollenServiceContext serviceContext = (PollenServiceContext) servletRequest.getAttribute(SERVICE_CONTEXT_PARAMETER);
        return serviceContext.newService(serviceClass);
    }
}
