package org.chorem.pollen.rest.api.converter;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.ext.ParamConverter;
import org.chorem.pollen.rest.api.PollenRestApiApplicationContext;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactory;

/**
 * Created on 5/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenEntityIdConverter<E extends TopiaEntity> implements ParamConverter<PollenEntityId<E>> {

    protected final Class<E> type;

    public PollenEntityIdConverter(Class<E> type) {
        this.type = type;
    }

    @Override
    public String toString(PollenEntityId<E> object) {
        return object.getReducedId();
    }

    @Override
    public PollenEntityId<E> fromString(String string) {
        PollenEntityId<E> result = PollenEntityId.newId(type);
        result.setReducedId(string);

        PollenRestApiApplicationContext applicationContext = PollenRestApiApplicationContext.getApplicationContext();

        TopiaIdFactory topiaIdFactory = applicationContext.getTopiaApplicationContext().getTopiaIdFactory();
        result.decode(topiaIdFactory);

        return result;
    }
}
