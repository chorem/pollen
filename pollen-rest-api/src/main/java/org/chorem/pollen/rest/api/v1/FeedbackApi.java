package org.chorem.pollen.rest.api.v1;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import org.apache.commons.codec.digest.DigestUtils;
import org.chorem.pollen.services.bean.FeedbackBean;
import org.chorem.pollen.services.service.FeedbackService;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FeedbackApi {

    @Inject
    private FeedbackService feedbackService;

    @Path("/feedback")
    @POST
    public boolean addFeedBack(@Context HttpServletRequest servletRequest,
                               FeedbackBean feedbackBean) {
        // Hash sessionId to prevent session hijacking
        return feedbackService.addFeedback(feedbackBean,
                new DigestUtils("SHA3-256").digestAsHex(servletRequest.getSession().getId()));
    }
}
