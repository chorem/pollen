package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.gson.Gson;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.brickred.socialauth.SocialAuthManager;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.chorem.pollen.persistence.entity.UserCredential;
import org.chorem.pollen.rest.api.beans.ChangePasswordBean;
import org.chorem.pollen.services.bean.*;
import org.chorem.pollen.services.bean.resource.ResourceFileBean;
import org.chorem.pollen.services.bean.resource.ResourceStreamBean;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.PollenResourceService;
import org.chorem.pollen.services.service.PollenUserService;
import org.chorem.pollen.services.service.SocialAuthService;
import org.chorem.pollen.services.service.security.PollenDefaultEmailAddressException;
import org.chorem.pollen.services.service.security.PollenEmailNotValidatedException;
import org.chorem.pollen.services.service.security.PollenInvalidEmailActivationTokenException;
import org.chorem.pollen.services.service.security.SecurityService;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import java.util.Map;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PollenUserApi {

    @Inject
    private PollenUserService pollenUserService;

    @Inject
    private SecurityService securityService;

    @Inject
    private SocialAuthService socialAuthService;

    @Inject
    private PollenResourceService pollenResourceService;

    @Path("/users")
    @GET
    @GZIP
    public PaginationResultBean<PollenUserBean> getUsers(@BeanParam PaginationParameterBean paginationParameter,
                                                         @QueryParam("search") String search) {
        return pollenUserService.getUsers(paginationParameter, search);
    }

    @Path("/user")
    @GET
    @GZIP
    public PollenUserBean getConnectedUser() {
        return pollenUserService.getUser();
    }

    @Path("/users/{userId}")
    @GET
    @GZIP
    public PollenUserBean getUser(@PathParam("userId") PollenEntityId<PollenUser> userId) {
        return pollenUserService.getUser(userId.getEntityId());
    }

    @Path("/users")
    @POST
    public PollenEntityRef<PollenUser> createUser(PollenUserBean user) throws InvalidFormException {
        return pollenUserService.createUser(user);
    }

    @Path("/users/{userId}")
    @POST
    public PollenUserBean editUser(PollenUserBean user) throws InvalidFormException {
        return pollenUserService.editUser(user);
    }

    @Path("/users/{userId}")
    @DELETE
    public Response deleteUser(@PathParam("userId") PollenEntityId<PollenUser> userId,
                               @QueryParam("anonymize") boolean anonymize) {
        boolean selfDeletion = pollenUserService.deleteUser(userId.getEntityId(), anonymize);
        if (selfDeletion) {
            securityService.logout();
        }
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Path("/users/{userId}")
    @PUT
    public void validateUserEmail(@PathParam("userId") PollenEntityId<PollenUser> userId,
                                  @QueryParam("token") String token) throws PollenInvalidEmailActivationTokenException {
        pollenUserService.validateUserEmail(userId.getEntityId(), token);
    }

    @Path("/users/{userId}/email/{emailAddressId}")
    @PUT
    public void validateUserEmail(@PathParam("userId") PollenEntityId<PollenUser> userId,
                                  @PathParam("emailAddressId") PollenEntityId<PollenUserEmailAddress> emailAddressId)
            throws PollenInvalidEmailActivationTokenException {
        pollenUserService.validateUserEmailByAdmin(userId.getEntityId(), emailAddressId.getEntityId());
    }

    @Path("/user/password")
    @PUT
    @POST
    public void changePassword(ChangePasswordBean bean) throws InvalidFormException {
        pollenUserService.changePassword(bean.getOldPassword(), bean.getNewPassword());
    }

    @Path("/user/credentials/{provider}")
    @POST
    public String addUserCredential(@Context HttpServletRequest request,
                                    String providerReturn) throws Exception {
        SocialAuthManager socialAuthManager =
                (SocialAuthManager) request.getSession().getAttribute(ApiUtils.SOCIAL_AUTH_MANAGER_SESSION_KEY);
        //socialAuthManager
        request.getSession().removeAttribute(ApiUtils.SOCIAL_AUTH_MANAGER_SESSION_KEY);
        Gson gson = new Gson();
        Map<String, String> paramsMap = gson.fromJson(providerReturn, Map.class);
        return socialAuthService.addCredentialToUser(socialAuthManager, paramsMap);
    }

    @Path("/user/credentials/{credentialId}")
    @DELETE
    public void deleteUserCredential(@PathParam("credentialId") PollenEntityId<UserCredential> credentialId) {
        socialAuthService.deleteUserCredential(credentialId);
    }

    @Path("/user/avatar/{provider}")
    @POST
    public void setAvatar(@Context HttpServletRequest request,
                          String providerReturn) throws Exception {
        SocialAuthManager socialAuthManager =
                (SocialAuthManager) request.getSession().getAttribute(ApiUtils.SOCIAL_AUTH_MANAGER_SESSION_KEY);
        //socialAuthManager
        request.getSession().removeAttribute(ApiUtils.SOCIAL_AUTH_MANAGER_SESSION_KEY);
        Gson gson = new Gson();
        Map<String, String> paramsMap = gson.fromJson(providerReturn, Map.class);
        socialAuthService.setAvatarToUser(socialAuthManager, paramsMap);
    }

    @Path("/users/{userId}/avatar")
    @GET
    public Response getUserAvatar(@PathParam("userId") PollenEntityId<PollenUser> userId) {
        ResourceStreamBean resource = pollenResourceService.getAvatar(userId.getEntityId());
        return Response.ok(resource.getResourceContent(), resource.getContentType()).build();
    }

    @Path("/user/avatar")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public PollenEntityRef<PollenResource> createResource(MultipartFormDataInput input) throws InvalidFormException {
        ResourceFileBean resourceBean = ApiUtils.multipartToResourceBean(input, "avatar");
        return pollenUserService.setAvatar(resourceBean);
    }

    @Path("/user/avatar")
    @DELETE
    public void deleteUserAvatar() {
        pollenUserService.deleteAvatar();
    }

    @Path("/users/{userId}/avatar")
    @DELETE
    public void deleteUserAvatar(@PathParam("userId") PollenEntityId<PollenUser> userId) {
        pollenUserService.deleteAvatar(userId.getEntityId());
    }

    @Path("/user/email")
    @POST
    public PollenEntityRef<PollenUserEmailAddress> addEmailAddress(String emailAddress) throws InvalidFormException {
        return pollenUserService.addEmailAddress(emailAddress);
    }

    @Path("/users/{userId}/email")
    @POST
    public PollenEntityRef<PollenUserEmailAddress> addEmailAddress(@PathParam("userId") PollenEntityId<PollenUser> userId,
                                                                   String emailAddress) throws InvalidFormException {
        return pollenUserService.addEmailAddress(userId.getEntityId(), emailAddress);
    }

    @Path("/user/email/default")
    @PUT
    public void setDefaultEmailAddress(@QueryParam("emailAddressId") PollenEntityId<PollenUserEmailAddress> emailAddressId)
            throws PollenEmailNotValidatedException {
        pollenUserService.setDefaultEmailAddress(emailAddressId.getEntityId());
    }

    @Path("/users/{userId}/email/default")
    @PUT
    public void setDefaultEmailAddress(@PathParam("userId") PollenEntityId<PollenUser> userId,
                                       @QueryParam("emailAddressId") PollenEntityId<PollenUserEmailAddress> emailAddressId)
            throws PollenEmailNotValidatedException {
        pollenUserService.setDefaultEmailAddress(userId.getEntityId(), emailAddressId.getEntityId());
    }

    @Path("/user/email/{emailAddressId}")
    @DELETE
    public void removeEmailAddress(@PathParam("emailAddressId") PollenEntityId<PollenUserEmailAddress> emailAddressId)
            throws PollenDefaultEmailAddressException {
        pollenUserService.removeEmailAddress(emailAddressId.getEntityId());
    }

    @Path("/users/{userId}/email/{emailAddressId}")
    @DELETE
    public void removeEmailAddress(@PathParam("userId") PollenEntityId<PollenUser> userId,
                                   @PathParam("emailAddressId") PollenEntityId<PollenUserEmailAddress> emailAddressId)
            throws PollenDefaultEmailAddressException {
        pollenUserService.removeEmailAddress(userId.getEntityId(), emailAddressId.getEntityId());
    }

    @Path("/users/{userId}/email/{emailAddressId}")
    @POST
    public PollenUserEmailAddressBean saveEmailAddress(@PathParam("userId") PollenEntityId<PollenUser> userId,
                                                       PollenUserEmailAddressBean emailAddress)
            throws PollenDefaultEmailAddressException {
        return pollenUserService.editEmailAddress(userId.getEntityId(), emailAddress);
    }

    @Path("/user/email/{emailAddressId}")
    @POST
    public PollenUserEmailAddressBean saveEmailAddress(PollenUserEmailAddressBean emailAddress)
            throws PollenDefaultEmailAddressException {
        return pollenUserService.editEmailAddress(emailAddress);
    }
}
