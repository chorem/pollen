package org.chorem.pollen.rest.api.beans;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.services.bean.PollBean;
import org.chorem.pollen.services.bean.VoterListBean;
import org.chorem.pollen.services.bean.VoterListMemberBean;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollCreateBean {

    protected PollBean poll;

    protected List<VoterListBean> voterLists;

    protected List<VoterListMemberBean> voterListMembers;

    public PollBean getPoll() {
        return poll;
    }

    public void setPoll(PollBean poll) {
        this.poll = poll;
    }

    public List<VoterListBean> getVoterLists() {
        return voterLists;
    }

    public void setVoterLists(List<VoterListBean> voterLists) {
        this.voterLists = voterLists;
    }

    public List<VoterListMemberBean> getVoterListMembers() {
        return voterListMembers;
    }

    public void setVoterListMembers(List<VoterListMemberBean> voterListMembers) {
        this.voterListMembers = voterListMembers;
    }
}
