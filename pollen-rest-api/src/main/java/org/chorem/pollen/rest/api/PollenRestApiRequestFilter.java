package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.container.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.NewCookie;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.MDC;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.PollenServiceContext;
import org.chorem.pollen.services.PollenUIContext;
import org.chorem.pollen.services.service.security.PollenCypherTechnicalException;
import org.chorem.pollen.services.service.security.PollenInvalidSessionTokenException;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.chorem.pollen.services.service.security.SecurityService;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Inject {@link } in services.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Provider
@PreMatching
public class PollenRestApiRequestFilter implements ContainerRequestFilter, ContainerResponseFilter {

    public static final String SERVICE_CONTEXT_PARAMETER = "pollen_PollenServiceContext";
    private static final String SECURITY_CONTEXT_PARAMETER = "pollen_PollenSecurityContext";
    public static final String REQUEST_PERMISSION_PARAMETER = "permission";
    public static final String REQUEST_HEADER_SESSION_TOKEN = "X-Pollen-Session-Token";
    public static final String REQUEST_HEADER_UI_CONTEXT = "X-Pollen-UI-context";
    public static final ImmutableList<Locale> ACCEPT_LANGUAGES = ImmutableList.of(Locale.FRENCH, Locale.ENGLISH);
    public static final Locale DEFAULT_LANGUAGE = Locale.FRENCH;
    public static final String COOKIE_POLLEN_AUTH = "pollen-auth";
    public static final int COOKIE_MAX_AGE = 60 * 60 * 24 * 365; // 1 year
    private static final String HEADER_ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    private static final String START_TIME = "startTime";
    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(PollenRestApiRequestFilter.class);
    @Context
    protected ResourceInfo resourceInfo;
    @Context
    protected HttpServletRequest servletRequest;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        MDC.clear();

        if (!HttpMethod.OPTIONS.equals(containerRequestContext.getMethod())) {
            // Start monitoring
            long start = System.currentTimeMillis();
            containerRequestContext.setProperty(START_TIME, start);
        }
        try {
            pushRequestContext(containerRequestContext);
        } catch (PollenInvalidSessionTokenException | PollenCypherTechnicalException e) {
            Response.ResponseBuilder builder = Response.status(Response.Status.UNAUTHORIZED)
                    .entity(e.getMessage());
            containerRequestContext.abortWith(builder.build());
        }
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        MultivaluedMap<String, Object> headers = containerResponseContext.getHeaders();

        if (HttpMethod.OPTIONS.equals(containerRequestContext.getMethod())) {
            if (Response.Status.OK == containerResponseContext.getStatusInfo()) {
                // add CORS response headers
                String requestHeaders = containerResponseContext.getHeaderString(HEADER_ACCESS_CONTROL_REQUEST_HEADERS);

                if (StringUtils.isNotBlank(requestHeaders)) {
                    headers.add(HEADER_ACCESS_CONTROL_ALLOW_HEADERS, requestHeaders);
                } else {
                    headers.add(HEADER_ACCESS_CONTROL_ALLOW_HEADERS, "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, " + REQUEST_HEADER_UI_CONTEXT);
                }
            }
        } else {
            if (containerResponseContext.getMediaType() != null && !"image".equals(containerResponseContext.getMediaType().getType())) {
                // add cookies
                addTokenToResponse(containerResponseContext);
            }
        }

        String origin = containerRequestContext.getHeaderString("Origin");
        if (origin != null) {
            headers.add(HEADER_ACCESS_CONTROL_ALLOW_ORIGIN, origin);
            headers.add(HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
            headers.add(HEADER_ACCESS_CONTROL_ALLOW_METHODS, "GET, POST, DELETE, PUT, OPTIONS");
        }

        if (!HttpMethod.OPTIONS.equals(containerRequestContext.getMethod())) {
            long start = Long.class.cast(containerRequestContext.getProperty(START_TIME));
            long end = System.currentTimeMillis();

            String httpMethod = containerRequestContext.getMethod();
            String uri = containerRequestContext.getUriInfo().getPath();
            String query = containerRequestContext.getUriInfo().getQueryParameters()
                    .entrySet()
                    .stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue().stream().collect(Collectors.joining(",")))
                    .collect(Collectors.joining("&"));
            String classApi = resourceInfo.getResourceClass() == null ? "???" : resourceInfo.getResourceClass().getSimpleName();
            String methodApi = resourceInfo.getResourceMethod() == null ? "???" : resourceInfo.getResourceMethod().getName();
            int httpCode = containerResponseContext.getStatus();

            log.info(String.format("%s %s?%s ==> %s.%s ==> %d : %dms",
                    httpMethod, uri, query,
                    classApi, methodApi,
                    httpCode, end - start));
        }
    }

    private void pushRequestContext(ContainerRequestContext context) throws PollenInvalidSessionTokenException, PollenCypherTechnicalException {
        Locale locale = getUserLocale(context);

        PollenRestApiApplicationContext applicationContext = PollenRestApiApplicationContext.getApplicationContext();

        PollenPersistenceContext persistenceContext =
                PollenTopiaTransactionFilter.getPersistenceContext(context);

        PollenServiceContext serviceContext = applicationContext.newServiceContext(persistenceContext, locale);
        servletRequest.setAttribute(SERVICE_CONTEXT_PARAMETER, serviceContext);

        PollenSecurityContext securityContext = createSecurityContext(context, applicationContext, serviceContext);
        serviceContext.setSecurityContext(securityContext);
        servletRequest.setAttribute(SECURITY_CONTEXT_PARAMETER, securityContext);

        serviceContext.setUIContext(extractUIContext(context));
    }

    private PollenUIContext extractUIContext(ContainerRequestContext context) {
        String uiContextJson = context.getHeaderString(REQUEST_HEADER_UI_CONTEXT);
        PollenUIContext uiContext = null;
        if (uiContextJson != null) {
            Gson gson = new Gson();
            uiContext = gson.fromJson(uiContextJson, PollenUIContext.class);
        }
        return uiContext;
    }

    private PollenSecurityContext createSecurityContext(ContainerRequestContext context,
                                                        PollenRestApiApplicationContext applicationContext,
                                                        PollenServiceContext serviceContext) throws PollenInvalidSessionTokenException, PollenCypherTechnicalException {
        SecurityService securityService = serviceContext.newService(SecurityService.class);

        String sessionId = servletRequest.getSession(true).getId();
        MDC.put("session", sessionId);

        // --- get session token (from request parameters) --- //
        String sessionTokenHeader = context.getHeaderString(REQUEST_HEADER_SESSION_TOKEN);

        if (StringUtils.isEmpty(sessionTokenHeader)) {
            // --- get session token (from request cookies) --- //
            Cookie cookie = context.getCookies().get(COOKIE_POLLEN_AUTH);
            if (cookie != null) {

                if (log.isDebugEnabled()) {
                    log.debug("Found pollen-auth cookie:: " + cookie.getValue());
                }
                sessionTokenHeader = cookie.getValue();
            }
        }

        PollenUser userConnected = securityService.getUserFromToken(sessionTokenHeader);
        MDC.put("user", userConnected == null ? "Anonyme" : securityService.getReduceId(userConnected));

        // --- get mainPrincipal (from request parameters) --- //
        String permission = null;
        List<String> permissions = context.getUriInfo().getQueryParameters().get(REQUEST_PERMISSION_PARAMETER);
        if (CollectionUtils.isNotEmpty(permissions)) {
            permission = permissions.get(0);
            MDC.put("permission", permission);
        }
        PollenPrincipal mainPrincipal = securityService.getPollenPrincipalByPermissionToken(permission);

        // --- create security context --- //
        return applicationContext.newSecurityContext(userConnected, mainPrincipal);
    }

    private Locale getUserLocale(ContainerRequestContext context) {
        Locale language = context.getLanguage();

        if (!ACCEPT_LANGUAGES.contains(language)) {
            List<Locale> languages = context.getAcceptableLanguages();

            if (log.isDebugEnabled()) {
                log.debug("Found Accept-Language: " + languages.stream()
                        .map(Locale::getDisplayName)
                        .reduce((l1, l2) -> l1 + ", " + l2));
            }

            language = languages.stream()
                    .filter(ACCEPT_LANGUAGES::contains)
                    .findFirst()
                    .orElse(DEFAULT_LANGUAGE);
        }

        return language;
    }

    private void addTokenToResponse(ContainerResponseContext containerResponseContext) {
        PollenServiceContext serviceContext = (PollenServiceContext) servletRequest.getAttribute(SERVICE_CONTEXT_PARAMETER);
        SecurityService securityService = serviceContext.newService(SecurityService.class);

        if (securityService == null) {
            if (log.isErrorEnabled()) {
                log.error("securityService no in REST context");
            }
        } else {
            String token = securityService.getToken();
            NewCookie authCookie = new NewCookie.Builder(COOKIE_POLLEN_AUTH)
                    .value(token)
                    .path("/")
                    .maxAge(StringUtils.isNotBlank(token) ? COOKIE_MAX_AGE : 0)
                    .secure(true)
                    .httpOnly(false)
                    .sameSite(NewCookie.SameSite.LAX)
                    .build();
            containerResponseContext.getHeaders().add(HttpHeaders.SET_COOKIE, authCookie);
        }
    }
}
