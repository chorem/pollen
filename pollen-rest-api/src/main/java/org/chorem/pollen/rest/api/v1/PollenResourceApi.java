package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.rest.api.beans.Resource64Bean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.resource.ResourceFileBean;
import org.chorem.pollen.services.bean.resource.ResourceMetaBean;
import org.chorem.pollen.services.bean.resource.ResourceStreamBean;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.PollenResourceService;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 * Created on 10/07/14.
 *
 * @author dralagen
 * @since 2.0
 */
@Path("")
public class PollenResourceApi {

    @Inject
    private PollenResourceService pollenResourceService;

    @Path("/resources/{resourceId}")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getResource(@PathParam("resourceId") PollenEntityId<PollenResource> resourceId,
                                @Context Request req) {
        ResourceStreamBean resource = pollenResourceService.getResource(resourceId.getEntityId());
        if (resource == null) {
            return Response.noContent().build();
        }
        // Cache policy (cache of 1 year as this ressource should never change)
        CacheControl cc = new CacheControl();
        cc.setMaxAge(31536000);
        //Calculate the ETag on resource ID
        EntityTag etag = new EntityTag(resource.getEntityId());
        //Verify if it matched with etag available in http request
        Response.ResponseBuilder rb = req.evaluatePreconditions(etag);
        //If ETag matches the rb will be non-null;
        //Use the rb to return the response without any further processing
        if (rb != null) {
            return rb.cacheControl(cc).tag(etag).build();
        }
        //If rb is null then either it is first time request; or resource is modified
        //Get the updated representation and return with Etag attached to it
        rb = Response.ok(resource.getResourceContent(), resource.getContentType()).cacheControl(cc).tag(etag);
        return rb.build();
    }

    @Path("/resources/{resourceId}/download")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getDownloadResource(@PathParam("resourceId") PollenEntityId<PollenResource> resourceId) {
        ResourceStreamBean resource = pollenResourceService.getResource(resourceId.getEntityId());
        return ApiUtils.ResponseDownload(resource.getResourceContent(), resource.getName(), resource.getContentType());
    }

    @Path("/resources/{resourceId}/preview")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getPreviewResource(@PathParam("resourceId") PollenEntityId<PollenResource> resourceId,
                                       @QueryParam("maxDimension") boolean maxDimension) {
        ResourceStreamBean resource = pollenResourceService.getResourcePreview(resourceId.getEntityId(), maxDimension);
        if (resource == null) {
            return Response.noContent().build();
        }
        // Cache policy (cache of 1 year as this ressource should never change)
        CacheControl cc = new CacheControl();
        cc.setMaxAge(31536000);
        Response.ResponseBuilder builder = Response.ok(resource.getResourceContent(), resource.getContentType());
        return builder.cacheControl(cc).build();
    }

    @Path("/resources/{resourceId}/meta")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceMetaBean getMetaResource(@PathParam("resourceId") PollenEntityId<PollenResource> resourceId) {
        return pollenResourceService.getMetaResource(resourceId.getEntityId());
    }

    @Path("/resources")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public PollenEntityRef<PollenResource> createResource(MultipartFormDataInput input,
                                                          @QueryParam("width") Integer maxWidth,
                                                          @QueryParam("height") Integer maxHeight) throws InvalidFormException {
        ResourceFileBean resourceBean;
        if (maxWidth != null || maxHeight != null) {
            resourceBean = ApiUtils.imageMultipartToResourceBean(input, "resource", maxWidth, maxHeight);
        } else {
            resourceBean = ApiUtils.multipartToResourceBean(input, "resource");
        }
        return pollenResourceService.createResource(resourceBean);
    }

    @Path("/resources64")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PollenEntityRef<PollenResource> createResource64(Resource64Bean input) throws InvalidFormException {
        ResourceFileBean resourceBean = ApiUtils.resource64ToResourceBean(input);
        return pollenResourceService.createResource(resourceBean);
    }

    @Path("/resources/{resourceId}")
    @DELETE
    public void deleteResource(@PathParam("resourceId") PollenEntityId<PollenResource> resourceId) {
        pollenResourceService.deleteResource(resourceId.getEntityId());
    }
}
