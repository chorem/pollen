package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import jakarta.servlet.ServletContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.PollenTopiaApplicationContext;
import org.chorem.pollen.persistence.PollenTopiaPersistenceContext;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.DefaultPollenServiceContext;
import org.chorem.pollen.services.PollenApplicationContext;
import org.chorem.pollen.services.PollenServiceContext;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.PollenUserService;
import org.chorem.pollen.services.service.security.DefaultPollenSecurityContext;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.chorem.pollen.votecounting.VoteCountingFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.i18n.init.I18nInitializer;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;

import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * PollenRestApiApplicationContext
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenRestApiApplicationContext implements PollenApplicationContext {

    protected static final String APPLICATION_CONTEXT_PARAMETER = "pollen_PollenApplicationContext";
    protected static PollenRestApiApplicationContext applicationContext;
    private static Log log = LogFactory.getLog(PollenRestApiApplicationContext.class);
    protected final AtomicBoolean started;
    protected final AtomicBoolean closed;
    protected final PollenTopiaApplicationContext topiaApplicationContext;
    protected final PollenServicesConfig applicationConfig;
    protected VoteCountingFactory voteCountingFactory;

    protected PollenRestApiApplicationContext(PollenServicesConfig applicationConfig, PollenTopiaApplicationContext topiaApplicationContext) {
        Preconditions.checkNotNull(applicationConfig, "Configuration can not be null!");
        Preconditions.checkNotNull(topiaApplicationContext, "topiaApplicationContext can not be null!");

        this.applicationConfig = applicationConfig;
        this.topiaApplicationContext = topiaApplicationContext;
        this.started = new AtomicBoolean(false);
        this.closed = new AtomicBoolean(false);
    }

    public static PollenRestApiApplicationContext getApplicationContext() {
        if (applicationContext == null) {
            PollenServicesConfig applicationConfig = new PollenServicesConfig("pollen-rest-api.properties");

            Map<String, String> topiaProperties = applicationConfig.getTopiaProperties();

            BeanTopiaConfiguration topiaConfiguration = new TopiaConfigurationBuilder().readMap(topiaProperties);
            PollenTopiaApplicationContext pollenTopiaApplicationContext = new PollenTopiaApplicationContext(topiaConfiguration);

            applicationContext = new PollenRestApiApplicationContext(applicationConfig, pollenTopiaApplicationContext);

            applicationContext.init();
        }
        return applicationContext;
    }

    public static void setApplicationContext(PollenRestApiApplicationContext applicationContext) {
        PollenRestApiApplicationContext.applicationContext = applicationContext;
    }

    public static PollenRestApiApplicationContext getApplicationContext(ServletContext servletContext) {
        return (PollenRestApiApplicationContext) servletContext.getAttribute(APPLICATION_CONTEXT_PARAMETER);
    }

    public static void setApplicationContext(ServletContext servletContext,
                                             PollenRestApiApplicationContext applicationContext) {
        servletContext.setAttribute(APPLICATION_CONTEXT_PARAMETER, applicationContext);
    }

    @Override
    public PollenTopiaApplicationContext getTopiaApplicationContext() {
        return topiaApplicationContext;
    }

    @Override
    public PollenServicesConfig getApplicationConfig() {
        return applicationConfig;
    }

    @Override
    public VoteCountingFactory getVoteCountingFactory() {
        if (voteCountingFactory == null) {
            voteCountingFactory = new VoteCountingFactory();
        }
        return voteCountingFactory;
    }

    @Override
    public PollenTopiaPersistenceContext newPersistenceContext() {
        return topiaApplicationContext.newPersistenceContext();
    }

    @Override
    public PollenServiceContext newServiceContext(PollenPersistenceContext persistenceContext, Locale locale) {
        DefaultPollenServiceContext newServiceContext = new DefaultPollenServiceContext();
        newServiceContext.setPollenServicesConfig(applicationConfig);
        newServiceContext.setTopiaApplicationContext(topiaApplicationContext);
        newServiceContext.setPersistenceContext(persistenceContext);
        newServiceContext.setVoteCountingFactory(getVoteCountingFactory());
        newServiceContext.setLocale(locale);
        return newServiceContext;

    }

    @Override
    public PollenSecurityContext newSecurityContext(PollenUser pollenUser, PollenPrincipal mainPrincipal) {
        DefaultPollenSecurityContext securityContext = new DefaultPollenSecurityContext();
        securityContext.setPollenUser(pollenUser);
        securityContext.setMainPrincipal(mainPrincipal);
        return securityContext;
    }

    @Override
    public void close() {
        if (closed.get()) {
            if (log.isWarnEnabled()) {
                log.warn("Already closed");
            }
            return;
        }

        if (!started.get()) {
            if (log.isWarnEnabled()) {
                log.warn("Not started");
            }
            return;
        }

        if (topiaApplicationContext != null && !topiaApplicationContext.isClosed()) {
            if (log.isInfoEnabled()) {
                log.info("stopping Pollen, will close persistence context");
            }
            topiaApplicationContext.close();
        }

        closed.set(true);
        started.set(false);
    }

    @Override
    public void init() {
        if (started.get()) {
            if (log.isWarnEnabled()) {
                log.warn("Already started!");
            }
            return;
        }

        Preconditions.checkState(applicationConfig != null, "No configuration initialized!");
        Preconditions.checkState(topiaApplicationContext != null, "No topiaApplicationContext initialized!");

        if (applicationConfig.isLogConfigurationProvided()) {
            File log4jConfigurationFile = applicationConfig.getLogConfigurationFile();
            String log4jConfigurationFileAbsolutePath = log4jConfigurationFile.getAbsolutePath();
            if (log4jConfigurationFile.exists()) {
                if (log.isInfoEnabled()) {
                    log.info("will use logging configuration " + log4jConfigurationFileAbsolutePath);
                }

                // reset logger configuration
                LogManager.resetConfiguration();

                // use generate log config file
                PropertyConfigurator.configure(log4jConfigurationFileAbsolutePath);

                log = LogFactory.getLog(PollenRestApiApplicationContext.class);
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("there is no file " + log4jConfigurationFileAbsolutePath + ". Default logging configuration will be used.");
                }
            }
        } else {
            log.info("will use default logging configuration");
        }

        I18nInitializer initializer = new DefaultI18nInitializer("pollen-i18n");
        // to show none translated sentences
        initializer.setMissingKeyReturnNull(true);

        I18n.init(initializer, Locale.FRANCE);

        try (PollenTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            PollenServiceContext serviceContext = newServiceContext(persistenceContext, Locale.FRANCE);
            serviceContext.setSecurityContext(new InitSecurityContext());
            serviceContext.newService(PollenUserService.class).createDefaultUsers();
        } catch (InvalidFormException e) {
            //Can't happen
        }

        started.set(true);
    }

    private class InitSecurityContext implements PollenSecurityContext {

        @Override
        public PollenPrincipal getMainPrincipal() {
            return null;
        }

        @Override
        public void setMainPrincipal(PollenPrincipal creator) {
        }

        @Override
        public PollenUser getPollenUser() {
            return null;
        }

        @Override
        public void setPollenUser(PollenUser pollenUser) {
        }

        @Override
        public boolean isConnected() {
            return true;
        }

        @Override
        public boolean isAdmin() {
            return true;
        }
    }
}
