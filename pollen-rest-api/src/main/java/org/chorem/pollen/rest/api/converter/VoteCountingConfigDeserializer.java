package org.chorem.pollen.rest.api.converter;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.chorem.pollen.votecounting.BordaConfig;
import org.chorem.pollen.votecounting.CumulativeConfig;
import org.chorem.pollen.votecounting.MajorityJudgmentConfig;
import org.chorem.pollen.votecounting.model.EmptyVoteCountingConfig;
import org.chorem.pollen.votecounting.model.MinMaxChoicesNumberConfig;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;

import java.io.IOException;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class VoteCountingConfigDeserializer extends JsonDeserializer<VoteCountingConfig> {

    @Override
    public VoteCountingConfig deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        VoteCountingConfigTemp configTemp = ctxt.readValue(p, VoteCountingConfigTemp.class);

        VoteCountingConfig config;

        if (configTemp.getMaxChoiceNumber() != null || configTemp.getMinChoiceNumber() != null) {
            if (configTemp.getPointsByRank() != null) {
                BordaConfig bordaConfig = new BordaConfig();
                bordaConfig.setMaxChoiceNumber(configTemp.getMaxChoiceNumber());
                bordaConfig.setPointsByRank(configTemp.getPointsByRank());
                config = bordaConfig;
            } else {
                MinMaxChoicesNumberConfig minMaxChoicesNumberConfig = new MinMaxChoicesNumberConfig();
                if (configTemp.getMaxChoiceNumber() != null) {
                    minMaxChoicesNumberConfig.setMaxChoiceNumber(configTemp.getMaxChoiceNumber());
                }
                if (configTemp.getMinChoiceNumber() != null) {
                    minMaxChoicesNumberConfig.setMinChoiceNumber(configTemp.getMinChoiceNumber());
                }
                config = minMaxChoicesNumberConfig;
            }
        } else if (configTemp.getPoints() != null) {
            CumulativeConfig cumulativeConfig = new CumulativeConfig();
            cumulativeConfig.setPoints(configTemp.getPoints());
            config = cumulativeConfig;
        } else if (configTemp.getGrades() != null) {
            MajorityJudgmentConfig majorityJudgmentConfig = new MajorityJudgmentConfig();
            majorityJudgmentConfig.setGrades(configTemp.getGrades());
            config = majorityJudgmentConfig;
        } else {
            config = new EmptyVoteCountingConfig();
        }

        return config;
    }

    static class VoteCountingConfigTemp {

        protected Integer maxChoiceNumber;

        protected Integer minChoiceNumber;

        protected List<Integer> pointsByRank;

        protected Integer points;

        protected List<String> grades;

        public Integer getMaxChoiceNumber() {
            return maxChoiceNumber;
        }

        public void setMaxChoiceNumber(Integer maxChoiceNumber) {
            this.maxChoiceNumber = maxChoiceNumber;
        }

        public Integer getMinChoiceNumber() {
            return minChoiceNumber;
        }

        public void setMinChoiceNumber(Integer minChoiceNumber) {
            this.minChoiceNumber = minChoiceNumber;
        }

        public List<Integer> getPointsByRank() {
            return pointsByRank;
        }

        public void setPointsByRank(List<Integer> pointsByRank) {
            this.pointsByRank = pointsByRank;
        }

        public Integer getPoints() {
            return points;
        }

        public void setPoints(Integer points) {
            this.points = points;
        }

        public List<String> getGrades() {
            return grades;
        }

        public void setGrades(List<String> grades) {
            this.grades = grades;
        }
    }
}
