package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.ws.rs.container.ContainerRequestContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.web.filter.TypedTopiaTransactionFilter;

public class PollenTopiaTransactionFilter extends TypedTopiaTransactionFilter<PollenPersistenceContext> {

    private static final Log log = LogFactory.getLog(PollenTopiaTransactionFilter.class);

    public PollenTopiaTransactionFilter() {
        super(PollenPersistenceContext.class);
    }

    public static PollenPersistenceContext getPersistenceContext(ContainerRequestContext context) {
        return (PollenPersistenceContext) context.getProperty(TOPIA_TRANSACTION_REQUEST_ATTRIBUTE);
    }

    @Override
    protected PollenPersistenceContext beginTransaction(ServletRequest request) throws TopiaException {
        PollenRestApiApplicationContext applicationContext =
                PollenRestApiApplicationContext.getApplicationContext(request.getServletContext());
        return applicationContext.newPersistenceContext();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        if (log.isInfoEnabled()) {
            log.info("Init Topia Filter");
        }
    }
}
