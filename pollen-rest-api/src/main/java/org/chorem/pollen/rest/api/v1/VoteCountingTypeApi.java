package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.chorem.pollen.services.bean.VoteCountingTypeBean;
import org.chorem.pollen.services.service.VoteCountingTypeService;
import org.jboss.resteasy.annotations.GZIP;

import java.util.List;

/**
 * Created on 03/07/14.
 *
 * @author garandel
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VoteCountingTypeApi {

    @Inject
    private VoteCountingTypeService voteCountingTypeService;

    @Path("/voteCountingTypes")
    @GET
    @GZIP
    public List<VoteCountingTypeBean> getVoteCountingTypes() {
        return voteCountingTypeService.getVoteCountingTypes();
    }

    @Path("/voteCountingTypes/{id}")
    @GET
    @GZIP
    public VoteCountingTypeBean getVoteCountingType(@PathParam("id") int id) {
        return voteCountingTypeService.getVoteCountingType(id);
    }
}
