package org.chorem.pollen.rest.api.exceptionMappers;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollenAbstractExceptionMapper<E extends Exception> implements ExceptionMapper<E> {

    protected final Response.Status status;

    public PollenAbstractExceptionMapper(Response.Status status) {
        this.status = status;
    }

    protected Object getEntity(E exception) {
        return exception.getMessage();
    }


    @Override
    public Response toResponse(E exception) {
        Response.ResponseBuilder builder = Response.status(status)
                .entity(getEntity(exception));
        return builder.build();
    }
}
