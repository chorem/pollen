package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.gson.Gson;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.NewCookie;
import jakarta.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.brickred.socialauth.SocialAuthManager;
import org.chorem.pollen.persistence.entity.LoginProvider;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.bean.LoginProviderBean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.service.PollenUserService;
import org.chorem.pollen.services.service.SocialAuthService;
import org.chorem.pollen.services.service.security.*;

import java.util.Base64;
import java.util.List;
import java.util.Map;

import static org.chorem.pollen.rest.api.PollenRestApiRequestFilter.COOKIE_POLLEN_AUTH;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthApi {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(AuthApi.class);

    @Inject
    private SecurityService securityService;

    @Inject
    private SocialAuthService socialAuthService;

    @Inject
    private PollenUserService pollenUserService;

    @Path("/login")
    @POST
    @PUT
    public PollenEntityRef<PollenUser> login(@HeaderParam("Authorization") String authHeader)
            throws PollenAuthenticationException,
            MissingAuthenticationException,
            PollenEmailNotValidatedException,
            PollenUserBannedException {

        if (StringUtils.startsWith(authHeader, "Basic ")) {
            String s = new String(Base64.getDecoder().decode(StringUtils.substringAfter(authHeader, "Basic ")));
            String[] lp = s.split(":");
            String login = lp[0];
            String password = lp[1];

            if (log.isDebugEnabled()) {
                log.info("login: " + login);
            }

            PollenEntityRef<PollenUser> userPollenEntityRef = securityService.login(login, password, false);
            return userPollenEntityRef;
        }

        throw new MissingAuthenticationException();
    }

    @Path("/login2")
    @POST
    @PUT
    public PollenEntityRef<PollenUser> login2(@QueryParam("login") String login,
                                              @QueryParam("password") String password,
                                              @QueryParam("rememberMe") Boolean rememberMe)
            throws PollenAuthenticationException,
            PollenEmailNotValidatedException,
            PollenUserBannedException {

        return securityService.login(login, password, rememberMe);
    }

    @Path("/login/{providerId}")
    @GET
    public String getLoginProviderUrl(@Context HttpServletRequest request,
                                      @PathParam("providerId") String providerId,
                                      @QueryParam("providerRedirection") String providerRedirection) throws Exception {
        SocialAuthManager socialAuthManager = socialAuthService.getSocialAuthManager();
        request.getSession(true).setAttribute(ApiUtils.SOCIAL_AUTH_MANAGER_SESSION_KEY, socialAuthManager);
        return socialAuthManager.getAuthenticationUrl(providerId, providerRedirection);
    }

    @Path("/login/{providerId}")
    @POST
    public PollenEntityRef<PollenUser> loginProvider(@Context HttpServletRequest request,
                                                     String providerReturn)
            throws Exception {

        SocialAuthManager socialAuthManager =
                (SocialAuthManager) request.getSession().getAttribute(ApiUtils.SOCIAL_AUTH_MANAGER_SESSION_KEY);
        //socialAuthManager
        request.getSession().removeAttribute(ApiUtils.SOCIAL_AUTH_MANAGER_SESSION_KEY);
        Gson gson = new Gson();
        Map<String, String> paramsMap = gson.fromJson(providerReturn, Map.class);
        PollenEntityRef<PollenUser> userPollenEntityRef = socialAuthService.login(socialAuthManager, paramsMap);
        return userPollenEntityRef;
    }

    @Path("/logout")
    @DELETE
    public Response logout() {
        securityService.logout();
        NewCookie cookie = new NewCookie.Builder(COOKIE_POLLEN_AUTH)
                .value("")
                .path("/")
                .maxAge(0)
                .secure(true)
                .httpOnly(false)
                .build();
        return Response.status(Response.Status.NO_CONTENT)
                .header(HttpHeaders.SET_COOKIE, cookie)
                .build();
    }

    @Path("/lostpassword")
    @POST
    public void lostPassword(String login) throws PollenUserUnknownException, PollenEmailNotValidatedException {
        securityService.lostPassword(login);
    }

    @Path("/resendValidation")
    @POST
    @PUT
    public void resendValidation(String login) {
        pollenUserService.resendValidation(login);
    }

    @Path("/loginproviders")
    @GET
    public List<LoginProviderBean> getAllLoginProviders() {
        return socialAuthService.getAllLoginProviders();
    }

    @Path("/loginproviders")
    @POST
    public LoginProviderBean addLoginProvider(LoginProviderBean loginProvider) {
        return socialAuthService.saveLoginProvider(loginProvider, false);
    }

    @Path("/loginproviders/{providerId}")
    @POST
    @PUT
    public LoginProviderBean saveLoginProvider(LoginProviderBean loginProvider) {
        return socialAuthService.saveLoginProvider(loginProvider, true);
    }

    @Path("/loginproviders/{providerId}")
    @DELETE
    public void deleteLoginProvider(@PathParam("providerId") PollenEntityId<LoginProvider> providerId) {
        socialAuthService.deleteLoginProvider(providerId.getEntityId());
    }

    @Path("/loginproviders/active")
    @GET
    public List<String> getActiveLoginProviders() {
        return socialAuthService.getActiveLoginProviders();
    }

    @Path("/loginproviders/available")
    @GET
    public List<String> getAvailableLoginProviders() {
        return socialAuthService.getAvailableLoginProviders();
    }
}
