package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.ChoiceType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.rest.api.beans.PollCreateBean;
import org.chorem.pollen.services.bean.*;
import org.chorem.pollen.services.bean.export.ExportBean;
import org.chorem.pollen.services.service.ChoiceService;
import org.chorem.pollen.services.service.FeedService;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.PollService;
import org.jboss.resteasy.annotations.GZIP;

import java.util.Date;
import java.util.Set;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@GZIP
public class PollApi {

    @Inject
    private PollService pollService;

    @Inject
    private FeedService feedService;

    @Inject
    private ChoiceService choiceService;

    @Path("/polls")
    @GET
    @GZIP
    public PaginationResultBean<PollBean> getPolls(@BeanParam PaginationParameterBean paginationParameter,
                                                   @QueryParam("search") String search,
                                                   @QueryParam("use") String context,
                                                   @QueryParam("filter") String filter) {
        if (StringUtils.isBlank(context)) {
            // Connection as admin
            return pollService.getPolls(paginationParameter, search);
        } else {
            switch (context) {
                case "invited":
                    return pollService.getInvitedPolls(paginationParameter, search, filter);
                case "participated":
                    return pollService.getParticipatedPolls(paginationParameter, search, filter);
                case "created":
                    return pollService.getCreatedPolls(paginationParameter, search, filter);
                default:
                    return pollService.getAllPolls(paginationParameter, search, filter);
            }
        }
    }

    @Path("/polls")
    @POST
    public PollenEntityRef<Poll> createPoll(PollCreateBean pollCreateBean) throws InvalidFormException {
        return pollService.createPoll(pollCreateBean.getPoll(),
                pollCreateBean.getVoterLists(),
                pollCreateBean.getVoterListMembers());
    }

    @Path("/polls/{pollId}")
    @GET
    @GZIP
    public PollBean getPoll(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        if ("new".equals(pollId.getReducedId())) {
            return pollService.getNewPoll(ChoiceType.TEXT);
        } else {
            return pollService.getPoll(pollId.getEntityId());
        }
    }

    @Path("/polls/{pollId}")
    @PUT
    @POST
    public PollBean editPoll(PollBean poll) throws InvalidFormException {
        return pollService.editPoll(poll);
    }

    @Path("/polls/{pollId}")
    @DELETE
    public void deletePoll(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        pollService.deletePoll(pollId.getEntityId());
    }

    @Path("/polls/{pollId}/clone")
    @POST
    public PollenEntityRef<Poll> clonePoll(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        return pollService.clonePoll(pollId.getEntityId());
    }

    @Path("/polls/{pollId}/close")
    @PUT
    public Date closePoll(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        return pollService.closePoll(pollId.getEntityId());
    }

    @Path("/polls/{pollId}/reopen")
    @PUT
    public void reopenPoll(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        pollService.reopenPoll(pollId.getEntityId());
    }

    @Path("/polls/{pollId}/export")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response exportPoll(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        ExportBean exportBean = pollService.exportPoll(pollId.getEntityId());
        return ApiUtils.exportBeanToResponse(exportBean);
    }

    @Path("/polls/{pollId}/assign")
    @PUT
    public PollBean assignPoll(@PathParam("pollId") PollenEntityId<Poll> pollId) throws InvalidFormException {
        return pollService.assignPollToConnectedUser(pollId.getEntityId());
    }

    @Path("/polls/{pollId}/feed")
    @Produces(MediaType.APPLICATION_ATOM_XML)
    @GET
    public Response getFeedForPoll(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        String feedContent = feedService.getFeedContentForPoll(pollId.getEntityId());
        return Response.ok(feedContent).build();
    }

    @Path("/polls/{pollId}/invalidEmails")
    @GET
    @GZIP
    public Set<String> getInvalidEmails(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        return pollService.getInvalidEmails(pollId.getEntityId());
    }

    @Path("polls/{pollId}/questions/{questionId}/choices")
    @POST
    public PollenEntityRef<Choice> addChoice(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                             @PathParam("questionId") PollenEntityId<Question> questionId,
                                             ChoiceBean choice) throws InvalidFormException {
        return choiceService.addChoice(pollId.getEntityId(), questionId.getEntityId(), choice);
    }
}
