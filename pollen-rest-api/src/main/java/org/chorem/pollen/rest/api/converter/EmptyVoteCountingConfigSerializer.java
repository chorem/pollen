package org.chorem.pollen.rest.api.converter;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.chorem.pollen.votecounting.model.EmptyVoteCountingConfig;

import java.io.IOException;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class EmptyVoteCountingConfigSerializer extends JsonSerializer<EmptyVoteCountingConfig> {

    @Override
    public void serialize(EmptyVoteCountingConfig voteCountingConfig,
                          JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeEndObject();
    }
}
