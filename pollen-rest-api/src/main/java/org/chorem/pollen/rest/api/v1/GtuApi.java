package org.chorem.pollen.rest.api.v1;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.chorem.pollen.services.bean.resource.GtuMetaBean;
import org.chorem.pollen.services.bean.resource.ResourceStreamBean;
import org.chorem.pollen.services.service.GtuService;
import org.jboss.resteasy.annotations.GZIP;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GtuApi {

    @Inject
    private GtuService gtuService;

    @Path("/gtus")
    @GET
    @GZIP
    @Produces(MediaType.APPLICATION_JSON)
    public List<GtuMetaBean> getGtus() {
        return gtuService.getAllGtus();
    }

    @Path("/gtu/define")
    @GET
    @GZIP
    @Produces(MediaType.APPLICATION_JSON)
    public boolean isGtu() {
        return gtuService.isGtu();
    }

    @Path("/gtu")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getGtu() {
        ResourceStreamBean resource = gtuService.getCurrentGtu();
        return Response.ok(resource.getResourceContent(), resource.getContentType())
                .header("content-disposition", "filename=\"" + resource.getName() + "\"")
                .build();
    }

    @Path("/gtu/validate")
    @PUT
    @POST
    public void validateGtu() {
        gtuService.validateGtu();
    }
}
