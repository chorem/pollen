package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.services.bean.*;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.VoteService;
import org.jboss.resteasy.annotations.GZIP;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VoteApi {

    @Inject
    private VoteService voteService;

    @Path("/polls/{pollId}/questions/{questionId}/votes")
    @GET
    @GZIP
    public PaginationResultBean<VoteBean> getVotes(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                   @PathParam("questionId") PollenEntityId<Question> questionId,
                                                   @BeanParam PaginationParameterBean paginationParameter) {
        return voteService.getVotes(pollId.getEntityId(), questionId.getEntityId(), paginationParameter);
    }

    @Path("/polls/{pollId}/questions/{questionId}/votes")
    @POST
    public PollenEntityRef<Vote> addVote(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                         @PathParam("questionId") PollenEntityId<Question> questionId,
                                         VoteBean vote) throws InvalidFormException {
        return voteService.addVote(pollId.getEntityId(), questionId.getEntityId(), vote);
    }

    @Path("/polls/{pollId}/questions/{questionId}/votes/{voteId}")
    @GET
    @GZIP
    public VoteBean getVote(@PathParam("pollId") PollenEntityId<Poll> pollId,
                            @PathParam("questionId") PollenEntityId<Question> questionId,
                            @PathParam("voteId") PollenEntityId<Vote> voteId) {
        if ("new".equals(voteId.getReducedId())) {
            return voteService.getNewVote(pollId.getEntityId(), questionId.getEntityId());
        } else {
            return voteService.getVote(pollId.getEntityId(), questionId.getEntityId(), voteId.getEntityId());
        }
    }

    @Path("/polls/{pollId}/questions/{questionId}/votes/{voteId}")
    @POST
    @PUT
    public VoteBean editVote(@PathParam("pollId") PollenEntityId<Poll> pollId,
                             @PathParam("questionId") PollenEntityId<Question> questionId,
                             VoteBean vote) throws InvalidFormException {
        return voteService.editVote(pollId.getEntityId(), questionId.getEntityId(), vote);
    }

    @Path("/polls/{pollId}/questions/{questionId}/votes/{voteId}")
    @DELETE
    public void deleteVote(@PathParam("pollId") PollenEntityId<Poll> pollId,
                           @PathParam("questionId") PollenEntityId<Question> questionId,
                           @PathParam("voteId") PollenEntityId<Vote> voteId) throws InvalidFormException {
        voteService.deleteVote(pollId.getEntityId(), questionId.getEntityId(), voteId.getEntityId());
    }
}
