package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.chorem.pollen.persistence.entity.ChildFavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteListMember;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.services.bean.*;
import org.chorem.pollen.services.bean.export.ExportBean;
import org.chorem.pollen.services.bean.resource.ResourceFileBean;
import org.chorem.pollen.services.service.FavoriteListImportException;
import org.chorem.pollen.services.service.FavoriteListService;
import org.chorem.pollen.services.service.InvalidFormException;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FavoriteListApi {

    @Inject
    private FavoriteListService favoriteListService;

    @Path("/favoriteLists")
    @GET
    @GZIP
    public PaginationResultBean<FavoriteListBean> getFavoriteLists(@BeanParam PaginationParameterBean paginationParameter,
                                                                   @QueryParam("search") String search) {
        return favoriteListService.getFavoriteLists(paginationParameter, search);
    }

    @Path("/favoriteLists")
    @POST
    public PollenEntityRef<FavoriteList> createFavoriteList(FavoriteListBean favoriteList) throws InvalidFormException {
        return favoriteListService.createFavoriteList(favoriteList);
    }

    @Path("/favoriteLists/imports")
    @POST
    @Consumes("multipart/form-data")
    public int importFavoriteLists(MultipartFormDataInput input) throws InvalidFormException {
        ResourceFileBean resourceFileBean = ApiUtils.multipartToResourceBean(input, "importFile");
        return favoriteListService.importFavoriteLists(resourceFileBean.getFile());
    }

    @Path("/favoriteLists/exports")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response exportFavoriteLists() {
        ExportBean exportBean = favoriteListService.exportFavoriteLists();
        return ApiUtils.exportBeanToResponse(exportBean);
    }

    @Path("/favoriteLists/{favoriteListId}")
    @GET
    @GZIP
    public FavoriteListBean getFavoriteList(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId) {
        return favoriteListService.getFavoriteList(favoriteListId.getEntityId());
    }

    @Path("/favoriteLists/{favoriteListId}")
    @PUT
    @POST
    public FavoriteListBean editFavoriteList(FavoriteListBean favoriteList) throws InvalidFormException {
        return favoriteListService.editFavoriteList(favoriteList);
    }

    @Path("/favoriteLists/{favoriteListId}")
    @DELETE
    public void deleteFavoriteList(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId) {
        favoriteListService.deleteFavoriteList(favoriteListId.getEntityId());
    }

    @Path("/favoriteLists/{favoriteListId}/importCsv")
    @POST
    @Consumes("multipart/form-data")
    public void importFavoriteListMembersFromCsv(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                                 MultipartFormDataInput input) throws FavoriteListImportException {
        ResourceFileBean resourceFileBean = ApiUtils.multipartToResourceBean(input, "csvFile");
        favoriteListService.importFavoriteListMembersFromCsv(favoriteListId.getEntityId(), resourceFileBean.getFile());
    }

    @Path("/favoriteLists/{favoriteListId}/importLdap")
    @POST
    public void importFavoriteListMembersFromLdap(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                                  String ldap) throws FavoriteListImportException {
        favoriteListService.importFavoriteListMembersFromLdap(favoriteListId.getEntityId(), ldap);
    }

    @Path("/favoriteLists/importVoterList")
    @POST
    public FavoriteListBean importFavoriteListMembersFromVoterList(@QueryParam("voterListId") PollenEntityId<VoterList> voterListId) throws InvalidFormException {
        return favoriteListService.importFavoriteListMembersFromVoterList(voterListId.getEntityId());
    }

    @Path("/favoriteLists/{favoriteListId}/members")
    @GET
    @GZIP
    public PaginationResultBean<FavoriteListMemberBean> getMembers(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                                                   @QueryParam("search") String search,
                                                                   @BeanParam PaginationParameterBean paginationParameter) {
        return favoriteListService.getFavoriteListMembers(favoriteListId.getEntityId(), search, paginationParameter);
    }

    @Path("/favoriteLists/{favoriteListId}/members/{memberId}")
    @GET
    @GZIP
    public FavoriteListMemberBean getMember(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                            @PathParam("memberId") PollenEntityId<FavoriteListMember> memberId) {
        return favoriteListService.getFavoriteListMember(favoriteListId.getEntityId(), memberId.getEntityId());
    }

    @Path("/favoriteLists/{favoriteListId}/members")
    @POST
    public PollenEntityRef<FavoriteListMember> addMember(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                                         FavoriteListMemberBean member) throws InvalidFormException {
        return favoriteListService.addFavoriteListMember(favoriteListId.getEntityId(), member);
    }

    @Path("/favoriteLists/{favoriteListId}/members/{memberId}")
    @PUT
    @POST
    public FavoriteListMemberBean editMember(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                             FavoriteListMemberBean member) throws InvalidFormException {
        return favoriteListService.editFavoriteListMember(favoriteListId.getEntityId(), member);
    }

    @Path("/favoriteLists/{favoriteListId}/members/{memberId}")
    @DELETE
    public void removeMember(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                             @PathParam("memberId") PollenEntityId<FavoriteListMember> memberId) {
        favoriteListService.deleteFavoriteListMember(favoriteListId.getEntityId(), memberId.getEntityId());
    }

    @Path("/favoriteLists/{favoriteListId}/lists")
    @GET
    @GZIP
    public PaginationResultBean<ChildFavoriteListBean> getChildrenLists(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                                                        @QueryParam("search") String search,
                                                                        @BeanParam PaginationParameterBean paginationParameter) {
        return favoriteListService.getChildrenLists(favoriteListId.getEntityId(), search, paginationParameter);
    }

    @Path("/favoriteLists/{favoriteListId}/lists/{childListId}")
    @GET
    @GZIP
    public ChildFavoriteListBean getChildList(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                              @PathParam("childListId") PollenEntityId<ChildFavoriteList> childListId) {
        return favoriteListService.getChildList(favoriteListId.getEntityId(), childListId.getEntityId());
    }

    @Path("/favoriteLists/{favoriteListId}/lists")
    @POST
    public PollenEntityRef<ChildFavoriteList> addChildList(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                                           ChildFavoriteListBean childList) throws InvalidFormException {
        return favoriteListService.addChildList(favoriteListId.getEntityId(), childList);
    }

    @Path("/favoriteLists/{favoriteListId}/lists/{childListId}")
    @POST
    @PUT
    public ChildFavoriteListBean editChildList(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                               ChildFavoriteListBean childList) throws InvalidFormException {
        return favoriteListService.editChildList(favoriteListId.getEntityId(), childList);
    }

    @Path("/favoriteLists/{favoriteListId}/lists/{childListId}")
    @DELETE
    public void removeChildList(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                @PathParam("childListId") PollenEntityId<ChildFavoriteList> childListId) {
        favoriteListService.removeChildList(favoriteListId.getEntityId(), childListId.getEntityId());
    }

    @Path("/favoriteLists/{favoriteListId}/all")
    @GET
    @GZIP
    public PaginationResultBean<PollenBean> getAllChildren(@PathParam("favoriteListId") PollenEntityId<FavoriteList> favoriteListId,
                                                           @QueryParam("search") String search,
                                                           @BeanParam PaginationParameterBean paginationParameter) {
        return favoriteListService.getAllChildren(favoriteListId.getEntityId(), search, paginationParameter);
    }
}
