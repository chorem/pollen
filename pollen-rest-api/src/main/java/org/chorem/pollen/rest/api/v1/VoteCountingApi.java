package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.bean.ListVoteCountingResultBean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.VoteCountingResultBean;
import org.chorem.pollen.services.service.VoteCountingService;
import org.jboss.resteasy.annotations.GZIP;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VoteCountingApi {

    @Inject
    private VoteCountingService voteCountingService;

    @Path("/polls/{pollId}/questions/{questionId}/results")
    @GET
    @GZIP
    public VoteCountingResultBean getMainResult(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                @PathParam("questionId") PollenEntityId<Question> questionId) {
        return voteCountingService.getMainResult(questionId.getEntityId());
    }

    @Path("/polls/{pollId}/questions/{questionId}/results/group")
    @GET
    @GZIP
    public ListVoteCountingResultBean getGroupResult(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                     @PathParam("questionId") PollenEntityId<Question> questionId) {
        return voteCountingService.getGroupResult(questionId.getEntityId());
    }
}
