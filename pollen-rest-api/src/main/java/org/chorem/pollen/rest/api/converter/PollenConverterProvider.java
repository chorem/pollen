package org.chorem.pollen.rest.api.converter;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;
import org.chorem.pollen.services.bean.PollenEntityId;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@Provider
public class PollenConverterProvider implements ParamConverterProvider {

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        ParamConverter<T> converter = null;

        if (PollenEntityId.class.isAssignableFrom(rawType)) {
            Class entityClass = (Class) ((ParameterizedType) genericType).getActualTypeArguments()[0];
            converter = new PollenEntityIdConverter(entityClass);
        }

        return converter;
    }
}
