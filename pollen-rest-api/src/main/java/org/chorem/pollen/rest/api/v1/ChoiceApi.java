package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.service.ChoiceService;
import org.chorem.pollen.services.service.InvalidFormException;
import org.jboss.resteasy.annotations.GZIP;

import java.util.List;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ChoiceApi {

    @Inject
    private ChoiceService choiceService;

    @Path("polls/{pollId}/questions/{questionId}/choices")
    @GET
    @GZIP
    public List<ChoiceBean> getChoices(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                       @PathParam("questionId") PollenEntityId<Question> questionId) {
        return choiceService.getChoices(questionId.getEntityId());
    }

    @Path("polls/{pollId}/questions/{questionId}/choices/{choiceId}")
    @GET
    @GZIP
    public ChoiceBean getChoice(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                @PathParam("questionId") PollenEntityId<Question> questionId,
                                @PathParam("choiceId") PollenEntityId<Choice> choiceId) {
        return choiceService.getChoice(questionId.getEntityId(), choiceId.getEntityId());
    }

    @Path("polls/{pollId}/questions/{questionId}/choices/{choiceId}")
    @POST
    @PUT
    public ChoiceBean editChoice(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                 @PathParam("questionId") PollenEntityId<Question> questionId,
                                 ChoiceBean choice) throws InvalidFormException {
        return choiceService.editChoice(questionId.getEntityId(), choice);
    }

    @Path("polls/{pollId}/questions/{questionId}/choices/{choiceId}")
    @DELETE
    public void deleteChoice(@PathParam("pollId") PollenEntityId<Poll> pollId,
                             @PathParam("questionId") PollenEntityId<Question> questionId,
                             @PathParam("choiceId") PollenEntityId<Choice> choiceId) throws InvalidFormException {
        choiceService.deleteChoice(questionId.getEntityId(), choiceId.getEntityId());
    }
}
