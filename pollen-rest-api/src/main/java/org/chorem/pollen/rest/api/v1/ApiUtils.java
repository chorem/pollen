package org.chorem.pollen.rest.api.v1;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import jakarta.ws.rs.core.Response;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.chorem.pollen.persistence.entity.ResourceType;
import org.chorem.pollen.rest.api.beans.Resource64Bean;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.bean.export.ExportBean;
import org.chorem.pollen.services.bean.resource.ResourceFileBean;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ApiUtils {

    public static final String SOCIAL_AUTH_MANAGER_SESSION_KEY = "socialAuthManager";

    public static Response exportBeanToResponse(ExportBean exportBean) {
        InputStream inputStream = IOUtils.toInputStream(exportBean.getContent(), Charsets.UTF_8);

        return ResponseDownload(inputStream, exportBean.getName(), exportBean.getContentType());
    }

    public static Response ResponseDownload(InputStream inputStream, String name, String contentType) {
        return Response.ok(inputStream, contentType)
                .header("content-disposition", "attachment; filename=\"" + name + "\"")
                .build();
    }


    public static ResourceFileBean imageMultipartToResourceBean(MultipartFormDataInput multipartFormDataInput, String inputName, Integer maxWidth, Integer maxHeight) {
        try {
            InputPart inputPart = multipartFormDataInput
                    .getFormDataMap()
                    .get(inputName)
                    .get(0);
            File uploadFile = uploadFile(multipartFormDataInput, inputPart, inputName);

            if (maxWidth != null || maxHeight != null) {
                resizeAndCropImage(uploadFile, maxWidth, maxHeight);
            }

            ResourceFileBean resourceBean = createResourceFileBean(multipartFormDataInput, uploadFile, inputPart);

            return resourceBean;
        } catch (IOException e) {
            throw new PollenTechnicalException(e);
        }
    }

    public static ResourceFileBean multipartToResourceBean(MultipartFormDataInput multipartFormDataInput, String inputName) {
        try {
            InputPart inputPart = multipartFormDataInput
                    .getFormDataMap()
                    .get(inputName)
                    .get(0);
            File uploadFile = uploadFile(multipartFormDataInput, inputPart, inputName);

            ResourceFileBean resourceBean = createResourceFileBean(multipartFormDataInput, uploadFile, inputPart);

            return resourceBean;
        } catch (IOException e) {
            throw new PollenTechnicalException(e);
        }
    }


    private static File uploadFile(MultipartFormDataInput multipartFormDataInput, InputPart inputPart, String inputName) throws IOException {
        InputStream in = multipartFormDataInput.getFormDataPart(inputName, InputStream.class, null);
        java.nio.file.Path tempPath = Files.createTempDirectory("pollen");
        String contentDisposition = inputPart
                .getHeaders()
                .get("Content-Disposition")
                .get(0);
        Pattern FilenamePattern = Pattern.compile("filename=\"(.*)\"");
        Matcher matcher = FilenamePattern.matcher(contentDisposition);
        String fileName = inputName;
        if (matcher.find()) {
            fileName = matcher.group(1);
        }
        File uploadFile = new File(tempPath.toFile(), fileName);
        Files.copy(in, uploadFile.toPath());

        return uploadFile;
    }

    private static ResourceFileBean createResourceFileBean(MultipartFormDataInput multipartFormDataInput, File uploadFile, InputPart inputPart) throws IOException {
        ResourceFileBean resourceBean = new ResourceFileBean();
        resourceBean.setFile(uploadFile);
        resourceBean.setName(uploadFile.getName());

        String contentType = inputPart.getMediaType().toString();

        resourceBean.setContentType(contentType);
        resourceBean.setSize(uploadFile.length());
        List<InputPart> resourceTypeInputs = multipartFormDataInput
                .getFormDataMap()
                .get("resourceType");
        if (CollectionUtils.isNotEmpty(resourceTypeInputs)) {
            resourceBean.setResourceType(ResourceType.valueOf(resourceTypeInputs.get(0).getBodyAsString()));
        }
        return resourceBean;
    }


    private static void resizeAndCropImage(File uploadFile, Integer maxWidth, Integer maxHeight) {

        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(uploadFile);
            if (bufferedImage != null) {

                BufferedImage resizeImage = resizeImage(bufferedImage, bufferedImage.getType(), maxWidth, maxHeight);
                BufferedImage image = cropImage(resizeImage, maxWidth, maxHeight);

                ImageIO.write(image, FilenameUtils.getExtension(uploadFile.getName()), uploadFile);
            }
        } catch (IOException e) {
            throw new PollenTechnicalException(e);
        }
    }

    private static BufferedImage cropImage(BufferedImage originalImage, Integer maxWidth, Integer maxHeight) {

        if (originalImage.getHeight() > maxHeight) {

            int width = originalImage.getWidth() > maxWidth ? maxWidth : originalImage.getWidth();

            int xIndex = (originalImage.getHeight() - maxHeight) / 2;

            return originalImage.getSubimage(0, xIndex, width, maxHeight);
        } else {
            return originalImage;
        }
    }

    private static BufferedImage resizeImage(BufferedImage bufferedImage, int type, Integer maxWidth, Integer maxHeight) {

        int originalWidth = bufferedImage.getWidth();
        int originalHeight = bufferedImage.getHeight();

        if (originalWidth > maxWidth) {

            int targetHeight = originalHeight * maxWidth / originalWidth;

            BufferedImage resizedImage = new BufferedImage(maxWidth, targetHeight, type);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(bufferedImage, 0, 0, maxWidth, targetHeight, null);
            g.dispose();

            return resizedImage;
        } else {
            return bufferedImage;
        }
    }

    public static ResourceFileBean resource64ToResourceBean(Resource64Bean resource64Bean) {
        try {

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] data = decoder.decode(resource64Bean.getData());

            java.nio.file.Path tempPath = Files.createTempDirectory("pollen");
            File uploadFile = new File(tempPath.toFile(), resource64Bean.getName());
            InputStream in = new ByteArrayInputStream(data);
            Files.copy(in, uploadFile.toPath());

            ResourceFileBean resourceBean = new ResourceFileBean();
            resourceBean.setFile(uploadFile);
            resourceBean.setName(resource64Bean.getName());

            resourceBean.setContentType(resource64Bean.getContentType());
            resourceBean.setSize(uploadFile.length());
            resourceBean.setResourceType(resource64Bean.getResourceType());


            return resourceBean;

        } catch (IOException e) {
            throw new PollenTechnicalException(e);
        }

    }
}
