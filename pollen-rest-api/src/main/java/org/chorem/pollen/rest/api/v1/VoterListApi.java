package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.persistence.entity.VoterListMember;
import org.chorem.pollen.rest.api.beans.VoterListSaveBean;
import org.chorem.pollen.services.bean.*;
import org.chorem.pollen.services.bean.export.ExportBean;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.VoterListService;
import org.jboss.resteasy.annotations.GZIP;

import java.util.List;
import java.util.Set;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VoterListApi {

    @Inject
    private VoterListService voterListService;

    @Path("/polls/{pollId}/voterLists")
    @POST
    public PollenEntityRef<VoterList> createVoterList(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                      VoterListBean voterList,
                                                      VoterListMemberBean... members) throws InvalidFormException {
        List<VoterListMemberBean> memberList = Lists.newArrayList(members);
        return voterListService.addVoterList(pollId.getEntityId(), voterList, memberList);
    }

    @Path("/polls/{pollId}/voterLists")
    @PUT
    public void saveVoters(@PathParam("pollId") PollenEntityId<Poll> pollId,
                           VoterListSaveBean voterListSaveBean) throws InvalidFormException {
        List<String> listIds = Lists.newLinkedList();
        PollType pollType = voterListSaveBean.getPollType();
        for (VoterListBean list : voterListSaveBean.getListsToDelete()) {
            listIds.add(list.getEntityId());
        }
        List<String> memberIds = Lists.newLinkedList();
        for (VoterListMemberBean member : voterListSaveBean.getMembersToDelete()) {
            memberIds.add(member.getEntityId());
        }
        voterListService.saveVoters(pollId.getEntityId(),
                Lists.newArrayList(voterListSaveBean.getListsToSave()),
                Lists.newArrayList(voterListSaveBean.getMembersToSave()),
                listIds,
                memberIds,
                pollType);
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}")
    @GET
    @GZIP
    public VoterListBean getVoterList(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                      @PathParam("voterListId") PollenEntityId<VoterList> voterListId) {
        if ("main".equals(voterListId.getReducedId())) {
            return voterListService.getMainVoterList(pollId.getEntityId());
        } else {
            return voterListService.getVoterList(pollId.getEntityId(), voterListId.getEntityId());
        }
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}")
    @POST
    @PUT
    public VoterListBean editVoterList(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                       VoterListBean voterList) throws InvalidFormException {
        return voterListService.editVoterList(pollId.getEntityId(), voterList);

    }

    @Path("/polls/{pollId}/voterLists/{voterListId}")
    @DELETE
    public void deleteVoterList(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                @PathParam("voterListId") PollenEntityId<VoterList> voterListId) {
        voterListService.deleteVoterList(pollId.getEntityId(), voterListId.getEntityId());
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/lists")
    @GET
    @GZIP
    public List<VoterListBean> getVoterLists(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                             @PathParam("voterListId") PollenEntityId<VoterList> voterListId) {
        return voterListService.getVoterLists(pollId.getEntityId(), voterListId.getEntityId());
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/members")
    @GET
    @GZIP
    public Set<VoterListMemberBean> getMembers(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                               @PathParam("voterListId") PollenEntityId<VoterList> voterListId) {
        return voterListService.getVoterListMembers(pollId.getEntityId(), voterListId.getEntityId());
    }

    @Path("/polls/{pollId}/participants")
    @GET
    @GZIP
    public PaginationResultBean<VoterListMemberBean> getMembers(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                                @BeanParam PaginationParameterBean paginationParameter) {
        return voterListService.getAllVoterListMembers(pollId.getEntityId(), paginationParameter);
    }

    @Path("/polls/{pollId}/participants/exports")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response exportFavoriteLists(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        ExportBean exportBean = voterListService.exportAllVoterListMembers(pollId.getEntityId());
        return ApiUtils.exportBeanToResponse(exportBean);
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/members")
    @POST
    public VoterListMemberBean addMember(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                         VoterListMemberBean member) throws InvalidFormException {
        return voterListService.addVoterListMember(pollId.getEntityId(), member);
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/members/{memberId}")
    @GET
    @GZIP
    public VoterListMemberBean getMember(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                         @PathParam("voterListId") PollenEntityId<VoterList> voterListId,
                                         @PathParam("memberId") PollenEntityId<VoterListMember> memberId) {
        return voterListService.getVoterListMember(pollId.getEntityId(), voterListId.getEntityId(), memberId.getEntityId());
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/members/{memberId}")
    @POST
    @PUT
    public VoterListMemberBean editMember(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                          VoterListMemberBean member) throws InvalidFormException {
        return voterListService.editVoterListMember(pollId.getEntityId(), member);
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/members/{memberId}")
    @DELETE
    public void deleteMember(@PathParam("pollId") PollenEntityId<Poll> pollId,
                             @PathParam("voterListId") PollenEntityId<VoterList> voterListId,
                             @PathParam("memberId") PollenEntityId<VoterListMember> memberId) throws InvalidFormException {
        voterListService.deleteVoterListMember(pollId.getEntityId(), voterListId.getEntityId(), memberId.getEntityId());
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/send")
    @GET
    public int sendInvitationVoterList(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                       @PathParam("voterListId") PollenEntityId<VoterList> voterListId) {
        return voterListService.sendInvitationVoterList(pollId.getEntityId(), voterListId.getEntityId(), false);
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/members/{memberId}/send")
    @GET
    public boolean sendInvitationMember(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                        @PathParam("voterListId") PollenEntityId<VoterList> voterListId,
                                        @PathParam("memberId") PollenEntityId<VoterListMember> memberId) {
        return voterListService.sendInvitationMember(pollId.getEntityId(), voterListId.getEntityId(), memberId.getEntityId(), false);
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/resend")
    @GET
    public int resendInvitationVoterList(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                         @PathParam("voterListId") PollenEntityId<VoterList> voterListId) {
        return voterListService.sendInvitationVoterList(pollId.getEntityId(), voterListId.getEntityId(), true);
    }

    @Path("/polls/{pollId}/voterLists/{voterListId}/members/{memberId}/resend")
    @GET
    public boolean resendInvitationMember(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                          @PathParam("voterListId") PollenEntityId<VoterList> voterListId,
                                          @PathParam("memberId") PollenEntityId<VoterListMember> memberId) {
        return voterListService.sendInvitationMember(pollId.getEntityId(), voterListId.getEntityId(), memberId.getEntityId(), true);
    }
}
