package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.bean.*;
import org.chorem.pollen.services.service.CommentService;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.ReportService;
import org.jboss.resteasy.annotations.GZIP;

import java.util.List;


/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CommentApi {

    @Inject
    private CommentService commentService;

    @Inject
    private ReportService reportService;

    @Path("polls/{pollId}/comments")
    @GET
    @GZIP
    public PaginationResultBean<CommentBean> getComments(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                         @BeanParam PaginationParameterBean paginationParameter) {
        return commentService.getComments(pollId.getEntityId(), paginationParameter);
    }

    @Path("polls/{pollId}/comments")
    @POST
    public PollenEntityRef<Comment> addComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                               CommentBean comment) throws InvalidFormException {
        return commentService.addComment(pollId.getEntityId(), comment);
    }

    @Path("polls/{pollId}/comments/{commentId}")
    @GET
    @GZIP
    public CommentBean getComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                  @PathParam("commentId") PollenEntityId<Comment> commentId) {
        if ("new".equals(commentId.getReducedId())) {
            return commentService.getNewComment(pollId.getEntityId());
        } else {
            return commentService.getComment(pollId.getEntityId(), commentId.getReducedId());
        }
    }

    @Path("polls/{pollId}/comments/{commentId}")
    @PUT
    @POST
    public CommentBean editComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                   CommentBean comment) throws InvalidFormException {
        return commentService.editComment(pollId.getEntityId(), comment);
    }

    @Path("polls/{pollId}/comments/{commentId}")
    @DELETE
    public void deleteComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                              @PathParam("commentId") PollenEntityId<Comment> commentId) {
        commentService.deleteComment(pollId.getEntityId(), commentId.getEntityId());
    }

    @Path("polls/{pollId}/comments/{commentId}/reports")
    @POST
    public void addReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                          @PathParam("commentId") PollenEntityId<Comment> commentId,
                          ReportBean report) throws InvalidFormException {
        reportService.addCommentReport(pollId.getEntityId(), commentId.getEntityId(), report);
    }

    @Path("polls/{pollId}/comments/{commentId}/reports")
    @GET
    @GZIP
    public List<ReportBean> getReports(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                       @PathParam("commentId") PollenEntityId<Comment> commentId) {

        return reportService.getCommentReports(pollId.getEntityId(), commentId.getEntityId());
    }

    @Path("polls/{pollId}/comments/{commentId}/reports/{reportId}")
    @POST
    public void saveReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                           @PathParam("commentId") PollenEntityId<Comment> commentId,
                           ReportBean report) {
        reportService.saveCommentReport(pollId.getEntityId(), commentId.getEntityId(), report);
    }

    @Path("polls/{pollId}/questions/{questionId}/comments")
    @GET
    @GZIP
    public PaginationResultBean<CommentBean> getQuestionComments(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                                 @PathParam("questionId") PollenEntityId<Question> questionId,
                                                                 @BeanParam PaginationParameterBean paginationParameter) {
        return commentService.getQuestionComments(pollId.getEntityId(), questionId.getEntityId(), paginationParameter);
    }

    @Path("polls/{pollId}/questions/{questionId}/comments")
    @POST
    public PollenEntityRef<Comment> addQuestionComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                                       @PathParam("questionId") PollenEntityId<Question> questionId,
                                                       CommentBean comment) throws InvalidFormException {
        return commentService.addQuestionComment(pollId.getEntityId(), questionId.getEntityId(), comment);
    }

    @Path("polls/{pollId}/questions/{questionId}/comments/{commentId}")
    @GET
    @GZIP
    public CommentBean getQuestionComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                          @PathParam("questionId") PollenEntityId<Question> questionId,
                                          @PathParam("commentId") PollenEntityId<Comment> commentId) {
        if ("new".equals(commentId.getReducedId())) {
            return commentService.getNewQuestionComment(pollId.getEntityId(), questionId.getEntityId());
        } else {
            return commentService.getQuestionComment(pollId.getEntityId(), questionId.getEntityId(), commentId.getReducedId());
        }
    }

    @Path("polls/{pollId}/questions/{questionId}/comments/{commentId}")
    @PUT
    @POST
    public CommentBean editQuestionComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                           @PathParam("questionId") PollenEntityId<Question> questionId,
                                           CommentBean comment) throws InvalidFormException {
        return commentService.editQuestionComment(pollId.getEntityId(), questionId.getEntityId(), comment);
    }

    @Path("polls/{pollId}/questions/{questionId}/comments/{commentId}")
    @DELETE
    public void deleteQuestionComment(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                      @PathParam("questionId") PollenEntityId<Question> questionId,
                                      @PathParam("commentId") PollenEntityId<Comment> commentId) {
        commentService.deleteQuestionComment(pollId.getEntityId(), questionId.getEntityId(), commentId.getEntityId());
    }

    @Path("polls/{pollId}/questions/{questionId}/comments/{commentId}/reports")
    @POST
    public void addQuestionReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                  @PathParam("questionId") PollenEntityId<Question> questionId,
                                  @PathParam("commentId") PollenEntityId<Comment> commentId,
                                  ReportBean report) throws InvalidFormException {
        reportService.addQuestionCommentReport(pollId.getEntityId(), questionId.getEntityId(), commentId.getEntityId(), report);
    }

    @Path("polls/{pollId}/questions/{questionId}/comments/{commentId}/reports")
    @GET
    @GZIP
    public List<ReportBean> getQuestionReports(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                               @PathParam("questionId") PollenEntityId<Question> questionId,
                                               @PathParam("commentId") PollenEntityId<Comment> commentId) {
        return reportService.getQuestionCommentReports(pollId.getEntityId(), questionId.getEntityId(), commentId.getEntityId());
    }

    @Path("polls/{pollId}/questions/{questionId}/comments/{commentId}/reports/{reportId}")
    @POST
    public void saveQuestionReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                   @PathParam("questionId") PollenEntityId<Question> questionId,
                                   @PathParam("commentId") PollenEntityId<Comment> commentId,
                                   ReportBean report) {
        reportService.saveQuestionCommentReport(pollId.getEntityId(), questionId.getEntityId(), commentId.getEntityId(), report);
    }
}
