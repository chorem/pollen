package org.chorem.pollen.rest.api.converter;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import jakarta.ws.rs.ext.ContextResolver;
import jakarta.ws.rs.ext.Provider;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.votecounting.model.EmptyVoteCountingConfig;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@Provider
public class JacksonConfig implements ContextResolver<ObjectMapper> {

    private ObjectMapper objectMapper;

    public JacksonConfig() {
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(PollenEntityId.class, new PollenEntityIdSerializer());
        module.addSerializer(PollenEntityRef.class, new PollenEntityRefSerializer());
        module.addSerializer(EmptyVoteCountingConfig.class, new EmptyVoteCountingConfigSerializer());
        module.addDeserializer(PollenEntityId.class, new PollenEntityIdDeserializer());
        module.addDeserializer(PollenEntityRef.class, new PollenEntityRefDeserializer());
        module.addDeserializer(VoteCountingConfig.class, new VoteCountingConfigDeserializer());
        objectMapper.registerModule(module);
    }

    public ObjectMapper getContext(Class<?> objectType) {
        return objectMapper;
    }
}
