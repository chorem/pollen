package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.job.AbstractPollenJob;
import org.chorem.pollen.services.job.AnonymizeOlderVoteJob;
import org.chorem.pollen.services.job.CheckMailBoxJob;
import org.chorem.pollen.services.job.DeleteObsoleteSessionTokensJob;
import org.chorem.pollen.services.job.SendEmailInErrorsJob;
import org.chorem.pollen.services.job.SendPollEndReminderJob;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * PollenRestApiApplicationListener
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenRestApiApplicationListener implements ServletContextListener {

//    protected static final Set<Class<?>> BEAN_TYPES = Sets.newHashSet(
//            PollBean.class,
//            ChoiceBean.class,
//            CommentBean.class,
//            VoteBean.class,
//            VoteToChoiceBean.class,
//            PollBean.class,
//            PollenUserBean.class,
//            FavoriteListBean.class,
//            FavoriteListMemberBean.class,
//            ChildFavoriteListBean.class,
//            VoterListBean.class,
//            VoterListMemberBean.class,
//            PaginationParameterBean.class,
//            PollenUIContext.class,
//            ReportBean.class,
//            PollenUserEmailAddressBean.class
//    );

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(PollenRestApiApplicationListener.class);
    private Scheduler scheduler;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        // --- init application context --- //
        PollenRestApiApplicationContext applicationContext = PollenRestApiApplicationContext.getApplicationContext();

        PollenRestApiApplicationContext.setApplicationContext(sce.getServletContext(), applicationContext);

        // --- init crons --- //
        JobDataMap data = new JobDataMap();
        data.put(AbstractPollenJob.APPLICATION_CONTEXT, applicationContext);

        JobDetail sendPollEndReminderJob = JobBuilder.newJob(SendPollEndReminderJob.class)
                .usingJobData(data)
                .withIdentity("sendPollEndReminderJob", "pollenJobs")
                .build();

        JobDetail resendEmailsJob = JobBuilder.newJob(SendEmailInErrorsJob.class)
                .usingJobData(data)
                .withIdentity("resendEmailsJob", "pollenJobs")
                .build();

        JobDetail deleteObsoleteSessionTokensJob = JobBuilder.newJob(DeleteObsoleteSessionTokensJob.class)
                .usingJobData(data)
                .withIdentity("deleteObsoleteSessionTokensJob", "pollenJobs")
                .build();

        JobDetail checkMailBoxJob = JobBuilder.newJob(CheckMailBoxJob.class)
                .usingJobData(data)
                .withIdentity("checkMailBoxJob", "pollenJobs")
                .build();

        JobDetail anonymizeOlderVotesJob = JobBuilder.newJob(AnonymizeOlderVoteJob.class)
                .usingJobData(data)
                .withIdentity("anonymizeOlderVotesJob", "pollenJobs")
                .build();

        try {
            scheduler = new StdSchedulerFactory().getScheduler();

            PollenServicesConfig applicationConfig = applicationContext.getApplicationConfig();

            // schedule send poll end reminders
            Trigger sendPollEndReminderTrigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("sendPollEndReminderTrigger", "pollenTriggers")
                    .withSchedule(CronScheduleBuilder.cronSchedule(applicationConfig.getSendEndPollRemindersCronSchedule()))
                    .build();

            scheduler.scheduleJob(sendPollEndReminderJob, sendPollEndReminderTrigger);

            // schedule send poll end reminders
            Trigger resendEmailsTrigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("resendEmailsTrigger", "pollenTriggers")
                    .withSchedule(CronScheduleBuilder.cronSchedule(applicationConfig.getResendEmailsCronSchedule()))
                    .build();

            scheduler.scheduleJob(resendEmailsJob, resendEmailsTrigger);

            // schedule send poll end reminders
            Trigger deleteObsoleteSessionTokensTrigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("deleteObsoleteSessionTokensTrigger", "pollenTriggers")
                    .withSchedule(CronScheduleBuilder.cronSchedule(applicationConfig.getDeleteObsoleteSessionTokensCronSchedule()))
                    .build();

            scheduler.scheduleJob(deleteObsoleteSessionTokensJob, deleteObsoleteSessionTokensTrigger);

            // schedule send poll end reminders
            Trigger checkMailBoxTrigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("checkMailBoxTrigger", "pollenTriggers")
                    .withSchedule(CronScheduleBuilder.cronSchedule(applicationConfig.getMailBoxCronSchedule()))
                    .build();

            scheduler.scheduleJob(checkMailBoxJob, checkMailBoxTrigger);

            // schedule rgpd purge
            Trigger anonymizeOlderVotesTrigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("anonymizeOlderVotesTrigger", "pollenTriggers")
                    .withSchedule(CronScheduleBuilder.cronSchedule(applicationConfig.getAnonymizeOlderVotesCronSchedule()))
                    .build();

            scheduler.scheduleJob(anonymizeOlderVotesJob, anonymizeOlderVotesTrigger);

            scheduler.start();

            if (log.isDebugEnabled()) {
                log.debug("schedulers launched");
            }
        } catch (SchedulerException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while launching the jobs", e);
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //-- close scheduler ---//
        if (scheduler != null) {
            if (log.isInfoEnabled()) {
                log.info("Stopping quartz sheduler");
            }

            try {
                // wait for thread to complete
                scheduler.shutdown();
            } catch (SchedulerException e) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't stop quartz", e);
                }
            }
        }

        // Get application context
        PollenRestApiApplicationContext applicationContext =
                PollenRestApiApplicationContext.getApplicationContext(
                        sce.getServletContext());

        // close it (and all underlined resources)
        if (applicationContext != null) {
            applicationContext.close();
        }
    }
}
