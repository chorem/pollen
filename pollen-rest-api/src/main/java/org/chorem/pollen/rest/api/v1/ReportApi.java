package org.chorem.pollen.rest.api.v1;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.ReportBean;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.ReportService;
import org.jboss.resteasy.annotations.GZIP;

import java.util.List;

@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ReportApi {

    @Inject
    private ReportService reportService;

    @Path("/polls/{pollId}/reports")
    @POST
    public void addReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                          ReportBean report) throws InvalidFormException {
        reportService.addPollReport(pollId.getEntityId(), report);
    }

    @Path("/polls/{pollId}/reports")
    @GET
    @GZIP
    public List<ReportBean> getReports(@PathParam("pollId") PollenEntityId<Poll> pollId) {
        return reportService.getPollReports(pollId.getEntityId());
    }

    @Path("/polls/{pollId}/reports/{reportId}")
    @POST
    public void saveReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                           ReportBean report) {
        reportService.savePollReport(pollId.getEntityId(), report);
    }

    @Path("polls/{pollId}/questions/{questionId}/reports")
    @POST
    public void addReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                          @PathParam("questionId") PollenEntityId<Question> questionId,
                          ReportBean report) throws InvalidFormException {
        reportService.addQuestionReport(pollId.getEntityId(), questionId.getEntityId(), report);
    }

    @Path("polls/{pollId}/questions/{questionId}/reports")
    @GET
    @GZIP
    public List<ReportBean> getReports(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                       @PathParam("questionId") PollenEntityId<Question> questionId) {
        return reportService.getQuestionReports(pollId.getEntityId(), questionId.getEntityId());
    }

    @Path("polls/{pollId}/questions/{questionId}/reports/{reportId}")
    @POST
    public void saveReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                           @PathParam("questionId") PollenEntityId<Choice> questionId, ReportBean report) {
        reportService.saveQuestionReport(pollId.getEntityId(), questionId.getEntityId(), report);
    }

    @Path("polls/{pollId}/questions/{questionId}/choices/{choiceId}/reports")
    @POST
    public void addReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                          @PathParam("questionId") PollenEntityId<Poll> questionId,
                          @PathParam("choiceId") PollenEntityId<Choice> choiceId,
                          ReportBean report) throws InvalidFormException {
        reportService.addChoiceReport(pollId.getEntityId(), questionId.getEntityId(), choiceId.getEntityId(), report);
    }

    @Path("polls/{pollId}/questions/{questionId}/choices/{choiceId}/reports")
    @GET
    @GZIP
    public List<ReportBean> getReports(@PathParam("pollId") PollenEntityId<Poll> pollId,
                                       @PathParam("questionId") PollenEntityId<Poll> questionId,
                                       @PathParam("choiceId") PollenEntityId<Choice> choiceId) {
        return reportService.getChoiceReports(pollId.getEntityId(), questionId.getEntityId(), choiceId.getEntityId());
    }

    @Path("polls/{pollId}/questions/{questionId}/choices/{choiceId}/reports/{reportId}")
    @POST
    public void saveReport(@PathParam("pollId") PollenEntityId<Poll> pollId,
                           @PathParam("questionId") PollenEntityId<Poll> questionId,
                           @PathParam("choiceId") PollenEntityId<Choice> choiceId, ReportBean report) {
        reportService.saveChoiceReport(pollId.getEntityId(), questionId.getEntityId(), choiceId.getEntityId(), report);
    }
}
