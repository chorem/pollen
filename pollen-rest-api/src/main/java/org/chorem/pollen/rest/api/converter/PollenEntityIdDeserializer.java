package org.chorem.pollen.rest.api.converter;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.type.SimpleType;
import org.chorem.pollen.rest.api.PollenRestApiApplicationContext;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactory;

import java.io.IOException;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollenEntityIdDeserializer extends JsonDeserializer<PollenEntityId> implements ContextualDeserializer {

    private JavaType valueType;

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        SimpleType type = (SimpleType) ctxt.getContextualType();
        JavaType valueType = type.containedType(0);
        PollenEntityIdDeserializer deserializer = new PollenEntityIdDeserializer();
        deserializer.valueType = valueType;
        return deserializer;
    }

    @Override
    public PollenEntityId deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String id = p.getText();

        PollenEntityId pollenEntityId = PollenEntityId.newId((Class<? extends TopiaEntity>) valueType.getRawClass());
        pollenEntityId.setReducedId(id);

        PollenRestApiApplicationContext applicationContext = PollenRestApiApplicationContext.getApplicationContext();
        TopiaIdFactory topiaIdFactory = applicationContext.getTopiaApplicationContext().getTopiaIdFactory();
        pollenEntityId.decode(topiaIdFactory);

        return pollenEntityId;

    }
}
