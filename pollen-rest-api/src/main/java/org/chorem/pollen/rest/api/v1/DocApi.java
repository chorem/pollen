package org.chorem.pollen.rest.api.v1;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.apache.commons.io.IOUtils;
import org.chorem.pollen.services.PollenTechnicalException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Created on 4/29/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@Path("")
public class DocApi {

    @Path("/doc")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String showMapping() {
        InputStream mappingUrl = getClass().getResourceAsStream("/mapping");
        try {
            String content = IOUtils.toString(mappingUrl, StandardCharsets.UTF_8);
            return content;
        } catch (IOException e) {
            throw new PollenTechnicalException(e);
        }
    }
}
