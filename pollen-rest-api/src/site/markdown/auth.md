# Auth API

## /v1/login

Method **POST** or **PUT**

Use basic authentication.

This will send you back two cookies: ``pollen-auth`` and ``pollen-connected``.

## /v1/login2

Method **POST** or **PUT**

Use POST form with parameters: `` login, password ``

This will send you back two cookies: ``pollen-auth`` and ``pollen-connected``.

## /v1/logout

Method **GET**

No parameters. Will remove the two cookies ``pollen-auth`` and ``pollen-connected`` from you browser.

## /v1/lostpassword/{email}

Method **GET**

Generates a new password and send it to user by email.

## /v1/resendValidation/{email}

Method **GET**

Generates a new invitation and send it to user by email.

## /v1/users

Method **POST**

Create the user given in ``user`` request parameter.

## /v1/users/connected

Method **GET**

Return the connected user.

## /v1/users/{userId}?{token}

Method **GET**

Validate user account.
