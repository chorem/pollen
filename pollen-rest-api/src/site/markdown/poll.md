# Poll API

## /v1/polls

Method **POST** or **PUT**

Create a new poll.

## /v1/polls/created

Method **GET**

Get polls created by connected user.