package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.assertTrue;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenUserApiTest extends AbstractPollenRestApiTest {

    @Before
    public void setUp() {

        loadFixtures("user-fixtures");
        loadFixtures("fixtures");
    }

    @Test
    public void getUsers() throws Exception {

        String sessionToken = login();

        URI uri = createRequest(RestApiFixtures.users(null, null))
                .addParameter("pageNumber", "0")
                .addParameter("pageSize", "-1")
                .build();
        Request request = Request.Get(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        String content = assertResponse(response);
        assertTrue(content.contains("email"));

    }

    @Test
    public void getUser() throws Exception {

        String sessionToken = login();

        PollenUser pollenUser = fixture("user_jean");
        String userId = encodeId(pollenUser.getTopiaId());
        URI uri = createRequest(RestApiFixtures.users(userId, null)).build();
        Request request = Request.Get(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        String content = assertResponse(response);
        assertTrue(content.contains("email"));
        assertTrue(content.contains("jean@pollen.org"));

    }

    @Ignore
    @Test
    public void postUser() throws Exception {

        URI uri = createRequest(RestApiFixtures.users(null, null)).build();
        Request request = Request.Post(uri);
        Response response = request.execute();
        String content = assertResponse(response);
        assertTrue(content.contains("email2"));

    }

    @Ignore
    @Test
    public void putUser() throws Exception {

        PollenUser pollenUser = fixture("user_jean");
        String userId = pollenUser.getTopiaId();
        URI uri = createRequest(RestApiFixtures.users(userId, null)).build();
        Request request = Request.Get(uri);
        Response response = request.execute();
        String content = assertResponse(response);
        assertTrue(content.contains("email3"));

    }

    @Test
    public void deleteUserAsAdmin() throws Exception {

        String sessionToken = login();

        PollenUser pollenUser = fixture("user_jean");
        String userId = encodeId(pollenUser.getTopiaId());
        URI uri = createRequest(RestApiFixtures.users(userId, null)).build();
        Request request = Request.Delete(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        assertEmptyResponse(response);
    }

    @Ignore
    @Test
    public void validateUserEmail() throws Exception {

        PollenUser pollenUser = fixture("user_jean");
        String userId = encodeId(pollenUser.getTopiaId());
        String token = "";
        URI uri = createRequest(RestApiFixtures.users(userId, token)).build();
        Request request = Request.Put(uri);
        Response response = request.execute();
        String content = assertResponse(response);
        assertTrue(content.contains("OK!"));
    }
}
