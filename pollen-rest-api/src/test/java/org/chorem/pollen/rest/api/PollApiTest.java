package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.chorem.pollen.persistence.entity.ChoiceType;
import org.chorem.pollen.persistence.entity.CommentVisibility;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.ResultVisibility;
import org.chorem.pollen.persistence.entity.VoteVisibility;
import org.chorem.pollen.rest.api.beans.PollCreateBean;
import org.chorem.pollen.services.PollenFixtures;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PollBean;
import org.chorem.pollen.services.bean.QuestionBean;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created on 4/29/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollApiTest extends AbstractPollenRestApiTest {

    @Before
    public void setUp() {
        loadFixtures("fixtures");
        loadFixtures("restricted-fixtures");
    }

    @Test
    public void getPollsNew() throws URISyntaxException, IOException {
        URI uri = createRequest(RestApiFixtures.polls("new", null)).build();
        Request request = Request.Get(uri);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void getPolls() throws URISyntaxException, IOException {
        String sessionToken = login();

        URI uri = createRequest(RestApiFixtures.polls(null, null)).build();
        Request request = Request.Get(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void getPollsCreated() throws URISyntaxException, IOException {

        String sessionToken = login();
        URI uri = createRequest(RestApiFixtures.polls(null, null))
                .addParameter("use", "created").build();
        Request request = Request.Get(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void getPollsInvited() throws URISyntaxException, IOException {
        String sessionToken = login();

        URI uri = createRequest(RestApiFixtures.polls(null, null))
                .addParameter("use", "invited").build();
        Request request = Request.Get(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void getPollsParticipated() throws URISyntaxException, IOException {
        String sessionToken = login();

        URI uri = createRequest(RestApiFixtures.polls(null, null))
                .addParameter("use", "participated").build();
        Request request = Request.Get(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void getPoll() throws URISyntaxException, IOException {

        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        String pollId = encodeId(poll.getTopiaId());
        URI uri = createRequest(RestApiFixtures.polls(pollId, null)).build();
        Request request = Request.Get(uri);
        Response response = request.execute();
        assertResponse(response);

    }

    @Test
    public void postPoll() throws Throwable {

        PollBean poll = new PollBean();
        poll.setPollType(PollType.FREE);
        poll.setCommentVisibility(CommentVisibility.EVERYBODY);
        poll.setResultVisibility(ResultVisibility.EVERYBODY);
        poll.setVoteVisibility(VoteVisibility.EVERYBODY);
        poll.setTitle("title");
        poll.setEndDate(null);

        List<QuestionBean> questions = new ArrayList<>();
        QuestionBean question = new QuestionBean();
        question.setTitle("Question A");
        question.setVoteCountingType(1);
        questions.add(question);

        List<ChoiceBean> choices = new ArrayList<>();
        ChoiceBean choice1 = new ChoiceBean();
        choice1.setChoiceType(ChoiceType.TEXT);
        choice1.setChoiceValue("choiceA");
        choices.add(choice1);
        ChoiceBean choice2 = new ChoiceBean();
        choice2.setChoiceType(ChoiceType.TEXT);
        choice2.setChoiceValue("choiceB");
        choices.add(choice2);

        question.setChoices(choices);
        poll.setQuestions(questions);

        PollCreateBean pollCreateBean = new PollCreateBean();
        pollCreateBean.setPoll(poll);

        String pollCreateStr = getObjectMapper().writeValueAsString(pollCreateBean);

        URI uri = createRequest(RestApiFixtures.polls(null, null)).build();

        Request request = Request.Post(uri);
        request.bodyString(pollCreateStr, ContentType.APPLICATION_JSON);
        request.setHeader(PollenRestApiRequestFilter.REQUEST_HEADER_UI_CONTEXT,
                getObjectMapper().writeValueAsString(fixture("pollenUIContext_chorem")));
        Response response = request.execute();
        assertResponse(response);

    }

    @Test
    public void postBadPoll() throws Throwable {

        PollBean poll = new PollBean();
        poll.setPollType(PollType.FREE);
        poll.setCommentVisibility(CommentVisibility.EVERYBODY);
        poll.setResultVisibility(ResultVisibility.EVERYBODY);
        poll.setVoteVisibility(VoteVisibility.EVERYBODY);
        poll.setTitle("title");

        List<QuestionBean> questions = new ArrayList<>();
        QuestionBean question = new QuestionBean();
        question.setTitle("Question A");
        question.setVoteCountingType(1);
        questions.add(question);

        List<ChoiceBean> choices = new ArrayList<>();

        question.setChoices(choices);
        poll.setQuestions(questions);

        PollCreateBean pollCreateBean = new PollCreateBean();
        pollCreateBean.setPoll(poll);

        String pollCreateStr = getObjectMapper().writeValueAsString(pollCreateBean);

        URI uri = createRequest(RestApiFixtures.polls(null, null)).build();
        Request request = Request.Post(uri);
        request.bodyString(pollCreateStr, ContentType.APPLICATION_JSON);
        request.setHeader(PollenRestApiRequestFilter.REQUEST_HEADER_UI_CONTEXT,
                getObjectMapper().writeValueAsString(fixture("pollenUIContext_chorem")));
        Response response = request.execute();
        HttpResponse httpResponse = response.returnResponse();
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        Assert.assertEquals(400, statusCode);

    }

    @Ignore
    @Test
    public void putPoll() throws URISyntaxException, IOException {
        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        String pollId = poll.getTopiaId();
        URI uri = createRequest(RestApiFixtures.polls(pollId, null)).build();
        Request request = Request.Put(uri);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void deletePoll() throws URISyntaxException, IOException {
        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        String pollId = encodeId(poll.getTopiaId());
        URI uri = createRequest(RestApiFixtures.polls(pollId, null))
                .addParameter(PollenRestApiRequestFilter.REQUEST_PERMISSION_PARAMETER, poll.getCreator().getPermission().getToken())
                .build();
        Request request = Request.Delete(uri);

        Response response = request.execute();
        assertEquals(204, response.returnResponse().getStatusLine().getStatusCode());
    }

    @Ignore
    @Test
    public void clonePoll() throws URISyntaxException, IOException {
        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        String pollId = encodeId(poll.getTopiaId());
        URI uri = createRequest(RestApiFixtures.polls(pollId, null)).build();
        Request request = Request.Post(uri);
        Response response = request.execute();
        assertResponse(response);
    }

    @Ignore
    @Test
    public void exportPoll() throws URISyntaxException, IOException {
        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        String pollId = encodeId(poll.getTopiaId());
        URI uri = createRequest(RestApiFixtures.polls(pollId, "export")).build();
        Request request = Request.Get(uri);
        Response response = request.execute();
        assertResponse(response);
    }

    @Ignore
    @Test
    public void closePoll() throws URISyntaxException, IOException {
        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        String pollId = encodeId(poll.getTopiaId());
        URI uri = createRequest(RestApiFixtures.polls(pollId, "close")).build();
        Request request = Request.Post(uri);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void assignPoll() throws URISyntaxException, IOException {
        String sessionToken = login();

        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        String pollId = encodeId(poll.getTopiaId());

        URI uri = createRequest(RestApiFixtures.polls(pollId, "assign"))
                .addParameter(PollenRestApiRequestFilter.REQUEST_PERMISSION_PARAMETER, poll.getCreator().getPermission().getToken())
                .build();
        Request request = Request.Put(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        assertResponse(response);
    }

    @Test
    public void listParticipants() throws URISyntaxException, IOException {
        String sessionToken = login();

        Poll poll = fixture(PollenFixtures.POLL_RESTRICTED_ID);
        String pollId = encodeId(poll.getTopiaId());

        URI uri = createRequestWithoutPagination(RestApiFixtures.polls(pollId, "participants"))
                .addParameter(PollenRestApiRequestFilter.REQUEST_PERMISSION_PARAMETER, poll.getCreator().getPermission().getToken())
                .build();
        Request request = Request.Get(uri);
        request.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, sessionToken);
        Response response = request.execute();
        assertResponse(response);
    }
}
