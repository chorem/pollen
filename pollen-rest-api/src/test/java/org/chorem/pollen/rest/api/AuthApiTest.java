package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


/**
 * Created on 5/20/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class AuthApiTest extends AbstractPollenRestApiTest {

    @Test
    public void loginTest() throws URISyntaxException, IOException {


        URI uri = createRequest(RestApiFixtures.login())
                .addParameter("login", "admin@chorem.org")
                .addParameter("password", "admin")
                .build();

        Request request = Request.Post(uri);
        Response response = request.execute();
        assertResponse(response);

    }

    @Test
    public void loginThenLogout() throws URISyntaxException, IOException {

        URI uri = createRequest(RestApiFixtures.login())
                .addParameter("login", "admin@chorem.org")
                .addParameter("password", "admin")
                .build();

        Request login = Request.Post(uri);

        Response response = login.execute();
        String loginContent = assertResponse(response);

        PollenEntityRef createBeanRef = getObjectMapper().readValue(loginContent, new TypeReference<PollenEntityRef<PollenUser>>() {});

        URI uriOut = createRequest(RestApiFixtures.logout()).build();

        Request logout = Request.Delete(uriOut);
        logout.addHeader(PollenRestApiRequestFilter.REQUEST_HEADER_SESSION_TOKEN, createBeanRef.getPermission());

        int statusCode = logout.execute().returnResponse().getStatusLine().getStatusCode();
        Assert.assertEquals(204, statusCode);

    }

    @Test
    public void badLogin() throws URISyntaxException, IOException {

        URI uri = createRequest(RestApiFixtures.login())
                .addParameter("login", "admin@chorem.org" + System.nanoTime())
                .addParameter("password", "admin" + System.nanoTime())
                .build();

        Request request = Request.Post(uri);

        Response response = request.execute();
        int statusCode = response.returnResponse().getStatusLine().getStatusCode();
        Assert.assertEquals(401, statusCode);

    }

    @Test
    public void badPassword() throws URISyntaxException, IOException {

        URI uri = createRequest(RestApiFixtures.login())
                .addParameter("login", "admin@chorem.org")
                .addParameter("password", "admin" + System.nanoTime())
                .build();

        Request request = Request.Post(uri);

        Response response = request.execute();
        int statusCode = response.returnResponse().getStatusLine().getStatusCode();
        Assert.assertEquals(401, statusCode);

    }
}
