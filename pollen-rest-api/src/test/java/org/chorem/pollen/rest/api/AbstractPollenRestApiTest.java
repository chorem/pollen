package org.chorem.pollen.rest.api;

/*
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.Globals;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.util.EntityUtils;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.PollenTopiaPersistenceContext;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.rest.api.converter.JacksonConfig;
import org.chorem.pollen.services.PollenServiceContext;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.chorem.pollen.services.test.FakePollenApplicationContext;
import org.chorem.pollen.services.test.FakePollenSecurityContext;
import org.chorem.pollen.services.test.FakePollenServiceContext;
import org.jboss.weld.environment.servlet.Listener;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.util.DateUtil;

import jakarta.ws.rs.core.HttpHeaders;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

import static org.apache.tomcat.JarScanType.TLD;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class AbstractPollenRestApiTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractPollenRestApiTest.class);

    @Rule
    public final FakePollenApplicationContext application = new FakePollenApplicationContext("pollen-rest-api-test.properties");

    protected ObjectMapper objectMapper;

    protected Tomcat server;

    protected ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            JacksonConfig jacksonConfig = new JacksonConfig();
            objectMapper = jacksonConfig.getContext(ObjectMapper.class);
        }
        return objectMapper;
    }

    protected void loadFixtures(String fixturesSetName) {
        PollenTopiaPersistenceContext persistenceContext = application.newPersistenceContext();
        PollenServiceContext serviceContext = application.newServiceContext(persistenceContext, Locale.FRANCE);
        application.loadFixtures(serviceContext, fixturesSetName);
    }

    protected <E> E fixture(String id) {
        return application.fixture(id);
    }

    protected String encodeId(String entityId) {
        TopiaIdFactory topiaIdFactory = application.getTopiaApplicationContext().getTopiaIdFactory();
        return PollenEntityId.encode(topiaIdFactory, entityId);
    }

    @Before
    public void startServer() throws Exception {
        PollenRestApiApplicationContext applicationContext =
                new PollenRestApiApplicationContext(application.getApplicationConfig(), application.getTopiaApplicationContext()) {
                    @Override
                    public PollenServiceContext newServiceContext(PollenPersistenceContext persistenceContext, Locale locale) {
                        return FakePollenServiceContext.newServiceContext(
                                DateUtil.createDate(1, 1, 2014),
                                Locale.FRANCE,
                                application.getApplicationConfig(),
                                application.getTopiaApplicationContext(),
                                application.newPersistenceContext(),
                                application.getVoteCountingFactory());
                    }

                    @Override
                    public PollenSecurityContext newSecurityContext(PollenUser pollenUser, PollenPrincipal mainPrincipal) {
                        FakePollenSecurityContext securityContext = new FakePollenSecurityContext();
                        securityContext.setMainPrincipal(mainPrincipal);
                        securityContext.setPollenUser(pollenUser);
                        return securityContext;
                    }
                };

        applicationContext.init();

        PollenRestApiApplicationContext.setApplicationContext(applicationContext);

        // set catalina base directory
        System.setProperty(Globals.CATALINA_BASE_PROP, new File(getServerBaseDirectory()).getAbsolutePath());
        System.setProperty("pollen.data.directory", application.getTestBasedir().getAbsolutePath());

        // create server
        server = new Tomcat();
        server.setPort(application.getPort());

        // create web app
        final StandardContext context = (StandardContext) server.addWebapp(getContextPath(), getWebappLocation());
        final WebResourceRoot resources = new StandardRoot(context);
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes", new File("target/classes").getAbsolutePath(), "/"));
        context.setResources(resources);

        // trick to exclude mchange-commons-java jar file from jar scanner that cause error
        context.getJarScanner().setJarScanFilter((jarScanType, s) -> TLD.equals(jarScanType) && !s.contains("mchange-commons-java"));

        // add listener for weld that do CDI
        context.addApplicationListener(Listener.class.getName());

        server.enableNaming();

        //Depuis Tomcat9 il n'y a plus de connector par défaut, appeler le getter en crée un
        server.getConnector();
        server.start();
    }

    protected String getWebappLocation() {
        return new File("src/main/webapp").getAbsolutePath();
    }

    protected String getServerBaseDirectory() {
        return new File(application.getTestBasedir(), "tomcat_" + application.getPort()).getAbsolutePath();
    }

    @After
    public void stopServer() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("closing application context " + application);
        }
        application.close();
        server.stop();
        server.destroy();
    }

    protected String assertResponse(Response response) throws IOException {
        HttpResponse httpResponse = response.returnResponse();
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        Assert.assertEquals(200, statusCode);
        String content = EntityUtils.toString(httpResponse.getEntity());
        showTestResult(content);
        Assert.assertNotNull(content);
        return content;
    }

    protected void assertEmptyResponse(Response response) throws IOException {
        HttpResponse httpResponse = response.returnResponse();
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        Assert.assertEquals(204, statusCode);
        Assert.assertNull(httpResponse.getEntity());
    }

    protected void showTestResult(String content) {
        String testName = application.getMethodName();
        if (log.isInfoEnabled()) {
            log.info("test *" + testName + "* result\n" + content);
        }
    }

    protected String getContextPath() {
        return "/";
    }

    public URIBuilder createRequest(String url) {
        String contextPath = getContextPath();

        String path;
        if (contextPath.endsWith("/") && url.startsWith("/")) {
            path = getContextPath() + url.substring(1);
        } else if (contextPath.endsWith("/") ^ url.startsWith("/")) {
            path = getContextPath() + url;
        } else {
            path = getContextPath() + "/" + url;
        }

        URIBuilder builder = new URIBuilder()
                .setScheme("http")
                .setHost("localhost")
                .setPort(application.getPort())
                .setPath(path)
                .setParameter("order", "topiaCreateDate")
                .setParameter("desc", "true")
                .setParameter("pageSize","-1");
        return builder;
    }

    public URIBuilder createRequestWithoutPagination(String url) {
        String contextPath = getContextPath();

        String path;
        if (contextPath.endsWith("/") && url.startsWith("/")) {
            path = getContextPath() + url.substring(1);
        } else if (contextPath.endsWith("/") ^ url.startsWith("/")) {
            path = getContextPath() + url;
        } else {
            path = getContextPath() + "/" + url;
        }

        URIBuilder builder = new URIBuilder()
                .setScheme("http")
                .setHost("localhost")
                .setPort(application.getPort())
                .setPath(path);
        return builder;
    }

    protected String login() throws URISyntaxException, IOException {
        URI uri = createRequest(RestApiFixtures.login())
                .addParameter("login", "admin@chorem.org")
                .addParameter("password", "admin")
                .build();
        Request request = Request.Post(uri);

        Response response = request.execute();
        HttpResponse httpResponse = response.returnResponse();
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        Assert.assertEquals(200, statusCode);
        String content = EntityUtils.toString(httpResponse.getEntity());
        showTestResult(content);
        Assert.assertNotNull(content);

        String token = "";
        HeaderElementIterator it = new BasicHeaderElementIterator(httpResponse.headerIterator(HttpHeaders.SET_COOKIE));
        while (it.hasNext()) {
            HeaderElement elem = it.nextElement();
            if (elem.getName().equals(PollenRestApiRequestFilter.COOKIE_POLLEN_AUTH)) {
                token = elem.getValue();
            }
        }
        return token;
    }
}
