package org.chorem.pollen.rest.api;

/*-
 * #%L
 * Pollen :: Rest Api
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Created on 15/01/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class RestApiFixtures {

    protected static final String LOGIN_API = "/v1/login2";
    protected static final String LOGOUT_API = "/v1/logout";
    protected static final String USERS_API = "/v1/users";
    protected static final String POLLS_API = "/v1/polls";

    public static String login() {
        return LOGIN_API;
    }

    public static String users(String userId, String token) {
        return api(USERS_API, userId, "?token=", token);
    }

    public static String polls(String pollId, String token) {
        return api(POLLS_API, pollId, "/", token);
    }

    public static String votes(String pollId, String questionId, String voteId, String token) {
        return api(POLLS_API + pollId + "/questions/" + questionId + "/votes", voteId, "/", token);
    }


    public static String logout() {
        return LOGOUT_API;
    }

    private static String api(String api, String idOrAction, String tokenPrefix, String token) {
        String result = api;
        if (idOrAction != null) {
            result += "/" + idOrAction;
        }
        if (token != null) {
            result += tokenPrefix + token;
        }
        return result;
    }
}
