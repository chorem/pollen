/*
 * #%L
 * Pollen :: VoteCounting :: Borda
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Condorcet.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class BordaVoteCountingStrategy extends AbstractVoteCountingStrategy<BordaConfig> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(BordaVoteCountingStrategy.class);

    @Override
    public VoteCountingResult votecount(Set<Voter> voters) {

        // get empty result by choice
        Map<String, ChoiceScore> scores = newEmptyChoiceScoreMap(voters);
        BordaDetailResult detailResult = new BordaDetailResult();

        // nb of choices
        int nbChoices = scores.keySet().size();

        if (log.isDebugEnabled()) {
            log.debug("Nb choices: " + nbChoices);
        }

        // nb Ranks
        int nbRanks = nbChoices;
        if (config.getMaxChoiceNumber() > 0) {
            nbRanks = config.getMaxChoiceNumber();
        }

        // calcul pour chaque votant de sa liste des choix dans son ordre préféré

        Map<Voter, List<Set<String>>> voterSortedChoices =
                buildVoterSortedChoices(voters);

        // calcul des points pour chaque choix selon son ordre d'arrivé (
        // le 1er à nbChoices * weight point, le second (nbChoices-1) * weight,...)

        for (Map.Entry<Voter, List<Set<String>>> entry : voterSortedChoices.entrySet()) {
            Voter voter = entry.getKey();
            double weight = voter.getWeight();
            List<Integer> pointsByRank = config.getPointsByRank();

            int rank = 0;

            for (Set<String> sortedChoiceId : entry.getValue()) {

                double choiceWeight;

                if (CollectionUtils.isNotEmpty(pointsByRank)) {

                    if (pointsByRank.size() > rank)  {

                        choiceWeight = pointsByRank.get(rank) * weight;

                    } else {

                        choiceWeight = 0;

                    }

                } else {

                    choiceWeight = (nbRanks - rank) * weight;

                }

                for (String choiceId : sortedChoiceId) {

                    scores.get(choiceId).addScoreValue(choiceWeight);
                    addChoiceRank(detailResult, choiceId, rank, weight);

                }
                rank++;
                
                if (rank > nbRanks) {
                    break;
                }
            }
        }

        // order scores (using their value) and return result
        return orderByValues(scores.values(), detailResult);
    }

    @Override
    public Set<VoteForChoice> toVoteForChoices(VoteCountingResult voteCountingResult) {
        Set<VoteForChoice> voteForChoices = Sets.newHashSet();

        for (ChoiceScore choiceScore : voteCountingResult.getScores()) {

            double score = choiceScore.getScoreOrder();
            VoteForChoice voteForChoice = VoteForChoice.newVote(
                    choiceScore.getChoiceId(),
                    score);
            voteForChoices.add(voteForChoice);
        }
        return voteForChoices;
    }

    protected void addChoiceRank(BordaDetailResult detailResult, String choiceId, int rank, double score) {
        Optional<BordaChoiceRank> choiceRankOptional = detailResult.getChoiceRanks().stream()
                .filter(cr -> cr.getChoiceId().equals(choiceId)
                        && cr.getRank() == rank)
                .findFirst();
        BordaChoiceRank choiceRank;
        if (choiceRankOptional.isPresent()) {
            choiceRank = choiceRankOptional.get();
        } else {
            choiceRank = new BordaChoiceRank();
            choiceRank.setChoiceId(choiceId);
            choiceRank.setRank(rank);
            detailResult.getChoiceRanks().add(choiceRank);
        }
        choiceRank.addScoreValue(score);
    }
}
