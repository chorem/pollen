package org.chorem.pollen.votecounting;

/*
 * #%L
 * Pollen :: VoteCounting :: Borda
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import org.chorem.pollen.votecounting.model.ChoiceToVoteRenderType;
import org.chorem.pollen.votecounting.model.VoteForChoice;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.n;

/**
 * Borda vote counting entry point.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6
 */
public class BordaVoteCounting extends AbstractVoteCountingMinMaxChoice<BordaVoteCountingStrategy, BordaConfig> {

    public static final int VOTECOUNTING_ID = 5;

    public BordaVoteCounting() {
        super(VOTECOUNTING_ID,
              BordaVoteCountingStrategy.class,
              BordaConfig.class,
              n("pollen.voteCountingType.borda"),
              n("pollen.voteCountingType.borda.shortHelp"),
              n("pollen.voteCountingType.borda.help")
        );
    }

    @Override
    public ChoiceToVoteRenderType getVoteValueEditorType() {
        return ChoiceToVoteRenderType.TEXTFIELD;
    }

    @Override
    public Double getMinimumValue() {
        return 1d;
    }

    @Override
    public Multimap<String, String> checkVoteForChoice(VoteForChoice voteForChoice, BordaConfig config, Locale locale) {
        Multimap<String, String> errorMap = super.checkVoteForChoice(voteForChoice, config, locale);

        Double voteValue = voteForChoice.getVoteValue();
        if (voteValue != null && voteValue < 0) {
            errorMap.put(
                    VoteForChoice.PROPERTY_VOTE_VALUE,
                    l(locale, "pollen.voteCountingType.borda.voteValue.error.positive", voteValue));
        }

        return errorMap;

    }
}
