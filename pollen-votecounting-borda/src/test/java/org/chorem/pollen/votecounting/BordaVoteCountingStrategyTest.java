/*
 * #%L
 * Pollen :: VoteCounting :: Borda
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package org.chorem.pollen.votecounting;

import com.google.common.collect.Sets;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.ListOfVoter;
import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.SimpleVoter;
import org.chorem.pollen.votecounting.model.SimpleVoterBuilder;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the {@link BordaVoteCountingStrategy}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.5
 */
public class BordaVoteCountingStrategyTest {

    public static final String CHOICE_A = "a";

    public static final String CHOICE_B = "b";

    public static final String CHOICE_C = "c";

    public static final String CHOICE_D = "d";

    protected static BordaVoteCounting voteCounting;

    protected BordaVoteCountingStrategy strategy;

    @BeforeClass
    public static void beforeClass() throws Exception {
        VoteCountingFactory factory = new VoteCountingFactory();
        voteCounting = BordaVoteCounting.class.cast(factory.getVoteCounting(BordaVoteCounting.VOTECOUNTING_ID));
    }

    @Before
    public void setUp() throws Exception {
        strategy = voteCounting.newStrategy();
        BordaConfig config = new BordaConfig();
        strategy.setConfig(config);
    }

    @Test
    public void simpleVotecount() throws Exception {

        // see http://fr.wikipedia.org/wiki/M%C3%A9thode_Borda

        // Ville	1re	2e	3e	4e	Points
        // A	    42	0	0	58	226 (=42*4+58*1)
        // B	    26	42	32	0	294 (=26*4+42*3+32*2)
        // C	    15	43	42	0	273 (=15*4+43*3+42*2)
        // D	    17	15	26	42	207 (=17*4+15*3+26*2+42)


        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("Ville A", 42.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 3.)
                .addVoteForChoice(CHOICE_D, 4.)
                .newVoter("Ville B", 26.)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .addVoteForChoice(CHOICE_D, 3.)
                .addVoteForChoice(CHOICE_A, 4.)
                .newVoter("Ville C", 15.)
                .addVoteForChoice(CHOICE_C, 1.)
                .addVoteForChoice(CHOICE_D, 2.)
                .addVoteForChoice(CHOICE_B, 3.)
                .addVoteForChoice(CHOICE_A, 4.)
                .newVoter("Ville D", 17.)
                .addVoteForChoice(CHOICE_D, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .addVoteForChoice(CHOICE_B, 3.)
                .addVoteForChoice(CHOICE_A, 4.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(226.0), 2),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(294.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(273.0), 1),
                        ChoiceScore.newScore(CHOICE_D, BigDecimal.valueOf(207.0), 3))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount0() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=1 b=2    c=null) a(3) > b(2) > c(1)
        // 2      (a=3 b=2    c=1)    c(3) > b(2) > a(1)
        // 3      (a=1 b=null c=2)    a(3) > c(2) > b(1)
        // Result a = 2*3+1=7, b =2*2+1=5, c = 1*3+1*2+1=6
        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 3.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(7.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(5.0), 2),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(6.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount2() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=1    b=2    c=null) a>b>c
        // 2      (a=1    b=2    c=1)    a|c>b
        // 3      (a=null b=null c=2)    c>a|b
        // Result (a=3*2+2=8    b=2*3=6    c=1+2*3=7)

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(8.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(6.0), 2),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(7.0), 1))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void simpleVotecount3() throws Exception {

        // Simple poll (all weight to 1)
        // 1      (a=1    b=null c=null) a(3) > b|c(2)
        // 2      (a=null b=1    c=null) b(3) > a|c(2)
        // 3      (a=null b=null c=1)    c(3) > a|c(2)
        // Result (a=3+2*2=7 b=3+2*2=7  c=3+2*2=7)

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 1.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(7.0), 0),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(7.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(7.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount1() throws Exception {

        // poll with weighted vote
        // 1 (x2) (a=1    b=null c=null) a(3) > b|c (2)
        // 2 (x1) (a=null b=1    c=null) b(3) > a|c (2)
        // 3 (x1) (a=null b=1    c=2)    b(3) > c(2) > a(1)
        // Result (a=2*3+1*2+1*1=9   b=2*2+1*3+1*3=10  c=2*2+1*2+1*2=8)

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, 2.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(9.0), 1),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(10.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(8.0), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }

    @Test
    public void weightedVotecount2() throws Exception {

        // poll with weighted vote
        // 1 (x2) (a=1    b=null c=null) a(3) > b|c (2)
        // 2 (x1) (a=null b=1    c=null) b(3) > a|c (2)
        // 3 (x3) (a=null b=2    c=1)    c(3) > b(2) > a(1)
        // Result (a=1*3*2+1*2+1*1*3=11    b=2*2+1*3+3*2=13    c=2*2+1*2+3*3=15)

        Set<Voter> voters = new SimpleVoterBuilder()
                .newVoter("1", 2.)
                .addVoteForChoice(CHOICE_A, 1.)
                .addVoteForChoice(CHOICE_B, null)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("2", 1.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 1.)
                .addVoteForChoice(CHOICE_C, null)
                .newVoter("3", 3.)
                .addVoteForChoice(CHOICE_A, null)
                .addVoteForChoice(CHOICE_B, 2.)
                .addVoteForChoice(CHOICE_C, 1.)
                .getVoters();

        VoteCountingResult result = strategy.votecount(voters);

        assertThat(result).isNotNull();
        assertThat(result.getScores())
                .isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(11.0), 2),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(13.0), 1),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(15.0), 0))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }



    @Test
    public void listVotecount() throws Exception {
        //           A  B  C
        // G1U1      1  2  3  * 2
        // G1U2      1  2  2
        // G1U3      3  2  1
        // Result G1 6  4  3
        // Vote G1   1  2  3

        // G2U1      1  2  3  * 2
        // G2U2      3  2  1
        // G2U3      3  1  2
        // Result G2 4  4  3
        // Vote G1   1  1  2 * 2

        // Result    6  3  1

        ListOfVoter voters = ListOfVoter.newVoter(null, 1, Sets.newHashSet(
                ListOfVoter.newVoter("G1", 1, Sets.newHashSet(
                        SimpleVoter.newVoter("G1U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 3d))),
                        SimpleVoter.newVoter("G1U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 2d))),
                        SimpleVoter.newVoter("G1U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 3d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 1d))))),
                ListOfVoter.newVoter("G2", 2, Sets.newHashSet(
                        SimpleVoter.newVoter("G2U1", 2, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 1d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 3d))),
                        SimpleVoter.newVoter("G2U2", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 3d),
                                VoteForChoice.newVote(CHOICE_B, 2d),
                                VoteForChoice.newVote(CHOICE_C, 1d))),
                        SimpleVoter.newVoter("G2U3", 1, Sets.newHashSet(
                                VoteForChoice.newVote(CHOICE_A, 3d),
                                VoteForChoice.newVote(CHOICE_B, 1d),
                                VoteForChoice.newVote(CHOICE_C, 2d)))))
        ));

        ListVoteCountingResult result = strategy.votecount(voters);

        assertThat(result)
                .isNotNull();
        assertThat(result.getMainResult()).isNotNull();
        assertThat(result.getMainResult().getScores()).isNotNull()
                .containsExactlyInAnyOrder(
                        ChoiceScore.newScore(CHOICE_A, BigDecimal.valueOf(7.0), 1),
                        ChoiceScore.newScore(CHOICE_B, BigDecimal.valueOf(8.0), 0),
                        ChoiceScore.newScore(CHOICE_C, BigDecimal.valueOf(3.0), 2))
                .isSortedAccordingTo(ChoiceScore::compareTo);
    }


}
