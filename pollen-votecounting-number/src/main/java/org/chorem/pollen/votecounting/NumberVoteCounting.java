package org.chorem.pollen.votecounting;

/*
 * #%L
 * Pollen :: VoteCounting :: Number
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.votecounting.model.ChoiceToVoteRenderType;
import org.chorem.pollen.votecounting.model.MinMaxChoicesNumberConfig;

import static org.nuiton.i18n.I18n.n;

/**
 * Number vote counting entry point.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6
 */
public class NumberVoteCounting extends AbstractVoteCountingMinMaxChoice<NumberVoteCountingStrategy, MinMaxChoicesNumberConfig> {

    public static final int VOTECOUNTING_ID = 4;

    public NumberVoteCounting() {
        super(VOTECOUNTING_ID,
                NumberVoteCountingStrategy.class,
                MinMaxChoicesNumberConfig.class,
                n("pollen.voteCountingType.number"),
                n("pollen.voteCountingType.number.shortHelp"),
                n("pollen.voteCountingType.number.help")
        );
    }

    @Override
    public ChoiceToVoteRenderType getVoteValueEditorType() {
        return ChoiceToVoteRenderType.TEXTFIELD;
    }

    @Override
    public Double getMinimumValue() {
        return null;
    }
}
