#!/bin/bash

VERSION=16
DB=pollen
docker exec -it postgres-${VERSION}-${DB} psql -U postgres ${DB}