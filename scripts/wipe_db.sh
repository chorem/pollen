#!/bin/bash

VERSION=16
DB_DIR=/home/postgresql-${VERSION}
DB=pollen
docker stop postgres-${VERSION}-${DB}
sudo rm ${DB_DIR}-${DB} -rf
