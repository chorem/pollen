## 3.0.0 (2017/18/28)

  - The registration form does not display on mobiles
  - Impossible de réouvrir un sondage ou de le cloner
  - Github sends the "null" string as email
  - Problème en cas d'égalité sur un dépouillement de type Coombs
  - Wrong implementation of Condorcet
  - Les sauts de ligne ne sont pas affihés dans les descriptions
  - Cannot sign in just after logging out
  - Number does not work if it is not first choice
  - Do not allow a user tom import a voter list if he has no list
  - soumission multiple de commentaire
  - soumission multiple de choix
  - le bascule de langue ne marche pas pour les écran de création de sondage
  - Création sondage : vidage d'un choix apres la soumission d'un sondage avec des choix en doublon
  - Message d'erreur a l'import d'un fichier de liste de favorie non explicite
  - Mauvaise URL sans message
  - Creéation sondage, Perte d'info en navigant dans les onglet
  - Erreur pseudo trop long dans le commentaire
  - Detection de robot en cas de saisie automatique
  - Letter avatar avec un seul lettre
  - Choix de type resource avec un fichier sans extension

## 3.0.1 (2017/10/03)

  - I cannot edit my vote in a restricted poll
  - The icons to log in through 3rd party providers do not display if I want to log in after my token expires
  - I cannot vote to a restricted poll if I am logged in as the creator

## 3.0.2 (2017/10/18)
  - Heure GMT dans les mails
  - Import des membres de liste de votants
## 3.1.0 (2017/23/05)

  - I cannot delete my account
  - Retour sur les adresse mail inexistante
  - sauvegarde de la langue de l'UI
  - Remove DevMode
  - Several email addresses for the users
  - Avatar for the users
  - Yes/No/Maybe choice
  - Suggest all the user's email addresses when he creates a poll
  - Revoire le composant Riot JS de votes
  - Services de gestion des droits utilisateur
  - Créationde sondage a répétition
  - Add me automatically in the voters of a restricted poll
  - Add request parameters to choose the size of the resource preview
  - Ajout du Jugement Majoritaire
  - Configuration des méthodes de scrutins
  - écran des votes, optimisation des requêtes serveur
  - Add user documentation for new pollen3 interface
  - droit utilisateurs
  - Limiter le nombre de participants des sondage pour une utilisation de l'api
  - Legal disclaimers

## 3.1.1 (2018/07/08)

  - I cannot add choices to my polls anymore
  - Mauvais dépouillement en Coombs
  - Wrong display of yes/no votes
  - Affichage du décompte des votes incomplet en Coombs
  - Résultat non visible pour un sondage ferné
  - Bug saisie des date et heure sous firefox 57
  - Agrandir la taille des miniatures dans le cas de choix de type 'image'
  - Améliorer le style des tooltip sur les choix

## 3.1.2 (2018/02/02)

  - Ajout de choix : on ne peut pas en ajouter plusieurs à la suite sans devoir recharger la page
  - Vote Anonyme : les votes réalisés n'apparaissent pas dans le tableau de descriptif

## 3.1.3 (2018/03/19)

  - Correction de la visibilité du sondage pour le créateur

## 3.1.4 (2018/05/15)

  - [Vote cumulatif] Je suis limité à 100 par vote alors que le max est à 500
  - on peut voter plusieurs fois sur un sondage restreint
  - L'ajout d'un choix fait disparaitre les prévisualisation d'une image
  - la description d'un choix ne s'ouvre pas
  - Lien pour un don dans l'entête
  - Vote de Condorcet : Duels et non Combats
  - Ajouter un lien vers la doc pollen sur la page d'accueil de Pollen (footer ?)
  - Les colonnes "Résultats" devraient s'intituler "Score" en Borda, nombre et cumulatif
  - Ajout de documentation fonctionnelle sur http://doc.pollen.cl/
  - Ajouter le mail dans le feedback

## 3.2.0 (2018/07/01)

  - Jugement majoritaire : ne plus modifer les mention après le premier vote
  - Ordre des choix
  - envoie de mail chiffré
  - Créer l'option Liste d'émargement
  - Composant de vote par ordre
  - Indiquer le numéro de déclaration  CNIL de Pollen dans la page de politique de confidentialité
  - Restreindre un sondage sur les emails des votants
  - Améliorer l'affichage des résultats en 'Coombs' en précisant le tour d'élimintation
  - Lors d'un vote cumulatif, le "Nombre de points à distribuer" devrait être affiché à l'utilisateur
  - Validation des emails sans token en base
  - Envoie des mails d'invitations différé
  - Revoir la validation des votes par les méthodes de votes

## 3.2.3 (2018/09/07)

  - Erreur à la création d'un sondage avec un seul choix

## 3.2.4 (2018/09/21)

  - Suppression des emails de la liste d'émargement
  - Prendre en compte la RGPD

## 3.3.0 (2020/03/26)

  - Commentaires non visibles par défaut
  - Error à la modification d'un sondage de type Borda
  - Ajouter la symbolique "Champ obligatoire" *
  - Error during the building 3.3.0 snapshot - Build only on jdk 8
  - Confirmation de vote
  - Ajouter du style dans la description du vote et des choix.
  - Mise-à-jour automatique d'un sondage
  - Organisation du pied de page
  - Ne plus valider les CGU en cas de modification d'un vote
  - Corriger la génération de la documentation
  - Vote associatif  - Revoir l'écran de création du sondage
  - Vote associatif - Page de résumé du vote (choix fait à chaque question)
  - Vote associatif - Modification API
  - Vote associatif - Modification du modèle

## 3.3.1 (2020/04/21)

Security release
  - Can hack poll by sending vote edits without previous VoteToChoice ids
  - Cannot change default admin user password
  - Typo

## 3.3.2 (2020/04/22)

no change

## 3.3.3 (2020/04/23)

  - Docker 3.3.1 and 3.3.2 does not start

## 3.3.4 (2020/04/28)

  - Cannot clone a vote

## 3.3.5 (2020/04/30)

  - Votecounting type config not showing up at vote creation/modification (when changing type)

## 3.3.6 (2020/05/05)

  - Cannot display comments

## 3.3.7 (2020/05/12)

  - Cannot create poll : Unrecognized field "choiceAddAllowed"
  - Reset automatique des choix

## 3.3.8 (2020/05/13)

  - Wrong poll displayed
  - Adding choice feature not working
  - Format incorrect des cases à cocher pour le vote "normal"

## 3.3.9 (2020/11/19)

  - Envoi de certains mails impossible
  - Vote restreint - impossible de voter authentifié
  - Cases à cocher Pollen - impossible
  - La liste des participants n'a pas d'ordre déterministe

## 3.3.10 (2020/11/26)

  - Sur un vote jugement majoritaire, quelques votants ne peuvent pas voter (pas de liste déroulante)

## 3.3.11 (2021/01/11)

no change

## 3.3.12 (2021/01/11)

no change

## 3.3.13 (2021/01/11)

no change

## 3.3.14 (2021/01/12)

  - Sur le tableau des votes, l'affichage montre des 0 et des 1 au lieu de l'affichage "classique"
  - Sur un vote avec votes publics, la tableau n'est pas affiché à l'arrivée sur la page

## 3.3.15 (2021/02/05)

  - Après un ajout de choix sur un jugement majoritaire, les résultats ne s'affichent plus

## 3.4.3 (2024/09/04)

This is the first release in the 3.4.x series (3.4.0 - 3.4.2 were made during
upgrades to the release pipeline). It contains major dependency updates, many
bug fixes and some breaking changes.

### Breaking changes

- Configuration: The database properties in `pollen-rest-api.properties` are changing:
  - `hibernate.connection.driver_class` -> `jakarta.persistence.jdbc.driver`
  - `hibernate.connection.username` -> `jakarta.persistence.jdbc.user`
  - `hibernate.connection.password` -> `jakarta.persistence.jdbc.password`
  - `hibernate.connection.url` -> `jakarta.persistence.jdbc.url`
- Docker: the image listens now on port `8080` instead of `80` (`8080` is the upstream image default)

### Fixed issues

- #367 - Erreur de codage des caractères dans les courriels envoyés
- #371 - fin de traduction de ChoiceReportForAdminEmail_fr.mustache
- #370 - orthographe dans PollCreatedEmail_fr.mustache
- #366 - Affichage du jour de la semaine pour les choix de type date
- #365 - Wrong values displayed for add choices begin and end date
- #354 Import CSV - Mise à jour liste et non erreur si doublon
- #363 Les champs «add hour to date» sont effacés lors de la modification d'un sondage
- #355 Fonctionnement du composant de création de choix non intuitif - non suppression de la valeur texte au changement de type
- #364 Impossible de mettre un nombre minimum de vote à 0 si on souhaite ajouter un maximum

There is also progress on:

- #108 - Carrouselle dans la popup de choix

## 3.4.4 (2024/09/17)

This release fixes a migration bug from Pollen 3.3.15: [fix prod db table and column names](https://gitlab.nuiton.org/chorem/pollen/-/commit/0c1db041d1c9d41dfc5839010e82bf90f37694a6)
