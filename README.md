# Pollen

Pollen est une application Web de sondages en ligne qui permet de créer et de
gérer des sondages avec différents types de choix (texte, date, image).
Les votants peuvent participer au sondage en suivant un lien qui identifie le
sondage.

Le but de Pollen n'est pas de permettre de faire du vote en ligne et de remplacer
les scrutins existants, mais d'introduire de nouveaux usages de
consultation/sondage/vote là où il n'y en a pas. De ce fait aucune réflexion sur
la sécurité des votes a été conduite, car ce n'est pas notre but.

Pour plus d'informations, consultez notre site [doc.pollen.cl](http://doc.pollen.cl)

## Construction du projet

### Backend

<details>
  <summary>Sans Docker</summary>

Prérequis :

* Maven 3.9
* JDK 21

Cloner ce dépôt et lancer à la racine :

```sh
mvn clean package
```

Le `.war` à installer dans votre serveur Tomcat est `pollen-rest-api/target/pollen-rest-api-X.Y.Z-SNAPSHOT.war`

Vous pouvez lancer directement le backend via le plugin Maven Cargo dans le dossier `pollen-rest-api`

```sh
mvn cargo:run
```

L'API est disponible à l'URL `http://localhost:8888/pollen-rest-api`

Exemple :

* http://localhost:8888/pollen-rest-api/v1/users retourne la liste des utilisateurs.
* http://localhost:8888/pollen-rest-api/v1/status permet de savoir si le backend est bien démarré.

</details>

<details>
  <summary>Avec Docker</summary>

Cloner ce dépôt et lancer à la racine :

```sh
docker run --rm -t -v "$PWD:/data" -v m2:/root/.m2/repository -w /data maven:3.8-openjdk-11-slim mvn clean package
```

* `--rm`: effacer le conteneur une fois qu'on aura fini
* `-t`: préserver les couleurs de la sortie Maven
* `-v "$PWD:/data"` monter le dossier courant dans le dossier `/data` à l'intérieur du conteneur
* `-v m2:/root/.m2/repository` monter un volume `m2` (géré par Docker) dans le dossier `/root/.m2/repository` qui
  servira de cache pour les dépendances
* `-w /data`: utiliser `/data` comme répertoire de travail
* `maven:3.8-openjdk-11-slim` est l'image utilisée pour le build. Le build avec des images plus récentes
  échoue : https://gitlab.nuiton.org/chorem/pollen/-/issues/372

Le `.war` produit est `pollen-rest-api/target/pollen-rest-api-X.Y.Z-SNAPSHOT.war`

</details>

#### Configuration minimale du backend

Par défaut, le backend sera lancé avec une base `H2` qui enregistre dans un fichier les données.\
Il faut par conséquent configurer le dossier où sera écrit ce fichier avec la variable `pollen.data.directory`.

Cette configuration se fait à l'aide d'un fichier `pollen-rest-api.properties` qui peut être placé dans :

- `/etc/pollen-rest-api.properties` ou `~/.config/pollen-rest-api.properties` (préférable pour une configuration par
  utilisateur) pour un environnement GNU/Linux
- quelque part pour Windows

Cela donne un fichier minimal de la sorte :

```properties
pollen.data.directory=/var/local/pollen/db
```

### Interface web

<details>
  <summary>Sans Docker</summary>

Prérequis : Npm

Dans le dossier `pollen-ui-riot-js`, lancer

```sh
npm install
npm run-script package
```

Le contenu du dossier `pollen-ui-riot-js/target/dist` est à installer dans votre serveur HTTP.

Vous pouver lancer directement un serveur local via la commande

```sh
npm start
```

L'UI est disponible à l'URL `http://localhost:8080`

### Configuration des URLs

Les URLs peuvent être personnalisées par les variables d'environnements suivantes :

* `POLLEN_SERVER_CONTEXT` : Définit pour l'UI, l'URL de l'API REST (ex : `http://<serveur>:<port>/pollen-rest-api`)
* `POLLEN_UI_CONTEXT` : Définit le domaine de l'UI (ex : `<serveur>:<port>`)

</details>

<details>
  <summary>Avec Docker</summary>

ToDo
</details>

## Contribution

Les contributions sont bienvenues ! Les règles de codage restent à documenter, mais en attendant essaye de te baser sur
le code existant et ça sera bien.

Pour pouvoir contribuer, tu auras besoin de :

* [Créer un compte sur notre GitLab](https://gitlab.nuiton.org/users/sign_in) (sera validé à la main pour éviter le
  spam, ça peut prendre quelques jours) ou te connecter avec Google ou GitHub
* Nous signaler que tu souhaites contribuer à Pollen, par exemple sur
  notre [salon Matrix](https://matrix.to/#/#Pollen:codelutin.com) ou par mail sur info@codelutin.com. Comme ça on pourra
  t'ajouter sur ce projet en tant que `Developer`
* Créer une branche avec tes modifs et nous envoyer une MR sur `develop`

## Releases

Le workflow sur ce projet est de type Gitflow. Pour faire une release:

* Compléter le fichier `CHANGELOG.md` pour la release à venir
* Créer et pousser une branche `/release/x.y.z`
* Il est possible de faire des derniers ajustements sur cette branche
* Quand on souhaite faire la release, lancer le job `release`. Ce job va merge la branche `/release/x.y.z` sur `master`, faire un tag `x.y.z` et merge ce tag sur `develop`
* Le front, le back et l'image docker pour la release seront construits sur le pipeline du tag `x.y.z`
