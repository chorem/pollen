#Le choix de la méthodes de votes

Pollen vous propose différentes méthodes de votes. Chacune a ses caractères et ses cas d’usage que nous allons détailler.

## Critères

### L'indépendance des alternatives non-pertinentes

> « Pour un sondage où A gagne, Si l’on ajoute un choix B, alors seuls A où B peuvent gagner le nouveau sondage. »

On trouve aussi une formulation plus générale :
> « Dans une élection l’ajout ou le retrait d’une alternative ne doit pas modifier l’ordre relatif des autres alternatives. »

Si ce critère n’est pas respecté les votants se retrouvent dans le dilemme du « vote utile ». C’est-à-dire, à ne pas 
voter en fonction de leurs convictions mais pour l’alternative ayant la plus grande probabilité de gagner sans être 
trop éloignée de leurs convictions.

*Plus d'information sur l’[Indépendance des alternatives non-pertinentes](https://fr.wikipedia.org/wiki/Ind%C3%A9pendance_des_alternatives_non-pertinentes)* 

### Le principe de Condorcet

> « si une alternative est préférée à tout autre par une majorité, alors cette alternative doit être élue. »

Autrement dit, il n’existe pas de concurrent qui pourrait désavouer le vainqueur de Condorcet dans un scrutin à 
deux alternatives.

*Plus d'information sur la [méthode de Condorcet](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Condorcet)*

## Le vote normal

C’est un vote majoritaire à un tour.
C’est la vote le plus simple et le plus connu. Chaque votant sélectionne le ou les choix préférés. Le choix ayant 
le plus de voix gagne.

Par défaut le scrutin est plurinominal (vote pour plusieurs choix).
L’option `Limiter le nombre de choix par vote` permet de rendre le scrutin uninominal, binominal,...

Attention : Ce système de vote ne respecte ni l’Indépendance des alternatives non-pertinentes ni le principe de Condorcet.
Il est donc conseillé de ne l’utiliser que pour des questions binaires (« oui » ou « Non », A ou B etc).

*Plus d'information sur le [scrutin uninominal majoritaire à un tour](https://fr.wikipedia.org/wiki/Scrutin_uninominal_majoritaire_%C3%A0_un_tour)*

## Le vote cumulatif

Chaque votant possède un nombre de points donnés (100 par défaut). Il les distribue librement entre les choix. 
Le gagnant est l’alternative ayant récoltée le plus de points.

Ce système permet aux votants d’être plus nuancés. Une alternative capable de recueillir 
quelques points chez beaucoup de votant a une chance de gagner.
L’ajout d’un choix peut capter suffisamment de points au vainqueur pour lui faire perdre la première place. 
Donc stratégiquement un votant aura intérêt à donner tous ses points à une alternative 
dont il pense qu’elle a de grande chance de gagner. 
Ce scrutin n’est donc pas indépendant aux alternatives non-pertinentes.

*Plus d'information sur le [vote cumulatif](https://fr.wikipedia.org/wiki/Vote_cumulatif)*

## La méthode de Condorcet

Chaque votant classe les choix par ordre de préférence.

Le dépouillement du scrutin consiste à simuler l’ensemble des duels possibles. 
Pour chaque paire de choix, on détermine le nombre de votants ayant préféré l’un à l’autre. 
Ainsi pour chaque duel, il y a une alternative gagnante. 
Dans la plupart des cas, il y a une unique alternative qui remporte tous ses duels : il s’agit du vainqueur du scrutin.
Cette méthode est indépendante aux alternatives non-pertinentes et respect le principe de Condorcet.
Dans de rare cas, il n’existe aucune alternative qui remporte tous ses duels. 
Condorcet avait remarqué cet important paradoxe inhérent à la méthode, appelée le paradoxe de Condorcet. 
La probabilité d’arriver dans ce cas diminue avec le nombre de votants.

*Plus d'information sur la [méthode de Condorcet](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Condorcet)*

## La méthode Borda

Chaque votant classe les *n* choix par ordre de préférence.

Au premier de la liste, on attribue *n* points, au second *n - 1* points, et ainsi de suite, 
le *n-ième* de la liste se voyant attribuer 1 point. 
Le score d'un choix est la somme de tous les points qui lui ont été attribués. 
Le ou les alternatives dont les scores sont les plus élevés remportent les élections.

La méthode Borda encourage les votes tactiques ou raisonnés. 
Les votants sont parfois amenés à abandonner leur choix favori 
s’ils s’aperçoivent que celui-ci n’a aucune chance de l’emporter.

Par défaut, les votant doivent classer tous les choix. 
Vous pouvez limiter le nombre de choix dans les options du sondage. 
Vous pouvez aussi modifier le nombre de points accordé par rang. 

Par exemple pour le concours de l’Eurovision de la chanson le nombre de choix est limité à 10 chansons. 
La première obtient 12 points, la deuxième : 10 puis 8, 7, 6, 5, 4, 3, 2 et 1 point.

*Plus d'information sur la [méthode Borda](https://fr.wikipedia.org/wiki/M%C3%A9thode_Borda)*

## Le vote alternatif

Chaque votant classe les choix par ordre de préférence.

On compte les voix des choix premiers de liste. Si un choix obtient la majorité absolue des voix, il est le gagnant. 
Sinon, on supprime le choix qui a recueilli le moins de voix et on le barre dans tous les bulletins des votants, 
modifiant ainsi le rang des choix après le choix éliminé. De nouveau, on compte les voix des choix premiers de liste… 
On répète l’opération jusqu’à obtention d’une majorité absolue, ce qui arrive inévitablement 
(au pire lorsqu’il ne reste plus que deux choix en lice).

*Plus d'information sur la [vote alternatif](https://fr.wikipedia.org/wiki/Vote_alternatif)*

## La méthode de Coombs 

Chaque votant classe tous les alternatives par ordre de préférence. 
On compte les voix des choix premiers de liste. Si un choix obtient la majorité absolue des voix, il est le gagnant. 
Sinon, on supprime le choix qui apparaît le plus souvent en dernier de liste et on le barre dans tous les bulletins 
des votants, modifiant ainsi le rang des choix placés après le choix éliminé.
De nouveau, on compte les voix des choix premiers de liste. 
On poursuit le processus jusqu’à ce qu’une majorité absolue se dessine, ce qui arrive inévitablement 
(au pire lorsqu’il ne reste plus que deux choix en lice).

*Plus d'information sur la [méthode de Coombs](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Coombs)*

## Le jugement majoritaire

Chaque votant note tous les choix selon un échelle de valeur commune.

Par défaut :

- Excellent
- Très bien
- Bien
- Assez bien
- Passable
- Insuffisant
- À rejeter

Ces mentions sont modifiables dans les options du sondage.

Un votant peut donner la même mention à plusieurs choix.

Pour chaque choix on totalise les appréciations reçus puis on calcule la mention médiane. 
C’est-à-dire la meilleure mention pour laquelle la moitié des votants ou plus ont donnés
une note supérieure ou égale à cette mention. Elle est appelée « mention majoritaire ».

Le choix avec la meilleure mention majoritaire gagne.
En cas d’égalité on calcule le nombre de votants ayant données une appréciation strictement supérieure et inférieure. 
La plus grande de ses quatre valeurs détermine le gagnant.

- Si cette valeur correspond à une appréciation strictement inférieure alors ce choix perd (*Critère de défaite*)
- Si cette valeur correspond à une appréciation strictement supérieure alors ce choix gagne (*Critère de victoire*)

La notation étant indépendante d’un choix à l’autre, l’ajout d’une alternative ne perturbe pas l’ordre d’arrivée 
des autres choix, ce système de vote est donc indépendant aux alternatives non-pertinentes.

*Plus d'information sur le [jugement majoritaire](https://fr.wikipedia.org/wiki/Jugement_majoritaire)*

## Les sondages de type nombre

Ce sondage n’est pas fait pour déterminer un gagnant mais plus pour comptabiliser des valeurs. 

Par exemple pour l’organisation d’un événement, les choix seront :

- À combien venez-vous ?
- Combien pour la cagnotte ?
- Pour le repas combien de poisson ?
- Pour le repas, combien de viande ?
- …

Pour chaque choix vous obtiendrez en résultat la somme des valeurs des votants.

