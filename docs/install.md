# Installation

- Vous pouvez installer Pollen sur votre propre serveur pour garder le contrôle
  de vos données (consultation confidentielle par exemple).
- Pollen est libre, vous pouvez le modifier, l'améliorer ou l'adapter a vos
  besoins.
- Vous pouvez nous remonter les bugs ou demandes d'améliorations et suivre le
  projet sur notre forge.

## Avec Docker

Pour faciliter l'installation de Pollen nous publions une image
[Docker](https://www.docker.com/) contenant une configuration par défaut de
Pollen.

On peut lancer un conteneur en local avec :

```sh
docker run -p 8080:8080 registry.nuiton.org/chorem/pollen:develop
```

Pollen sera disponible sur <http://localhost:8080/>

!!! note

    Sur les versions 3.3.15 et précédentes, le port utilisé était le `80`, il
    faut donc utiliser `-p 8080:80` si on veut déployer une de ces versions

Par défaut toute la configuration et les données de Pollen se trouvent dans le
dossier  `/var/local/pollen` du conteneur.

## Manuellement

Pollen se compose de deux parties, un backend Java et un frontend Riot.js. Des
builds pour chaque release sont disponibles sur notre GitLab.

Pour télécharget une version en particulier:

* Aller sur la page des tags: https://gitlab.nuiton.org/chorem/pollen/-/tags
* Click sur la flèche à côté du tag qui nous intéresse, puis télécharger `build-java` et `build-js`

![](assets/images/download_screenshot.png)

### Backend

En téléchargeant `build-java` on obtient un Zip qu'il faut extraire pour obtenir
un fichier WAR, à utiliser avec un conteneur de servlets (par exemple
[Tomcat](http://tomcat.apache.org/) ou [Jetty](http://www.eclipse.org/jetty/)).

Avant de le déployer vous devez indiquer le répertoire d’enregistrement des
données. Pour cela, créez le fichier `/etc/pollen-rest-api.properties`

```properties
pollen.data.directory=<repertoire des données>
```

Vous pouvez le tester sur votre navigateur à l'adresse
<http://localhost:8080/pollen-rest-api/v1/configuration>.

### Frontend

En téléchargeant `build-js` on obtient un Zip contenant des fichiers statiques.
On peut utiliser n'importe quel serveur web pour le déployer (par exemple
[Apache](https://httpd.apache.org/) ou [Nginx](https://nginx.org/)).

Modifier le fichier `conf.js` pour faire pointer le site sur le backend

```js
window.pollenConf = {
    endPoint: "http://<adresse backend>/pollen-rest-api",
    // (...)
}
```

### Compte par défaut

Au premier démarrage, Pollen créé un compte administrateur

* Email : `admin@chorem.org`
* Mot de passe : `admin`
