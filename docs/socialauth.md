Services tiers
==============

Les utilisateurs peuvent créer des comptes Pollen et s'y connecter à partir des services tiers suivants (l'id du service est entre parenthèse):

- Amazon (*amazon*)
- Facebook (*facebook*)
- Flickr (*flickr*)
- Foursquare (*foursquare*)
- Github (*github*)
- Google Plus (*googleplus*)
- Hotmail (*hotmail*)
- Instagram (*instagram*)
- LinkedIn (*linkedin* ou *linkedin2*)
- Mendeley (*mendeley*)
- Myspace (*myspace*)
- Nimble (*nimble*)
- Runkeeper (*runkeeper*)
- Salesforce (*salesforce*)
- Stack Exchange (*stackexchange*)
- Twitter (*twitter*)
- Yahoo (*yahoo*)
- Yammer (*yammer*)

Pour activer la connexion à partir de ces services, vous devez obtenir une clé d'API auprès de chacun des services 
que vous voulez activer. Ils pourront vous demander le nom de domaine sur lequel est installée votre instance de Pollen,
mais également les URLs de redirection à autoriser (c'est notamment le cas pour Google Plus). Les URLs utilisées dans Pollen sont :

- http(s)://{*url de l'instance Pollen*}/?loginProvider=*id du service*&action=signin
- http(s)://{*url de l'instance Pollen*}/?loginProvider=*id du service*&action=link

How to run application with Localhost on windows
https://github.com/3pillarlabs/socialauth/wiki/How-to-run-application-with-Localhost-on-windows