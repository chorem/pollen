Pollen
======

Pollen est une application de consultation web. Elle est destinée aux petites entités
qui veulent un moyen simple de gérer des consultations tout en restant maître de leurs
données.

Le but de Pollen n'est pas de permettre de faire du vote en ligne et de remplacer
les scrutins existant mais d'introduire de nouveaux usages de
consultation/sondage/vote là où il n'y en a pas. De ce fait aucune réflexion sur
la sécurité des votes a été conduite, car ce n'est pas notre but.


Comment utiliser pollen ?
-------------------------

- Utiliser notre [instance de Pollen](https://pollen.cl), sans rien avoir à installer.
- Créer votre propre instance sur votre serveur en suivant le [guide d'installation](/install).


Pourquoi utiliser Pollen ?
--------------------------------------------

Pollen permet de créer des consultations sur des textes, dates ou images avec les possibilités suivantes :

- Plusieurs modes de dépouillement disponibles, comme le Condorcet.
- Votes anonymes.
- Notifications par courriel et rappel.
- Discussions propres à un sondage sous forme de commentaires.
- Vous pouvez modifier vos votes (même anonyme).
- Restriction de la consultation à une liste définie de personnes (par exemple, une liste par entreprise ou département).

En créant un compte vous aurez également :

- Historique des consultations créés et des participations à des sondages.
- Import de listes de votants via des fichiers de CSV ou des
  annuaires LDAP.
- Import/export des consultations.


Les types de scrutin de Pollen
------------------------------

Type de scrutin      | Vote                                                | Dépouillement
-------------------- | --------------------------------------------------- | -------------
Normal               | Réponses binaires (*Oui* ou *Non*, *A* ou *B*, ...) | Le gagnant est le candidat majoritairement sélectionné
Borda                | Classement des choix par ordre de préférence        | Attribution de points à chaque candidats en fonction de leur classement par votant. Le gagnant étant le candidat ayant le plus de points
Condorcet            | Classement des choix par ordre de préférence        | Duel entre chaque candidat pour déterminer le gagnant, vainqueur du plus grand nombre de duels
Coombs               | Classement des choix par ordre de préférence        | Dépouillements successif (en retirant le candidat le plus souvent classé dernier) jusqu'à obtenir une majorité absolue
Cumulatif            | Répartition d'un nombre de points par candidats     | Le gagnant est le candidat ayant récoltée le plus de points
Jugement majoritaire | Notation selon une échelle de valeur commune        | Le gagnant est le candidat ayant la notation médiane la plus élevée
Nombre               | Saisie libre d'une valeur | Total des valeurs
Vote alternatif      | Classement des choix par ordre de préférence        | Dépouillements successif (en retirant le candidat ayant le moins de voix) jusqu'à obtenir une majorité absolue

Plus de détails sur les [méthodes de vote](/electionMethods).
