Configuration
=============

API REST
--------

Pour configurer le serveur REST créer le fichier `/etc/pollen-rest-api.properties`.  

### La base de données

#### H2

Modifier le dossier d'enregistrement de la base [H2](http://www.h2database.com/html/main.html).

    pollen.data.directory=/var/local/pollen/data  

#### PostgreSQL

Par défaut Pollen utilise un base H2 mais vous pouvez aussi configurer une base [PostgreSQL](https://www.postgresql.org/).

    hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
    jakarta.persistence.jdbc.driver=org.postgresql.Driver
    hibernate.default_schema=public
    jakarta.persistence.jdbc.user=<loginBD>
    jakarta.persistence.jdbc.password=<passwordBD>
    jakarta.persistence.jdbc.url=jdbc:postgresql://<serveurBD>/<DB>

### Les mails

Vous pouvez modifier le serveur SMTP sortant et l’adresse de l’expéditeur.

    pollen.smtp.host=<serveurSMTP>
    pollen.smtp.port=<portSMPT>
    pollen.smtp.from=Nom de l'expéditeur <expéditeur@serveur.com>

L’adresse de destination des remarques ou bugs.

    pollen.feedback.mails=feedback@serveur.com  

Pollen peut scruter les mails reçus sur la boite mail de `pollen.smtp.from`.
Si le mail reçu provient d’un retour d’une invitation à un sondage, il sera transféré au créateur de ce sondage.

    pollen.mailBox.host=<serveur IMAP>
    pollen.mailBox.protocol=<'imaps' ou 'imap'>
    pollen.mailBox.user=<identifiant de la boite mail>
    pollen.mailBox.password=<le mote de pass de la boite mail>


### Les droits utilisateurs

- Limiter l’inscription à certaines adresses mail.

      pollen.registration.emailAddressPattern=*@organisation.com

  La valeur doit être une expression régulière. Seul les adresses la validant  peuvent s’enregistrer.

- Rendre obligatoire l’inscription avant d’utiliser Pollen

      pollen.default.userConnectedRequired=true

- Limiter la création de sondage

      pollen.default.usersCanCreatePoll=<ALL_USER ou USERS_CONNECTED ou USERS_SELECTED>

  - ALL_USER : Tous les utilisateurs (par défaut)
  - USERS_CONNECTED : Seul les utilisateurs connectés
  - USERS_SELECTED : Seul les utilisateurs autorisés dans le menu d’administration des utilisateurs

- Limiter la taille des fichiers téléversé

      pollen.resource.maxSize=100000

  exprimé en octets (10000000 pour 10 Mo)


### Liste des options

| Clés                                              | Descriptions                                                                                          | Types                                         | Valeurs par défaut    |
|---------------------------------------------------|-------------------------------------------------------------------------------------------------------|-----------------------------------------------|-----------------------|
| pollen.data.directory                             | Répertoire de données de l’application                                                                | Alpha-numérique                               |                       |
| pollen.token.secret                               | Clés de chiffrement des jetons d'authentification (encodé en base 64)(16, 24 ou 32 bytes)             | Alpha-numérique                               | IFYD6KLx/GrLZwSFBH5arXMIamWSA8qiVtFImIimDZs= |   
| pollen.token.issue                                | Le nom de l'émetteur des jetons d'authentification                                                    | Alpha-numérique                               | Pollen                |   
| pollen.token.timeout                              | Durée de validité d'un jeton d'authentification (en secondes)                                         | Numérique                                     | 3600                  |   
| pollen.default.pollType                           | Type de sondage par défaut lors de la création d'un nouveau sondage                                   | FREE, RESTRICTED                              | FREE                  |                
| pollen.default.voteCountingType                   | Type de dépouillement par défaut lors de la création d'un nouveau sondage                             | Numérique                                     | 1                     |  
| pollen.default.voteVisibility                     | Visibilité des votes par défaut                                                                       | ANONYMOUS, CREATOR, VOTER ou EVERYBODY        | EVERYBODY             |   
| pollen.default.commentVisibility                  | Visibilité des commentaires par défaut                                                                | CREATOR, VOTER ou EVERYBODY                   | EVERYBODY             |
| pollen.default.resultVisibility                   | Visibilité des résultats par défaut                                                                   | CREATOR, VOTER ou EVERYBODY                   | EVERYBODY             |                                                                                                     
| pollen.default.continuousResults                  | Voire les résultats en continue par défaut                                                            | Booléen                                       | true                  |
| pollen.default.voteNotification                   | Recevoir une notification pour les nouveaux votes par défaut                                          | Booléen                                       | false                 |
| pollen.default.commentNotification                | Recevoir une notification pour les nouveaux commentaires par défaut                                   | Booléen                                       | false                 |
| pollen.default.newChoiceNotification              | Recevoir une notification pour les nouveaux choix par défaut                                          | Booléen                                       | false                 |
| pollen.default.notifyMeHoursBeforePollEnds        | Nombres d’heures avant la fin du vote pour recevoir une notification (0 pas de notification)          | Numérique                                     | 0                     |
| pollen.smtp.host                                  | Adresse du serveur SMTP                                                                               | Alpha-numérique                               |                       |
| pollen.smtp.port                                  | Port du serveur SMTP                                                                                  | Numérique                                     | 25                    |
| pollen.smtp.from                                  | Adresse mail de l’expéditeur                                                                          | Alpha-numérique                               | noreply@serveur.com   |
| pollen.smtp.wait                                  | Durée d'attente entre l'envoie de deux mails (en millisecondes)                                       | Numérique                                     | 1000                  |
| pollen.mailBox.host                               | Serveur de la boite mail de Pollen                                                                    | Alpha-numérique                               |                       |
| pollen.mailBox.protocol                           | Protocol de la boite mail de Pollen                                                                   | imap ou imaps                                 | imap                  |
| pollen.mailBox.user                               | Nom d’utilisateur de la boite mail de Pollen                                                          | Alpha-numérique                               |                       |
| pollen.mailBox.password                           | Mot de passe de la boite mail de Pollen                                                               | Alpha-numérique                               |                       |
| pollen.mailBox.cronSchedul                        | Intervalle entre deux verification de la boite mail de Pollen                                         | CRON                                          | 0 2/5 * * * ?         |
| pollen.logConfigurationFile                       | Chemin vers le fichier de configuration des logs                                                      | Alpha-numérique                               |                       |
| pollen.sendEndPollRemindersCronSchedule           | Intervalle entre deux lancements de la tâche d’envoi de mails de rappel de fin de sondage             | CRON                                          | 0 0/1 * * * ?         |
| pollen.resource.maxSize                           | Taille maximal pour un fichier de ressource de Pollen (en octets)                                     | Numérique                                     | 10000000              |
| pollen.resource.preview.max                       | Dimension maximal de la prévisualisation d'un image                                                   | Numérique                                     | 200                   |
| pollen.registration.emailAddressPattern           | Expression régulière que doivent vérifier les adresses émail des utilisateurs lors de l’inscription   | Alpha-numérique                               |                       |
| pollen.resendEmailsCronSchedule                   | Intervalle entre deux lancements de la tâche de renvoi des mails en erreur                            | org.quartz.CronExpression                     | 0 0/5 * * * ?         | 
| pollen.report.maxScore                            | Score maximum pour un signalement avant que les administrateurs soient avertis                        | Numérique                                     | 100                   |
| pollen.feedback.mails                             | Mails destinataires des retours utilisateur                                                           | Alpha-numérique                               |                       |  
| pollen.feedback.locale                            | La locale pour envoyer les retours utlisateur                                                         | fr ou en                                      | en                    |
| pollen.default.timeZoneId                         | Fuseau horaire pou l'envoie des mails par défaut                                                      | Alpha-numérique                               | Europe/Paris          |
| pollen.default.maxVoters                          | Nombre maximum de votes pris en compte pour de sondage standard (0 : pas de limite)                   | Numérique                                     | 0                     |
| pollen.default.userConnectedRequired              | Seul les utilisateurs connectés peuvent accéder à l'application                                       | Booléen                                       | false                 |
| pollen.default.usersCanCreatePoll                 | Quels utilisateurs peuvent créer des sondages                                                         | All_USERS, USERS_CONNECTED ou USERS_SELECTED  | All_USERS             |
| pollen.deleteObsoleteSessionTokensCronSchedule    | Intervalle entre deux lancements de la suppression des tokens de session obsolètes                    | CRON                                          | 0 0/5 * * * ?         |
| pollen.anonymizeOlderVotesCronSchedule            | Intervalle entre deux lancements de l'anonymisation des anciens votes                                 | CRON                                          | 0 0 0 * * ?           |
| pollen.anonymizeOlderVotesAge                     | Age minimum des votes anonymisés (en secondes). Les votes plus agés sont anonymisés                   | Numérique                                     | 63072000              |


Site public
-----------  

La configuration du site public est définie dans le fichier `conf.js` (dans le docker le fichier se trouve dans `/var/local/pollen`)

Par défaut la configuration est la suivante :

    window.pollenConf = {
        endPoint: "http://localhost:8888/pollen-rest-api",
        piwikUrl: "", // add the piwik url, eg: http://localhost/piwik
        piwikSiteId: "", // add the site id, eg: 3
        defaultMessageTimeout: 15,
        resourceMaxSize: 10000000 // octets => 10 Mo
    };

- `endPoint` : L’URL de l'API REST,
- `piwikUrl` : L’URL de votre tracker [Piwik](https://piwik.org/),
- `piwikSiteId` : l'identifiant du site de Pollen dans votre tracker Piwik,
- `defaultMessageTimeout` : Durée (en secondes) d'affichage des message,
- `resourceMaxSize` : Taille maximal pour un fichier de ressource de Pollen (en octets).
