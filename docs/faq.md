# FAQ

Si vous ne trouvez la réponse à votre question dans la FAQ, vous pouvez nous 
contacter directement depuis le logiciel en cliquant sur ***!?*** en haut à droite
de l'interface.

## Création et/ou administration d'un sondage

### Puis-je inviter autant de personnes que je le souhaite ?

Oui, vous pouvez inviter autant de personnes que vous le souhaitez. Jusqu'à 100 personnes invitées, le service est gratuit, à partir du 101ème, le tarif est de 100€ par mois. Pour plus d'information, n'hésitez pas à nous contacter, nous nous ferons un plaisir de vous répondre ! sales@pollen.cl
  
les 100 premiers votes font partie du dépouillement

### Puis-je créer un sondage sans m'authentifier ?

Oui, vous pouvez créer un sondage sans authentification. Néanmoins, en créant un compte, vous pouvez revenir sur vos sondages plus facilement, accéder aux résultats, voir vos invitations, consulter vos participations aux sondages, créer des listes de votants... Tout cela via votre espace (que vous trouvez en haut à droite de votre page). Vos données personnelles ne sont et ne seront jamais réutilisées.

### J'ai perdu mon mot de passe comment faire ?

Pas de souci, cliquez sur le bouton "mot de passe perdu" sur la page d'authentification et entrez votre adresse mail. Vous recevez un mail vous indiquant votre nouveau mot de passe. Vous pouvez ensuite le modifier à votre guise.

### Quelle est la procédure à suivre lorsque je ne reçois pas le mail de validation alors que j'ai rempli les champs pour m'inscrire sur la plateforme ?

Oups... avez-vous vérifié votre dossier Spam ? Si oui, patientez un peu, il ne devrait pas tarder à arriver ! Si non, envoyez nous un mail : support@pollen.cl.

### Comment mettre une légende ou un titre sur une photo ? 

  - Nous vous conseillons de nommer votre image ou votre document avec un titre explicite car les votants pourront le voir en cliquant sur la miniature.
  - Sur la page "Nouveau sondage/choix", lorsque vous téléchargez l'image, vous pouvez cliquer sur des points de suspension ce qui ouvrira une fenêtre dans laquelle vous pouvez ajouter une description. Ainsi, en passant la souris sur l'image, le votant voit apparaître une fenêtre "pop up" indiquant cette description.

### Comment puis-je faire pour que mes choix ne soient pas toujours dans le même ordre lorsque les votants accèdent à la page ?

Cette fonctionnalité a déjà été évoquée, nous ne savons pas si nous développerons cette fonctionnalité. Nous attendons vos merge requests ! :)

### À quoi servent les flèches dans l'onglet "Choix" ?

Sur la page nouveau sondage / choix, il y a en effet des flèches à gauche des listes de choix, celles-ci vous permettent d'ordonner vos choix. Vous pouvez en effet faire glisser vos choix vers le haut ou le bas.

### Comment puis-je créer une liste de votants ?

   Vous pouvez importer dans le logiciel un fichier au format CSV contenant des participants pour ne pas avoir à les saisir individuellement.
   Pour cela, vous devez être authentifiés et vous rendre dans votre espace personnel (clic sur votre nom en haut à droite de l'interface) puis sur *« Mes listes de votants »*.

  - Créez une liste de votants, ou cliquez une liste déjà crée
  - Cliquez sur *« + »* (en bas a droite de l'interface) puis sur *« Importer un
  fichier CSV »*
  - Sélectionnez un fichier qui doit respecter le format ci-dessous.
  - Cliquez sur *Importer* pour voir apparaître les nouveaux votants dans la 
  liste
  - Vous pourrez ensuite utiliser cette liste lors de la création d'une 
  consultation en restreignant les votants.
  
```
# Adresse électronique;Nom;Poids
dupond@domaine.fr;Jean Dupond;1.00
pseudo@bleu.fr;Arnaud Lemaitre;4.50
miller@courriel.com;Benjamin Miller;3.00
```

### Comment puis-je inviter des participants à mon sondage ?

Plusieurs solutions s'offrent à vous :
  - Vous créez une liste de votants, un mail leur sera automatiquement envoyé (vous pouvez vous référer à la question "Comment puis-je inviter des participants à mon sondage ?")
  - En dernière page de création d'un sondage, vous accédez à la page "résumé". Il vous est alors proposé de partager ce sondage via un lien, un QR code ou par mail.

### Comment puis-je faire pour que seuls les employés de ma société accèdent au vote ?

C'est possible ! Dans l'onglet "option" puis "utilisateurs enregistrés", le premier choix s'intitule : "Limiter à certaines adresses électroniques". En la cochant, vous pouvez indiquer le suffixe des adresses de votre entreprise, par exemple : @pollen.cl. Si les personnes ciblées n'ont pas encore de compte Pollen, ils devront s'en créer un pour pouvoir voter.

### Puis-je avoir plusieurs adresses électroniques associées à mon compte ? 

Oui! allez sur votre profil pour en associer une nouvelle.

### Pouvez-vous m'en dire plus sur les modes de scrutin ?

Bien entendu, http://doc.pollen.cl/electionMethods/. Ce lien vous donnera toutes les indications que vous souhaitez sur les différents modes de scrutin. Si ce n'est toujours pas clair, nous vous invitons à regarder cette vidéo : https://pollen.cl/help/voteCountingTypes.webm.

### Est-il possible d'envoyer des relances aux participants ?

Oui, cette fonctionnalité est disponible si vous avez défini une liste de votants.

### Où puis-je voir les participants que j'ai invités ?

Il y a plusieurs réponses en fonction du mode d'invitation :  
  * Une liste de votants : vous pouvez les retrouver dans votre espace "liste de votants".
  * Des mails que vous avez envoyés : Vous pouvez les retrouver dans vos "messages envoyés" de votre boite mail.
  * Un lien que vous avez mis à disposition : Malheureusement vous ne pouvez pas. 

### Où puis-je voir les participants qui n'ont pas encore voter ?

Lorsque vous avez créé un sondage réstreint, les votants émargent après leur participation, ainsi vous pouvez avoir une vue d'ensemble des votants et des non-votants.

### Comment télécharger les résultats du sondage ?

Vous pouvez faire un copier-coller dans un tableur.

### Comment accéder à la version "accessible pour les mal-voyants" ?

Nous avons développé le site en prenant en compte les règlementations liées à l'accessibilité des sites internet. Ainsi, les mal-voyants dotés d'un logiciel de lecture automatique peuvent voter sur le site Pollen.

### Comment faire émarger les votants ?

Lors d'un sondage restreint, l'émargement est automatique.

### Comment ajouter une liste de votants via un fichier csv ?

Vous pouvez importer dans le logiciel un fichier au format CSV contenant des 
participants pour ne pas avoir à les saisir individuellement.
Pour cela, vous devez être authentifiés et vous rendre dans votre espace 
personnel (clic sur votre nom en haut à droite de l'interface) puis sur *« Mes 
listes de votants »*.

  - Créez une liste de votants, ou cliquez une liste déjà crée
  - Cliquez sur *« + »* (en bas a droite de l'interface) puis sur *« Importer un
  fichier CSV »*
  - Sélectionnez un fichier qui doit respecter le format ci-dessous.
  - Cliquez sur *Importer* pour voir apparaître les nouveaux votants dans la 
  liste
  - Vous pourrez ensuite utiliser cette liste lors de la création d'une 
  consultation en restreignant les votants.
  
```
# Adresse électronique;Nom;Poids
dupond@domaine.fr;Jean Dupond;1.00
pseudo@bleu.fr;Arnaud Lemaitre;4.50
miller@courriel.com;Benjamin Miller;3.00
```

### Comment restreindre un sondage à une liste de personnes identifiées ?

Lors de la création d'un sondage, parmi les options se trouve une case à cocher 
*Sondage restreint*. 
En activant cette option, une nouvelle étape apparaît dans le processus de 
création du sondage.
Vous pouvez alors :

  - Ajouter des participants en renseignant leur nom et leur adresse 
  électronique.
  - Ajouter des sous-groupes pour organiser votre liste de participant ou 
  répartir les poids des votes par groupe de participant.
  - Importer une liste de votants que vous avez déjà définie dans votre espace
  personnel.

### Comment importer une liste de votants ?

La procédure est la même que pour l'ajout de votants via une liste CSV. Nous vous conseillons de vous référer à cette question. 

### À quoi servent les poids parmi les listes et les participants d'un sondage ?

Les poids permettent de préciser que certaines voix ont plus d'importance que 
d'autres, ce qui peut être utile pour certaines organisations. Le calcul du 
résultat est pondéré par le poids de chaque votant.

### Une même personne peut-elle voter plusieurs fois ?

Dans le cadre d'un sondage ouvert, paramétrage par défaut, un votant peut voter plusieurs fois avec des 
noms différents.
Pour les sondages restreints, option à cocher lors de la création d'un sondage, 
les liens de vote envoyés ne sont valable que pour un seul vote. Un participant 
ne peut alors pas voter au nom d'un autre.


## Participation à un sondage

### Dois-je me connecter pour participer à un sondage ?

Non, vous n'êtes pas obligés de vous connecter. 
En revanche, si vous souhaitez accéder rapidement à l'ensemble des sondages auxquels vous avez participé, nous vous conseillons de créer un compte et de vous connecter.

### Qu'est-ce que le "flux RSS du sondage" ?

Le flux RSS d'un sondage trace les commentaires, les nouveaux votes et la clôture du sondage. Cela permet d'être notifié des différents événements liés au sondage.

### Pourquoi n'ai-je pas reçu de mail suite à mon vote ?

Il ne nous semble pas utile de spammer vos boites ! un grand pas pour la planète !

### Comment suis-je sûr que mon vote a été pris en compte ?

Lorsque vous votez, votre participation apparait dans le tableau situé sous les choix de vote.

### Je me suis trompée dans ma réponse, comment la changer ?

Vous pouvez modifier votre réponse à tout moment en cliquant sur le pictogramme "crayon" à gauche de votre vote.

## Pollen sur mobile

### Pourquoi ne vois-je pas tout le texte lorsque j'ouvre le sondage sur mon mobile ? Pourquoi le sondage ne s'affiche pas lorsque j'essaie de m'y connecter ? Pourquoi je ne peux pas lire la légende de la photo ? Comment faire pour voir la légende de la photo ?

En fonction de la version de votre navigateur, vous pouvez en effet avoir des difficultés à lire les sondages. Nous vous conseillons de le mettre à jour. Nous nous efforçons d'améliorer l'adaptabilité de Pollen aux différentes résolution et différents navigateurs.

## A propos de Pollen

### Qui est derrière Pollen ? 

Ce sont les lutins de la société Code Lutin. Pour nos besoins professionnels, nous avons créé cette plateforme que nous mettons à disposition de tous. Nous sommes des experts en Java et JavaScript et promoteurs du logiciel libre.

### Comment contacter l'équipe ?

Lorsque vous cliquez en haut à droite sur le point d'interrogation et le point d'exclamation, une fenêtre s'ouvre ; vous pouvez dès lors nous faire part de votre remarque. Nous vous répondrons dans des délais surprenants !

### Combien coûte Pollen ? 

Pollen est gratuit jusqu'à 100 votants. Au-delà, nous vous invitons à nous contacter. Vous pouvez vous désabonner lorsque vous le souhaitez, même à l'issue de votre premier vote. Pollen étant un logiciel libre, vous pouvez également l'installer gratuitement sur le serveur de votre choix, sans limitation !

### Pourquoi le nom de Pollen ?

Poll en anglais signifie vote, nous nous sommes inspirés de ce nom pour créer le nôtre.

### Quelle est la différence avec Doodle ?

A notre connaissance, Doodle est un outil de planification en ligne. Pollen vous le permet également mais c'est avant tout une plateforme de sondage permettant différents modes de scrutin et n'est pas limité aux sondages sur des dates. De plus, Doodle est un outil propriétaire, vous n'êtes pas maître de vos données.

### Que faites-vous de mes données personnelles ?

Nous n'utilisons jamais vos données personnelles à des fins commerciales et ne les partageons pas avec des services tiers.

### Comment faites-vous pour vous rémunérer, je ne vois pas de pub sur le site ?
Bien vu ! Il n'y a pas de publicité sur le site. Nous nous rémunérons uniquement via vos dons ou vos abonnements mensuels. N'ayez crainte, cela ne signifie pas que nous allons arrêter le service au premier écueil rencontré ! Nous l'avons développé pour nos besoins et l'utilisons toujours à des fins professionnelles et personnelles.
