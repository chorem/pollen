package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.chorem.pollen.services.AbstractPollenServiceTest;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.PollenUserBean;
import org.chorem.pollen.services.bean.PollenUserEmailAddressBean;
import org.chorem.pollen.services.service.security.PollenAuthenticationException;
import org.chorem.pollen.services.service.security.PollenDefaultEmailAddressException;
import org.chorem.pollen.services.service.security.PollenEmailNotValidatedException;
import org.chorem.pollen.services.service.security.PollenInvalidSessionTokenException;
import org.chorem.pollen.services.service.security.PollenUserBannedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaNoResultException;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenUserServiceTest extends AbstractPollenServiceTest {

    protected PollenUserService service;

    protected PollenUser user;

    @Before
    public void setUp() throws Exception {

        loadFixtures("user-fixtures");

        loadFixtures("fixtures");

        service = newService(PollenUserService.class);

        getServiceContext().setDate(new Date(1363948427576L));

        user = application.fixture("user_tony");
    }

    @Test
    public void testGetPollenUsers() throws PollenAuthenticationException, PollenInvalidSessionTokenException, PollenEmailNotValidatedException, PollenUserBannedException {


        login("jean@pollen.org", "fake");

        PaginationResultBean<PollenUserBean> users = service.getUsers(null, "");
        Assert.assertNotNull(users);
        Assert.assertTrue(CollectionUtils.isNotEmpty(users.getElements()));

    }

    @Test
    public void testGetPollenUser() throws PollenInvalidSessionTokenException, PollenAuthenticationException, PollenEmailNotValidatedException, PollenUserBannedException {

        login("jean@pollen.org", "fake");

        try {
            service.getUser("pollen_user_tony_" + System.nanoTime());

        } catch (TopiaNoResultException e) {
            Assert.assertTrue(true);
        }

        PollenUserBean user = service.getUser(this.user.getTopiaId());

        Assert.assertNotNull(user);
        Assert.assertEquals(this.user.getName(), user.getName());
        Assert.assertEquals(this.user.isAdministrator(), user.isAdministrator());
        Assert.assertEquals(this.user.sizeEmailAddresses(), user.getEmailAddresses().size());
        Assert.assertEquals(this.user.getDefaultEmailAddress().getEmailAddress(), user.getDefaultEmailAddress().getEmailAddress());
        Assert.assertEquals(this.user.getDefaultEmailAddress().getTopiaId(), user.getDefaultEmailAddress().getEntityId());
        Assert.assertEquals(this.user.isBanned(), user.isBanned());
    }

    @Test
    public void testCreatePollenUser() throws InvalidFormException {

        try {
            PollenUserBean u = service.toPollenUserBean(user);
            service.createUser(u);
            Assert.fail();
        } catch (IllegalStateException e) {
            // Should having id
            Assert.assertTrue(true);
        }

        PollenUserBean newUser = new PollenUserBean();
        setDefaultEmail(newUser, "pollen@notpollen.org");

        try {
            // no name
            // no generate password
            // unauthorized email
            service.createUser(newUser);
            Assert.fail();
        } catch (InvalidFormException e) {
            // no password
            assertErrorKeyFound(e, "name", "password", "email");
        }

        setDefaultEmail(newUser, "pollen@pollen.org");
        newUser.setPassword("password");
        newUser.setName("Pollen");

        PollenEntityRef<PollenUser> savedUser = service.createUser(newUser);
        Assert.assertNotNull(savedUser);
        Assert.assertNotNull(savedUser.getEntityId());

        PollenUserBean newUser2 = new PollenUserBean();
        newUser2.setName("Pollen");
        setDefaultEmail(newUser2, "pollen@pollen.org");

        try {
            service.createUser(newUser2);
            Assert.fail();
        } catch (InvalidFormException e) {
            // no password
            // not available email
            assertErrorKeyFound(e, "password", "email");
        }

        newUser2.setPassword("password");

        try {
            service.createUser(newUser2);
            Assert.fail();
        } catch (InvalidFormException e) {
            // not available email
            assertErrorKeyFound(e, "email");
        }

        setDefaultEmail(newUser2, "pollen2@pollen.org");
        PollenEntityRef<PollenUser> savedUser2 = service.createUser(newUser2);
        Assert.assertNotNull(savedUser2);
        Assert.assertNotNull(savedUser2.getEntityId());

        // generate password by the server
        PollenUserBean newUser3 = new PollenUserBean();
        newUser3.setPassword("azerty");
        newUser3.setName("Pollen");

        try {
            service.createUser(newUser3);
            Assert.fail();
        } catch (InvalidFormException e) {
            // null email
            assertErrorKeyFound(e, "email");
        }

        setDefaultEmail(newUser3, "pollen@pollen.org");

        try {
            service.createUser(newUser3);
            Assert.fail();
        } catch (InvalidFormException e) {
            // not available email
            assertErrorKeyFound(e, "email");
        }

        setDefaultEmail(newUser3, "pollen3@pollen.org");

        PollenEntityId<PollenUser> savedUser3 = service.createUser(newUser3);
        Assert.assertNotNull(savedUser3);
        Assert.assertNotNull(savedUser3.getEntityId());
    }

    @Test
    public void testEditUser() throws InvalidFormException, PollenInvalidSessionTokenException, PollenAuthenticationException, PollenEmailNotValidatedException, PollenUserBannedException, PollenDefaultEmailAddressException {

        login("jean@pollen.org", "fake");

        PollenUserBean user = service.getUser(this.user.getTopiaId());
        String originalEmail = user.getDefaultEmailAddress().getEmailAddress();
        user.setPassword("password");

        Assert.assertNotNull(user);

        String email = "tony4@pollen.org";
        setEmails(user, email);

        service.editUser(user);

        PollenUserBean savedUser = service.getUser(user.getEntityId());
        Assert.assertNotNull(savedUser);
        // the email addresses are not updated in the edit method
        Assert.assertEquals(2, savedUser.getEmailAddresses().size());
        Assert.assertEquals(originalEmail, savedUser.getDefaultEmailAddress().getEmailAddress());

        try {
            service.addEmailAddress(user.getEntityId(), "jean@pollen.org");
            Assert.fail();
        } catch (InvalidFormException e) {
            // not available email
            assertErrorKeyFound(e, "email");
        }

        PollenEntityRef<PollenUserEmailAddress> lastEmailAddress = service.addEmailAddress(savedUser.getEntityId(), email);

        savedUser = service.getUser(savedUser.getEntityId());
        Assert.assertNotNull(savedUser);
        Assert.assertEquals(3, savedUser.getEmailAddresses().size());
        Optional<PollenUserEmailAddressBean> lastEmailAddressBean = savedUser.getEmailAddresses().stream()
                .filter(emailAddress -> emailAddress.getEntityId().equals(lastEmailAddress.getEntityId()))
                .findFirst();
        Assert.assertFalse(lastEmailAddressBean.get().isValidated());

        try {
            service.setDefaultEmailAddress(savedUser.getEntityId(), lastEmailAddress.getEntityId());
            Assert.fail();
        } catch (PollenEmailNotValidatedException e) {
            savedUser = service.getUser(savedUser.getEntityId());
            Assert.assertEquals(originalEmail, savedUser.getDefaultEmailAddress().getEmailAddress());
        }

        Optional<PollenUserEmailAddressBean> emailAddressValidatedButNotDefaultOptional = savedUser.getEmailAddresses().stream()
                .filter(emailAddress -> !emailAddress.getEmailAddress().equals(originalEmail) && emailAddress.isValidated())
                .findFirst();
        PollenUserEmailAddressBean emailAddressValidatedButNotDefaultBean = emailAddressValidatedButNotDefaultOptional.get();
        service.setDefaultEmailAddress(savedUser.getEntityId(), emailAddressValidatedButNotDefaultBean.getEntityId());
        savedUser = service.getUser(savedUser.getEntityId());
        Assert.assertEquals(emailAddressValidatedButNotDefaultBean.getEmailAddress(),
                            savedUser.getDefaultEmailAddress().getEmailAddress());

        try {
            service.removeEmailAddress(savedUser.getEntityId(), savedUser.getDefaultEmailAddress().getEntityId());
            Assert.fail();
        } catch (PollenDefaultEmailAddressException e) {
            savedUser = service.getUser(savedUser.getEntityId());
            Assert.assertEquals(3, savedUser.getEmailAddresses().size());
        }

        service.removeEmailAddress(savedUser.getEntityId(), lastEmailAddress.getEntityId());
        savedUser = service.getUser(savedUser.getEntityId());
        Assert.assertEquals(2, savedUser.getEmailAddresses().size());

    }

    protected void setEmails(PollenUserBean user, String ... emailAddresses) {
        List<PollenUserEmailAddressBean> emailAddressBeans = Arrays.stream(emailAddresses).map(emailAddress -> {
            PollenUserEmailAddressBean emailAddressBean = new PollenUserEmailAddressBean();
            emailAddressBean.setEmailAddress(emailAddress);
            return emailAddressBean;
        }).collect(Collectors.toList());
        user.setEmailAddresses(emailAddressBeans);
    }

    protected void setDefaultEmail(PollenUserBean user, String emailAddress) {
        PollenUserEmailAddressBean emailAddressBean = new PollenUserEmailAddressBean();
        emailAddressBean.setEmailAddress(emailAddress);
        user.setDefaultEmailAddress(emailAddressBean);
    }

}
