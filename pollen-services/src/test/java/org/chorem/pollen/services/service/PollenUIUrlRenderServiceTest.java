package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.chorem.pollen.services.AbstractPollenServiceTest;
import org.chorem.pollen.services.PollenUIContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

public class PollenUIUrlRenderServiceTest extends AbstractPollenServiceTest {

    protected PollenUIUrlRenderService service;
    protected PollenUIContext pollenUIContext;

    protected static final String UI_END_POINT = "http://localhost:8080";

    @Before
    public void setUp() throws ParseException {
        service = newService(PollenUIUrlRenderService.class);

        pollenUIContext = new PollenUIContext();
        pollenUIContext.setUiEndPoint(UI_END_POINT);
        pollenUIContext.setUserValidateUrl(UI_END_POINT + "/#signcheck/{userId}/{token}");
        pollenUIContext.setPollEditUrl(UI_END_POINT + "/#poll/edit/{pollId}/{token}");
        pollenUIContext.setPollVoteUrl(UI_END_POINT + "/#poll/vote/{pollId}/{token}");
        pollenUIContext.setPollVoteEditUrl(UI_END_POINT + "/#poll/vote/{pollId}/{voteId}/{token}");
        pollenUIContext.setProfileUrl(UI_END_POINT + "/#user/profile");
    }

    @Test
    public void pollEditUrlTest() {
        String url;

        url = service.getPollEditUrl(pollenUIContext.getPollEditUrl(), "PollId", null);
        Assert.assertNotNull(url);
        Assert.assertEquals(UI_END_POINT + "/#poll/edit/PollId", url);

        url = service.getPollEditUrl(pollenUIContext.getPollEditUrl(),"PollId", "Token");
        Assert.assertNotNull(url);
        Assert.assertEquals(UI_END_POINT + "/#poll/edit/PollId/Token", url);
    }

    @Test
    public void pollVoteUrlTest() {
        String url;

        url = service.getPollVoteUrl(pollenUIContext.getPollVoteUrl(),"PollId", null);
        Assert.assertNotNull(url);
        Assert.assertEquals(UI_END_POINT + "/#poll/vote/PollId", url);

        url = service.getPollVoteUrl(pollenUIContext.getPollVoteUrl(),"PollId", "Token");
        Assert.assertNotNull(url);
        Assert.assertEquals(UI_END_POINT + "/#poll/vote/PollId/Token", url);
    }

    @Test
    public void pollVoteEditUrlTest() {
        String url;

        url = service.getPollVoteEditUrl(pollenUIContext.getPollVoteEditUrl(),"PollId", "Token", "VoteToken");
        Assert.assertNotNull(url);
        Assert.assertEquals(UI_END_POINT + "/#poll/vote/PollId/Token/VoteToken", url);
    }

    @Test
    public void userValidateUrl() {

        String url = service.getUserValidateUrl(pollenUIContext.getUserValidateUrl(),"UserId", "Token");

        Assert.assertNotNull(url);
        Assert.assertEquals(UI_END_POINT + "/#signcheck/UserId/Token", url);
    }

}
