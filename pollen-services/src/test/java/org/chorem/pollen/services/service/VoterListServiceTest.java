package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.chorem.pollen.persistence.entity.ChoiceType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.services.AbstractPollenServiceTest;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PollBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.QuestionBean;
import org.chorem.pollen.services.bean.VoterListBean;
import org.chorem.pollen.services.bean.VoterListMemberBean;
import org.chorem.pollen.services.service.security.PollenAuthenticationException;
import org.chorem.pollen.services.service.security.PollenEmailNotValidatedException;
import org.chorem.pollen.services.service.security.PollenInvalidSessionTokenException;
import org.chorem.pollen.services.service.security.PollenUserBannedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created on 6/6/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoterListServiceTest extends AbstractPollenServiceTest {

    protected PollenUserService userService;

    protected PollService pollService;

    protected FavoriteListService favoriteListService;

    protected VoterListService voterListService;

    protected VoterListService service;

    protected PollenUser user;

    @Before
    public void setUp() throws Exception {

        loadFixtures("user-fixtures");

        loadFixtures("fixtures");

        userService = newService(PollenUserService.class);
        pollService = newService(PollService.class);
        favoriteListService = newService(FavoriteListService.class);
        voterListService = newService(VoterListService.class);
        service = newService(VoterListService.class);

        getServiceContext().setDate(new Date(1363948427576L));

        user = application.fixture("user_jean");
    }

//    @Test
//    public void importFavoriteList() throws FavoriteListImportException, InvalidFormException, IOException, PollenInvalidSessionTokenException, PollenAuthenticationException {
//
//        login("jean@pollen.org", "fake");
//
//        // create favorite list
//
//        FavoriteListBean favoriteListBean1 = new FavoriteListBean();
//
//        favoriteListBean1.setName("list1");
//        PollenEntityRef<FavoriteList> savedList1 = favoriteListService.createFavoriteList(favoriteListBean1);
//        Assert.assertNotNull(savedList1);
//        String favoriteListId = savedList1.getEntityId();
//        Assert.assertNotNull(favoriteListId);
//
//        // create import file
//
//        File importFile = new File(application.getTestBasedir(), "importFavoriteListFromFile_" + System.nanoTime());
//        Files.write(FavoriteListServiceTest.IMPORT_FILE_CONTENT, importFile, Charsets.UTF_8);
//
//        // import file
//
//        favoriteListService.importFavoriteListMembersFromCsv(favoriteListId, importFile);
//
//        // create poll and voter list
//
//        PollBean poll = pollService.getNewPoll(ChoiceType.TEXT);
//
//        poll.setPollType(PollType.RESTRICTED);
//        poll.setTitle("poll1");
//        poll.setParticipants(Sets.newHashSet());
//
//        List<ChoiceBean> choices = new ArrayList<>();
//
//        ChoiceBean choice1 = new ChoiceBean();
//        choices.add(choice1);
//        choice1.setChoiceType(ChoiceType.TEXT);
//        choice1.setChoiceValue("A");
//        choice1.setDescription("Choice A");
//
//        ChoiceBean choice2 = new ChoiceBean();
//        choice2.setChoiceType(ChoiceType.TEXT);
//        choice2.setChoiceValue("B");
//        choice2.setDescription("Choice B");
//        choices.add(choice2);
//
//        PollenEntityRef<Poll> createdPoll = pollService.createPoll(poll, choices);
//        Assert.assertNotNull(createdPoll);
//
//        String pollId = createdPoll.getEntityId();
//
//        PollenEntityRef<VoterList> createdVoterList = voterListService.importFavoriteList(pollId, favoriteListId);
//        Assert.assertNotNull(createdVoterList);
//
//        Set<VoterListMemberBean> voterListMembers = voterListService.getVoterListMembers(pollId, createdVoterList.getEntityId());
//        Assert.assertNotNull(voterListMembers);
//        Assert.assertEquals(2, voterListMembers.size());
//
//    }

    @Test
    public void createVoterList() throws InvalidFormException, PollenInvalidSessionTokenException, PollenAuthenticationException, PollenEmailNotValidatedException, PollenUserBannedException {

        login("jean@pollen.org", "fake");

        PollBean poll = pollService.getNewPoll(ChoiceType.TEXT);

        poll.setPollType(PollType.RESTRICTED);
        poll.setTitle("poll1");

        List<QuestionBean> questions = new ArrayList<>();

        QuestionBean question1 = new QuestionBean();
        questions.add(question1);
        question1.setQuestionOrder(1);
        question1.setTitle("question1");

        List<ChoiceBean> choices = new ArrayList<>();

        ChoiceBean choice1 = new ChoiceBean();
        choices.add(choice1);
        choice1.setChoiceType(ChoiceType.TEXT);
        choice1.setChoiceValue("A");
        choice1.setDescription("Choice A");

        ChoiceBean choice2 = new ChoiceBean();
        choice2.setChoiceType(ChoiceType.TEXT);
        choice2.setChoiceValue("B");
        choice2.setDescription("Choice B");
        choices.add(choice2);

        question1.setChoices(choices);
        poll.setQuestions(questions);

        PollenEntityRef<Poll> createdPoll = pollService.createPoll(poll);
        Assert.assertNotNull(createdPoll);

        String pollId = createdPoll.getEntityId();

        VoterListBean voterList = new VoterListBean();
        try {
            voterListService.addVoterList(pollId, voterList, Collections.emptyList());
            Assert.fail();
        } catch (InvalidFormException e) {
            // missing name
            // missing member
            // missing weight
            assertErrorKeyFound(e, "name", "member", "weight");
        }
        voterList.setWeight(1);
        try {
            voterListService.addVoterList(pollId, voterList, Collections.emptyList());
            Assert.fail();
        } catch (InvalidFormException e) {
            // missing name
            // missing member
            assertErrorKeyFound(e, "name", "member");
        }
        voterList.setName("voterList1");
        List<VoterListMemberBean> listMember = new ArrayList<>();
        try {
            voterListService.addVoterList(pollId, voterList, listMember);
            Assert.fail();
        } catch (InvalidFormException e) {
            // missing member
            assertErrorKeyFound(e, "member");
        }
        VoterListMemberBean member = new VoterListMemberBean();
        member.setName("member1");
        member.setEmail("member1@pollen.org");
        member.setWeight(1);
    
        voterList.setCountMembers(1);

        listMember.add(member);
        PollenEntityRef<VoterList> voterListPersisted = voterListService.addVoterList(pollId, voterList, listMember);
        Assert.assertNotNull(voterListPersisted);

    }
}
