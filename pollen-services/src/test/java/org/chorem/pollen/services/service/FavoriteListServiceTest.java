package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.chorem.pollen.persistence.entity.FavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteListMember;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.AbstractPollenServiceTest;
import org.chorem.pollen.services.bean.ChildFavoriteListBean;
import org.chorem.pollen.services.bean.FavoriteListBean;
import org.chorem.pollen.services.bean.FavoriteListMemberBean;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.service.security.PollenAuthenticationException;
import org.chorem.pollen.services.service.security.PollenEmailNotValidatedException;
import org.chorem.pollen.services.service.security.PollenInvalidSessionTokenException;
import org.chorem.pollen.services.service.security.PollenUserBannedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

/**
 * Created on 5/29/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class FavoriteListServiceTest extends AbstractPollenServiceTest {

    protected PollenUserService userService;

    protected FavoriteListService service;

    protected PollenUser user;

    @Before
    public void setUp() throws Exception {

        loadFixtures("user-fixtures");

        loadFixtures("fixtures");

        userService = newService(PollenUserService.class);
        service = newService(FavoriteListService.class);

        getServiceContext().setDate(new Date(1363948427576L));

        user = application.fixture("user_jean");
    }

    public static String IMPORT_FILE_CONTENT = "user1@pollen.org;user1\n" +
            "user2@pollen.org";
    public static String IMPORT_FILE_CONTENT_WITH_ERROR_EMAIL = "user1@pollen.org;user3";
    public static String IMPORT_FILE_CONTENT_WITH_ERROR_NAME = "user3@pollen.org;user1";

    @Test
    public void importFavoriteListFromFile() throws PollenInvalidSessionTokenException, PollenAuthenticationException, InvalidFormException, IOException, FavoriteListImportException, PollenEmailNotValidatedException, PollenUserBannedException {

        login("jean@pollen.org", "fake");

        FavoriteListBean favoriteListBean1 = new FavoriteListBean();

        favoriteListBean1.setName("list1");
        PollenEntityRef<FavoriteList> savedList1 = service.createFavoriteList(favoriteListBean1);
        Assert.assertNotNull(savedList1);
        String favoriteListId = savedList1.getEntityId();
        Assert.assertNotNull(favoriteListId);

        // create import file

        File importFile = new File(application.getTestBasedir(), "importFavoriteListFromFile_" + System.nanoTime());
        Files.asCharSink(importFile, Charsets.UTF_8).write(IMPORT_FILE_CONTENT);

        PaginationResultBean<FavoriteListMemberBean> members = service.getFavoriteListMembers(favoriteListId, null, PaginationParameterBean.of(0, -1));
        Assert.assertEquals(0, members.getElements().size());

        // import file

        service.importFavoriteListMembersFromCsv(favoriteListId, importFile);

        members = service.getFavoriteListMembers(favoriteListId, null, PaginationParameterBean.of(0, -1));
        Assert.assertEquals(2, members.getElements().size());

        service.importFavoriteListMembersFromCsv(favoriteListId, importFile);

        members = service.getFavoriteListMembers(favoriteListId, null, PaginationParameterBean.of(0, -1));
        Assert.assertEquals(2, members.getElements().size());

        File importFile2 = new File(application.getTestBasedir(), "importFavoriteListFromFile_" + System.nanoTime());
        Files.asCharSink(importFile2, Charsets.UTF_8).write(IMPORT_FILE_CONTENT_WITH_ERROR_EMAIL);
        try {
            service.importFavoriteListMembersFromCsv(favoriteListId, importFile2);
            Assert.fail("importFavoriteListMembersFromCsv call with a different email and name should have fail");
        } catch (FavoriteListImportException e) {
            Assert.assertTrue(e.getErrors().contains("Ligne 1 : Courriel « user1@pollen.org » est déjà utilisé"));
        }

        File importFile3 = new File(application.getTestBasedir(), "importFavoriteListFromFile_" + System.nanoTime());
        Files.asCharSink(importFile3, Charsets.UTF_8).write(IMPORT_FILE_CONTENT_WITH_ERROR_NAME);
        try {
            service.importFavoriteListMembersFromCsv(favoriteListId, importFile3);
            Assert.fail("importFavoriteListMembersFromCsv call with a different email and name should have fail");
        } catch (FavoriteListImportException e) {
            Assert.assertTrue(e.getErrors().contains("Ligne 1 : Nom « user1 » est déjà utilisé"));
        }
    }

    @Test
    public void createFavoriteList() throws PollenInvalidSessionTokenException, PollenAuthenticationException, InvalidFormException, PollenEmailNotValidatedException, PollenUserBannedException {

        login("jean@pollen.org", "fake");

        // create a first list

        FavoriteListBean favoriteListBean1 = new FavoriteListBean();

        try {
            service.createFavoriteList(favoriteListBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // no name
            assertErrorKeyFound(e, "name");
        }

        favoriteListBean1.setName("list1");
        PollenEntityRef<FavoriteList> savedList1 = service.createFavoriteList(favoriteListBean1);
        Assert.assertNotNull(savedList1);
        Assert.assertNotNull(savedList1.getEntityId());

        // create a first member

        FavoriteListMemberBean memberBean1 = new FavoriteListMemberBean();
        try {
            service.addFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // no name
            // no email
            assertErrorKeyFound(e, "name", "email", "weight");
        }

        memberBean1.setName("member1");
        try {
            service.addFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // no email
            assertErrorKeyFound(e, "email", "weight");
        }

        memberBean1.setEmail("member1@");
        try {
            service.addFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // invalid email
            assertErrorKeyFound(e, "email", "weight");
        }

        memberBean1.setEmail("member1@pollen.org");
        try {
            service.addFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // invalid email
            assertErrorKeyFound(e, "weight");
        }

        memberBean1.setWeight(2);
        PollenEntityRef<FavoriteListMember> savedMmember1 = service.addFavoriteListMember(savedList1.getEntityId(), memberBean1);
        Assert.assertNotNull(savedMmember1);
        Assert.assertNotNull(savedMmember1.getEntityId());

        // create a second member

        FavoriteListMemberBean memberBean2 = new FavoriteListMemberBean();

        memberBean2.setName("member1");
        memberBean2.setEmail("member1@pollen.org");
        memberBean2.setWeight(1);
        try {
            service.addFavoriteListMember(savedList1.getEntityId(), memberBean2);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated name
            // duplicated email
            assertErrorKeyFound(e, "name", "email");
        }

        memberBean2.setName("member2");
        try {
            service.addFavoriteListMember(savedList1.getEntityId(), memberBean2);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated email
            assertErrorKeyFound(e, "email");
        }

        memberBean2.setEmail("member2@pollen.org");

        PollenEntityRef<FavoriteListMember> savedMmember2 = service.addFavoriteListMember(savedList1.getEntityId(), memberBean2);
        Assert.assertNotNull(savedMmember2);
        Assert.assertNotNull(savedMmember2.getEntityId());


        // create a second list

        FavoriteListBean favoriteListBean2 = new FavoriteListBean();
        favoriteListBean2.setName("list1");
        try {
            service.createFavoriteList(favoriteListBean2);
            Assert.fail();
        } catch (InvalidFormException e) {
            // same name
            assertErrorKeyFound(e, "name");
        }

        favoriteListBean2.setName("list2");
        PollenEntityRef<FavoriteList> savedList2 = service.createFavoriteList(favoriteListBean2);
        Assert.assertNotNull(savedList2);
        Assert.assertNotNull(savedList2.getEntityId());

    }

    @Test
    public void editFavoriteList() throws PollenInvalidSessionTokenException, PollenAuthenticationException, InvalidFormException, PollenEmailNotValidatedException, PollenUserBannedException {

        login("jean@pollen.org", "fake");

        // create a first list

        FavoriteListBean favoriteListBean1 = new FavoriteListBean();

        favoriteListBean1.setName("list1");
        PollenEntityRef<FavoriteList> savedList1 = service.createFavoriteList(favoriteListBean1);
        Assert.assertNotNull(savedList1);
        Assert.assertNotNull(savedList1.getEntityId());

        // create a first member

        FavoriteListMemberBean memberBean1 = new FavoriteListMemberBean();
        memberBean1.setName("member1");
        memberBean1.setEmail("member1@pollen.org");
        memberBean1.setWeight(1);
        PollenEntityRef<FavoriteListMember> savedMmember1 = service.addFavoriteListMember(savedList1.getEntityId(), memberBean1);
        Assert.assertNotNull(savedMmember1);
        Assert.assertNotNull(savedMmember1.getEntityId());

        // create a second member

        FavoriteListMemberBean memberBean2 = new FavoriteListMemberBean();
        memberBean2.setName("member2");
        memberBean2.setEmail("member2@pollen.org");
        memberBean2.setWeight(1);
        PollenEntityRef<FavoriteListMember> savedMmember2 = service.addFavoriteListMember(savedList1.getEntityId(), memberBean2);
        Assert.assertNotNull(savedMmember2);
        Assert.assertNotNull(savedMmember2.getEntityId());

        // create a second list

        FavoriteListBean favoriteListBean2 = new FavoriteListBean();
        favoriteListBean2.setName("list2");
        PollenEntityRef<FavoriteList> savedList2 = service.createFavoriteList(favoriteListBean2);
        Assert.assertNotNull(savedList2);
        Assert.assertNotNull(savedList2.getEntityId());

        // edit first list

        try {
            service.editFavoriteList(favoriteListBean1);
            Assert.fail();
        } catch (IllegalStateException e) {
            // Should having id
            Assert.assertTrue(true);
        }

        favoriteListBean1.setEntityId(savedList1.getEntityId());
        service.editFavoriteList(favoriteListBean1);

        favoriteListBean1.setName("list2");
        try {
            service.editFavoriteList(favoriteListBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated name
            assertErrorKeyFound(e, "name");
        }

        favoriteListBean1.setName("list3");
        service.editFavoriteList(favoriteListBean1);

    }

    @Test
    public void editFavoriteListMember() throws PollenInvalidSessionTokenException, PollenAuthenticationException, InvalidFormException, PollenEmailNotValidatedException, PollenUserBannedException {

        login("jean@pollen.org", "fake");

        // create a first list

        FavoriteListBean favoriteListBean1 = new FavoriteListBean();

        favoriteListBean1.setName("list1");
        PollenEntityRef<FavoriteList> savedList1 = service.createFavoriteList(favoriteListBean1);
        Assert.assertNotNull(savedList1);
        Assert.assertNotNull(savedList1.getEntityId());

        // create a first member

        FavoriteListMemberBean memberBean1 = new FavoriteListMemberBean();
        memberBean1.setName("member1");
        memberBean1.setEmail("member1@pollen.org");
        memberBean1.setWeight(1);
        PollenEntityRef<FavoriteListMember> savedMmember1 = service.addFavoriteListMember(savedList1.getEntityId(), memberBean1);
        Assert.assertNotNull(savedMmember1);
        Assert.assertNotNull(savedMmember1.getEntityId());

        // create a second member

        FavoriteListMemberBean memberBean2 = new FavoriteListMemberBean();
        memberBean2.setName("member2");
        memberBean2.setEmail("member2@pollen.org");
        memberBean2.setWeight(1);
        PollenEntityRef<FavoriteListMember> savedMmember2 = service.addFavoriteListMember(savedList1.getEntityId(), memberBean2);
        Assert.assertNotNull(savedMmember2);
        Assert.assertNotNull(savedMmember2.getEntityId());

        // edit first member

        try {
            service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (IllegalStateException e) {
            // Should having id
            Assert.assertTrue(true);
        }

        memberBean1.setEntityId(savedMmember1.getEntityId());
        service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);

        memberBean1.setName("member2");
        memberBean1.setEmail("member2[pollen.org");
        memberBean1.setWeight(0);
        try {
            service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated name
            // invalid email
            assertErrorKeyFound(e, "name", "email", "weight");
        }

        memberBean1.setName("member3");
        try {
            service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // invalid email
            assertErrorKeyFound(e, "email", "weight");
        }

        memberBean1.setEmail("member2@pollen.org");
        try {
            service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated email
            assertErrorKeyFound(e, "email", "weight");
        }

        memberBean1.setEmail("member1@pollen.org");
        try {
            service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated email
            assertErrorKeyFound(e,  "weight");
        }

        memberBean1.setWeight(1);
        service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);

        memberBean1.setEmail("member3@pollen.org");
        service.editFavoriteListMember(savedList1.getEntityId(), memberBean1);

    }


    @Test
    public void editChildFavoriteList() throws PollenInvalidSessionTokenException, PollenAuthenticationException, InvalidFormException, PollenEmailNotValidatedException, PollenUserBannedException {
        login("jean@pollen.org", "fake");

        // create a first list

        FavoriteListBean favoriteListBean1 = new FavoriteListBean();
        favoriteListBean1.setName("list1");
        PollenEntityRef<FavoriteList> savedList1 = service.createFavoriteList(favoriteListBean1);
        favoriteListBean1.setId(savedList1);

        FavoriteListBean favoriteListBean2 = new FavoriteListBean();
        favoriteListBean2.setName("list2");
        PollenEntityRef<FavoriteList> savedList2 = service.createFavoriteList(favoriteListBean2);
        favoriteListBean2.setId(savedList2);

        ChildFavoriteListBean childList1 = new ChildFavoriteListBean();
        childList1.setChild(favoriteListBean1);

        try {
            service.addChildList(favoriteListBean1.getEntityId(), childList1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // same parent an child, wieght null
            assertErrorKeyFound(e, "child", "weight");
        }

        childList1.setWeight(1);
        try {
            service.addChildList(favoriteListBean1.getEntityId(), childList1);
            Assert.fail();
        } catch (InvalidFormException e) {
            // same parent an child,
            assertErrorKeyFound(e, "child");
        }

        childList1.setChild(favoriteListBean2);
        service.addChildList(favoriteListBean1.getEntityId(), childList1);

        ChildFavoriteListBean childList2 = new ChildFavoriteListBean();
        childList2.setChild(favoriteListBean2);
        childList2.setWeight(2);
        try {
            service.addChildList(favoriteListBean1.getEntityId(), childList2);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated child
            assertErrorKeyFound(e, "child");
        }

        childList2.setChild(favoriteListBean1);
        try {
            service.addChildList(favoriteListBean2.getEntityId(), childList2);
            Assert.fail();
        } catch (InvalidFormException e) {
            // child is ancestor
            assertErrorKeyFound(e, "child");
        }

    }
}
