package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.ChoiceType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.AbstractPollenServiceTest;
import org.chorem.pollen.services.PollenFixtures;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PollBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.QuestionBean;
import org.chorem.pollen.services.service.security.PollenAuthenticationException;
import org.chorem.pollen.services.service.security.PollenEmailNotValidatedException;
import org.chorem.pollen.services.service.security.PollenInvalidPermissionException;
import org.chorem.pollen.services.service.security.PollenInvalidSessionTokenException;
import org.chorem.pollen.services.service.security.PollenUnauthorizedException;
import org.chorem.pollen.services.service.security.PollenUserBannedException;
import org.chorem.pollen.services.service.security.SecurityService;
import org.chorem.pollen.services.test.FakePollenSecurityContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollServiceTest extends AbstractPollenServiceTest {

    protected PollService service;

    protected QuestionService questionService;

    protected ChoiceService choiceService;

    protected VoterListService voterListService;

    protected SecurityService securityService;

    @Before
    public void setUp() throws Exception {

        loadFixtures("user-fixtures");

        loadFixtures("fixtures");

        service = newService(PollService.class);
        questionService = newService(QuestionService.class);
        choiceService = newService(ChoiceService.class);
        voterListService = newService(VoterListService.class);
        securityService = newService(SecurityService.class);

        getServiceContext().setDate(new Date(1363948427576L));

        getServiceContext().setSecurityContext(new FakePollenSecurityContext());
    }

    @Test
    public void createSafeFreePoll() throws InvalidFormException {

        PollBean poll = service.getNewPoll(ChoiceType.TEXT);

        poll.setPollType(PollType.FREE);
        poll.setTitle("poll1");

        List<QuestionBean> questions = new ArrayList<>();

        QuestionBean question1 = new QuestionBean();
        questions.add(question1);
        question1.setQuestionOrder(1);
        question1.setTitle("question1");
        question1.setVoteCountingType(1);

        List<ChoiceBean> choices = new ArrayList<>();

        ChoiceBean choice1 = new ChoiceBean();
        choices.add(choice1);
        choice1.setChoiceType(ChoiceType.TEXT);
        choice1.setChoiceValue("A");
        choice1.setDescription("Choice A");


        ChoiceBean choice2 = new ChoiceBean();
        choice2.setChoiceType(ChoiceType.TEXT);
        choice2.setChoiceValue("B");
        choice2.setDescription("Choice B");
        choices.add(choice2);

        question1.setChoices(choices);
        poll.setQuestions(questions);

        PollenEntityRef<Poll> createdPoll = service.createPoll(poll);
        Assert.assertNotNull(createdPoll);
        String createdPollId = createdPoll.getEntityId();
        Assert.assertNotNull(createdPollId);

        PollBean reloadedPoll = service.getPoll(createdPollId);

        Assert.assertEquals(getServiceContext().getNow(), reloadedPoll.getBeginDate());

        List<QuestionBean> createdQuestions = questionService.getQuestions(createdPollId);

        Assert.assertNotNull(createdQuestions);
        Assert.assertEquals(1, createdQuestions.size());

        String questionId = createdQuestions.get(0).getEntityId();

        List<ChoiceBean> createdChoices = choiceService.getChoices(questionId);

        Assert.assertNotNull(createdChoices);
        Assert.assertEquals(2, createdChoices.size());


        ChoiceBean createdChoice1 = createdChoices.get(0);
        Assert.assertNotNull(createdChoice1);
        Assert.assertNotNull(createdChoice1.getEntityId());

        ChoiceBean reloadedChoice1 = choiceService.getChoice(questionId, createdChoice1.getEntityId());
        Assert.assertEquals(createdChoice1, reloadedChoice1);

        Assert.assertEquals(choice1.getChoiceValue(), createdChoice1.getChoiceValue());
        Assert.assertEquals(choice1.getDescription(), createdChoice1.getDescription());


        ChoiceBean createdChoice2 = createdChoices.get(1);
        Assert.assertNotNull(createdChoice2);
        Assert.assertNotNull(createdChoice2.getEntityId());

        ChoiceBean reloadedChoice2 = choiceService.getChoice(questionId, createdChoice2.getEntityId());
        Assert.assertEquals(createdChoice2, reloadedChoice2);

        Assert.assertEquals(choice2.getChoiceValue(), createdChoice2.getChoiceValue());
        Assert.assertEquals(choice2.getDescription(), createdChoice2.getDescription());

    }

    @Test
    public void testCreateFreePoll() throws InvalidFormException {

        PollBean poll = service.getNewPoll(ChoiceType.TEXT);

        poll.setPollType(PollType.FREE);

        try {
            service.createPoll(poll);
            Assert.fail();
        } catch (InvalidFormException e) {
            // missing title
            // missing questions[0].choice
            assertErrorKeyFound(e, "title", "questions[0].choice");
        }

        poll.setTitle("poll1");

        List<QuestionBean> questions = new ArrayList<>();

        QuestionBean question1 = new QuestionBean();
        questions.add(question1);
        question1.setQuestionOrder(1);
        question1.setTitle("question1");
        question1.setVoteCountingType(1);

        List<ChoiceBean> choices = new ArrayList<>();
        ChoiceBean choice1 = new ChoiceBean();
        choices.add(choice1);

        question1.setChoices(choices);
        poll.setQuestions(questions);

        try {
            service.createPoll(poll);
            Assert.fail();
        } catch (InvalidFormException e) {
            // missing choice type
            assertErrorKeyFound(e, "questions[0].choice[0].choiceType");
        }

        choice1.setChoiceType(ChoiceType.TEXT);
        try {
            service.createPoll(poll);
            Assert.fail();
        } catch (InvalidFormException e) {
            // missing choice name
            assertErrorKeyFound(e, "questions[0].choice[0].choiceValue");
        }

        choice1.setChoiceValue("A");
        choice1.setDescription("Choice A");


        ChoiceBean choice2 = new ChoiceBean();
        choice2.setChoiceType(ChoiceType.TEXT);
        choice2.setChoiceValue("A");
        choice2.setDescription("Choice B");
        choices.add(choice2);

        try {
            service.createPoll(poll);
            Assert.fail();
        } catch (InvalidFormException e) {
            // duplicated choice name
            assertErrorKeyFound(e, "questions[0].choice[1].choiceValue");
        }

        choice2.setChoiceValue("B");


        PollenEntityRef<Poll> createdPoll = service.createPoll(poll);
        Assert.assertNotNull(createdPoll);
        String createdPollId = createdPoll.getEntityId();
        Assert.assertNotNull(createdPollId);

        PollBean reloadedPoll = service.getPoll(createdPollId);
        Assert.assertEquals(createdPollId, reloadedPoll.getEntityId());

        Assert.assertEquals(getServiceContext().getNow(), reloadedPoll.getBeginDate());

//        Assert.assertNotNull(createdPoll.getCreator());
//        Assert.assertNotNull(createdPoll.getCreator().getTopiaId());
//        Assert.assertNull(createdPoll.getCreator().getChoiceValue());
//        Assert.assertNull(createdPoll.getCreator().getEmail());
//        Assert.assertNull(createdPoll.getComment());
//        Assert.assertNull(createdPoll.getVote());
//        Assert.assertNull(createdPoll.getVoterList());

        List<QuestionBean> createdQuestions = questionService.getQuestions(createdPollId);

        Assert.assertNotNull(createdQuestions);
        Assert.assertEquals(1, createdQuestions.size());

        String questionId = createdQuestions.get(0).getEntityId();

        List<ChoiceBean> createdChoices = choiceService.getChoices(questionId);

        Assert.assertNotNull(createdChoices);
        Assert.assertEquals(2, createdChoices.size());


        ChoiceBean createdChoice1 = createdChoices.get(0);
        Assert.assertNotNull(createdChoice1);
        Assert.assertNotNull(createdChoice1.getEntityId());

        ChoiceBean reloadedChoice1 = choiceService.getChoice(questionId, createdChoice1.getEntityId());
        Assert.assertEquals(createdChoice1, reloadedChoice1);

        Assert.assertEquals(choice1.getChoiceValue(), createdChoice1.getChoiceValue());
        Assert.assertEquals(choice1.getDescription(), createdChoice1.getDescription());

        ChoiceBean createdChoice2 = createdChoices.get(1);
        Assert.assertNotNull(createdChoice2);
        Assert.assertNotNull(createdChoice2.getEntityId());

        ChoiceBean reloadedChoice2 = choiceService.getChoice(questionId, createdChoice2.getEntityId());
        Assert.assertEquals(createdChoice2, reloadedChoice2);

        Assert.assertEquals(choice2.getChoiceValue(), createdChoice2.getChoiceValue());
        Assert.assertEquals(choice2.getDescription(), createdChoice2.getDescription());

    }

    //FIXME Review how to validate restricted poll
//    @Test
//    public void createSafeRestrictedPoll() throws InvalidFormException {
//
//        PollBean poll = service.getNewPoll();
//
//        poll.setPollType(PollType.GROUP);
//        poll.setTitle("poll1");
//
//        List<ChoiceBean> choices = new ArrayList<>();
//
//        ChoiceBean choice1 = new ChoiceBean();
//        choices.add(choice1);
//        choice1.setChoiceType(ChoiceType.TEXT);
//        choice1.setChoiceValue("A");
//        choice1.setDescription("Choice A");
//
//
//        ChoiceBean choice2 = new ChoiceBean();
//        choice2.setChoiceType(ChoiceType.TEXT);
//        choice2.setChoiceValue("B");
//        choice2.setDescription("Choice B");
//        choices.add(choice2);
//
//        List<VoterListBean> voterLists = new ArrayList<>();
//
//        PollenEntityRef<Poll> createdPoll = service.createPoll(poll, choices);
//        Assert.assertNotNull(createdPoll);
//        String createdPollId = createdPoll.getEntityId();
//        Assert.assertNotNull(createdPollId);
//
//        try {
//            service.getPoll(createdPollId);
//            Assert.fail();
//        } catch (PollenInvalidPermissionException e) {
//            Assert.assertTrue(true);
//        }
//
//        FakePollenSecurityContext securityContext = serviceContext.getSecurityContext();
//
//        PollenPrincipal principal = securityService.getPollenPrincipalByPermissionToken(createdPoll.getPermission());
//        securityContext.setMainPrincipal(principal);
//
//        PollBean reloadedPoll = service.getPoll(createdPollId);
////        Assert.assertEquals(createdPoll, reloadedPoll);
//
//        Assert.assertEquals(getServiceContext().getNow(), reloadedPoll.getBeginDate());
//
////        Assert.assertNotNull(createdPoll.getCreator());
////        Assert.assertNotNull(createdPoll.getCreator().getTopiaId());
////        Assert.assertNull(createdPoll.getCreator().getChoiceValue());
////        Assert.assertNull(createdPoll.getCreator().getEmail());
//
//        List<ChoiceBean> createdChoices = choiceService.getChoices(createdPollId);
//
//        Assert.assertNotNull(createdChoices);
//        Assert.assertEquals(2, createdChoices.size());
//
//        ChoiceBean createdChoice1 = createdChoices.get(0);
//        Assert.assertNotNull(createdChoice1);
//        Assert.assertNotNull(createdChoice1.getEntityId());
//
//        ChoiceBean reloadedChoice1 = choiceService.getChoice(createdPollId, createdChoice1.getEntityId());
//        Assert.assertEquals(createdChoice1, reloadedChoice1);
//
//        Assert.assertEquals(choice1.getChoiceValue(), createdChoice1.getChoiceValue());
//        Assert.assertEquals(choice1.getDescription(), createdChoice1.getDescription());
////        Assert.assertEquals(createdPoll.getCreator(), createdChoice1.getCreator());
//
//        ChoiceBean createdChoice2 = createdChoices.get(1);
//        Assert.assertNotNull(createdChoice2);
//        Assert.assertNotNull(createdChoice2.getEntityId());
//
//        ChoiceBean reloadedChoice2 = choiceService.getChoice(createdPollId, createdChoice2.getEntityId());
//        Assert.assertEquals(createdChoice2, reloadedChoice2);
//
//        Assert.assertEquals(choice2.getChoiceValue(), createdChoice2.getChoiceValue());
//        Assert.assertEquals(choice2.getDescription(), createdChoice2.getDescription());
////        Assert.assertEquals(createdPoll.getCreator(), createdChoice2.getCreator());
//
//        List<VoterListBean> createdVoterLists = voterListService.getVoterLists(createdPollId);
//
//        Assert.assertNotNull(createdVoterLists);
//        Assert.assertEquals(1, createdVoterLists.size());
//        VoterListBean createdVoterList = createdVoterLists.get(0);
//        Assert.assertNotNull(createdVoterList);
//        Assert.assertNotNull(createdVoterList.getEntityId());
//
//        VoterListBean reloadedVoterList = voterListService.getVoterList(createdPollId, createdVoterList.getEntityId());
//        Assert.assertEquals(createdVoterList, reloadedVoterList);
//
//        Assert.assertEquals(voterList.getChoiceValue(), createdVoterList.getChoiceValue());
//        Assert.assertEquals(voterList.getWeight(), createdVoterList.getWeight(), 0);
//
//        Assert.assertNotNull(createdVoterList.getMember());
//        Assert.assertEquals(2, createdVoterList.getMember().size());
//
//    }

//    @Test
//    public void testCreateRestrictedPoll() throws InvalidFormException {
//
//        PollBean poll = service.getNewPoll();
//        poll.setPollType(PollType.RESTRICTED);
//
//        poll.setTitle("poll1");
//
//        List<ChoiceBean> choices = new ArrayList<>();
//
//        ChoiceBean choice1 = new ChoiceBean();
//        choice1.setChoiceType(ChoiceType.TEXT);
//        choice1.setChoiceValue("A");
//        choice1.setDescription("Choice A");
//
//        choices.add(choice1);
//
//        ChoiceBean choice2 = new ChoiceBean();
//        choice2.setChoiceType(ChoiceType.TEXT);
//        choice2.setChoiceValue("B");
//        choice2.setDescription("Choice B");
//
//        choices.add(choice2);
//
//        try {
//            service.createPoll(poll, choices);
//            Assert.fail();
//        } catch (InvalidFormException e) {
//            // missing voterList
//            assertErrorKeyFound(e, "voterList");
//        }
//
//        List<VoterListBean> voterLists = new ArrayList<>();
//
//        // add voter list
//        VoterListBean voterList = new VoterListBean();
//
//        voterLists.add(voterList);
//
//        try {
//            service.createPoll(poll, choices);
//            Assert.fail();
//        } catch (InvalidFormException e) {
//            // missing name
//            // missing weight
//            // missing voterListMember
//            assertErrorKeyFound(e, "voterList[0].name", "voterList[0].weight", "voterList[0].member");
//        }
//
//        voterList.setChoiceValue("voterList1");
//        voterList.setWeight(1);
//
//        VoterListMemberBean voterListMember1 = new VoterListMemberBean();
//        voterList.addMember(voterListMember1);
//
//        try {
//            service.createPoll(poll, choices);
//            Assert.fail();
//        } catch (InvalidFormException e) {
//            // missing member name
//            // missing member email
//            assertErrorKeyFound(e, "voterList[0].member[0].name", "voterList[0].member[0].email", "voterList[0].member[0].weight");
//        }
//
//        voterListMember1.setChoiceValue("voter1");
//        voterListMember1.setWeight(0.3);
//        voterListMember1.setEmail("voter1_pollen.org");
//
//        try {
//            service.createPoll(poll, choices);
//            Assert.fail();
//        } catch (InvalidFormException e) {
//            // invalid member email
//            assertErrorKeyFound(e, "voterList[0].member[0].email");
//        }
//
//        voterListMember1.setEmail("voter1@pollen.org");
//
//        VoterListMemberBean voterListMember2 = new VoterListMemberBean();
//        voterListMember2.setChoiceValue("voter1");
//        voterListMember2.setWeight(0.7);
//        voterListMember2.setEmail("voter1@pollen.org");
//        voterList.addMember(voterListMember2);
//
//        try {
//            service.createPoll(poll, choices);
//            Assert.fail();
//        } catch (InvalidFormException e) {
//            // same name
//            // same email
//            assertErrorKeyFound(e, "voterList[0].member[1].name", "voterList[0].member[1].email");
//        }
//
//        voterListMember2.setChoiceValue("voter2");
//        voterListMember2.setEmail("voter2@pollen.org");
//
//        PollenEntityRef<Poll> createdPoll = service.createPoll(poll, choices);
//        Assert.assertNotNull(createdPoll);
//        String createdPollId = createdPoll.getEntityId();
//        Assert.assertNotNull(createdPollId);
//
//        try {
//            service.getPoll(createdPollId);
//            Assert.fail();
//        } catch (PollenInvalidPermissionException e) {
//            Assert.assertTrue(true);
//        }
//
//        PollenPrincipal principal = securityService.getPollenPrincipalByPermissionToken(createdPoll.getPermission());
//        FakePollenSecurityContext securityContext = (FakePollenSecurityContext) serviceContext.getSecurityContext();
//        securityContext.setMainPrincipal(principal);
//
//        PollBean reloadedPoll = service.getPoll(createdPollId);
////        Assert.assertEquals(createdPoll, reloadedPoll);
//
//        Assert.assertEquals(getServiceContext().getNow(), reloadedPoll.getBeginDate());
//
////        Assert.assertNotNull(createdPoll.getCreator());
////        Assert.assertNotNull(createdPoll.getCreator().getTopiaId());
////        Assert.assertNull(createdPoll.getCreator().getChoiceValue());
////        Assert.assertNull(createdPoll.getCreator().getEmail());
////        Assert.assertNull(createdPoll.getComment());
////        Assert.assertNull(createdPoll.getVote());
//
//        List<ChoiceBean> createdChoices = choiceService.getChoices(createdPollId);
//
//        Assert.assertNotNull(createdChoices);
//        Assert.assertEquals(2, createdChoices.size());
//
//        ChoiceBean createdChoice1 = createdChoices.get(0);
//        Assert.assertNotNull(createdChoice1);
//        Assert.assertNotNull(createdChoice1.getEntityId());
//
//        ChoiceBean reloadedChoice1 = choiceService.getChoice(createdPollId, createdChoice1.getEntityId());
//        Assert.assertEquals(createdChoice1, reloadedChoice1);
//
//        Assert.assertEquals(choice1.getChoiceValue(), createdChoice1.getChoiceValue());
//        Assert.assertEquals(choice1.getDescription(), createdChoice1.getDescription());
////        Assert.assertEquals(createdPoll.getCreator(), createdChoice1.getCreator());
//
//        ChoiceBean createdChoice2 = createdChoices.get(1);
//        Assert.assertNotNull(createdChoice2);
//        Assert.assertNotNull(createdChoice2.getEntityId());
//
//        ChoiceBean reloadedChoice2 = choiceService.getChoice(createdPollId, createdChoice2.getEntityId());
//        Assert.assertEquals(createdChoice2, reloadedChoice2);
//
//        Assert.assertEquals(choice2.getChoiceValue(), createdChoice2.getChoiceValue());
//        Assert.assertEquals(choice2.getDescription(), createdChoice2.getDescription());
////        Assert.assertEquals(createdPoll.getCreator(), createdChoice2.getCreator());
//
//        List<VoterListBean> createdVoterLists = voterListService.getVoterLists(createdPollId);
//
//        Assert.assertNotNull(createdVoterLists);
//        Assert.assertEquals(1, createdVoterLists.size());
//        VoterListBean createdVoterList = createdVoterLists.get(0);
//        Assert.assertNotNull(createdVoterList);
//        Assert.assertNotNull(createdVoterList.getEntityId());
//
//        VoterListBean reloadedVoterList = voterListService.getVoterList(createdPollId, createdVoterList.getEntityId());
//        Assert.assertEquals(createdVoterList, reloadedVoterList);
//
//        Assert.assertEquals(voterList.getChoiceValue(), createdVoterList.getChoiceValue());
//        Assert.assertEquals(voterList.getWeight(), createdVoterList.getWeight(), 0);
//
//        Assert.assertNotNull(createdVoterList.getMember());
//        Assert.assertEquals(2, createdVoterList.getMember().size());
//
////        VoterListMember createdVoterListMember1 = Iterables.get(createdVoterList.getMember(), 0);
////
////        Assert.assertNotNull(createdVoterListMember1);
////        Assert.assertNotNull(createdVoterListMember1.getTopiaId());
////        VoterListMember reloadedVoterListMember1 = voterListService.getMember(createdPoll.getTopiaId(), createdVoterList.getTopiaId(), createdVoterListMember1.getTopiaId());
////        Assert.assertEquals(createdVoterListMember1, reloadedVoterListMember1);
////
////        Assert.assertEquals(reloadedVoterListMember1.getChoiceValue(), createdVoterListMember1.getChoiceValue());
////        Assert.assertEquals(reloadedVoterListMember1.getEmail(), createdVoterListMember1.getEmail());
////        Assert.assertEquals(reloadedVoterListMember1.getWeight(), createdVoterListMember1.getWeight(), 0);
////
////        VoterListMember createdVoterListMember2 = Iterables.get(createdVoterList.getMember(), 1);
////
////        Assert.assertNotNull(createdVoterListMember2);
////        Assert.assertNotNull(createdVoterListMember2.getTopiaId());
////
////        VoterListMember reloadedVoterListMember2 = voterListService.getMember(createdPoll.getTopiaId(), createdVoterList.getTopiaId(), createdVoterListMember2.getTopiaId());
////        Assert.assertEquals(createdVoterListMember2, reloadedVoterListMember2);
////
////        Assert.assertEquals(reloadedVoterListMember2.getChoiceValue(), createdVoterListMember2.getChoiceValue());
////        Assert.assertEquals(reloadedVoterListMember2.getEmail(), createdVoterListMember2.getEmail());
////        Assert.assertEquals(reloadedVoterListMember2.getWeight(), createdVoterListMember2.getWeight(), 0);
//
//    }

    @Test
    public void testAssignPollToConnectedUser() throws PollenInvalidSessionTokenException, InvalidFormException, PollenAuthenticationException, PollenEmailNotValidatedException, PollenUserBannedException {
        Poll poll = fixture(PollenFixtures.POLL_NORMAL_ID);
        Assert.assertNull(poll.getCreator().getPollenUser());
        try {
            service.assignPollToConnectedUser(poll.getTopiaId());
            Assert.fail("An error should be thrown as no user is connected");

        } catch (PollenUnauthorizedException e) {
            Assert.assertNull(poll.getCreator().getPollenUser());
        }

        login("tony@pollen.org", "fake");
        try {
            service.assignPollToConnectedUser(poll.getTopiaId());
            Assert.fail("An error should be thrown if the connected user does not provide the permission to edit the poll");

        } catch (PollenInvalidPermissionException e) {
            Assert.assertNull(poll.getCreator().getPollenUser());
        }

        securityService.getSecurityContext().setMainPrincipal(poll.getCreator());
        service.assignPollToConnectedUser(poll.getTopiaId());

        PollenUser pollenUser = poll.getCreator().getPollenUser();
        Assert.assertNotNull(pollenUser);
        Assert.assertTrue(pollenUser.getEmailAddresses().stream()
                                  .anyMatch(emailAddress -> "tony@pollen.org".equals(emailAddress.getEmailAddress())));

        login("jean@pollen.org", "fake");
        try {
            service.assignPollToConnectedUser(poll.getTopiaId());
            Assert.fail("An error should be thrown as the poll is already assigned to a user");

        } catch (InvalidFormException e) {
            Assert.assertNotNull(pollenUser);
            Assert.assertTrue(pollenUser.getEmailAddresses().stream()
                                      .anyMatch(emailAddress -> "tony@pollen.org".equals(emailAddress.getEmailAddress())));
        }

    }

}
