package org.chorem.pollen.services;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import org.chorem.pollen.services.service.InvalidFormException;
import org.chorem.pollen.services.service.security.PollenAuthenticationException;
import org.chorem.pollen.services.service.security.PollenEmailNotValidatedException;
import org.chorem.pollen.services.service.security.PollenInvalidSessionTokenException;
import org.chorem.pollen.services.service.security.PollenUserBannedException;
import org.chorem.pollen.services.service.security.SecurityService;
import org.chorem.pollen.services.test.FakePollenApplicationContext;
import org.chorem.pollen.services.test.FakePollenServiceContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;

import java.util.Locale;

public abstract class AbstractPollenServiceTest {

    protected static final double DELTA = 0.0001;

    @Rule
    public final FakePollenApplicationContext application = new FakePollenApplicationContext("pollen-services.properties");

    protected FakePollenServiceContext serviceContext;

    public void loadFixtures(String fixturesSetName) {

        application.loadFixtures(getServiceContext(), fixturesSetName);
    }

    public <E> E fixture(String id) {

        return application.fixture(id);
    }

    @After
    public void tearDown() {
        logout();
    }

    protected FakePollenServiceContext getServiceContext() {

        if (serviceContext == null) {
            serviceContext = application.newServiceContext(application.newPersistenceContext(), Locale.FRANCE);
        }

        return serviceContext;
    }

    protected <E extends PollenService> E newService(Class<E> serviceClass) {

        return getServiceContext().newService(serviceClass);
    }

    protected void assertErrorKeyFound(InvalidFormException e, String... keys) {
        Multimap<String, String> errors = e.getErrors();

        Assert.assertEquals("Expected " + keys.length + " errors, but had " + errors.keySet(), keys.length, errors.keySet().size());
        for (String key : keys) {

            Assert.assertTrue("Expected error:" + key + ", but not found among:" + errors.keySet(), errors.containsKey(key));
        }
    }

    protected void login(String login, String password) throws PollenInvalidSessionTokenException,
            PollenAuthenticationException, PollenEmailNotValidatedException, PollenUserBannedException {

        SecurityService securityService = newService(SecurityService.class);

        securityService.login(login, password, null);
        
    }

    protected void logout() {

        SecurityService securityService = newService(SecurityService.class);
        securityService.logout();

    }

}
