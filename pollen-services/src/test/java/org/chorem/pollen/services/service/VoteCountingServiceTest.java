package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.time.DateUtils;
import org.chorem.pollen.persistence.entity.ChoiceType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.services.AbstractPollenServiceTest;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PollBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.QuestionBean;
import org.chorem.pollen.services.bean.VoteBean;
import org.chorem.pollen.services.bean.VoteCountingResultBean;
import org.chorem.pollen.services.bean.VoteToChoiceBean;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 5/22/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoteCountingServiceTest extends AbstractPollenServiceTest {

    private PollService pollService;

    private QuestionService questionService;

    private ChoiceService choiceService;

    private VoteService voteService;

    private VoteCountingService service;

    @Before
    public void setUp() throws Exception {

        loadFixtures("user-fixtures");

        loadFixtures("fixtures");

        pollService = newService(PollService.class);
        questionService = newService(QuestionService.class);
        voteService = newService(VoteService.class);
        choiceService = newService(ChoiceService.class);
        service = newService(VoteCountingService.class);

        getServiceContext().setDate(new Date(1363948427576L));

    }

    @Test
    public void simplePoll() throws InvalidFormException {

        PollBean poll = pollService.getNewPoll(ChoiceType.TEXT);

        poll.setBeginDate(DateUtils.addMinutes(serviceContext.getNow(), -1));
        poll.setTitle("poll1");
        poll.setContinuousResults(true);

        List<QuestionBean> questions = new ArrayList<>();

        QuestionBean question1 = new QuestionBean();
        questions.add(question1);
        question1.setQuestionOrder(1);
        question1.setTitle("question1");
        question1.setVoteCountingType(1);

        List<ChoiceBean> choices = new ArrayList<>();

        ChoiceBean choice1 = new ChoiceBean();
        choices.add(choice1);
        choice1.setChoiceType(ChoiceType.TEXT);
        choice1.setChoiceValue("A");
        choice1.setDescription("Choice A");


        ChoiceBean choice2 = new ChoiceBean();
        choice2.setChoiceType(ChoiceType.TEXT);
        choice2.setChoiceValue("B");
        choice2.setDescription("Choice B");
        choices.add(choice2);

        question1.setChoices(choices);
        poll.setQuestions(questions);

        PollenEntityRef<Poll> createdPollRef = pollService.createPoll(poll);

        String pollId = createdPollRef.getEntityId();

        List<QuestionBean> createdQuestions = questionService.getQuestions(pollId);

        String questionId = createdQuestions.get(0).getEntityId();

        List<ChoiceBean> createdChoices = choiceService.getChoices(questionId);
        ChoiceBean createdChoiceBean1 = createdChoices.get(0);
        ChoiceBean createdChoiceBean2 = createdChoices.get(1);

        // vote 1 (A = 1)
        VoteBean vote1 = new VoteBean();
        vote1.setVoterName("voter1");
        VoteToChoiceBean vote1Choice1 = new VoteToChoiceBean();

        vote1Choice1.getChoiceId().setEntityId(createdChoiceBean1.getEntityId());
        vote1Choice1.setVoteValue(1d);
        vote1.addChoice(vote1Choice1);
        voteService.addVote(pollId, questionId, vote1);

        // vote 2 (B = 1)
        VoteBean vote2 = new VoteBean();
        vote2.setVoterName("voter2");
        VoteToChoiceBean vote2Choice2 = new VoteToChoiceBean();
        vote2Choice2.getChoiceId().setEntityId(createdChoiceBean2.getEntityId());
        vote2Choice2.setVoteValue(1d);
        vote2.addChoice(vote2Choice2);
        voteService.addVote(pollId, questionId, vote2);

        // vote 3 (A = 1)
        VoteBean vote3 = new VoteBean();
        vote3.setVoterName("voter3");
        VoteToChoiceBean vote3Choice1 = new VoteToChoiceBean();
        vote3Choice1.getChoiceId().setEntityId(createdChoiceBean1.getEntityId());
        vote3Choice1.setVoteValue(1d);
        vote3.addChoice(vote3Choice1);
        voteService.addVote(pollId, questionId, vote3);

        VoteCountingResultBean mainResult = service.getMainResult(questionId);
        Assert.assertNotNull(mainResult);
        Assert.assertNotNull(mainResult.getScores());
        Assert.assertEquals(2, mainResult.getScores().size());
//        Assert.assertNotNull(mainResult.getTopRanking());
//        Assert.assertEquals(1, mainResult.getTopRanking().size());
//
//        ChoiceScore choiceScore = mainResult.getTopRanking().get(0);
//        Assert.assertEquals(createdChoiceBean1.getEntityId(), choiceScore.getChoiceId());
//        Assert.assertEquals(2, choiceScore.getScoreValue().intValue());

    }

}
