package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.*;
import org.chorem.pollen.services.AbstractPollenServiceTest;
import org.chorem.pollen.services.bean.*;
import org.chorem.pollen.services.service.security.PollenInvalidPermissionException;
import org.chorem.pollen.services.service.security.SecurityService;
import org.chorem.pollen.services.test.FakePollenSecurityContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

public class VoteServiceTest extends AbstractPollenServiceTest {

    protected VoteService service;

    protected VoterListService voterListService;

    protected PollService pollService;

    protected SecurityService securityService;

    protected PaginationParameterBean pagination = PaginationParameterBean.of(0,-1);

    @Before
    public void setUp() throws Exception {

        loadFixtures("user-fixtures");

        login("jean@pollen.org", "fake");

        loadFixtures("fixtures");

        loadFixtures("restricted-fixtures");

        service = newService(VoteService.class);

        voterListService = newService(VoterListService.class);

        pollService = newService(PollService.class);

        securityService = newService(SecurityService.class);

        getServiceContext().setSecurityContext(new FakePollenSecurityContext());
    }

    @Test
    public void testPurgeOldVotes() {
        //TODO should also check that old votes are properly anonymized
        try {
            service.purgeOldVotes();
        } catch (Exception eee) {
            Assert.fail("Exception should not have been thrown");
        }
    }

    @Test
    public void testEditVote() throws Exception {
        PaginationParameterBean pagination = PaginationParameterBean.of(0,-1);
        var calendar = new GregorianCalendar(2014, Calendar.JANUARY, 2);
        getServiceContext().setDate(calendar.getTime());

        login("jean@pollen.org", "fake");

        //Get poll
        PollBean poll = pollService.getPolls(pagination,"").getElements()
                .stream()
                .filter(pollFilter -> pollFilter.getTitle().equals("Sondage normal"))
                .collect(Collectors.toList())
                .get(0);
        String pollId = poll.getEntityId();
        String questionId = poll.getQuestions().get(0).getEntityId();
        PollenEntityId<Choice> choice1Id = poll.getQuestions().get(0).getChoices().get(0).getId();

        //Create one vote
        VoteBean vote = new VoteBean();
        VoteToChoiceBean bean1 = new VoteToChoiceBean();
        bean1.setChoiceId(choice1Id);
        bean1.setVoteValue(1.0);
        vote.addChoice(bean1);
        vote.setVoterName("test1");
        service.addVote(pollId, questionId, vote);

        List<VoteBean> votes = service.getVotes(pollId, questionId, pagination).getElements();
        Assert.assertEquals(1, votes.size());
        Assert.assertEquals("test1", votes.get(0).getVoterName());
        Assert.assertEquals(1, votes.get(0).getChoice().size());

        //Try to edit it without setting ids, we still should have only one vote
        VoteBean vote2 = new VoteBean();
        vote2.setId(votes.get(0).getId());
        VoteToChoiceBean bean2 = new VoteToChoiceBean();
        bean2.setChoiceId(choice1Id);
        bean2.setVoteValue(1.0);
        vote2.addChoice(bean2);
        vote2.setVoterName("test2");
        vote2.setPermission(votes.get(0).getPermission());
        service.editVote(pollId, questionId, vote2);

        List<VoteBean> votes2 = service.getVotes(pollId, questionId, pagination).getElements();
        Assert.assertEquals(1, votes2.size());
        Assert.assertEquals("test2", votes2.get(0).getVoterName());
        Assert.assertEquals(1, votes2.get(0).getChoice().size());

        //Try to edit it setting ids
        VoteBean vote3 = new VoteBean();
        vote3.setId(votes.get(0).getId());
        VoteToChoiceBean bean3 = new VoteToChoiceBean();
        bean3.setChoiceId(choice1Id);
        bean3.setVoteValue(1.0);
        Object[] beans = votes2.get(0).getChoice().toArray();
        bean3.setId(((VoteToChoiceBean)beans[0]).getId());
        vote3.addChoice(bean3);
        vote3.setVoterName("test3");
        vote3.setPermission(votes.get(0).getPermission());
        service.editVote(pollId, questionId, vote3);

        List<VoteBean> votes3 = service.getVotes(pollId, questionId, pagination).getElements();
        Assert.assertEquals(1, votes3.size());
        Assert.assertEquals("test3", votes3.get(0).getVoterName());
        Assert.assertEquals(1, votes3.get(0).getChoice().size());
    }

    @Test
    public void testInvitedVote() throws Exception {
        var calendar = new GregorianCalendar(2014, Calendar.JANUARY, 2);
        getServiceContext().setDate(calendar.getTime());

        login("jean@pollen.org", "fake");

        //Get poll
        PollBean poll = pollService.getPolls(pagination,"").getElements()
                .stream()
                .filter(pollFilter -> pollFilter.getTitle().equals("Sondage restricted"))
                .collect(Collectors.toList())
                .get(0);
        String pollId = poll.getEntityId();
        String questionId = poll.getQuestions().get(0).getEntityId();
        PollenEntityId<Choice> choice1Id = poll.getQuestions().get(0).getChoices().get(0).getId();

        PollenPrincipal userPrincipal = securityService.generatePollenPrincipal();
        userPrincipal.setPollenUser(securityService.getSecurityContext().getPollenUser());

        securityService.getSecurityContext().setMainPrincipal(userPrincipal);

        //Voters
        VoterListBean voterList = new VoterListBean();
        voterList.setWeight(1);
        voterList.setName("voterList1");
        List<VoterListMemberBean> listMember = new ArrayList<>();
        VoterListMemberBean member1 = new VoterListMemberBean();
        member1.setName("member1");
        member1.setEmail("member1@pollen.org");
        member1.setWeight(1);
        listMember.add(member1);
        VoterListMemberBean member2 = new VoterListMemberBean();
        member2.setName("member2");
        member2.setEmail("jean@pollen.org");
        member2.setWeight(1);
        listMember.add(member2);
        VoterListMemberBean member3 = new VoterListMemberBean();
        member3.setName("member3");
        member3.setEmail("member3@pollen.org");
        member3.setWeight(1);
        listMember.add(member3);
        voterList.setCountMembers(3);
        PollenEntityRef<VoterList> voterListId = voterListService.addVoterList(pollId, voterList, listMember);

        VoterList list = voterListService.getVoterList0(poll.getEntityId(), voterListId.getEntityId());
        List<VoterListMember> members = voterListService.getVoterListMembers0(list.getTopiaId());
        PollenPrincipal member1Token = null;
        for (VoterListMember member:members) {
            if (member.getMember().getName().equals(member1.getName())) {
                member1Token = member.getMember();
            }
        }

        //The vote used for all tests
        VoteBean vote = new VoteBean();
        VoteToChoiceBean bean1 = new VoteToChoiceBean();
        bean1.setChoiceId(choice1Id);
        bean1.setVoteValue(1.0);
        vote.addChoice(bean1);
        vote.setVoterName("test1");

        //Cannot vote as wrongly logged in user
        login("tony@pollen.org", "fake");
        try {
            vote.setVoterName("tony@pollen.org");
            service.addVote(pollId, questionId, vote);
            Assert.fail("Exception should have been thrown");
        } catch (PollenInvalidPermissionException eee) {
            //Normal exception thrown
        }

        //should be able to vote with token
        securityService.getSecurityContext().setMainPrincipal(member1Token);
        vote.setVoterName("member1@pollen.org");
        vote.setPermission(member1Token.getPermission().getToken());
        service.addVote(pollId, questionId, vote);

        //cannot vote twice with token
        try {
            service.addVote(pollId, questionId, vote);
            Assert.fail("Exception should have been thrown");
        } catch (PollenInvalidPermissionException eee) {
            //Normal exception thrown
        }

        //should be able to vote as logged in user
        login("jean@pollen.org", "fake");
        securityService.getSecurityContext().setMainPrincipal(userPrincipal);
        vote.setPermission(null);
        vote.setVoterName("jean@pollen.org");
        service.addVote(pollId, questionId, vote);

        //cannot vote twice as logged in user
        try {
            service.addVote(pollId, questionId, vote);
            Assert.fail("Exception should have been thrown");
        } catch (PollenInvalidPermissionException eee) {
            //Normal exception thrown
        }

        //properly listed as voted in voterslist
        PaginationResultBean<VoterListMemberBean> voters = voterListService.getAllVoterListMembers(poll.getEntityId(), pagination);
        for (VoterListMemberBean voter:voters.getElements()) {
            if (voter.getName().equals("jean@pollen.org") || voter.getName().equals("member1@pollen.org")) {
                Assert.assertTrue(voter.isVoting());
            } else {
                Assert.assertFalse(voter.isVoting());
            }
        }

    }
}
