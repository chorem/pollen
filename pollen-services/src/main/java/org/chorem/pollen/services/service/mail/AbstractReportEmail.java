package org.chorem.pollen.services.service.mail;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Report;
import org.chorem.pollen.services.bean.ReportLevel;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class AbstractReportEmail<T extends TopiaEntity>  extends PollenMail {

    protected Poll poll;

    protected T target;

    protected Report report;

    protected String url;

    public AbstractReportEmail(Locale locale, TimeZone timeZone) {
        super(locale, timeZone);
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public T getTarget() {
        return target;
    }

    public void setTarget(T target) {
        this.target = target;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public boolean isIllegal() {
        return ReportLevel.ILLEGAL.getScore() == report.getLevel();
    }

    public boolean isSpam() {
        return ReportLevel.SPAM.getScore() == report.getLevel();
    }

    public boolean isOffTopic() {
        return ReportLevel.OFF_TOPIC.getScore() == report.getLevel();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public List<TopiaEntity> getEntitiesKey() {
        return Lists.newArrayList(poll, target, report);
    }
}
