package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.google.common.base.Preconditions;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.bean.CommentBean;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.ReportResumeBean;
import org.chorem.pollen.services.service.security.PollenPermissions;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import static org.nuiton.i18n.I18n.l;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class CommentService extends PollenServiceSupport {

    protected CommentBean toCommentBean(Comment entity) {

        CommentBean bean = new CommentBean();
        bean.setEntityId(entity.getTopiaId());

        PollenPrincipal author = entity.getAuthor();
        if (author == null
            || author.getPermission() == null
            || isNotPermitted(PollenPermissions.edit(entity))) {

            bean.setPermission(null);

        } else {

            bean.setPermission(author.getPermission().getToken());

        }

        bean.setText(entity.getText());
        bean.setPostDate(entity.getPostDate());

        if (author != null) {

            bean.setAuthorName(author.getName());

            if (author.getPollenUser() != null && author.getPollenUser().getAvatar() != null) {
                bean.setAuthorAvatar(getPollenResourceService().getReduceIdByTopiaId(author.getPollenUser().getAvatar().getTopiaId()));
            }

        }

        if (isPermitted(PollenPermissions.edit(entity))) {

            ReportResumeBean report = getReportService().getReport(entity.getTopiaId());
            bean.setReport(report);

        }
        return bean;
    };

    public PaginationResultBean<CommentBean> getComments(String pollId, PaginationParameterBean paginationParameter) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readComments(poll));

        PaginationParameter page = getPaginationParameter(paginationParameter);

        PaginationResult<Comment> comments = getCommentDao().find(poll, page);

        return toPaginationListBean(comments, this::toCommentBean);

    }

    public PaginationResultBean<CommentBean> getQuestionComments(String pollId, String questionId, PaginationParameterBean paginationParameter) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);

        Poll poll = getPollService().getPoll0(pollId);
        //FIXME JC181005 - APRIL - Pertinent ?
        checkPermission(PollenPermissions.readComments(poll));

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.readComments(question));

        PaginationParameter page = getPaginationParameter(paginationParameter);

        PaginationResult<Comment> comments = getCommentDao().find(question, page);

        return toPaginationListBean(comments, this::toCommentBean);

    }

    public long getCommentCount(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readComments(poll));

        return getCommentDao().forPollEquals(poll).count();
    }

    public long getQuestionCommentCount(String questionId) {
        checkIsConnectedRequired();

        checkNotNull(questionId);

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.readComments(question));

        return getCommentDao().forQuestionEquals(question).count();
    }

    public CommentBean getNewComment(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.addComment(poll));


        CommentBean commentBean = new CommentBean();
        PollenPrincipal mainPrincipal = getSecurityContext().getMainPrincipal();
        PollenUser connectedUser = getConnectedUser();

        if (mainPrincipal != null) {

            commentBean.setAuthorName(mainPrincipal.getName());

        } else if (connectedUser != null) {

            commentBean.setAuthorName(connectedUser.getName());
        }

        return commentBean;

    }

    public CommentBean getNewQuestionComment(String pollId, String questionId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.addComment(poll));

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.addComment(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        CommentBean commentBean = new CommentBean();
        PollenPrincipal mainPrincipal = getSecurityContext().getMainPrincipal();
        PollenUser connectedUser = getConnectedUser();

        if (mainPrincipal != null) {

            commentBean.setAuthorName(mainPrincipal.getName());

        } else if (connectedUser != null) {

            commentBean.setAuthorName(connectedUser.getName());
        }

        return commentBean;
    }

    public CommentBean getComment(String pollId, String commentId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(commentId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readComments(poll));

        Comment comment = getComment(poll, commentId);

        return toCommentBean(comment);

    }

    public CommentBean getQuestionComment(String pollId, String questionId, String commentId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(commentId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readComments(poll));

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.addComment(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Comment comment = getComment(question, commentId);

        return toCommentBean(comment);
    }

    public PollenEntityRef<Comment> addComment(String pollId, CommentBean comment) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(comment);
        checkIsNotPersisted(comment);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.addComment(poll));

        ErrorMap errorMap = checkComment(comment);
        errorMap.failIfNotEmpty();

        Comment result = saveComment(poll, comment);
        commit();

        getNotificationService().onCommentAdded(poll, result);
        getFeedService().onCommentAdded(poll, result);

        return PollenEntityRef.of(result);

    }

    public PollenEntityRef<Comment> addQuestionComment(String pollId, String questionId, CommentBean comment) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(comment);
        checkIsNotPersisted(comment);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.addComment(poll));

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.addComment(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        ErrorMap errorMap = checkComment(comment);
        errorMap.failIfNotEmpty();

        Comment result = saveComment(question, comment);
        commit();

        getNotificationService().onCommentAdded(question, result);
        getFeedService().onCommentAdded(question, result);

        return PollenEntityRef.of(result);
    }

    public CommentBean editComment(String pollId, CommentBean comment) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(comment);
        checkIsPersisted(comment);

        Poll poll = getPollService().getPoll0(pollId);

        Comment commentBd = getComment(poll, comment.getEntityId());
        checkPermission(PollenPermissions.edit(commentBd));

        ErrorMap errorMap = checkComment(comment);
        errorMap.failIfNotEmpty();
        Comment result = saveComment(poll, comment);
        commit();

        getNotificationService().onCommentEdited(poll, result);
        getFeedService().onCommentEdited(poll, result);

        return toCommentBean(result);

    }

    public CommentBean editQuestionComment(String pollId, String questionId, CommentBean comment) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(comment);
        checkIsPersisted(comment);

        Poll poll = getPollService().getPoll0(pollId);

        Question question = getQuestionService().getQuestion0(questionId);

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Comment commentBd = getComment(poll, comment.getEntityId());
        checkPermission(PollenPermissions.edit(commentBd));

        ErrorMap errorMap = checkComment(comment);
        errorMap.failIfNotEmpty();
        Comment result = saveComment(poll, comment);
        commit();

        getNotificationService().onCommentEdited(question, result);
        getFeedService().onCommentEdited(question, result);

        return toCommentBean(result);
    }

    public void deleteComment(String pollId, String commentId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(commentId);

        Poll poll = getPollService().getPoll0(pollId);

        Comment comment = getComment(poll, commentId);

        checkPermission(PollenPermissions.delete(comment));

        getCommentDao().delete(comment);
        commit();

        getNotificationService().onCommentDeleted(poll, comment);
        getFeedService().onCommentDeleted(poll, comment);

    }

    public void deleteQuestionComment(String pollId, String questionId, String commentId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(commentId);

        Poll poll = getPollService().getPoll0(pollId);

        Question question = getQuestionService().getQuestion0(questionId);

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Comment comment = getComment(question, commentId);

        checkPermission(PollenPermissions.delete(comment));

        getCommentDao().delete(comment);
        commit();

        getNotificationService().onCommentDeleted(poll, comment);
        getFeedService().onCommentDeleted(poll, comment);
    }

    protected Comment getComment(Poll poll, String commentId) {

        Comment result = getCommentDao().forTopiaIdEquals(commentId).findUnique();

        if (!poll.equals(result.getPoll())) {

            throw new InvalidEntityLinkException(Comment.PROPERTY_POLL, result, poll);

        }

        return result;
    }

    protected Comment getComment(Question question, String commentId) {

        Comment result = getCommentDao().forTopiaIdEquals(commentId).findUnique();

        if (!question.equals(result.getQuestion())) {
            throw new InvalidEntityLinkException(Comment.PROPERTY_QUESTION, result, question);
        }

        return result;
    }

    protected ErrorMap checkComment(CommentBean comment) {

        ErrorMap errors = new ErrorMap();

        checkNotBlank(errors, "text", comment.getText(), l(getLocale(), "pollen.error.comment.text.mandatory"));
        checkNotBlank(errors, "author.name", comment.getAuthorName(), l(getLocale(), "pollen.error.comment.author.name.mandatory"));

        return errors;

    }

    protected Comment saveComment(Poll poll, CommentBean comment) {

        boolean commentExists = comment.isPersisted();

        Comment toSave;

        if (commentExists) {

            toSave = getCommentDao().forTopiaIdEquals(comment.getEntityId()).findUnique();

        } else {

            toSave = getCommentDao().create();
            toSave.setPostDate(serviceContext.getNow());

            // -- author -- //

            PollenPrincipal author = getSecurityService().generatePollenPrincipal();
            toSave.setAuthor(author);
            toSave.setPoll(poll);

        }

        toSave.setText(comment.getText());

        // -- author -- //

        PollenUser connectedUser = getConnectedUser();

        if (connectedUser != null) {

            // link to connected user
            toSave.getAuthor().setPollenUser(connectedUser);

        }

        toSave.getAuthor().setName(comment.getAuthorName());

        return toSave;

    }

    protected Comment saveComment(Question question, CommentBean comment) {

        boolean commentExists = comment.isPersisted();

        Comment toSave;

        if (commentExists) {

            toSave = getCommentDao().forTopiaIdEquals(comment.getEntityId()).findUnique();

        } else {

            toSave = getCommentDao().create();
            toSave.setPostDate(serviceContext.getNow());

            // -- author -- //

            PollenPrincipal author = getSecurityService().generatePollenPrincipal();
            toSave.setAuthor(author);
            toSave.setQuestion(question);

        }

        toSave.setText(comment.getText());

        // -- author -- //

        PollenUser connectedUser = getConnectedUser();

        if (connectedUser != null) {

            // link to connected user
            toSave.getAuthor().setPollenUser(connectedUser);

        }

        toSave.getAuthor().setName(comment.getAuthorName());

        return toSave;
    }

}
