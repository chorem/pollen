package org.chorem.pollen.services.bean;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.ReportResume;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ReportResumeBean {

    public static ReportResumeBean of(ReportResume report) {
        ReportResumeBean reportBean = null;

        if (report.getCount() > 0) {

            reportBean = new ReportResumeBean();

            reportBean.setScore(report.getScore());
            reportBean.setCount(report.getCount());
            reportBean.setIgnore(report.getIgnore());
        }
        return reportBean;
    }

    protected long score;

    protected long count;

    protected long ignore;

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getIgnore() {
        return ignore;
    }

    public void setIgnore(long ignore) {
        this.ignore = ignore;
    }
}
