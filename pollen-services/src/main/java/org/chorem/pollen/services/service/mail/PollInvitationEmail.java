package org.chorem.pollen.services.service.mail;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.PollenTechnicalException;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaEntity;

import jakarta.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created on 4/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollInvitationEmail extends PollenMail {

    protected Poll poll;
    protected List<Question> questions;
    private String voteUrl;

    protected PollInvitationEmail(Locale locale, TimeZone timeZone) {
        super(locale, timeZone);
    }

    @Override
    public String getSubject() {
        return I18n.l(locale, "pollen.service.mail.RestrictedPollInvitationEmail.subject", poll.getTitle());
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public void setVoteUrl(String voteUrl) {
        this.voteUrl = voteUrl;
    }

    public String getVoteUrl() {
        return voteUrl;
    }

    public String getCreationDate() {
        return formatDate(poll.getTopiaCreateDate());
    }

    public String getBeginDate() {
        return formatDate(poll.getBeginDate());
    }

    public String getEndDate() {
        return formatDate(poll.getEndDate());
    }

    public String getBeginChoiceDate() {
        //FIXME JC181004 - APRIL - Prendre en compte les questions
        return formatDate(questions.get(0).getBeginChoiceDate());
    }

    public String getEndChoiceDate() {
        //FIXME JC181004 - APRIL - Prendre en compte les questions
        return formatDate(questions.get(0).getEndChoiceDate());
    }

    @Override
    public List<TopiaEntity> getEntitiesKey() {
        return Collections.singletonList(poll);
    }

    @Override
    public String getFromName() {
        return poll.getCreator().getName();
    }

    @Override
    public Collection<InternetAddress> getReplyTo() {
        Collection<InternetAddress> replyTo = Collections.emptyList();
        PollenPrincipal creator = getPoll().getCreator();
        if (StringUtils.isNotBlank(creator.getEmail())) {
            try {
                replyTo = Collections.singletonList(new InternetAddress(creator.getEmail(), creator.getName()));
            } catch (UnsupportedEncodingException e) {
                throw new PollenTechnicalException("Error on replyTo adress", e);
            }
        }

        return replyTo;
    }
}
