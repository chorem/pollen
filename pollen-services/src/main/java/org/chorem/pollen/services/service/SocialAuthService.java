package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.Constants;
import org.brickred.socialauth.util.OAuthConfig;
import org.chorem.pollen.persistence.entity.LoginProvider;
import org.chorem.pollen.persistence.entity.LoginProviderTopiaDao;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.chorem.pollen.persistence.entity.PollenUserImpl;
import org.chorem.pollen.persistence.entity.PollenUserTopiaDao;
import org.chorem.pollen.persistence.entity.ResourceType;
import org.chorem.pollen.persistence.entity.UserCredential;
import org.chorem.pollen.persistence.entity.UserCredentialImpl;
import org.chorem.pollen.services.bean.LoginProviderBean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.resource.ResourceStreamBean;
import org.chorem.pollen.services.service.security.PollenEmailOrProviderAccountAlreadyUsedException;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class SocialAuthService extends PollenServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SocialAuthService.class);

    public PollenEntityRef<PollenUser> login(SocialAuthManager manager,
                                             Map<String, String> paramsMap) throws Exception {

        AuthProvider provider = manager.connect(paramsMap);

        // get profile
        Profile p = provider.getUserProfile();

        PollenUser pollenUser;
        PollenUserTopiaDao userDao = getPollenUserDao();
        PollenUser pollenUserForCredential = userDao.findUserWithCredentialOrNull(p.getProviderId(), p.getValidatedId());

        if (pollenUserForCredential != null) {
            if (log.isDebugEnabled()) {
                log.debug("credentials found");
            }
            pollenUser = pollenUserForCredential;

        } else if (getPollenUserEmailAddressDao().emailExists(p.getEmail())) {
            throw new PollenEmailOrProviderAccountAlreadyUsedException();

        } else {
            String name = p.getFullName();
            if (name == null) {
                name = p.getFirstName() + " " + p.getLastName();
            }

            if (log.isDebugEnabled()) {
                log.debug("create new user : " + name);
            }

            UserCredential credential = createUserCredential(p);

            pollenUser = new PollenUserImpl();
            pollenUser.setName(name);

            String credentialEmail = getCleanMail(credential.getEmail());
            if (StringUtils.isNotBlank(credentialEmail)) {
                PollenUserEmailAddress defaultEmailAddress = getPollenUserEmailAddressDao().create();
                defaultEmailAddress.setEmailAddress(credentialEmail);
                pollenUser.addEmailAddresses(defaultEmailAddress);
                pollenUser.setDefaultEmailAddress(defaultEmailAddress);
            }

            pollenUser.setLanguage(p.getLanguage());
            pollenUser.setAdministrator(false);
            pollenUser.setBanned(false);
            pollenUser.addUserCredential(credential);
            pollenUser = userDao.create(pollenUser);

            commit();

            getNotificationService().onUserCreatedFromProvider(pollenUser, credential);
        }

        this.getSecurityContext().setPollenUser(pollenUser);
        return PollenEntityRef.of(pollenUser);
    }

    public String addCredentialToUser(SocialAuthManager manager,
                                      Map<String, String> paramsMap) throws Exception {

        PollenUser connectedUser = checkAndGetConnectedUser();
        AuthProvider provider = manager.connect(paramsMap);

        // get profile
        Profile p = provider.getUserProfile();

        String credentialEmail = getCleanMail(p.getEmail());
        boolean credentialValid = getUserCredentialDao().isCredentialValid(p.getProviderId(),
                                                                           p.getValidatedId(),
                                                                           connectedUser.getTopiaId(),
                                                                           credentialEmail);
        if (!credentialValid) {
            throw new PollenEmailOrProviderAccountAlreadyUsedException();
        }

        UserCredential credential = createUserCredential(p);
        connectedUser.addUserCredential(credential);

        if (StringUtils.isNotBlank(credential.getEmail())) {
            boolean addEmailAddress = connectedUser.getEmailAddresses().stream()
                    .map(PollenUserEmailAddress::getEmailAddress)
                    .noneMatch(email -> Objects.equals(credentialEmail, email));
            if (addEmailAddress) {
                PollenUserEmailAddress emailAddress = getPollenUserEmailAddressDao().create();
                emailAddress.setEmailAddress(credentialEmail);
                connectedUser.addEmailAddresses(emailAddress);
            }
        }

        commit();

        return credential.getUserName();
    }

    protected UserCredential createUserCredential(Profile p) {
        String accountName = p.getDisplayName();
        if (accountName == null) {
            accountName = p.getFullName();
        }
        String userEmail = null;
        if (StringUtils.isNotBlank(p.getEmail()) && !"null".equalsIgnoreCase(p.getEmail())) {
            userEmail = p.getEmail();
        }

        UserCredential credential = new UserCredentialImpl();
        credential.setProvider(p.getProviderId());
        credential.setUserId(p.getValidatedId());
        credential.setUserName(accountName);
        credential.setEmail(userEmail);
        credential = getUserCredentialDao().create(credential);
        return credential;
    }

    public SocialAuthManager getSocialAuthManager() throws Exception {
        checkIsConnectedRequired();
        //Create an instance of SocialAuthConfgi object
        SocialAuthConfig config = SocialAuthConfig.getDefault();

        List<LoginProvider> allActiveProviders = getLoginProviderDao().forActiveEquals(true).findAll();
        allActiveProviders.forEach(provider -> {
            try {
                OAuthConfig providerConfig = new OAuthConfig(provider.getKey(), provider.getSecret());
                if (StringUtils.isNotBlank(provider.getPermissions())) {
                    providerConfig.setCustomPermissions(provider.getPermissions());
                }
                config.addProviderConfig(provider.getName(), providerConfig);

            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Error while adding config for provider " + provider.getName(), e);
                }
            }
        });

        //load configuration. By default load the configuration from oauth_consumer.properties.
        //You can also pass input stream, properties object or properties file name.
        config.load();

        //Create an instance of SocialAuthManager and set config
        SocialAuthManager manager = new SocialAuthManager();
        manager.setSocialAuthConfig(config);

        return manager;
    }

    public LoginProviderBean toLoginProviderBean(LoginProvider entity) {
        LoginProviderBean bean = new LoginProviderBean();

        bean.setEntityId(entity.getTopiaId());
        bean.setName(entity.getName());
        bean.setKey(entity.getKey());
        bean.setSecret(entity.getSecret());
        bean.setPermissions(entity.getPermissions());
        bean.setActive(entity.isActive());

        return bean;
    }

    public List<LoginProviderBean> getAllLoginProviders() {
        checkIsAdmin();
        LoginProviderTopiaDao dao = getLoginProviderDao();
        List<LoginProvider> loginProviders = dao.findAll();
        return toBeanList(loginProviders, this::toLoginProviderBean);
    }

    public List<String> getActiveLoginProviders() {
        LoginProviderTopiaDao dao = getLoginProviderDao();
        return dao.forActiveEquals(true).findAll().stream()
                .map(LoginProvider::getName)
                .collect(Collectors.toList());
    }

    public List<String> getAvailableLoginProviders() {
        checkIsAdmin();
        return Lists.newArrayList(
            Constants.AMAZON,
            Constants.FACEBOOK,
            Constants.FLICKR,
            Constants.FOURSQUARE,
            Constants.GITHUB,
            Constants.GOOGLE_PLUS,
            Constants.HOTMAIL,
            Constants.INSTAGRAM,
            Constants.LINKEDIN,
            Constants.LINKEDINOAUTH2,
            Constants.MENDELEY,
            Constants.MYSPACE,
            Constants.NIMBLE,
            Constants.RUNKEEPER,
            Constants.SALESFORCE,
            Constants.STACK_EXCHANGE,
            Constants.TWITTER,
            Constants.YAHOO,
            Constants.YAMMER
        );
    }

    public LoginProviderBean saveLoginProvider(LoginProviderBean loginProvider, boolean loginProviderExists) {
        checkIsAdmin();
        checkNotNull(loginProvider);

        LoginProviderTopiaDao dao = getLoginProviderDao();
        LoginProvider toSave;
        if (loginProviderExists) {
            toSave = dao.forTopiaIdEquals(loginProvider.getEntityId()).findUnique();
        } else {
            toSave = dao.create();
        }
        toSave.setName(loginProvider.getName());
        toSave.setKey(loginProvider.getKey());
        toSave.setSecret(loginProvider.getSecret());
        toSave.setPermissions(loginProvider.getPermissions());
        toSave.setActive(loginProvider.isActive());

        commit();
        return toLoginProviderBean(toSave);
    }

    public void deleteLoginProvider(String providerId) {
        checkIsAdmin();
        checkNotNull(providerId);
        LoginProviderTopiaDao dao = getLoginProviderDao();
        dao.delete(dao.forTopiaIdEquals(providerId).findUnique());
        commit();
    }

    public void deleteUserCredential(PollenEntityId<UserCredential> credentialId) {
        checkNotNull(credentialId);
        PollenUser pollenUser = checkAndGetConnectedUser();
        UserCredential credential = pollenUser.getUserCredentialByTopiaId(credentialId.getEntityId());
        getUserCredentialDao().delete(credential);
        commit();
    }

    public void setAvatarToUser(SocialAuthManager manager, Map<String, String> paramsMap) throws Exception {
        PollenUser connectedUser = checkAndGetConnectedUser();
        PollenResourceService pollenResourceService = getPollenResourceService();

        if (connectedUser.getAvatar() != null) {
            getPollenResourceDao().delete(connectedUser.getAvatar());
        }

        // get profile
        AuthProvider provider = manager.connect(paramsMap);
        Profile p = provider.getUserProfile();
        String avatarUrl = p.getProfileImageURL();
        URLConnection connection = new URL(avatarUrl).openConnection();
        String contentType = connection.getContentType();
        ResourceStreamBean resourceStreamBean  = new ResourceStreamBean();
        resourceStreamBean.setResourceContent(connection.getInputStream());
        resourceStreamBean.setName(connectedUser.getTopiaId());
        resourceStreamBean.setContentType(contentType);
        resourceStreamBean.setSize(connection.getContentLength());
        resourceStreamBean.setResourceType(ResourceType.AVATAR);
        PollenResource avatarResource = pollenResourceService.createAvatarResource(resourceStreamBean);
//TODO check error in avatar downloading
        connectedUser.setAvatar(avatarResource);
        commit();
    }
}
