package org.chorem.pollen.services.test;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.StandardSystemProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.PollenTopiaApplicationContext;
import org.chorem.pollen.persistence.PollenTopiaPersistenceContext;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.PollenApplicationContext;
import org.chorem.pollen.services.PollenFixtures;
import org.chorem.pollen.services.PollenServiceContext;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.config.PollenServicesConfigOption;
import org.chorem.pollen.services.service.FixturesService;
import org.chorem.pollen.votecounting.VoteCountingFactory;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.util.DateUtil;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class FakePollenApplicationContext extends TestWatcher implements PollenApplicationContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FakePollenApplicationContext.class);

    protected static final AtomicInteger portNumberCounter = new AtomicInteger(9999);

    protected File testBasedir;

    protected final List<PollenTopiaPersistenceContext> openedTransactions = new LinkedList<>();

    protected PollenTopiaApplicationContext applicationContext;

    protected PollenServicesConfig configuration;

    protected List<PollenFixtures> fixtures;

    protected VoteCountingFactory voteCountingFactory;

    protected String methodName;

    protected int currentPortNumber;

    protected final String configurationPath;

    protected static final String TIMESTAMP = String.valueOf(System.nanoTime());

    public FakePollenApplicationContext(String configurationPath) {
        this.configurationPath = configurationPath;
    }

    @Override
    protected void starting(Description description) {

        // get an available port
        currentPortNumber = getAvailablePort();
        if (log.isDebugEnabled()) {
            log.debug("Using port: " + currentPortNumber);
        }

        methodName = description.getMethodName();

        String javaIoTmpDir = StandardSystemProperty.JAVA_IO_TMPDIR.value();
        Preconditions.checkState(
                StringUtils.isNotBlank(javaIoTmpDir),
                "'" + StandardSystemProperty.JAVA_IO_TMPDIR.key() + "' is not defined in environment"
        );
        File tempDirectoryFile = new File(javaIoTmpDir);
        FileUtil.createDirectoryIfNecessary(tempDirectoryFile);

        String path = Joiner.on(File.separator).join(TIMESTAMP, description.getTestClass().getName(), description.getMethodName(), "h2");

        // get test directory
        testBasedir = new File(tempDirectoryFile, path);
//        testBasedir = ConfigurationHelper.getTestSpecificDirectory(
//                description.getTestClass(),
//                description.getMethodName());

        if (log.isDebugEnabled()) {
            log.debug("testBasedir: " + testBasedir);
        }

        init();

    }

    @Override
    public void finished(Description description) {

        close();
    }

    @Override
    public void init() {

        // --- create configuration --- //

        Properties defaultvalues = new Properties();
        defaultvalues.put(PollenServicesConfigOption.DATA_DIRECTORY.getKey(), testBasedir.getAbsolutePath());

        configuration = new PollenServicesConfig(configurationPath, defaultvalues);

        // --- create topia application context --- //

        Map<String, String> topiaProperties = configuration.getTopiaProperties();
        BeanTopiaConfiguration topiaConfiguration = new TopiaConfigurationBuilder().readMap(topiaProperties);
        applicationContext = new PollenTopiaApplicationContext(topiaConfiguration);

    }

    @Override
    public void close() {

        if (applicationContext != null && !applicationContext.isClosed()) {

            for (PollenTopiaPersistenceContext openedTransaction : openedTransactions) {

                if (log.isTraceEnabled()) {
                    log.trace("closing transaction " + openedTransaction);
                }

                if (!openedTransaction.isClosed()) {

                    openedTransaction.close();

                }

            }

            if (log.isTraceEnabled()) {
                log.trace("closing transaction " + applicationContext);
            }

            applicationContext.close();

        }
    }

    @Override
    public PollenTopiaApplicationContext getTopiaApplicationContext() {
        return applicationContext;
    }

    @Override
    public PollenServicesConfig getApplicationConfig() {
        return configuration;
    }

    @Override
    public VoteCountingFactory getVoteCountingFactory() {

        if (voteCountingFactory == null) {
            voteCountingFactory = new VoteCountingFactory();
        }

        return voteCountingFactory;

    }

    @Override
    public PollenTopiaPersistenceContext newPersistenceContext() {

        PollenTopiaPersistenceContext persistenceContext;

        persistenceContext = applicationContext.newPersistenceContext();

        if (log.isTraceEnabled()) {
            log.trace("opened transaction " + persistenceContext);
        }

        openedTransactions.add(persistenceContext);

        return persistenceContext;

    }

    @Override
    public FakePollenServiceContext newServiceContext(PollenPersistenceContext persistenceContext, Locale locale) {

        return FakePollenServiceContext.newServiceContext(
                DateUtil.createDate(1, 1, 2014),
                Locale.FRANCE,
                getApplicationConfig(),
                getTopiaApplicationContext(),
                newPersistenceContext(),
                getVoteCountingFactory());

    }

    @Override
    public FakePollenSecurityContext newSecurityContext(PollenUser pollenUser, PollenPrincipal mainPrincipal) {

        FakePollenSecurityContext securityContext = new FakePollenSecurityContext();
        securityContext.setMainPrincipal(mainPrincipal);
        securityContext.setPollenUser(pollenUser);
        return securityContext;

    }

    public File getTestBasedir() {
        return testBasedir;
    }

    public int getPort() {
        return currentPortNumber;
    }

    public String getMethodName() {
        return methodName;
    }

    public void loadFixtures(PollenServiceContext serviceContext, String fixturesSetName) {

        FixturesService fixturesService = serviceContext.newService(FixturesService.class);
        if (fixtures == null) {
            fixtures = new ArrayList<>();
        }

        fixtures.add(fixturesService.loadFixtures(fixturesSetName));

    }

    public <E> E fixture(String id) {
        for (PollenFixtures fixture: fixtures) {
            if (fixture.fixture(id) != null) {
                return fixture.fixture(id);
            }
        }

        return null;

    }

    protected int getAvailablePort() {

        int port = portNumberCounter.getAndIncrement();

        boolean portTaken = false;
        ServerSocket socket = null;

        try {

            socket = new ServerSocket(port);

        } catch (IOException e) {

            portTaken = true;

        } finally {

            if (socket != null)

                try {

                    socket.close();

                } catch (IOException e) {

                    if (log.isDebugEnabled()) {
                        log.debug("Already used port: " + port);
                    }

                }

        }

        if (portTaken) {

            port = getAvailablePort();

        }

        return port;

    }

}
