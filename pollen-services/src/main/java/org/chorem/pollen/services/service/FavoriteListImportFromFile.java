package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.entity.FavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteListMember;
import org.chorem.pollen.services.PollenTechnicalException;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 6/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class FavoriteListImportFromFile extends PollenServiceSupport implements FavoriteListImport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FavoriteListImportFromFile.class);

    protected File file;

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public void doImport(FavoriteList favoriteList, List<FavoriteListMember> existingFavoriteListMembers) throws FavoriteListImportException {

        checkIsConnected();
        checkNotNull(favoriteList);
        checkNotNull(existingFavoriteListMembers);
        checkNotNull(file);
        checkState(file.exists());


        Map<String, String> usedNamesByEmails = new HashMap<>();
        if (CollectionUtils.isNotEmpty(existingFavoriteListMembers)) {
            for (FavoriteListMember member : existingFavoriteListMembers) {
                usedNamesByEmails.put(member.getEmail(), member.getName());
            }
        }

        List<String> lines;
        try {
            lines = Files.readLines(file, Charsets.UTF_8);
        } catch (IOException e) {
            throw new PollenTechnicalException(e);
        }

        List<String> errors = new LinkedList<>();
        List<ImportLine> importLines = new LinkedList<>();

        int lineNumber = 1;
        for (String line : lines) {
            if (StringUtils.isNotBlank(line) && !line.startsWith("#")) {
                String[] columns = line.split(";");

                if (columns.length > 0) {
                    String email = getCleanMail(columns[0]);
                    String memberName = columns.length >= 2 ? columns[1] : email;
                    String weight = columns.length >= 3 ? columns[2] : null;

                    ImportLine importLine = new ImportLine(email, memberName, weight);
                    // if the email and membername match an existing account, do not handle the line
                    if (!memberName.equals(usedNamesByEmails.get(email))) {
                        importLines.add(importLine);
                    }
                    checkLine(errors, importLine, lineNumber, usedNamesByEmails);
                }
            }
            lineNumber++;
        }

        if (CollectionUtils.isNotEmpty(errors)) {
            throw new FavoriteListImportException(errors);
        }

        for (ImportLine importLine : importLines) {
            FavoriteListMember member = getFavoriteListMemberDao().create();
            String memberName = importLine.getMemberName();
            String email = importLine.getEmail();
            String weightString = importLine.getWeightString();
            double weight = StringUtils.isNotBlank(weightString) ? Double.parseDouble(weightString) : 1;

            member.setName(memberName);
            member.setEmail(email);
            member.setWeight(weight);
            member.setFavoriteList(favoriteList);

            if (log.isDebugEnabled()) {
                log.debug(String.format("imported member %s / %s", memberName, email));
            }
        }

        commit();
    }

    protected void checkLine(List<String> errors, ImportLine importLine, int lineNumber, Map<String, String> usedNamesByEmails) {

        Locale locale = serviceContext.getLocale();

        String email = importLine.getEmail();
        if (!StringUtil.isEmail(email)) {
            // email is not valid
            errors.add(l(locale, "pollen.error.favoriteList.import.csv.invalid.email", lineNumber, email));
        }

        String memberName = importLine.getMemberName();
        if (usedNamesByEmails.containsKey(email) && !usedNamesByEmails.containsValue(memberName)) {
            // email already exists but with a different name
            errors.add(l(locale, "pollen.error.favoriteList.import.csv.already.used.email", lineNumber, email));
        } else if (!usedNamesByEmails.containsKey(email) && usedNamesByEmails.containsValue(memberName)) {
            // name already exists but with a different email
            errors.add(l(locale, "pollen.error.favoriteList.import.csv.already.used.name", lineNumber, memberName));
        } else {
            usedNamesByEmails.put(email, memberName);
        }

        String weightString = importLine.getWeightString();
        if (StringUtils.isNotBlank(weightString)) {
            try {
                double weight = Double.parseDouble(weightString);
                if (weight <= 0) {
                    errors.add(l(locale, "pollen.error.favoriteList.import.csv.weight.negativeOrNull", lineNumber, weightString));
                }
            } catch (NumberFormatException e) {
                errors.add(l(locale, "pollen.error.favoriteList.import.csv.weight.notNumber", lineNumber, weightString));
            }
        }
    }

    private static class ImportLine {
        private final String email;
        private final String memberName;
        private final String weightString;

        public ImportLine(String email, String memberName, String weightString) {
            this.email = email;
            this.memberName = memberName;
            this.weightString = weightString;
        }

        public String getEmail() {
            return email;
        }

        public String getMemberName() {
            return memberName;
        }

        public String getWeightString() {
            return weightString;
        }

    }
}
