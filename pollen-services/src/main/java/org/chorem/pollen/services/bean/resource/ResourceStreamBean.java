package org.chorem.pollen.services.bean.resource;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.services.PollenTechnicalException;

import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created on 11/07/14.
 *
 * @author dralagen
 */
public class ResourceStreamBean extends AbstractResourceBean {

    protected InputStream resourceContent;

    public ResourceStreamBean() {
        super(PollenResource.class);
    }

    public InputStream getResourceContent() {
        return resourceContent;
    }

    public void setResourceContent(InputStream resourceContent) {
        this.resourceContent = resourceContent;
    }

    @Override
    public Blob getResourceBlob() {
        try {
            return new SerialBlob(IOUtils.toByteArray(getResourceContent()));
        } catch (SQLException | IOException e) {
            throw new PollenTechnicalException(e);
        }
    }
}
