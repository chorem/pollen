package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Created on 25/08/14.
 *
 * @author dralagen
 */
public class PollenUIUrlRenderService extends PollenServiceSupport {

    public String getPollEditUrl(String pollEditUrl, String pollId, String token) {
        String url = null;

        if (pollEditUrl != null) {
            checkNotNull(pollId);

            url = pollEditUrl.replace("{pollId}", pollId);

            if (token == null) {
                url = url.replace("/{token}", "");
            } else {
                url = url.replace("{token}", token);
            }
        }
        return url;
    }

    public String getPollVoteUrl(String pollVoteUrl, String pollId, String token) {
        String url = null;

        if (pollVoteUrl != null) {
            checkNotNull(pollId);

            url = pollVoteUrl.replace("{pollId}", pollId);

            if (token == null) {
                url = url.replace("/{token}", "");
            } else {
                url = url.replace("{token}", token);
            }
        }

        return url;
    }

    public String getPollVoteEditUrl(String pollVoteEditUrl, String pollId, String voteId, String token) {
        String url = null;

        if (pollVoteEditUrl != null) {
            checkNotNull(pollId);
            checkNotNull(voteId);

            url = pollVoteEditUrl
                    .replace("{pollId}", pollId)
                    .replace("{voteId}", voteId);

            if (token == null) {
                url = url.replace("/{token}", "");
            } else {
                url = url.replace("{token}", token);
            }
        }

        return url;
    }

    public String getUserValidateUrl(String userValidateUrl, String userId, String token) {
        String url = null;

        if (userValidateUrl != null) {
            checkNotNull(userId);
            checkNotNull(token);

            url = userValidateUrl.replace("{userId}", userId);
            url = url.replace("{token}", token);
        }
        return url;
    }

    public String getResourceUrl(String resourceUrl, String resourceId) {
        String url = null;

        if (resourceUrl != null) {
            checkNotNull(resourceId);

            url = resourceUrl.replace("{resourceId}", resourceId);
        }
        return url;
    }
}
