package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.QuestionImpl;
import org.chorem.pollen.persistence.entity.QuestionTopiaDao;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.QuestionBean;
import org.chorem.pollen.services.bean.ReportResumeBean;
import org.chorem.pollen.services.service.security.PollenPermissions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

public class QuestionService  extends PollenServiceSupport {

    public QuestionBean toQuestionBean(Question entity) {
        QuestionBean bean = new QuestionBean();

        Poll pollEntity = entity.getPoll();

        bean.setEntityId(entity.getTopiaId());

        PollenPrincipal creator = pollEntity.getCreator();

        if (creator == null || creator.getPermission() == null) {

            bean.setPermission(null);

        } else {

            bean.setPermission(creator.getPermission().getToken());

        }

        bean.setTitle(entity.getTitle());
        bean.setDescription(entity.getDescription());
        bean.setCreateDate(entity.getTopiaCreateDate());
        bean.setVoteCountingType(entity.getVoteCountingType());
        bean.setVoteCountingConfig(getVoteCountingService().getVoteCountingConfig(entity));
        bean.setChoiceAddAllowed(entity.isChoiceAddAllowed());
        bean.setEndChoiceDate(entity.getEndChoiceDate());
        bean.setBeginChoiceDate(entity.getBeginChoiceDate());

        /* Choices */
        ChoiceService choiceService = getChoiceService();
        if (bean.getEntityId() != null) {
            List<ChoiceBean> choices = choiceService.getChoices(bean.getEntityId());
            bean.setChoices(choices);
        }

        if (isNotPermitted(PollenPermissions.edit(entity))) {
            bean.setPermission(null);

        } else {
            ReportResumeBean report = getReportService().getReport(entity.getTopiaId());
            bean.setReport(report);
        }

        boolean commentIsVisible = entity.isPersisted() && isPermitted(PollenPermissions.readComments(entity));
        bean.setCommentIsVisible(commentIsVisible);

        bean.setVoteIsVisible(entity.isPersisted() && isPermitted(PollenPermissions.readVotes(entity)));

        boolean resultIsVisible = entity.isPersisted() && isPermitted(PollenPermissions.readResult(entity));

        bean.setResultIsVisible(resultIsVisible);

        boolean canReadParticipants = entity.isPersisted() && isPermitted(PollenPermissions.readParticipants(entity));
        bean.setParticipantsIsVisible(canReadParticipants);

        boolean canVote = entity.isPersisted() && isPermitted(PollenPermissions.addVote(entity));

        bean.setCanVote(canVote);
        if (entity.isPersisted()) {
            if (commentIsVisible) {
                long commentCount = getCommentService().getQuestionCommentCount(entity.getTopiaId());
                bean.setCommentCount(commentCount);
            }

            long voteCount = getVoteService().getVoteCount(entity);
            bean.setVoteCount(voteCount);
            long choiceCount = getChoiceService().getChoiceCount(entity);
            bean.setChoiceCount(choiceCount);
        }

        return bean;
    }

    public Question toQuestion(QuestionBean bean) {
        Question entity = new QuestionImpl();
        entity.setTopiaId(bean.getEntityId());
        entity.setTitle(bean.getTitle());
        entity.setDescription(bean.getDescription());
        entity.setVoteCountingType(bean.getVoteCountingType());
        entity.setChoiceAddAllowed(bean.isChoiceAddAllowed());
        entity.setEndChoiceDate(bean.getEndChoiceDate());
        entity.setBeginChoiceDate(bean.getBeginChoiceDate());
        entity.setQuestionOrder(bean.getQuestionOrder());

        return entity;
    }

    public List<Question> getQuestions(Poll poll) {
        checkIsConnectedRequired();

        checkNotNull(poll);

        return getQuestionDao().forPollEquals(poll).findAll();
    }

    public List<QuestionBean> getQuestions(String pollId) {

        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        List<Question> questions = getQuestionDao().findAll(poll);
        return toBeanList(questions, this::toQuestionBean);
    }

    protected Question getQuestion(Poll poll, String questionId) {

        Question result = getQuestionDao().forTopiaIdEquals(questionId).findUnique();

        if (!poll.equals(result.getPoll())) {

            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, result, poll);

        }

        return result;
    }

    protected Question getQuestion0(String questionId) {

        return getQuestionDao().forTopiaIdEquals(questionId).findUnique();

    }

    protected Question saveQuestion(Poll poll, QuestionBean question) {

        QuestionTopiaDao questionDao = getQuestionDao();

        boolean questionExists = question.isPersisted();

        Question toSave;

        if (questionExists) {

            // get existing question
            toSave = getQuestion(poll, question.getEntityId());
            
        } else {

            // create a new question
            toSave = questionDao.create();
            toSave.setPoll(poll);
            
        }

        toSave.setChoiceAddAllowed(question.isChoiceAddAllowed());
        if (question.isChoiceAddAllowed()) {

            toSave.setBeginChoiceDate(question.getBeginChoiceDate());
            toSave.setEndChoiceDate(question.getEndChoiceDate());
        }

        toSave.setDescription(question.getDescription());
        toSave.setTitle(question.getTitle());
        toSave.setVoteCountingType(question.getVoteCountingType());

        String configToJson = getVoteCountingService().configToJson(question.getVoteCountingConfig());
        toSave.setVoteCountingConfig(configToJson);

        // -- choices -- //
        ChoiceService choiceService = getChoiceService();
        choiceService.saveChoices(toSave, question.getChoices());
        
        return toSave;
    }

    protected ErrorMap checkQuestion(List<Question> existingQuestions, QuestionBean question) {

        ErrorMap errors = new ErrorMap();

        boolean questionExists = question.isPersisted();

        Set<String> questionNames = Sets.newHashSet();

        if (CollectionUtils.isNotEmpty(existingQuestions)) {

            // get all used names

            for (Question question1 : existingQuestions) {

                if (questionExists &&
                        question1.getTopiaId().equals(question.getEntityId())) {

                    continue;

                }

                questionNames.add(question1.getTitle());

            }
        }

        checkChoices(errors, question.getChoices());

        return errors;

    }

    protected void checkChoices(ErrorMap errorMap, List<ChoiceBean> choices) {

        boolean choicesNotEmpty = CollectionUtils.isNotEmpty(choices);

        check(errorMap, "choice", choicesNotEmpty, l(getLocale(), "pollen.error.poll.choice.mandatory"));

        if (choicesNotEmpty) {

            List<Choice> existingChoices = new ArrayList<>();

            for (int i = 0; i < choices.size(); i++) {

                ChoiceBean choice = choices.get(i);

                ErrorMap choiceErrors = getChoiceService().checkChoice(existingChoices, choice);
                choiceErrors.copyTo(errorMap, "choice[" + i + "].");

                existingChoices.add(getChoiceService().toChoice(choice));

            }

        }
    }
}
