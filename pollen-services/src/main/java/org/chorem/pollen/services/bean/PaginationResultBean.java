package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

/**
 * Created on 5/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PaginationResultBean<O extends PollenBean> {

    protected List<O> elements;

    public static class PaginationResultContextBean {

        protected long count;

        protected int currentPage;

        protected int lastPage;

        protected int pageSize;

        protected String order;

        protected boolean desc;

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public boolean isDesc() {
            return desc;
        }

        public void setDesc(boolean desc) {
            this.desc = desc;
        }
    }

    protected final PaginationResultContextBean pagination = new PaginationResultContextBean();

    public List<O> getElements() {
        return elements;
    }

    public void setElements(List<O> elements) {
        this.elements = elements;
    }

    public PaginationResultContextBean getPagination() {
        return pagination;
    }

    public void setCount(long count) {
        pagination.count = count;
    }

    public void setCurrentPage(int currentPage) {
        pagination.currentPage = currentPage;
    }

    public void setLastPage(int lastPage) {
        pagination.lastPage = lastPage;
    }

    public void setPageSize(int pageSize) {
        pagination.pageSize = pageSize;
    }

    public void setOrder(String order) {
        pagination.order = order;
    }

    public void setDesc(boolean desc) {
        pagination.desc = desc;
    }
}
