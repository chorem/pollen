package org.chorem.pollen.services.bean.voteCounting.majorityJugment;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.services.bean.voteCounting.VoteCountingDetailResultBean;
import org.chorem.pollen.votecounting.MajorityJudgmentDetailResult;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MajorityJudgmentDetailResultBean extends VoteCountingDetailResultBean {

    protected BigDecimal sumWeight;

    protected BigDecimal halfWeight;

    protected List<MajorityJudgmentChoiceResultBean> choiceResults;

    public void fromResult(MajorityJudgmentDetailResult result) {

        setSumWeight(result.getSumWeight());
        setHalfWeight(result.getHalfWeight());

        setChoiceResults(result.getChoiceResults().stream()
                .map(choiceResult -> {
                    MajorityJudgmentChoiceResultBean majorityJudgmentChoiceResultBean = new MajorityJudgmentChoiceResultBean();
                    majorityJudgmentChoiceResultBean.fromResult(choiceResult);
                    return majorityJudgmentChoiceResultBean;
                })
                .collect(Collectors.toList()));
    }

    public BigDecimal getSumWeight() {
        return sumWeight;
    }

    public void setSumWeight(BigDecimal sumWeight) {
        this.sumWeight = sumWeight;
    }

    public BigDecimal getHalfWeight() {
        return halfWeight;
    }

    public void setHalfWeight(BigDecimal halfWeight) {
        this.halfWeight = halfWeight;
    }

    public List<MajorityJudgmentChoiceResultBean> getChoiceResults() {
        return choiceResults;
    }

    public void setChoiceResults(List<MajorityJudgmentChoiceResultBean> choiceResults) {
        this.choiceResults = choiceResults;
    }
}
