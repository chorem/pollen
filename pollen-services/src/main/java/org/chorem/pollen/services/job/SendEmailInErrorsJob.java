package org.chorem.pollen.services.job;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.services.service.mail.EmailService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionException;

/**
 * @author Kevin Morin (Code Lutin)
 */
@DisallowConcurrentExecution
public class SendEmailInErrorsJob extends AbstractPollenJob {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SendEmailInErrorsJob.class);

    @Override
    protected void execute() throws JobExecutionException {

        if (log.isDebugEnabled()) {
            log.debug("Start job to resend emails in errors");
        }

        EmailService emailService = newService(EmailService.class);
        emailService.resendEmails();

    }
}
