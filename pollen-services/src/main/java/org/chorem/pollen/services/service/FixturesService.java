package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.PollenTopiaApplicationContext;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddressTopiaDao;
import org.chorem.pollen.persistence.entity.PollenUserTopiaDao;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.PollenFixtures;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PollBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.QuestionBean;
import org.chorem.pollen.services.service.security.SecurityService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FixturesService extends PollenServiceSupport {

    private static final Log log = LogFactory.getLog(FixturesService.class);

    protected final Map<String, PollenFixtures> fixtureSets = Maps.newHashMap();

    public PollenFixtures cleanDatabaseAndLoadFixtures(String fixturesSetName) {

        return loadFixtures(fixturesSetName, true);

    }

    public PollenFixtures loadFixtures(String fixturesSetName) {

        return loadFixtures(fixturesSetName, false);

    }

    protected PollenFixtures loadFixtures(String fixturesSetName, boolean cleanDatabase) {

        PollenFixtures fixtures = fixtureSets.get(fixturesSetName);

        if (fixtures == null) {

            fixtures = new PollenFixtures(fixturesSetName);

            fixtureSets.put(fixturesSetName, fixtures);

            if (log.isInfoEnabled()) {
                log.info("will restore database with fixture set " + fixturesSetName);
            }

            PollenPersistenceContext persistenceContext = serviceContext.getPersistenceContext();

            if (cleanDatabase) {

                PollenTopiaApplicationContext topiaApplicationContext = serviceContext.getTopiaApplicationContext();

                topiaApplicationContext.dropSchema();
                topiaApplicationContext.createSchema();

            }
            
            serviceContext.setUIContext(fixtures.fixture("pollenUIContext_chorem"));

            SecurityService securityService = getSecurityService();

            PollenUserTopiaDao userDao = persistenceContext.getPollenUserDao();
            PollenUserEmailAddressTopiaDao emailAddressDao = persistenceContext.getPollenUserEmailAddressDao();

            Collection<PollenUser> users = fixtures.fixture("users");

            if (users != null) {
                for (PollenUser user : users) {
                    for (PollenUserEmailAddress emailAddress : user.getEmailAddresses()) {
                        PollenUserEmailAddress createdEmailAddress = emailAddressDao.create(emailAddress);
                        emailAddress.setTopiaId(createdEmailAddress.getTopiaId());
                        emailAddress.setValidated(true);
                    }

                    securityService.setUserPassword(user, user.getPassword());
                    PollenUser createdUser = userDao.create(user);
                    user.setTopiaId(createdUser.getTopiaId());
                }
            }

            PollService pollService = newService(PollService.class);

            Collection<Poll> polls = fixtures.fixture("polls");
            List<Question> questions = fixtures.fixture("questions");
            List<Choice> choices = fixtures.fixture("choices");

            if (polls != null) {
                for (Poll poll : polls) {
                    try {
                        PollBean pollBean = getPollService().toPollBean(poll);

                        pollBean.setQuestions(
                                questions.stream()
                                        .filter(question -> question.getPoll().equals(poll))
                                        .map(getQuestionService()::toQuestionBean)
                                        .collect(Collectors.toList())
                        );

                        for (QuestionBean question : pollBean.getQuestions()) {
                            question.setChoices(
                                    choices.stream()
                                            .filter(choice -> choice.getQuestion().getTitle().equals(question.getTitle()))
                                            .map(getChoiceService()::toChoiceBean)
                                            .collect(Collectors.toList())
                            );
                        }

                        PollenEntityRef<Poll> createdPoll = pollService.createPoll(pollBean, Collections.emptyList(), Collections.emptyList());
                        poll.setTopiaId(createdPoll.getEntityId());

                        //Set Questions and choices topiaId
                        PollBean createdPollBean = pollService.toPollBean(pollService.getPoll0(createdPoll.getEntityId()));
                        for (QuestionBean questionBean : createdPollBean.getQuestions()) {
                            questions.stream()
                                    .filter(question -> question.getTitle().equals(questionBean.getTitle()))
                                    .forEach(question -> question.setTopiaId(questionBean.getEntityId()));
                            for (ChoiceBean choiceBean:questionBean.getChoices()) {
                                choices.stream()
                                        .filter(choice -> choice.getChoiceValue().equals(choiceBean.getChoiceValue()))
                                        .forEach(choice -> choice.setTopiaId(choiceBean.getEntityId()));
                            }
                        }

                        PollenPrincipal principal = securityService.getPollenPrincipalByPermissionToken(createdPoll.getPermission());
                        poll.setCreator(principal);
                    } catch (InvalidFormException e) {
                        throw new PollenTechnicalException(e);
                    }
                }
            }

            persistenceContext.commit();

        }

        return fixtures;

    }

}
