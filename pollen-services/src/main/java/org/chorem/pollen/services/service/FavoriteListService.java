package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.entity.ChildFavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteListMember;
import org.chorem.pollen.persistence.entity.FavoriteListMemberTopiaDao;
import org.chorem.pollen.persistence.entity.FavoriteListTopiaDao;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.persistence.entity.VoterListMember;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.bean.ChildFavoriteListBean;
import org.chorem.pollen.services.bean.FavoriteListBean;
import org.chorem.pollen.services.bean.FavoriteListMemberBean;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.export.ChildFavoriteListExport;
import org.chorem.pollen.services.bean.export.ExportBean;
import org.chorem.pollen.services.bean.export.FavoriteListExport;
import org.chorem.pollen.services.bean.export.FavoriteListMemberExport;
import org.chorem.pollen.services.bean.export.FavoriteListsExport;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class FavoriteListService extends PollenServiceSupport {

    public FavoriteListBean toFavoriteListBean(FavoriteList entity) {

        FavoriteListBean bean = new FavoriteListBean();

        bean.setEntityId(entity.getTopiaId());
        bean.setName(entity.getName());

        long countChildren = getChildFavoriteListDao()
                .forParentEquals(entity)
                .count();

        bean.setCountChildren(countChildren);

        long countMembers = getFavoriteListMemberDao()
                .forFavoriteListEquals(entity)
                .count();

        bean.setCountMembers(countMembers);

        return bean;
    }

    protected ChildFavoriteListBean toChildFavoriteListBean(ChildFavoriteList entity) {

        ChildFavoriteListBean bean = new ChildFavoriteListBean();

        bean.setEntityId(entity.getTopiaId());
        bean.setChild(toFavoriteListBean(entity.getChild()));
        bean.setWeight(entity.getWeight());
        
        return bean;
    }

    protected FavoriteListMemberBean toFavoriteListMemberBean(FavoriteListMember entity) {
        FavoriteListMemberBean bean = new FavoriteListMemberBean();

        bean.setEntityId(entity.getTopiaId());
        bean.setName(entity.getName());
        bean.setEmail(entity.getEmail());
        bean.setWeight(entity.getWeight());

        return bean;
    }

    public PaginationResultBean<FavoriteListBean> getFavoriteLists(PaginationParameterBean paginationParameter, String search) {

        PollenUser user = checkAndGetConnectedUser();

        PaginationParameter page = getFavoriteListPaginationParameter(paginationParameter);

        PaginationResult<FavoriteList> favoriteLists;

        if (StringUtils.isNotBlank(search)) {
            favoriteLists = getFavoriteListDao().search(user, search, page);
        } else {
            favoriteLists = getFavoriteListDao().forPollenUserEquals(user).findPage(page);
        }

        return toPaginationListBean(favoriteLists, this::toFavoriteListBean);

    }

    public FavoriteListBean getFavoriteList(String favoriteListId) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);


        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);
        return toFavoriteListBean(favoriteList);

    }

    public PollenEntityRef<FavoriteList> createFavoriteList(FavoriteListBean favoriteList) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteList);
        checkIsNotPersisted(favoriteList);


        List<FavoriteList> existingFavoriteLists = getFavoriteLists0(user);

        ErrorMap errorMap = checkFavoriteList(existingFavoriteLists, favoriteList);
        errorMap.failIfNotEmpty();

        FavoriteList result = saveFavoriteList(user, favoriteList);
        commit();

        getNotificationService().onFavoriteListAdded(user, result);

        return PollenEntityRef.of(result);

    }

    public FavoriteListBean editFavoriteList(FavoriteListBean favoriteList) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteList);
        checkIsPersisted(favoriteList);


        List<FavoriteList> existingFavoriteLists = getFavoriteLists0(user);


        ErrorMap errorMap = checkFavoriteList(existingFavoriteLists, favoriteList);
        errorMap.failIfNotEmpty();

        FavoriteList result = saveFavoriteList(user, favoriteList);
        commit();

        getNotificationService().onFavoriteListEdited(user, result);

        return toFavoriteListBean(result);

    }

    public void deleteFavoriteList(String favoriteListId) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);


        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);
        getFavoriteListDao().delete(favoriteList);

        commit();

        getNotificationService().onFavoriteListDeleted(user, favoriteList);

    }

    public PaginationResultBean<FavoriteListMemberBean> getFavoriteListMembers(String favoriteListId,
                                                                               String search,
                                                                               PaginationParameterBean paginationParameter) {
        return getFavoriteListMembers(favoriteListId, search, paginationParameter, 0);
    }

    protected PaginationResultBean<FavoriteListMemberBean> getFavoriteListMembers(String favoriteListId,
                                                                                  String search,
                                                                               PaginationParameterBean paginationParameter,
                                                                               int offset) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);

        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        PaginationParameter page = getFavoriteListPaginationParameter(paginationParameter);
        PaginationResult<FavoriteListMember> members;

        if (offset == 0 || page.getPageSize() == -1) {
            if (StringUtils.isNotBlank(search)) {
                members = getFavoriteListMemberDao().search(favoriteList, search, page);
            } else {
                members = getFavoriteListMemberDao().forFavoriteListEquals(favoriteList).findPage(page);
            }
        } else {
            int startIndex = Math.max(0, page.getStartIndex() - offset);
            int endIndex = Math.max(0, page.getEndIndex() - offset);
            List<FavoriteListMember> elements;
            long count;
            if (StringUtils.isNotBlank(search)) {
                elements = getFavoriteListMemberDao().search(favoriteList, search,
                        paginationParameter.getOrder() + (paginationParameter.isDesc() ? " DESC": ""),
                        startIndex, endIndex);
                count = getFavoriteListMemberDao().countSearch(favoriteList, search);

            } else {
                elements = getFavoriteListMemberDao()
                        .forFavoriteListEquals(favoriteList)
                        .setOrderByArguments(paginationParameter.getOrder() + (paginationParameter.isDesc() ? " DESC": ""))
                        .find(startIndex, endIndex);
                count = getFavoriteListMemberDao().forFavoriteListEquals(favoriteList).count();
            }
            members = PaginationResult.of(elements, count, page);

        }

        return toPaginationListBean(members, this::toFavoriteListMemberBean);

    }

    public FavoriteListMemberBean getFavoriteListMember(String favoriteListId, String memberId) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(memberId);

        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        FavoriteListMember member = getFavoriteListMember0(favoriteList, memberId);
        return toFavoriteListMemberBean(member);

    }

    public PollenEntityRef<FavoriteListMember> addFavoriteListMember(String favoriteListId,
                                                                     FavoriteListMemberBean member) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(member);
        checkIsNotPersisted(member);


        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        List<FavoriteListMember> existingFavorliteListMembers = getFavoriteListMembers0(favoriteList);

        ErrorMap errorMap = checkFavoriteListMember(existingFavorliteListMembers, member);
        errorMap.failIfNotEmpty();

        FavoriteListMember result = saveFavoriteListMember(favoriteList, member);
        commit();

        return PollenEntityRef.of(result);

    }

    public FavoriteListMemberBean editFavoriteListMember(String favoriteListId,
                                                         FavoriteListMemberBean member) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(member);
        checkIsPersisted(member);


        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        List<FavoriteListMember> existingFavorliteListMembers = getFavoriteListMembers0(favoriteList);

        ErrorMap errorMap = checkFavoriteListMember(existingFavorliteListMembers, member);
        errorMap.failIfNotEmpty();

        FavoriteListMember result = saveFavoriteListMember(favoriteList, member);
        commit();

        return toFavoriteListMemberBean(result);

    }

    public void deleteFavoriteListMember(String favoriteListId,
                                         String memberId) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(memberId);


        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        FavoriteListMember member = getFavoriteListMember0(favoriteList, memberId);

        getFavoriteListMemberDao().delete(member);
        commit();

    }

    public void importFavoriteListMembersFromCsv(String favoriteListId,
                                                 File file) throws FavoriteListImportException {

        PollenUser connectedUser = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(file);

        FavoriteList favoriteList = getFavoriteList0(connectedUser, favoriteListId);
        List<FavoriteListMember> favoriteListMembers = getFavoriteListMembers0(favoriteList);

        FavoriteListImportFromFile importer = newService(FavoriteListImportFromFile.class);
        importer.setFile(file);
        importer.doImport(favoriteList, favoriteListMembers);

    }

    public void importFavoriteListMembersFromLdap(String favoriteListId,
                                                  String ldap) throws FavoriteListImportException {

        PollenUser connectedUser = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(ldap);

        FavoriteList favoriteList = getFavoriteList0(connectedUser, favoriteListId);
        List<FavoriteListMember> favoriteListMembers = getFavoriteListMembers0(favoriteList);

        FavoriteListImportFromLdap importer = newService(FavoriteListImportFromLdap.class);
        importer.setLdap(ldap);
        importer.doImport(favoriteList, favoriteListMembers);

    }

    protected List<FavoriteList> getFavoriteLists0(PollenUser user) {

        return getFavoriteListDao().forPollenUserEquals(user).findAll();

    }

    protected FavoriteList getFavoriteList0(PollenUser user, String favoriteListId) {

        FavoriteList result = getFavoriteListDao().forTopiaIdEquals(favoriteListId).findUnique();

        if (!user.equals(result.getPollenUser())) {

            throw new InvalidEntityLinkException(FavoriteList.PROPERTY_POLLEN_USER, result, user);

        }

        return result;

    }

    protected List<FavoriteListMember> getFavoriteListMembers0(FavoriteList favoriteList) {

        return getFavoriteListMemberDao().forFavoriteListEquals(favoriteList).findAll();

    }

    protected FavoriteListMember getFavoriteListMember0(FavoriteList favoriteList, String memberId) {

        FavoriteListMember result = getFavoriteListMemberDao().forTopiaIdEquals(memberId).findUnique();

        if (!favoriteList.equals(result.getFavoriteList())) {

            throw new InvalidEntityLinkException(FavoriteListMember.PROPERTY_FAVORITE_LIST, favoriteList, result);

        }

        return result;

    }

    protected FavoriteList saveFavoriteList(PollenUser user, FavoriteListBean favoriteList) {

        FavoriteListTopiaDao favoriteListDao = getFavoriteListDao();

        boolean persisted = favoriteList.isPersisted();

        FavoriteList toSave;

        if (persisted) {

            // get existing favorite list
            toSave = getFavoriteList0(user, favoriteList.getEntityId());

        } else {

            // create a new favorite list
            toSave = favoriteListDao.create();
            toSave.setPollenUser(user);
            toSave.setOwner(getSecurityContext().getMainPrincipal());

        }

        toSave.setName(favoriteList.getName());

        return toSave;

    }

    protected FavoriteListMember saveFavoriteListMember(FavoriteList favoriteList, FavoriteListMemberBean favoriteListMember) {

        FavoriteListMemberTopiaDao favoriteListDao = getFavoriteListMemberDao();

        boolean persisted = favoriteListMember.isPersisted();

        FavoriteListMember toSave;

        if (persisted) {

            // get existing favorite list
            toSave = getFavoriteListMember0(favoriteList, favoriteListMember.getEntityId());

        } else {

            // create a new favorite list
            toSave = favoriteListDao.create();
            toSave.setFavoriteList(favoriteList);

        }

        toSave.setName(favoriteListMember.getName());
        toSave.setEmail(getCleanMail(favoriteListMember.getEmail()));
        toSave.setWeight(favoriteListMember.getWeight());

        return toSave;
    }

    protected ChildFavoriteList getChildList0(FavoriteList parentList, String childListId) {

        ChildFavoriteList result = getChildFavoriteListDao().forTopiaIdEquals(childListId).findUnique();

        if (!parentList.equals(result.getParent())) {

            throw new InvalidEntityLinkException(ChildFavoriteList.PROPERTY_PARENT, parentList, result);

        }

        return result;

    }

    protected List<ChildFavoriteList> getChildrenLists0(FavoriteList parentList) {

        List<ChildFavoriteList> result = getChildFavoriteListDao().forParentEquals(parentList).findAll();

        return result;

    }

    protected List<ChildFavoriteList> getAllChildrenLists0(PollenUser user) {


        List<ChildFavoriteList> result = getChildFavoriteListDao()
                .forProperties(ChildFavoriteList.PROPERTY_PARENT + "." + FavoriteList.PROPERTY_POLLEN_USER, user)
                .findAll();

        return result;

    }

    protected ErrorMap checkFavoriteList(List<FavoriteList> existingFavoriteLists, FavoriteListBean favoriteList) {

        ErrorMap errors = new ErrorMap();

        String favoriteListName = favoriteList.getName();

        boolean nameNotBlank = checkNotBlank(errors, "name", favoriteListName, l(getLocale(), "pollen.error.favoriteList.name.empty"));

        if (nameNotBlank) {

            Optional<FavoriteList> sameName = existingFavoriteLists.stream()
                    .filter(favoriteList1 -> favoriteList1.getTopiaId() == null || !favoriteList1.getTopiaId().equals(favoriteList.getEntityId()))
                    .filter(favoriteList1 -> favoriteList1.getName().equals(favoriteListName))
                    .findFirst();

            check(errors, "name", !sameName.isPresent(), l(getLocale(), "pollen.error.favoriteList.name.already.used", favoriteListName));
        }

        return errors;

    }

    protected ErrorMap checkFavoriteListMember(List<FavoriteListMember> existingFavoriteListMembers, FavoriteListMemberBean favoriteListMember) {

        ErrorMap errors = new ErrorMap();

        boolean voterListMemberExists = favoriteListMember.isPersisted();

        Set<String> memberNames = Sets.newHashSet();
        Set<String> memberEmails = Sets.newHashSet();

        if (CollectionUtils.isNotEmpty(existingFavoriteListMembers)) {

            // get all used names / emails

            for (FavoriteListMember favoriteListMember1 : existingFavoriteListMembers) {

                if (voterListMemberExists &&
                        favoriteListMember1.getTopiaId().equals(favoriteListMember.getEntityId())) {
                    continue;
                }

                memberNames.add(favoriteListMember1.getName());
                memberEmails.add(favoriteListMember1.getEmail());

            }

        }

        boolean nameNotBlank = checkNotBlank(errors, "name", favoriteListMember.getName(), l(getLocale(), "pollen.error.favoriteListMember.name.empty"));
        if (nameNotBlank) {

            boolean added = memberNames.add(favoriteListMember.getName());
            check(errors, "name", added, l(getLocale(), "pollen.error.favoriteListMember.name.already.used", favoriteListMember.getName()));

        }

        String memberEmail = getCleanMail(favoriteListMember.getEmail());

        boolean emailNotBlank = checkNotBlank(errors, "email", memberEmail, l(getLocale(), "pollen.error.favoriteListMember.email.empty"));

        if (emailNotBlank) {

            boolean emailValid = checkValidEmail(errors, "email", memberEmail, l(getLocale(), "pollen.error.favoriteListMember.email.invalid", memberEmail));

            if (emailValid) {

                boolean emailAdded = memberEmails.add(memberEmail);
                check(errors, "email", emailAdded, l(getLocale(), "pollen.error.favoriteListMember.email.already.used", memberEmail));

            }

        }

        check(errors, "weight", favoriteListMember.getWeight() > 0, l(getLocale(), "pollen.error.favoriteListMember.weight.negativeOrNull", favoriteListMember.getWeight()));

        return errors;

    }

    protected ErrorMap checkChildList(FavoriteList parentList, List<ChildFavoriteList> existingChildList, ChildFavoriteListBean childFavoriteList) {

        ErrorMap errors = new ErrorMap();

        boolean childListExists = childFavoriteList.isPersisted();

        if (!childListExists && CollectionUtils.isNotEmpty(existingChildList)) {

            // get all used
            Optional<String> childListAlreadyExist = existingChildList.stream()
                    .filter(child -> parentList.equals(child.getParent()))
                    .map(ChildFavoriteList::getChild)
                    .filter(list -> list.getName().equals(childFavoriteList.getChild().getName()))
                    .map(FavoriteList::getTopiaId)
                    .findFirst();

            check(errors, ChildFavoriteList.PROPERTY_CHILD,
                    !childListAlreadyExist.isPresent(),
                    l(getLocale(), "pollen.error.childFavoriteList.already.used", childFavoriteList.getChild().getName()));
        }


        //on se base sur le nom plutot que sur L'id car pour les import les id n'existe pas encore

        boolean sameList = parentList.getName().equals(childFavoriteList.getChild().getName());
        check(errors, ChildFavoriteList.PROPERTY_CHILD,
                !sameList,
                l(getLocale(), "pollen.error.childFavoriteList.sameParentChild", parentList.getName()));

        check(errors, ChildFavoriteList.PROPERTY_CHILD,
                sameList || !isAncestorOrSelf(childFavoriteList.getChild().getName(), parentList.getName(), existingChildList),
                l(getLocale(), "pollen.error.childFavoriteList.childIsAncestor", parentList.getName(), childFavoriteList.getChild().getName()));

        check(errors, "weight", childFavoriteList.getWeight() > 0, l(getLocale(), "pollen.error.childFavoriteList.weight.negativeOrNull", childFavoriteList.getWeight()));


        return errors;
    }


    protected boolean isAncestorOrSelf(String ancestorFavoriteListName, String favoriteListName, List<ChildFavoriteList> existingChildList) {

        boolean result = ancestorFavoriteListName.equals(favoriteListName);

        if (! result) {

            List<ChildFavoriteList> childrenFavoriteList = existingChildList.stream()
                    .filter(child -> favoriteListName.equals(child.getChild().getName()))
                    .collect(Collectors.toList());

            for (ChildFavoriteList childFavoriteList : childrenFavoriteList) {

                result = result || isAncestorOrSelf(ancestorFavoriteListName, childFavoriteList.getParent().getName(), existingChildList);

                if (result) {
                    break;
                }

            }
        }

        return result;

    }

    protected PaginationParameter getFavoriteListPaginationParameter(PaginationParameterBean paginationParameter) {

        if (paginationParameter == null) {

            paginationParameter = PaginationParameterBean.of(0, PAGE_SIZE_DEFAULT);

        }

        if (StringUtils.isEmpty(paginationParameter.getOrder())) {
            paginationParameter.setOrder(FavoriteList.PROPERTY_NAME);
        }

        return paginationParameter.toPaginationParameter();

    }

    public PaginationResultBean<ChildFavoriteListBean> getChildrenLists(String favoriteListId, String search, PaginationParameterBean paginationParameter) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);

        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        PaginationParameter page = getFavoriteListPaginationParameter(paginationParameter);

        if (!page.getOrderClauses().isEmpty() && FavoriteList.PROPERTY_NAME.equals(page.getOrderClauses().get(0).getClause())) {
            page.getOrderClauses().get(0).setClause(ChildFavoriteList.PROPERTY_CHILD + "." + FavoriteList.PROPERTY_NAME);
        }

        PaginationResult<ChildFavoriteList> children;

        if (StringUtils.isNotBlank(search)) {

            children = getChildFavoriteListDao().search(favoriteList, search, page);

        } else {

            children = getChildFavoriteListDao().forParentEquals(favoriteList).findPage(page);
            
        }

        return toPaginationListBean(children, this::toChildFavoriteListBean);

    }

    public ChildFavoriteListBean getChildList(String favoriteListId, String childListId) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(childListId);

        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        ChildFavoriteList child = getChildList0(favoriteList, childListId);
        return toChildFavoriteListBean(child);

    }

    public PollenEntityRef<ChildFavoriteList> addChildList(String favoriteListId, ChildFavoriteListBean childList) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(childList);
        checkIsNotPersisted(childList);

        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        List<ChildFavoriteList> existingChildFavoriteList = getAllChildrenLists0(user);

        ErrorMap errorMap = checkChildList(favoriteList, existingChildFavoriteList, childList);
        errorMap.failIfNotEmpty();

        ChildFavoriteList result = saveChildFavoriteList(favoriteList, childList);
        commit();

        return PollenEntityRef.of(result);

    }

    public ChildFavoriteListBean editChildList(String favoriteListId, ChildFavoriteListBean childList) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(childList);
        checkIsPersisted(childList);

        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        List<ChildFavoriteList> existingChildFavoriteList = getChildrenLists0(favoriteList);

        ErrorMap errorMap = checkChildList(favoriteList, existingChildFavoriteList, childList);
        errorMap.failIfNotEmpty();

        ChildFavoriteList result = saveChildFavoriteList(favoriteList, childList);
        commit();

        return toChildFavoriteListBean(result);

    }

    public void removeChildList(String favoriteListId, String childListId) {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(favoriteListId);
        checkNotNull(childListId);

        FavoriteList favoriteList = getFavoriteList0(user, favoriteListId);

        ChildFavoriteList childFavoriteList = getChildList0(favoriteList, childListId);

        getChildFavoriteListDao().delete(childFavoriteList);
        commit();

    }

    protected ChildFavoriteList saveChildFavoriteList(FavoriteList favoriteList, ChildFavoriteListBean childList) {

        boolean persisted = childList.isPersisted();

        ChildFavoriteList toSave;

        if (persisted) {

            // get existing favorite list
            toSave = getChildList0(favoriteList, childList.getEntityId());

        } else {

            // create a new favorite list
            toSave = getChildFavoriteListDao().create();
            toSave.setParent(favoriteList);
        }

        FavoriteList child = getFavoriteList0(getConnectedUser(), childList.getChild().getEntityId());
        toSave.setChild(child);
        toSave.setWeight(childList.getWeight());

        return toSave;
    }

    public FavoriteListBean importFavoriteListMembersFromVoterList(String voterListId) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(voterListId);
        VoterList voterList = getVoterListDao().forTopiaIdEquals(voterListId).findUnique();

        List<FavoriteList> existingFavoriteLists = getFavoriteLists0(user);

        ErrorMap errors = new ErrorMap();

        ErrorMap errorMap = checkImportVoterList(voterList, existingFavoriteLists, errors);
        errorMap.failIfNotEmpty();

        FavoriteList favoriteList = importFavoriteListMembersFromVoterList(voterList, user);

        commit();
        return toFavoriteListBean(favoriteList);
    }

    protected FavoriteList importFavoriteListMembersFromVoterList(VoterList voterList, PollenUser user) {
        FavoriteList toSave = getFavoriteListDao().create();

        toSave.setName(voterList.getName());
        toSave.setPollenUser(user);
        toSave.setOwner(getSecurityContext().getMainPrincipal());

        for (VoterListMember voterListMember : getVoterListMemberDao().forVoterListEquals(voterList).findAll()) {
            FavoriteListMember memberToSave  = getFavoriteListMemberDao().create();
            memberToSave.setName(voterListMember.getMember().getName());
            memberToSave.setEmail(voterListMember.getMember().getEmail());
            memberToSave.setWeight(voterListMember.getWeight());
            memberToSave.setFavoriteList(toSave);
        }

        for (VoterList childVoterList : getVoterListDao().forParentEquals(voterList).findAll()) {
            FavoriteList subFavoriteList  = importFavoriteListMembersFromVoterList(childVoterList, user);
            ChildFavoriteList childFavoriteListToSave = getChildFavoriteListDao().create();
            childFavoriteListToSave.setChild(subFavoriteList);
            childFavoriteListToSave.setParent(toSave);
            childFavoriteListToSave.setWeight(childVoterList.getWeight());
        }

        getNotificationService().onFavoriteListAdded(user, toSave);
        return toSave;
    }


    protected ErrorMap checkImportVoterList(VoterList voterList, List<FavoriteList> existingFavoriteLists, ErrorMap errors) {
        String name = voterList.getName();
        boolean nameAlreadyUsed = existingFavoriteLists.stream()
                .anyMatch(fl -> fl.getName().equals(name));

        check(errors, FavoriteList.PROPERTY_NAME, !nameAlreadyUsed, l(getLocale(), "pollen.error.favoriteList.name.already.used", name));

        List<VoterList> childrenVoterLists = getVoterListDao().forParentEquals(voterList).addEquals(VoterList.PROPERTY_POLL, voterList.getPoll()).findAll();
        for (VoterList childVoterList : childrenVoterLists) {
            checkImportVoterList(childVoterList, existingFavoriteLists, errors);
        }

        return errors;
    }

    public int importFavoriteLists(File favoriteListsExportFile) throws InvalidFormException {
        PollenUser user = checkAndGetConnectedUser();

        Gson gson = new Gson();
        FileReader reader;
        try {
            reader = new FileReader(favoriteListsExportFile);

        } catch (FileNotFoundException e) {

            // should never happens ?
            throw new PollenTechnicalException(e);

        }

        List<FavoriteList> favoriteLists = Lists.newArrayList();
        try {
            FavoriteListsExport favoriteListsExport = gson.fromJson(reader, FavoriteListsExport.class);

            List<FavoriteList> existingFavoriteLists = getFavoriteLists0(user);

            ErrorMap errorMap = checkImportFavoriteLists(favoriteListsExport, existingFavoriteLists);
            errorMap.failIfNotEmpty();

            favoriteLists = saveImportFavoriteLists(favoriteListsExport, user);

            commit();
        } catch (JsonSyntaxException e) {
            ErrorMap errors = new ErrorMap();

            errors.addError("file", l(getLocale(), "pollen.error.import.favoriteList.parser", e.getMessage()));
            errors.failIfNotEmpty();
        }



        return favoriteLists.size();

    }

    private List<FavoriteList> saveImportFavoriteLists(FavoriteListsExport favoriteListsExport, PollenUser user) {

        Map<FavoriteListExport, FavoriteList> favoriteListByExport = Maps.newHashMap();

        for (FavoriteListExport favoriteListExport : favoriteListsExport.getFavoriteLists()) {

            FavoriteList favoriteList = favoriteListExport.toEntity();
            favoriteList.setPollenUser(user);
            favoriteList.setOwner(getSecurityContext().getMainPrincipal());

            getFavoriteListDao().create(favoriteList);

            favoriteListByExport.put(favoriteListExport, favoriteList);

            for (FavoriteListMemberExport favoriteListMemberExport : favoriteListExport.getMembers()) {

                FavoriteListMember member = favoriteListMemberExport.toEntity();
                member.setFavoriteList(favoriteList);

                getFavoriteListMemberDao().create(member);

            }
        }

        List<FavoriteList> favoriteLists = getFavoriteLists0(user);

        for (FavoriteListExport favoriteListExport : favoriteListsExport.getFavoriteLists()) {

            FavoriteList favoriteList = favoriteListByExport.get(favoriteListExport);

            for (ChildFavoriteListExport childFavoriteListExport : favoriteListExport.getChildren()) {

                ChildFavoriteList childFavoriteList = childFavoriteListExport.toEntity(favoriteList, favoriteLists);

                getChildFavoriteListDao().create(childFavoriteList);

            }

        }

        return Lists.newArrayList(favoriteListByExport.values());
    }

    private ErrorMap checkImportFavoriteLists(
            FavoriteListsExport favoriteListsExport,
            List<FavoriteList> existingFavoriteLists) {

        ErrorMap errors = new ErrorMap();

        check(errors, "version", FavoriteListsExport.EXPORT_VERSION.equals(favoriteListsExport.getVersion()), l(getLocale(), "pollen.error.import.favoriteList.version", favoriteListsExport.getVersion()));

        Map<FavoriteListExport, FavoriteList> favoriteListByExport = Maps.newHashMap();

        int favoriteListIndex = 0;
        for (FavoriteListExport favoriteListExport : favoriteListsExport.getFavoriteLists()) {

            ErrorMap errorsList = checkFavoriteList(existingFavoriteLists, favoriteListExport.toBean());

            FavoriteList favoriteList = favoriteListExport.toEntity();

            existingFavoriteLists.add(favoriteList);

            favoriteListByExport.put(favoriteListExport, favoriteList);

            List<FavoriteListMember> members = Lists.newLinkedList();

            int memberIndex = 0;
            for (FavoriteListMemberExport favoriteListMemberExport : favoriteListExport.getMembers()) {

                ErrorMap errorsMember = checkFavoriteListMember(members, favoriteListMemberExport.toBean());


                errorsMember.copyTo(errorsList, ".members[" + memberIndex + "]");

                members.add(favoriteListMemberExport.toEntity());

                memberIndex++;
            }

            errorsList.copyTo(errors, "favoriteLists[" + favoriteListIndex + "]");

            favoriteListIndex++;
        }

        favoriteListIndex = 0;
        for (FavoriteListExport favoriteListExport : favoriteListsExport.getFavoriteLists()) {

            FavoriteList favoriteList = favoriteListByExport.get(favoriteListExport);

            List<ChildFavoriteList> children = Lists.newLinkedList();

            int childIndex = 0;
            for (ChildFavoriteListExport childFavoriteListExport : favoriteListExport.getChildren()) {

                ErrorMap errorsChild = checkChildList(favoriteList, children, toChildFavoriteListBean(childFavoriteListExport, existingFavoriteLists));

                errorsChild.copyTo(errors, "favoriteLists[" + favoriteListIndex + "].children[" + childIndex + "]");

                children.add(childFavoriteListExport.toEntity(favoriteList, existingFavoriteLists));

                childIndex++;
            }

        }

        return errors;
    }

    protected ChildFavoriteListBean toChildFavoriteListBean(ChildFavoriteListExport entity, List<FavoriteList> existingFavoriteLists) {
        ChildFavoriteListBean bean = new ChildFavoriteListBean();
        bean.setWeight(entity.getWeight());
        FavoriteList childEntity = existingFavoriteLists.stream()
                .filter(favoriteList -> entity.getChild().equals(favoriteList.getName()))
                .findFirst()
                .orElse(null);
        FavoriteListBean childBean = toFavoriteListBean(childEntity);
        bean.setChild(childBean);

        return bean;
    }

    public ExportBean exportFavoriteLists() {
        PollenUser user = checkAndGetConnectedUser();

        FavoriteListsExport result = new FavoriteListsExport();

        List<FavoriteList> favoriteLists = getFavoriteLists0(user);

        result.setVersion(FavoriteListsExport.EXPORT_VERSION);

        for (FavoriteList favoriteList : favoriteLists) {

            FavoriteListExport favoriteListExport = FavoriteListExport.fromEntity(favoriteList);

            List<ChildFavoriteList> children = getChildrenLists0(favoriteList);

            List<ChildFavoriteListExport> childrenExport = children.stream()
                    .map(ChildFavoriteListExport::fromEntity)
                    .collect(Collectors.toList());

            favoriteListExport.setChildren(childrenExport);

            List<FavoriteListMember> members = getFavoriteListMembers0(favoriteList);

            List<FavoriteListMemberExport> membersExport = members.stream()
                    .map(FavoriteListMemberExport::fromEntity)
                    .collect(Collectors.toList());

            favoriteListExport.setMembers(membersExport);

            result.getFavoriteLists().add(favoriteListExport);
        }

        ExportBean exportBean = new ExportBean();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        exportBean.setContent(gson.toJson(result));
        exportBean.setName(l(getLocale(), "pollen.export.favoriteLists", user.getName(), getNow()));
        exportBean.setContentType("application/json");


        return exportBean;
    }

    public PaginationResultBean<PollenBean> getAllChildren(String favoriteListId, String search, PaginationParameterBean paginationParameter) {

        checkIsConnected();
        checkNotNull(favoriteListId);

        // search children list in first
        PaginationParameter page = getFavoriteListPaginationParameter(paginationParameter);

        PaginationResultBean<ChildFavoriteListBean> resultChildList = getChildrenLists(favoriteListId, search, paginationParameter);
        PaginationResultBean<FavoriteListMemberBean> resultMember;

        if (paginationParameter.getPageSize() == -1) {
            resultMember = getFavoriteListMembers(favoriteListId, search, paginationParameter);
        } else {
            resultMember = getFavoriteListMembers(favoriteListId, search, paginationParameter, (int) resultChildList.getPagination().getCount());
        }

        List<PollenBean> elements = Lists.newLinkedList(resultChildList.getElements());
        elements.addAll(resultMember.getElements());

        PaginationResultBean<PollenBean> result = new PaginationResultBean<>();
        result.setElements(elements);
        long count = resultChildList.getPagination().getCount() + resultMember.getPagination().getCount();
        result.setCount(count);
        result.setCurrentPage(paginationParameter.getPageNumber());
        result.setLastPage((int) Math.max(0, Math.ceil(count * 1. / paginationParameter.getPageSize()) - 1));
        result.setPageSize(paginationParameter.getPageSize());
        result.setDesc(false);
        result.setOrder(FavoriteListMember.PROPERTY_NAME);

        return result;

    }

}
