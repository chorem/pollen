package org.chorem.pollen.services;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.PollenTopiaApplicationContext;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.chorem.pollen.votecounting.VoteCountingFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

public class DefaultPollenServiceContext implements PollenServiceContext {

    protected PollenServicesConfig pollenServicesConfig;

    protected PollenPersistenceContext persistenceContext;

    protected PollenSecurityContext securityContext;

    protected Locale locale;

    private PollenTopiaApplicationContext topiaApplicationContext;

    protected VoteCountingFactory voteCountingFactory;

    protected PollenUIContext uiContext;

    public void setPollenServicesConfig(PollenServicesConfig pollenServicesConfig) {
        this.pollenServicesConfig = pollenServicesConfig;
    }

    public void setTopiaApplicationContext(PollenTopiaApplicationContext topiaApplicationContext) {
        this.topiaApplicationContext = topiaApplicationContext;
    }

    public void setPersistenceContext(PollenPersistenceContext persistenceContext) {
        this.persistenceContext = persistenceContext;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public void setSecurityContext(PollenSecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    @Override
    public PollenTopiaApplicationContext getTopiaApplicationContext() {
        return topiaApplicationContext;
    }

    public PollenServicesConfig getPollenServicesConfig() {
        return pollenServicesConfig;
    }

    @Override
    public Date getNow() {
        return new Date();
    }

    @Override
    public PollenSecurityContext getSecurityContext() {

        return securityContext;

    }

    @Override
    public PollenUIContext getUIContext() {
        return uiContext;
    }

    @Override
    public void setUIContext(PollenUIContext uiContext) {
        this.uiContext = uiContext;
    }

    @Override
    public String generateSalt() {
        final Random r = new SecureRandom();
        byte[] salt = new byte[32];
        r.nextBytes(salt);
        return Base64.encodeBase64String(salt);
    }

    @Override
    public String generateToken() {

        // generate uuid
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");

        // decode in hexa base
        byte[] decode;
        try {
            decode = Hex.decodeHex(uuid.toCharArray());
        } catch (DecoderException e) {
            // can't happen!
            throw new RuntimeException(e);
        }

        // encode it in base64 (url safe version)
        return Base64.encodeBase64URLSafeString(decode);

    }

    @Override
    public String generatePassword() {
        // Generates a 8 code point string, using only the alphanumerical characters
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(LETTERS, DIGITS)
                .build();

        return generator.generate(8);
    }

    @Override
    public String encodePassword(String salt, String password) {
        String hashAlgorithmName = getPollenServicesConfig().getHashAlgorithmName();

        try {
            MessageDigest digest = MessageDigest.getInstance(hashAlgorithmName);

            digest.reset();
            digest.update(salt.getBytes());
            byte[] hashedBytes = digest.digest(password.getBytes());
            return Base64.encodeBase64String(hashedBytes);

        } catch (NoSuchAlgorithmException e) {
            throw new PollenTechnicalException("unable to hash password with " + hashAlgorithmName, e);
        }

    }

    @Override
    public VoteCountingFactory getVoteCountingFactory() {
        return voteCountingFactory;
    }

    public void setVoteCountingFactory(VoteCountingFactory voteCountingFactory) {
        this.voteCountingFactory = voteCountingFactory;
    }

    @Override
    public PollenPersistenceContext getPersistenceContext() {
        return persistenceContext;
    }

    @Override
    public Locale getLocale() {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        return locale;
    }

    @Override
    public String getCleanMail(String email) {
        return email == null ? null : StringUtils.lowerCase(email.trim());
    }

    public <E extends PollenService> E newService(Class<E> serviceClass) {

        E service;

        try {

            Constructor<E> constructor = serviceClass.getConstructor();

            service = constructor.newInstance();

        } catch (NoSuchMethodException e) {

            throw new PollenTechnicalException("all services must provide a default public constructor", e);

        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {

            throw new PollenTechnicalException("unable to instantiate pollen service", e);

        }

        service.setServiceContext(this);

        return service;

    }

}
