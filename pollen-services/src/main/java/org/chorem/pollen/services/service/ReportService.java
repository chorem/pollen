package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Report;
import org.chorem.pollen.persistence.entity.ReportImpl;
import org.chorem.pollen.persistence.entity.ReportResume;
import org.chorem.pollen.services.bean.ReportBean;
import org.chorem.pollen.services.bean.ReportLevel;
import org.chorem.pollen.services.bean.ReportResumeBean;
import org.chorem.pollen.services.service.security.PollenPermissions;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;

import static org.nuiton.i18n.I18n.l;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ReportService extends PollenServiceSupport {

    public ReportBean toReportBean(Report entity) {
        ReportBean bean = new ReportBean();

        bean.setEntityId(entity.getTopiaId());

        bean.setLevel(ReportLevel.of(entity.getLevel()));
        bean.setEmail(entity.getEmail());
        bean.setIgnore(entity.isIgnore());

        return bean;

    }


    public void addPollReport(String pollId, ReportBean reportBean) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        Report report = getReportService().addReport(reportBean, poll);

        getNotificationService().onAddPollReport(poll, report);

    }

    public List<ReportBean> getPollReports(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        return getReports(pollId);

    }

    public void savePollReport(String pollId, ReportBean reportBean) {
        checkIsConnectedRequired();
        checkNotNull(pollId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        saveReport(reportBean, pollId);
    }

    public void addQuestionReport(String pollId, String questionId, ReportBean reportBean) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));;

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.edit(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Report report = getReportService().addReport(reportBean, question);

        getNotificationService().onAddQuestionReport(question, report);
    }

    public List<ReportBean> getQuestionReports(String pollId, String questionId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));;

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.edit(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        return getReports(questionId);
    }

    public void saveQuestionReport(String pollId, String questionId, ReportBean reportBean) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));;

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.edit(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        saveReport(reportBean, questionId);
    }

    public void addChoiceReport(String pollId, String questionId, String choiceId, ReportBean reportBean) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(choiceId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));;

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.read(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Choice choice = getChoiceService().getChoice(question, choiceId);

        Question choiceQuestion = choice.getQuestion();
        if (!choiceQuestion.getTopiaId().equals(question.getTopiaId())){
            throw new InvalidEntityLinkException(Choice.PROPERTY_QUESTION, choice, question);
        }

        Report report = getReportService().addReport(reportBean, choice);

        getNotificationService().onAddChoiceReport(question, choice, report);

    }

    public List<ReportBean> getChoiceReports(String pollId, String questionId, String choiceId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(choiceId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));;

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.edit(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Choice choice = getChoiceService().getChoice(question, choiceId);

        Question choiceQuestion = choice.getQuestion();
        if (!choiceQuestion.getTopiaId().equals(question.getTopiaId())){
            throw new InvalidEntityLinkException(Choice.PROPERTY_QUESTION, choice, question);
        }

        return getReports(choiceId);

    }

    public void saveChoiceReport(String pollId, String questionId, String choiceId, ReportBean reportBean) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(choiceId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));;

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.edit(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Choice choice = getChoiceService().getChoice(question, choiceId);

        Question choiceQuestion = choice.getQuestion();
        if (!choiceQuestion.getTopiaId().equals(question.getTopiaId())){
            throw new InvalidEntityLinkException(Choice.PROPERTY_QUESTION, choice, question);
        }

        saveReport(reportBean, choiceId);
    }

    public void addCommentReport(String pollId, String commentId, ReportBean reportBean) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(commentId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readComments(poll));

        Comment comment = getCommentService().getComment(poll, commentId);

        Report report = getReportService().addReport(reportBean, comment);

        getNotificationService().onAddCommentReport(poll, comment, report);

    }

    public void addQuestionCommentReport(String pollId, String questionId, String commentId, ReportBean reportBean) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(commentId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readComments(poll));;

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.readComments(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Comment comment = getCommentService().getComment(question, commentId);

        Report report = getReportService().addReport(reportBean, comment);

        getNotificationService().onAddQuestionCommentReport(question, comment, report);

    }

    public List<ReportBean> getCommentReports(String pollId, String commentId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(commentId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        return getReports(commentId);

    }
    public List<ReportBean> getQuestionCommentReports(String pollId, String questionId, String commentId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(commentId);
        Poll poll = getPollService().getPoll0(pollId);
        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.edit(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        return getReports(commentId);

    }


    public void saveCommentReport(String pollId, String commentId, ReportBean reportBean) {
        checkIsConnectedRequired();
        checkNotNull(pollId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        saveReport(reportBean, commentId);
    }

    public void saveQuestionCommentReport(String pollId, String questionId, String commentId, ReportBean reportBean) {
        checkIsConnectedRequired();
        checkNotNull(pollId);
        Poll poll = getPollService().getPoll0(pollId);
        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.edit(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        saveReport(reportBean, commentId);
    }


    protected List<ReportBean> getReports(String targetId) {
        checkIsConnectedRequired();
        checkNotNull(targetId);

        List<Report> reports = getReportTopiaDao().forTargetIdEquals(targetId).findAll();

        return toBeanList(reports, this::toReportBean);
    }

    protected Report addReport(ReportBean report, TopiaEntity target) throws InvalidFormException {

        checkNotNull(report);
        checkIsNotPersisted(report);

        checkNotNull(target);
        checkIsPersisted(target);

        ErrorMap errorMap = checkReport(report);
        errorMap.failIfNotEmpty();

        Report reportEntity = new ReportImpl();

        reportEntity.setTopiaId(report.getEntityId());
        reportEntity.setLevel(report.getLevel().getScore());
        reportEntity.setEmail(report.getEmail());
        reportEntity.setIgnore(report.isIgnore());
        reportEntity.setTargetId(target.getTopiaId());

        reportEntity = getReportTopiaDao().create(reportEntity);

        commit();

        return reportEntity;

    }

    protected ErrorMap checkReport(ReportBean report) {
        ErrorMap errors = new ErrorMap();

        checkNotNull(errors, Report.PROPERTY_LEVEL, report.getLevel(), l(getLocale(), "pollen.error.report.level.mandatory"));
        checkNotNull(errors, Report.PROPERTY_EMAIL, report.getEmail(), l(getLocale(), "pollen.error.report.email.mandatory"));
        checkValidEmail(errors, Report.PROPERTY_EMAIL, report.getEmail(), l(getLocale(), "pollen.error.report.email.invalid"));

        return errors;
    }

    protected void saveReport(ReportBean reportBean, String targetId) {

        checkNotNull(reportBean);
        checkIsPersisted(reportBean);
        checkNotNull(targetId);

        Report report = getReportTopiaDao()
                .forTopiaIdEquals(reportBean.getEntityId())
                .addEquals(Report.PROPERTY_TARGET_ID, targetId)
                .findUnique();

        report.setIgnore(reportBean.isIgnore());

        commit();
    }

    public ReportResumeBean getReport(String targetId) {

        ReportResume report = getReportTopiaDao().getReportResume(targetId);

        return ReportResumeBean.of(report);
    }

    public long getScore(TopiaEntity target) {

        ReportResume report = getReportTopiaDao().getReportResume(target.getTopiaId());
        return report.getScore();
    }
}
