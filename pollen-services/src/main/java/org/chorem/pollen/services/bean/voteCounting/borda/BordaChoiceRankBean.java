package org.chorem.pollen.services.bean.voteCounting.borda;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.votecounting.BordaChoiceRank;

import java.math.BigDecimal;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BordaChoiceRankBean {

    protected PollenEntityId<Choice> choiceId;

    protected int rank;

    protected BigDecimal score;

    public BordaChoiceRankBean() {
        choiceId = PollenEntityId.newId(Choice.class);
    }

    public void fromResult(BordaChoiceRank result) {

        setChoiceId(result.getChoiceId());
        setRank(result.getRank());
        setScore(result.getScore());
    }

    public PollenEntityId<Choice> getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(PollenEntityId<Choice> choiceId) {
        this.choiceId = choiceId;
    }

    public void setChoiceId(String choiceId) {
        this.choiceId.setEntityId(choiceId);
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }
}
