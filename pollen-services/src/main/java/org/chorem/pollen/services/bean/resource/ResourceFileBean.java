package org.chorem.pollen.services.bean.resource;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.services.PollenTechnicalException;

import javax.sql.rowset.serial.SerialBlob;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

/**
 * Created on 11/07/14.
 *
 * @author dralagen
 */
public class ResourceFileBean extends AbstractResourceBean {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ResourceFileBean.class);

    protected File file;

    public ResourceFileBean() {
        super(PollenResource.class);
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public Blob getResourceBlob() {
        try {
            InputStream stream;
            try {
                stream = new GZIPInputStream(new FileInputStream(getFile()));
            } catch (ZipException e) {
                stream = new FileInputStream(getFile());
            }

            return new SerialBlob(IOUtils.toByteArray(stream));

        } catch (SQLException | IOException e) {
            throw new PollenTechnicalException(e);
        }
    }

}
