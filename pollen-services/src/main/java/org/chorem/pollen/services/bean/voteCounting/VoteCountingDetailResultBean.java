package org.chorem.pollen.services.bean.voteCounting;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.services.bean.voteCounting.Coombs.CoombsDetailResultBean;
import org.chorem.pollen.services.bean.voteCounting.InstantRunoff.InstantRunoffDetailResultBean;
import org.chorem.pollen.services.bean.voteCounting.borda.BordaDetailResultBean;
import org.chorem.pollen.services.bean.voteCounting.condorcet.CondorcetDetailResultBean;
import org.chorem.pollen.services.bean.voteCounting.majorityJugment.MajorityJudgmentDetailResultBean;
import org.chorem.pollen.votecounting.BordaDetailResult;
import org.chorem.pollen.votecounting.CondorcetDetailResult;
import org.chorem.pollen.votecounting.CoombsDetailResult;
import org.chorem.pollen.votecounting.InstantRunoffDetailResult;
import org.chorem.pollen.votecounting.MajorityJudgmentDetailResult;
import org.chorem.pollen.votecounting.model.VoteCountingDetailResult;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class VoteCountingDetailResultBean {

    public static VoteCountingDetailResultBean toBean(VoteCountingDetailResult detailResult) {
        VoteCountingDetailResultBean bean = null;

        if (detailResult != null) {

            if (CondorcetDetailResult.class.isInstance(detailResult)) {

                CondorcetDetailResultBean condorcetDetailResultBean = new CondorcetDetailResultBean();
                condorcetDetailResultBean.fromResult(CondorcetDetailResult.class.cast(detailResult));
                bean = condorcetDetailResultBean;

            } else if (BordaDetailResult.class.isInstance(detailResult)) {

                BordaDetailResultBean bordaDetailResultBean = new BordaDetailResultBean();
                bordaDetailResultBean.fromResult(BordaDetailResult.class.cast(detailResult));
                bean = bordaDetailResultBean;

            } else if (CoombsDetailResult.class.isInstance(detailResult)) {

                CoombsDetailResultBean coombsDetailResultBean = new CoombsDetailResultBean();
                coombsDetailResultBean.fromResult(CoombsDetailResult.class.cast(detailResult));
                bean = coombsDetailResultBean;

            } else if (InstantRunoffDetailResult.class.isInstance(detailResult)) {

                InstantRunoffDetailResultBean instantRunoffDetailResultBean = new InstantRunoffDetailResultBean();
                instantRunoffDetailResultBean.fromResult(InstantRunoffDetailResult.class.cast(detailResult));
                bean = instantRunoffDetailResultBean;
                
            } else if (MajorityJudgmentDetailResult.class.isInstance(detailResult)) {

                MajorityJudgmentDetailResultBean majorityJudgmentDetailResultBean = new MajorityJudgmentDetailResultBean();
                majorityJudgmentDetailResultBean.fromResult(MajorityJudgmentDetailResult.class.cast(detailResult));
                bean = majorityJudgmentDetailResultBean;

            }
        }

        return bean;
    }

}
