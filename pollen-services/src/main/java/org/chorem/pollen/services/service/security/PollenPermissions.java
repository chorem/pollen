package org.chorem.pollen.services.service.security;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Vote;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollenPermissions {

    public static PollenPermission addPoll() {
        return SecurityService::canAddPoll;
    }

    public static PollenPermission read(Poll poll) {
        return securityService -> securityService.canRead(poll);
    }

    public static PollenPermission readComments(Poll poll) {
        return securityService -> securityService.canReadComments(poll);
    }

    public static PollenPermission readParticipants(Poll poll) {
        return securityService -> securityService.canReadParticipants(poll);
    }

    public static PollenPermission readParticipants(Question question) {
        return securityService -> securityService.canReadParticipants(question);
    }

    public static PollenPermission read(Vote vote) {
        return securityService -> securityService.canRead(vote);
    }

    public static PollenPermission readResult(Question question) {
        return securityService -> securityService.canReadResult(question);
    }

    public static PollenPermission readResult(Poll poll) {
        return securityService -> securityService.canReadResult(poll);
    }

    public static PollenPermission addComment(Poll poll) {
        return securityService -> securityService.canAddComment(poll);
    }

    public static PollenPermission addVote(Question question) {
        return securityService -> securityService.canAddVote(question);
    }

    public static PollenPermission addVote(Poll poll) {
        return securityService -> securityService.canAddVote(poll);
    }

    public static PollenPermission edit(Poll poll) {
        return securityService -> securityService.canEdit(poll);
    }

    public static PollenPermission edit(Choice choice) {
        return securityService -> securityService.canEdit(choice);
    }

    public static PollenPermission edit(Comment comment) {
        return securityService -> securityService.canEdit(comment);
    }

    public static PollenPermission edit(Vote vote) {
        return securityService -> securityService.canEdit(vote);
    }
    public static PollenPermission edit(Question question) {
        return securityService -> securityService.canEdit(question);
    }

    public static PollenPermission delete(Poll poll) {
        return securityService -> securityService.canDelete(poll);
    }

    public static PollenPermission delete(Choice choice) {
        return securityService -> securityService.canDelete(choice);
    }

    public static PollenPermission delete(Comment comment) {
        return securityService -> securityService.canDelete(comment);
    }

    public static PollenPermission delete(Vote vote) {
        return securityService -> securityService.canDelete(vote);
    }

    public static PollenPermission close(Poll poll) {
        return securityService -> securityService.canClose(poll);
    }

    public static PollenPermission clone(Poll poll) {
        return securityService -> securityService.canClone(poll);
    }

    public static PollenPermission export(Poll poll) {
        return securityService -> securityService.canExport(poll);
    }

    public static PollenPermission read(Question question) {
        return securityService -> securityService.canRead(question);
    }

    public static PollenPermission readComments(Question question) {
        return securityService -> securityService.canReadComments(question);
    }

    public static PollenPermission readVotes(Question question) {
        return securityService -> securityService.canReadVotes(question);
    }

    public static PollenPermission readVotes(Poll poll) {
        return securityService -> securityService.canReadVotes(poll);
    }

    public static PollenPermission addChoice(Question question) {
        return securityService -> securityService.canAddChoice(question);
    }

    public static PollenPermission addComment(Question question) {
        return securityService -> securityService.canAddComment(question);
    }
}
