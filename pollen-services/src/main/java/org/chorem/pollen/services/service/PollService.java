package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.chorem.pollen.persistence.entity.ChoiceType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Polls;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.persistence.entity.VoterListMember;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.QuestionBean;
import org.chorem.pollen.services.bean.ReportResumeBean;
import org.chorem.pollen.services.bean.VoterListBean;
import org.chorem.pollen.services.bean.VoterListMemberBean;
import org.chorem.pollen.services.bean.export.ExportBean;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.security.PollenPermissions;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollService extends PollenServiceSupport {

    public static final String EMAIL_SUFFIX_SEPARATOR = ";";

    public PollBean toPollBean(Poll entity) {
        PollBean bean = new PollBean();

        bean.setEntityId(entity.getTopiaId());

        PollenPrincipal creator = entity.getCreator();

        if (creator == null || creator.getPermission() == null) {

            bean.setPermission(null);

        } else {

            bean.setPermission(creator.getPermission().getToken());

        }

        if (creator != null) {

            bean.setCreatorName(creator.getName());
            bean.setCreatorEmail(creator.getEmail());

        }

        bean.setTitle(entity.getTitle());
        bean.setDescription(entity.getDescription());
        bean.setCreateDate(entity.getTopiaCreateDate());
        bean.setBeginDate(entity.getBeginDate());
        bean.setEndDate(entity.getEndDate());
        bean.setAnonymousVoteAllowed(entity.isAnonymousVoteAllowed());
        bean.setContinuousResults(entity.isContinuousResults());
        bean.setPollType(entity.getPollType());
        bean.setVoteVisibility(entity.getVoteVisibility());
        bean.setCommentVisibility(entity.getCommentVisibility());
        bean.setResultVisibility(entity.getResultVisibility());
        bean.setNotifyMeHoursBeforePollEnds(entity.getNotifyMeHoursBeforePollEnds());
        bean.setVoteNotification(entity.isVoteNotification());
        bean.setCommentNotification(entity.isCommentNotification());
        bean.setNewChoiceNotification(entity.isNewChoiceNotification());
        bean.setNotificationLocale(entity.getNotificationLocale());
        if(entity.getPicture() != null) {
            bean.setPicture(getPollenResourceService().getReduceIdByTopiaId(
                    entity.getPicture().getTopiaId()));
        }
        String emailAddressSuffixes = entity.getEmailAddressSuffixes();
        if (StringUtils.isNotBlank(emailAddressSuffixes)) {
            bean.setEmailAddressSuffixes(Lists.newArrayList(emailAddressSuffixes.split(PollService.EMAIL_SUFFIX_SEPARATOR)));
        }

        Date now = getNow();
        if (Polls.isFinished(entity, now)) {
            bean.setStatus(PollBean.PollStatus.CLOSED);
            bean.setClosed(true);
        } else {
            bean.setClosed(false);
            bean.setStatus(PollBean.PollStatus.CREATED);

            Date beginDate = entity.getBeginDate();
            Date endDate = entity.getEndDate();
            if (beginDate != null && now.after(beginDate) && (endDate == null || now.before(endDate))) {
                bean.setStatus(PollBean.PollStatus.VOTING);
            }

        }

        /* Questions and choices */
        QuestionService questionService = getQuestionService();
        if (bean.getEntityId() != null) {
            List<QuestionBean> questions = questionService.getQuestions(bean.getEntityId());
            bean.setQuestions(questions);
        }

        /* Check adding choices */
        //XXX JC-200512 adding choice status should be on question instead of poll
        Date beginAddChoiceDate = null;
        Date endAddChoiceDate = null;
        Boolean addChoiceEnabled = false;
        if (bean.getQuestions() != null) {
            for (QuestionBean question : bean.getQuestions()) {
                addChoiceEnabled = addChoiceEnabled || question.isChoiceAddAllowed();
                if (question.getBeginChoiceDate() != null && (beginAddChoiceDate == null || question.getBeginChoiceDate().before(beginAddChoiceDate))) {
                    beginAddChoiceDate = question.getBeginChoiceDate();
                }
                if (question.getEndChoiceDate() != null && (endAddChoiceDate == null || question.getEndChoiceDate().after(endAddChoiceDate))) {
                    endAddChoiceDate = question.getEndChoiceDate();
                }
            }
        }

        if (addChoiceEnabled &&
            (
                (beginAddChoiceDate == null && (endAddChoiceDate == null || now.before(endAddChoiceDate))) ||
                (beginAddChoiceDate != null && now.after(beginAddChoiceDate) && (endAddChoiceDate == null || now.before(endAddChoiceDate)))
            )
        ){
            bean.setStatus(PollBean.PollStatus.ADDING_CHOICES);
        }


        if (isNotPermitted(PollenPermissions.edit(entity))) {
            bean.setPermission(null);
            bean.setCreatorEmail(null);

        } else {
            ReportResumeBean report = getReportService().getReport(entity.getTopiaId());
            bean.setReport(report);
        }

        boolean commentIsVisible = entity.getTopiaId() != null && isPermitted(PollenPermissions.readComments(entity));
        bean.setCommentIsVisible(commentIsVisible);

        bean.setVoteIsVisible(entity.getTopiaId() != null && isPermitted(PollenPermissions.readVotes(entity)));

        boolean resultIsVisible = entity.getTopiaId() != null && isPermitted(PollenPermissions.readResult(entity));

        bean.setResultIsVisible(resultIsVisible);

        boolean canReadParticipants = entity.getTopiaId() != null && isPermitted(PollenPermissions.readParticipants(entity));
        bean.setParticipantsIsVisible(canReadParticipants);

        boolean canVote = entity.getTopiaId() != null && isPermitted(PollenPermissions.addVote(entity));

        bean.setCanVote(canVote);
        if (entity.isPersisted()) {
            if (commentIsVisible) {
                long commentCount = getCommentService().getCommentCount(entity.getTopiaId());
                bean.setCommentCount(commentCount);
            }

            if (Polls.isPollRestricted(entity)) {
                long participantCount = getVoterListService().getVoterListMemberCount(entity);
                bean.setParticipantCount(participantCount);
                long participantInvitedCount = getVoterListService().getVoterListMemberInvitedCount(entity);
                bean.setParticipantInvitedCount(participantInvitedCount);
            }
        }

        bean.setGtuValidated(getGtuService().isGtuValidated(entity));
        bean.setMaxVoters(getMaxVoters(entity));

        if (creator!= null && creator.getPollenUser() != null && creator.getPollenUser().getAvatar() != null) {
            bean.setCreatorAvatar(getPollenResourceService().getReduceIdByTopiaId(
                    creator.getPollenUser().getAvatar().getTopiaId()));
        }

        return bean;
    }

    protected int getMaxVoters(Poll poll) {
        boolean premium = poll.isPremium() || getUserService().isPremium(poll.getCreator().getPollenUser());
        return premium ? 0 : getPollenServiceConfig().getMaxVoters();
    }

    public PaginationResultBean<PollBean> getPolls(PaginationParameterBean paginationParameter, String search) {

        checkIsAdmin();

        PaginationParameter page = getPaginationParameter(paginationParameter);
        PaginationResult<Poll> polls = getPollDao().findAll(page, search);
        return toPaginationListBean(polls, this::toPollBean);

    }

    public PaginationResultBean<PollBean> getAllPolls(PaginationParameterBean paginationParameter, String search, String filter) {

        PollenUser connectedUser = checkAndGetConnectedUser();

        PaginationParameter page = getPaginationParameter(paginationParameter);
        PaginationResult<Poll> polls = getPollDao().findAllUserPolls(connectedUser, page, search, filter);
        return toPaginationListBean(polls, this::toPollBean);

    }

    public PaginationResultBean<PollBean> getCreatedPolls(PaginationParameterBean paginationParameter, String search, String filter) {

        PollenUser connectedUser = checkAndGetConnectedUser();

        PaginationParameter page = getPaginationParameter(paginationParameter);
        PaginationResult<Poll> polls = getPollDao().findAllCreated(connectedUser, page, search, filter);
        return toPaginationListBean(polls, this::toPollBean);

    }

    public PaginationResultBean<PollBean> getInvitedPolls(PaginationParameterBean paginationParameter, String search, String filter) {

        PollenUser connectedUser = checkAndGetConnectedUser();

        PaginationParameter page = getPaginationParameter(paginationParameter);
        PaginationResult<Poll> polls = getPollDao().findAllInvited(connectedUser, page, search, filter);
        return toPaginationListBean(polls, this::toPollBean);

    }

    public PaginationResultBean<PollBean> getParticipatedPolls(PaginationParameterBean paginationParameter, String search, String filter) {

        PollenUser connectedUser = checkAndGetConnectedUser();

        PaginationParameter page = getPaginationParameter(paginationParameter);
        PaginationResult<Poll> polls = getPollDao().findAllParticipated(connectedUser, page, search, filter);
        return toPaginationListBean(polls, this::toPollBean);

    }

    public PollBean getPoll(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        return toPollBean(poll);

    }

    public PollBean getNewPoll(ChoiceType choiceType) {
        checkIsConnectedRequired();
        checkPermission(PollenPermissions.addPoll());

        PollBean pollBean = new PollBean();
        QuestionBean questionBean = new QuestionBean();

        // -- default values-- //

        PollenServicesConfig pollenServiceConfig = getPollenServiceConfig();
        int voteCountingType = pollenServiceConfig.getDefaultVoteCountingType();
        questionBean.setVoteCountingType(voteCountingType);
        questionBean.setVoteCountingConfig(getVoteCountingService().newConfig(voteCountingType));
        pollBean.addQuestion(questionBean);
        pollBean.setPollType(pollenServiceConfig.getDefaultPollType());
        pollBean.setVoteVisibility(pollenServiceConfig.getDefaultVoteVisibility());
        pollBean.setCommentVisibility(pollenServiceConfig.getDefaultCommentVisibility());
        pollBean.setResultVisibility(pollenServiceConfig.getDefaultResultVisibility());
        pollBean.setContinuousResults(pollenServiceConfig.getDefaultContinuousResults());
        pollBean.setVoteNotification(pollenServiceConfig.getDefaultVoteNotification());
        pollBean.setCommentNotification(pollenServiceConfig.getDefaultCommentNotification());
        pollBean.setNewChoiceNotification(pollenServiceConfig.getDefaultNewChoiceNotification());
        pollBean.setNotifyMeHoursBeforePollEnds(pollenServiceConfig.getDefaultNotifyMeHoursBeforePollEnds());

        // -- creator -- //

        PollenUser connectedUser = getConnectedUser();

        if (connectedUser != null) {

            pollBean.setCreatorName(connectedUser.getName());
            if (connectedUser.getDefaultEmailAddress() != null) {
                pollBean.setCreatorEmail(connectedUser.getDefaultEmailAddress().getEmailAddress());
            }
            pollBean.setGtuValidated(getGtuService().isGtuValidated(connectedUser));

        }

        boolean premium = getUserService().isPremium(connectedUser);
        pollBean.setMaxVoters(premium ? 0 : getPollenServiceConfig().getMaxVoters());

        return pollBean;

    }

    public PollenEntityRef<Poll> createPoll(PollBean poll) throws InvalidFormException {
        return createPoll(poll, Collections.emptyList(), Collections.emptyList());
    }

    public PollenEntityRef<Poll> createPoll(PollBean poll,
                                            List<VoterListBean> voterLists,
                                            List<VoterListMemberBean> voterListMembers) throws InvalidFormException {
        checkIsConnectedRequired();
        checkPermission(PollenPermissions.addPoll());

        checkNotNull(poll);
        checkIsNotPersisted(poll);

        ErrorMap errorMap = checkPoll(poll);
        errorMap.failIfNotEmpty();

        if (poll.getPollType() == PollType.RESTRICTED) {
            getVoterListService().checkVoters(null, voterLists, voterListMembers);
        }

        Poll savedPoll = savePoll(poll);

        if (poll.getPollType() == PollType.RESTRICTED) {

            getVoterListService().createVoters(savedPoll, voterLists, voterListMembers);
        }
        
        commit();

        getNotificationService().onPollCreated(savedPoll);
        getFeedService().onPollCreated(savedPoll);

        return PollenEntityRef.of(savedPoll);

    }

    public PollBean editPoll(PollBean poll) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(poll);
        checkIsPersisted(poll);

        // FIXME 01/06/2018 SBavencoff Evite les modifications concurantes en BD du sondage
        synchronized (getLock(poll.getEntityId())) {
            Poll pollDb = getPoll0(poll.getEntityId());
            checkPermission(PollenPermissions.edit(pollDb));

            ErrorMap errorMap = checkPoll(poll);
            errorMap.failIfNotEmpty();

            Poll savedPoll = savePoll(poll);

            commit();

            getNotificationService().onPollEdited(savedPoll);
            getFeedService().onPollEdited(savedPoll);

            return toPollBean(savedPoll);
        }

    }

    public void deletePoll(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        Poll poll = getPoll0(pollId);
        checkPermission(PollenPermissions.delete(poll));

        getPollDao().delete(poll);

        commit();

        getNotificationService().onPollDeleted(poll);

    }

    public PollenEntityRef<Poll> clonePoll(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPoll0(pollId);
        checkPermission(PollenPermissions.clone(poll));

        // Clone Poll
        PollBean clonedPoll = toPollBean(poll);

        clonedPoll.setEntityId(null);
        clonedPoll.setPermission(null);
        clonedPoll.setBeginDate(serviceContext.getNow());
        clonedPoll.setEndDate(null);

        clonedPoll.setTitle(clonedPoll.getTitle() + " (clone)");

        //Clone questions
        //TODO

        // Clone Choices
        //TODO attach to question
        List<ChoiceBean> clonedChoices = getChoiceService().getChoices(pollId);
        for (ChoiceBean choice : clonedChoices) {
            choice.setEntityId(null);
            choice.setPermission(null);
        }

        Poll savedPoll = savePoll(clonedPoll);

        commit();

        getNotificationService().onPollCreated(savedPoll);

        return PollenEntityRef.of(savedPoll);

    }

    public Date closePoll(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        // FIXME 01/06/2018 SBavencoff Evite les modifications concurantes en BD du sondage
        synchronized (getLock(pollId)) {

            Poll poll = getPoll0(pollId);
            checkPermission(PollenPermissions.close(poll));

            poll.setEndDate(getNow());

            commit();

            getNotificationService().onPollClosed(poll);
            getFeedService().onPollClosed(poll);

            return poll.getEndDate();
        }
    }

    public void reopenPoll(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        // FIXME 01/06/2018 SBavencoff Evite les modifications concurantes en BD du sondage
        synchronized (getLock(pollId)) {
            Poll poll = getPoll0(pollId);
            checkPermission(PollenPermissions.close(poll));

            poll.setEndDate(null);

            commit();

            getNotificationService().onPollReopened(poll);
            getFeedService().onPollReopened(poll);
        }

    }


    public ExportBean exportPoll(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        Poll poll = getPoll0(pollId);
        checkPermission(PollenPermissions.export(poll));

        //TODO
        return null;

    }

    public PollBean assignPollToConnectedUser(String pollId) throws InvalidFormException {

        PollenUser connectedUser = checkAndGetConnectedUser();

        checkNotNull(pollId);
        Poll poll = getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        PollenUser creator = poll.getCreator().getPollenUser();

        ErrorMap errorMap = new ErrorMap();
        check(errorMap,
                "poll",
                creator == null || creator.equals(connectedUser),
                l(getLocale(), "pollen.error.poll.assign.already"));

        errorMap.failIfNotEmpty();

        if (creator == null) {
            poll.getCreator().setPollenUser(connectedUser);
            commit();
        }

        return toPollBean(poll);
    }

    public Collection<Poll> getPollsWithReminderNeeded() {
        return getPollDao().findPollsWithReminderNeeded();
    }

    protected Poll savePoll(PollBean poll) {

        boolean pollExists = poll.isPersisted();

        List<Question> existingQuestions;

        Poll toSave;

        if (pollExists) {

            toSave = getPoll0(poll.getEntityId());

            existingQuestions = getQuestionDao().findAll(toSave);

        } else {

            toSave = getPollDao().create();

            PollenPrincipal creatorToPersist =
                    getSecurityService().generatePollenPrincipal();

            // -- creator -- //

            PollenUser connectedUser = getConnectedUser();
            if (connectedUser != null) {
                // link to connected user
                creatorToPersist.setPollenUser(connectedUser);

            } else if (getSecurityContext().getMainPrincipal() == null) {
                getSecurityContext().setMainPrincipal(creatorToPersist);
            }

            toSave.setCreator(creatorToPersist);

            existingQuestions = new ArrayList<>();

        }

        toSave.getCreator().setName(poll.getCreatorName());
        toSave.getCreator().setEmail(poll.getCreatorEmail());

        // -- simple properties -- //

        toSave.setVoteVisibility(poll.getVoteVisibility());
        toSave.setCommentVisibility(poll.getCommentVisibility());
        toSave.setResultVisibility(poll.getResultVisibility());
        toSave.setAnonymousVoteAllowed(poll.isAnonymousVoteAllowed());
        toSave.setContinuousResults(poll.isContinuousResults());
        toSave.setDescription(poll.getDescription());
        toSave.setPollType(poll.getPollType());
        toSave.setTitle(poll.getTitle());
        toSave.setVoteNotification(poll.isVoteNotification());
        toSave.setCommentNotification(poll.isCommentNotification());
        toSave.setNewChoiceNotification(poll.isNewChoiceNotification());
        toSave.setNotifyMeHoursBeforePollEnds(poll.getNotifyMeHoursBeforePollEnds());
        toSave.setNotificationLocale(poll.getNotificationLocale());
        if(StringUtils.isNotBlank(poll.getPicture())) {
            toSave.setPicture(getPollenResourceService().getResource0(getPollenResourceService().getTopiaIdByReduceId(poll.getPicture())));
        }
        toSave.setBeginDate(poll.getBeginDate());
        toSave.setEndDate(poll.getEndDate());

        String suffixes = null;
        if (CollectionUtils.isNotEmpty(poll.getEmailAddressSuffixes())) {
            suffixes = poll.getEmailAddressSuffixes().stream()
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.joining(PollService.EMAIL_SUFFIX_SEPARATOR));

        }
        toSave.setEmailAddressSuffixes(suffixes);

        if (toSave.getBeginDate() == null) {
            toSave.setBeginDate(getNow());
        }

        if (toSave.getEndDate() != null && toSave.getNotifyMeHoursBeforePollEnds() > 0 &&
            DateUtils.addHours(new Date(), toSave.getNotifyMeHoursBeforePollEnds()).before(toSave.getEndDate())) {
            toSave.setPollEndReminderSent(false);
        }

        // -- questions -- //
        if (CollectionUtils.isNotEmpty(poll.getQuestions())) {

            QuestionService questionService = getQuestionService();

            for (QuestionBean question : poll.getQuestions()) {
                Question persistedQuestion = questionService.saveQuestion(toSave, question);
                existingQuestions.remove(persistedQuestion);
            }

            // the remaining questions have been deleted by the user and must be removed
            if (CollectionUtils.isNotEmpty(existingQuestions)) {
                getQuestionDao().deleteAll(existingQuestions);
            }
        }


        // GTU
        PollenUser connectedUser = getConnectedUser();
        if (connectedUser == null) {
            if (!getGtuService().isGtuValidated(toSave) && poll.isGtuValidated()) {
                toSave.setGtuValidationDate(getNow());
            }
        } else {
            if (!getGtuService().isGtuValidated(connectedUser) && poll.isGtuValidated()) {
                connectedUser.setGtuValidationDate(getNow());
            }
        }

        return toSave;

    }

    protected Poll getPoll0(String pollId) {

        return getPollDao().forTopiaIdEquals(pollId).findUnique();

    }

    protected ErrorMap checkPoll(PollBean poll) {

        ErrorMap errors = new ErrorMap();

        checkNotNull(errors, Poll.PROPERTY_POLL_TYPE, poll.getPollType(), l(getLocale(), "pollen.error.poll.pollType.mandatory"));
        checkNotNull(errors, Poll.PROPERTY_COMMENT_VISIBILITY, poll.getCommentVisibility(), l(getLocale(), "pollen.error.poll.commentVisibility.mandatory"));
        checkNotNull(errors, Poll.PROPERTY_VOTE_VISIBILITY, poll.getVoteVisibility(), l(getLocale(), "pollen.error.poll.voteVisibility.mandatory"));
        checkNotNull(errors, Poll.PROPERTY_RESULT_VISIBILITY, poll.getResultVisibility(), l(getLocale(), "pollen.error.poll.resultVisibility.mandatory"));

        checkNotBlank(errors, Poll.PROPERTY_TITLE, poll.getTitle(), l(getLocale(), "pollen.error.poll.title.mandatory"));

        Date beginDate = poll.getBeginDate();
        if (beginDate == null) {
            beginDate = getNow();
        }

        if (poll.getEndDate() != null) {
            check(errors, Poll.PROPERTY_END_DATE, poll.getEndDate().compareTo(beginDate) > 0, l(getLocale(), "pollen.error.poll.endDate.beforeBeginDate"));
        }

        if (poll.getCreatorEmail() != null && !poll.getCreatorEmail().isEmpty()) {
            checkValidEmail(errors, Poll.PROPERTY_CREATOR + "." + PollenPrincipal.PROPERTY_EMAIL, poll.getCreatorEmail(), l(getLocale(), "pollen.error.poll.creator.email.invalid"));
        }

        checkQuestions(errors, poll.getQuestions());


        return errors;

    }

    protected void checkQuestions(ErrorMap errorMap, List<QuestionBean> questions) {

        boolean questionsNotEmpty = CollectionUtils.isNotEmpty(questions);

        check(errorMap, "questions", questionsNotEmpty, l(getLocale(), "pollen.error.poll.choice.mandatory"));

        if (questionsNotEmpty) {

            List<Question> existingQuestions = new ArrayList<>();

            for (int i = 0; i < questions.size(); i++) {

                QuestionBean question = questions.get(i);

                ErrorMap questionErrors = getQuestionService().checkQuestion(existingQuestions, question);
                questionErrors.copyTo(errorMap, "questions[" + i + "].");

                existingQuestions.add(getQuestionService().toQuestion(question));

            }
        }
    }



    protected PaginationParameter getPaginationParameter(PaginationParameterBean paginationParameter) {

        if (paginationParameter == null) {

            paginationParameter = PaginationParameterBean.of(0, PAGE_SIZE_DEFAULT);

        }
        if (paginationParameter.getOrder() == null) {
            paginationParameter.setOrder(Poll.PROPERTY_TITLE);
        }
        return paginationParameter.toPaginationParameter();

    }

    public Set<String> getInvalidEmails(String pollId) {
        Set<String> invalidEmails = Sets.newHashSet();
        Poll poll = getPoll0(pollId);

        if (isPermitted(PollenPermissions.edit(poll))) {

            if (poll.getCreator().isInvalid()) {
                invalidEmails.add(poll.getCreator().getEmail());
            }

            if (Polls.isPollRestricted(poll)) {
                getVoterListMemberDao()
                        .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                        .addEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_INVALID, true)
                        .findAll()
                        .stream()
                        .map(voterListMember -> voterListMember.getMember().getEmail())
                        .forEach(invalidEmails::add);
            }

        }

        return invalidEmails;
    }
}
