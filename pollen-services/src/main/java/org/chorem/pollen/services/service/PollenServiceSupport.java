package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.entity.*;
import org.chorem.pollen.services.PollenService;
import org.chorem.pollen.services.PollenServiceContext;
import org.chorem.pollen.services.PollenUIContext;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenBean;
import org.chorem.pollen.services.bean.PollenBeans;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.mail.EmailService;
import org.chorem.pollen.services.service.security.PollenInvalidPermissionException;
import org.chorem.pollen.services.service.security.PollenPermission;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.chorem.pollen.services.service.security.SecurityService;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.util.StringUtil;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;

/**
 * PollenServiceSupport
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public abstract class PollenServiceSupport implements PollenService {

    public static final int PAGE_SIZE_DEFAULT = 10;

    private static Map<String, Object> ENTITY_LOCK_BY_ID = Maps.newHashMap();

    public static synchronized Object getLock(String id) {
        return ENTITY_LOCK_BY_ID. computeIfAbsent(id, id2 -> new Object());
    }

    // -- PollenServiceSupport -- //

    protected PollenServiceContext serviceContext;

    @Override
    public void setServiceContext(PollenServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    // -- Delegate serviceContext -- //

    protected Date getNow() {
        return serviceContext.getNow();
    }

    protected PollenSecurityContext getSecurityContext() {
        return serviceContext.getSecurityContext();
    }

    protected String getCleanMail(String email) {
        return serviceContext.getCleanMail(email);
    }

    protected PollenPersistenceContext getPersistenceContext() {
        return serviceContext.getPersistenceContext();
    }

    protected PollenServicesConfig getPollenServiceConfig() {
        return serviceContext.getPollenServicesConfig();
    }

    protected Locale getLocale() {
        return serviceContext.getLocale();
    }

    // -- Services -- //

    public ChoiceService getChoiceService() {
        return newService(ChoiceService.class);
    }

    protected CommentService getCommentService() {
        return newService(CommentService.class);
    }

    protected TransverseService getConfigurationService() {
        return newService(TransverseService.class);
    }

    protected FavoriteListService getFavoriteListService() {
        return newService(FavoriteListService.class);
    }

    protected EmailService getEmailService() {
        return newService(EmailService.class);
    }

    protected NotificationService getNotificationService() {
        return newService(NotificationService.class);
    }

    protected FeedService getFeedService() {
        return newService(FeedService.class);
    }

    protected GtuService getGtuService() {
        return newService(GtuService.class);
    }

    protected PollService getPollService() {
        return newService(PollService.class);
    }

    protected QuestionService getQuestionService() {
        return newService(QuestionService.class);
    }

    protected SecurityService getSecurityService() {
        return newService(SecurityService.class);
    }

    protected ReportService getReportService() {
        return newService(ReportService.class);
    }

    protected PollenResourceService getPollenResourceService() {
        return newService(PollenResourceService.class);
    }

    protected PollenUIUrlRenderService getPollenUIUrlRenderService() {
        return newService(PollenUIUrlRenderService.class);
    }

    protected PollenUserService getUserService() {
        return newService(PollenUserService.class);
    }

    protected VoteCountingService getVoteCountingService() {
        return newService(VoteCountingService.class);
    }

    protected VoterListService getVoterListService() {
        return newService(VoterListService.class);
    }

    protected VoteService getVoteService() {
        return newService(VoteService.class);
    }

    protected CryptoService getCryptoService() {
        return newService(CryptoService.class);
    }

    protected <E extends PollenService> E newService(Class<E> serviceClass) {
        return serviceContext.newService(serviceClass);
    }

    // -- delegate persistenceContext method -- //

    protected ChoiceTopiaDao getChoiceDao() {
        return getPersistenceContext().getChoiceDao();
    }

    protected CommentTopiaDao getCommentDao() {
        return getPersistenceContext().getCommentDao();
    }

    protected FavoriteListTopiaDao getFavoriteListDao() {
        return getPersistenceContext().getFavoriteListDao();
    }

    protected FavoriteListMemberTopiaDao getFavoriteListMemberDao() {
        return getPersistenceContext().getFavoriteListMemberDao();
    }

    protected ChildFavoriteListTopiaDao getChildFavoriteListDao() {
        return getPersistenceContext().getChildFavoriteListDao();
    }

    protected PollTopiaDao getPollDao() {
        return getPersistenceContext().getPollDao();
    }

    protected QuestionTopiaDao getQuestionDao() {
        return getPersistenceContext().getQuestionDao();
    }

    protected PollenPrincipalTopiaDao getPollenPrincipalDao() {
        return getPersistenceContext().getPollenPrincipalDao();
    }

    protected PollenResourceTopiaDao getPollenResourceDao() {
        return getPersistenceContext().getPollenResourceDao();
    }

    protected PollenUserTopiaDao getPollenUserDao() {
        return getPersistenceContext().getPollenUserDao();
    }

    protected PollenUserEmailAddressTopiaDao getPollenUserEmailAddressDao() {
        return getPersistenceContext().getPollenUserEmailAddressDao();
    }

    protected UserCredentialTopiaDao getUserCredentialDao() {
        return getPersistenceContext().getUserCredentialDao();
    }

    protected ReportTopiaDao getReportTopiaDao() {
        return getPersistenceContext().getReportDao();
    }

    protected SessionTokenTopiaDao getSessionTokenDao() {
        return getPersistenceContext().getSessionTokenDao();
    }

    protected VoteTopiaDao getVoteDao() {
        return getPersistenceContext().getVoteDao();
    }

    protected VoteToChoiceTopiaDao getVoteToChoiceDao() {
        return getPersistenceContext().getVoteToChoiceDao();
    }

    protected VoterListTopiaDao getVoterListDao() {
        return getPersistenceContext().getVoterListDao();
    }

    protected VoterListMemberTopiaDao getVoterListMemberDao() {
        return getPersistenceContext().getVoterListMemberDao();
    }

    protected LoginProviderTopiaDao getLoginProviderDao() {
        return getPersistenceContext().getLoginProviderDao();
    }

    protected EmailToResendTopiaDao getEmailToResendDao() {
        return getPersistenceContext().getEmailToResendDao();
    }

    public void commit() {
        getPersistenceContext().commit();
    }

    public void rollback() {
        getPersistenceContext().rollback();
    }

    // -- check method -- //

    public void checkIsConnected() {
        getSecurityService().checkIsConnected();
    }

    public void checkIsConnectedRequired() {
        getSecurityService().checkIsConnectedRequired();
    }

    public void checkIsConnected(String userId) {
        getSecurityService().checkIsConnected(userId);
    }

    public void checkIsAdmin() {
        getSecurityService().checkIsAdmin();
    }

    protected boolean isPermitted(PollenPermission permission) {
        return permission.isPermitted(getSecurityService());
    }

    protected boolean isNotPermitted(PollenPermission permission) {
        return !isPermitted(permission);
    }

    protected void checkPermission(PollenPermission permission) {
        if (isNotPermitted(permission)) {
            throw new PollenInvalidPermissionException(permission.toString());
        }
    }

    protected void checkNotNull(Object object) {

        Preconditions.checkNotNull(object);

    }

    protected void checkIsPersisted(TopiaEntity entity) {

        Preconditions.checkState(entity.isPersisted());

    }

    protected void checkIsNotPersisted(TopiaEntity entity) {

        Preconditions.checkState(!entity.isPersisted());

    }

    protected void checkIsPersisted(PollenBean bean) {

        Preconditions.checkState(bean.isPersisted());

    }

    protected void checkIsNotPersisted(PollenBean bean) {

        Preconditions.checkState(!bean.isPersisted());

    }

    protected void checkState(boolean test) {

        Preconditions.checkState(test);

    }

    protected boolean check(Multimap<String, String> errors, String field, boolean condition, String error) {

        if (!condition) {
            errors.put(field, error);
        }
        return condition;

    }

    protected boolean checkNot(Multimap<String, String> errors, String field, boolean condition, String error) {

        return check(errors, field, !condition, error);

    }

    protected boolean checkNotNull(Multimap<String, String> errors, String field, Object value, String error) {

        return check(errors, field, value != null, error);

    }

    protected boolean checkEmpty(Multimap<String, String> errors, String field, Collection<?> value, String error) {

        return check(errors, field, CollectionUtils.isEmpty(value), error);

    }

    protected boolean checkNotEmpty(Multimap<String, String> errors, String field, Collection<?> value, String error) {

        return check(errors, field, CollectionUtils.isNotEmpty(value), error);

    }

    protected boolean checkNotBlank(Multimap<String, String> errors, String field, String value, String error) {

        return check(errors, field, StringUtils.isNotBlank(value), error);

    }

    protected boolean checkValidEmail(Multimap<String, String> errors, String field, String value, String error) {

        return check(errors, field, StringUtil.isEmail(value), error);

    }

    protected boolean checkNot(ErrorMap errors, String field, boolean condition, String error) {

        return check(errors, field, !condition, error);

    }

    protected boolean checkNotNull(ErrorMap errors, String field, Object value, String error) {

        return check(errors, field, value != null, error);

    }

    protected boolean checkEmpty(ErrorMap errors, String field, Collection<?> value, String error) {

        return check(errors, field, CollectionUtils.isEmpty(value), error);

    }

    protected boolean checkNotEmpty(ErrorMap errors, String field, Collection<?> value, String error) {

        return check(errors, field, CollectionUtils.isNotEmpty(value), error);

    }

    protected boolean checkNotBlank(ErrorMap errors, String field, String value, String error) {

        return check(errors, field, StringUtils.isNotBlank(value), error);

    }

    protected boolean checkValidEmail(ErrorMap errors, String field, String value, String error) {

        return check(errors, field, StringUtil.isEmail(value), error);

    }

    protected boolean checkEmailPattern(ErrorMap errors, String field, String value, String error) {

        String emailAddressPatternForRegistration = getPollenServiceConfig().getEmailAddressPatternForRegistration();
        return check(errors,
                     field,
                     emailAddressPatternForRegistration == null ||
                     value != null && value.matches(emailAddressPatternForRegistration),
                     error);

    }

    protected boolean check(ErrorMap errors, String field, boolean condition, String error) {

        if (!condition) {
            errors.addError(field, error);
        }
        return condition;

    }

    protected PollenUser getConnectedUser() {

        return serviceContext.getSecurityContext().getPollenUser();

    }

    protected PollenUser checkAndGetConnectedUser() {
        checkIsConnected();
        return getConnectedUser();
    }

    protected PollenUIContext getUIContext() {

        return serviceContext.getUIContext();

    }

    protected <T extends TopiaEntity, B extends PollenBean<T>> ImmutableList<B> toBeanList(Collection<T> entities, Function<T, B> beanFunction) {

        return PollenBeans.toBeanList(entities, beanFunction);

    }

    protected <T extends TopiaEntity, B extends PollenBean<T>> ImmutableSet<B> toBeanSet(Collection<T> entities, Function<T, B> beanFunction) {

        return PollenBeans.toBeanSet(entities, beanFunction);

    }

    protected <E extends TopiaEntity, B extends PollenBean<E>> PaginationResultBean<B> toPaginationListBean(PaginationResult<E> entities, Function<E, B> beanFunction) {

        return PollenBeans.toBean(entities, beanFunction);

    }

    protected <E extends TopiaEntity> PollenEntityId<E> getPollenEntityId(E entity) {
        PollenEntityId<E> pollenEntityId = PollenEntityId.newId((Class<E>) entity.getClass());
        pollenEntityId.setEntityId(entity.getTopiaId());
        pollenEntityId.encode(serviceContext.getTopiaApplicationContext().getTopiaIdFactory());
        return pollenEntityId;
    }

    public <E extends TopiaEntity> String getReduceId(E entity) {
        return PollenEntityId.encode(serviceContext.getTopiaApplicationContext().getTopiaIdFactory(), entity.getTopiaId());
    }

    protected <E extends TopiaEntity> E findEntity(Class<E> type, String reduceId) {
        TopiaIdFactory topiaIdFactory = serviceContext.getTopiaApplicationContext().getTopiaIdFactory();

        String topiaId = topiaIdFactory.newTopiaId(type, reduceId);

        TopiaDao<E> dao = getPersistenceContext().getDao(type);
        return dao.forTopiaIdEquals(topiaId).findUnique();
    }

    protected String getPollVoteUrl(Poll poll) {
        return getPollVoteUrl(getPollenEntityId(poll), null);
    }

    protected String getPollVoteUrl(Poll poll, String token) {
        return getPollVoteUrl(getPollenEntityId(poll), token);
    }

    protected String getPollVoteUrl(PollenEntityId<Poll> pollId, String token) {
        return getPollenUIUrlRenderService().getPollVoteUrl
                (getUIContext().getPollVoteUrl(),
                 pollId.getReducedId(),
                 token);
    }

    protected PaginationParameter getPaginationParameter(PaginationParameterBean paginationParameter) {

        if (paginationParameter == null) {

            paginationParameter = PaginationParameterBean.of(
                    0,
                    PAGE_SIZE_DEFAULT,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    true);

        }

        return paginationParameter.toPaginationParameter();

    }

}
