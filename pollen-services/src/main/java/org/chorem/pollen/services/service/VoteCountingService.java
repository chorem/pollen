package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Polls;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.persistence.entity.VoteToChoice;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.persistence.entity.VoterListMember;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.ListVoteCountingResultBean;
import org.chorem.pollen.services.bean.VoteBean;
import org.chorem.pollen.services.bean.VoteCountingResultBean;
import org.chorem.pollen.services.service.security.PollenPermissions;
import org.chorem.pollen.votecounting.VoteCounting;
import org.chorem.pollen.votecounting.VoteCountingFactory;
import org.chorem.pollen.votecounting.VoteCountingStrategy;
import org.chorem.pollen.votecounting.model.ListOfVoter;
import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.SimpleVoter;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;
import org.chorem.pollen.votecounting.model.VoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteForChoice;
import org.chorem.pollen.votecounting.model.Voter;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoteCountingService extends PollenServiceSupport {

    public VoteCountingResultBean getMainResult(String questionId) {
        checkIsConnectedRequired();

        Preconditions.checkNotNull(questionId);

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.readResult(question));

        VoteCountingResultBean mainResult;

        if (Polls.isPollRestricted(question.getPoll())) {

            ListVoteCountingResultBean groupResult = getGroupResult(questionId);
            mainResult = groupResult.getMainResult();

        } else {
            mainResult = getSimpleResult(questionId);

        }

        return mainResult;
    }

    public <S extends VoteCountingStrategy<C>, C extends VoteCountingConfig> C newConfig(int voteCountingType) {

        VoteCountingFactory voteCountingFactory = serviceContext.getVoteCountingFactory();

        VoteCounting<S, C> voteCounting = voteCountingFactory.getVoteCounting(voteCountingType);

        Class<C> configType = voteCounting.getConfigType();

        try {
            return configType.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new PollenTechnicalException(e);
        }
    }

    protected <C extends VoteCountingConfig> C configFromJson(String json, Class<C> configType) {
        Gson gson = new GsonBuilder().create();
        if (json == null || "null".equals(json)) {
            try {
                return configType.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new PollenTechnicalException(e);
            }
        }
        return gson.fromJson(json, configType);
    }

    protected <C extends VoteCountingConfig> String configToJson(C config) {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(config);
    }

    public <S extends VoteCountingStrategy<C>, C extends VoteCountingConfig> C getVoteCountingConfig(Question question) {
        VoteCounting<S, C> voteCounting = getVoteCounting(question);
        return configFromJson(question.getVoteCountingConfig(), voteCounting.getConfigType());
    }

    public <S extends VoteCountingStrategy<C>, C extends VoteCountingConfig> VoteCountingStrategy<C> getVoteCountingStrategy(Question question) {
        VoteCounting<S, C> voteCounting = getVoteCounting(question);
        VoteCountingStrategy<C> strategy = voteCounting.newStrategy();
        C config = configFromJson(question.getVoteCountingConfig(), voteCounting.getConfigType());
        strategy.setConfig(config);

        return strategy;
    }

    public VoteCountingResultBean getSimpleResult(String questionId) {

        Preconditions.checkNotNull(questionId);

        Question question = getQuestionService().getQuestion0(questionId);
        List<Vote> votes = getVotes(question);

        VoteCountingStrategy strategy = getVoteCountingStrategy(question);

        ListOfVoter groupOfVoter = toSimpleVotersGroup(votes);

        ListVoteCountingResult groupVoteCountingResult = strategy.votecount(groupOfVoter);

        VoteCountingResult mainResult = groupVoteCountingResult.getMainResult();

        VoteCountingResultBean result = new VoteCountingResultBean();
        result.fromResult(mainResult);

        return result;
    }

    protected List<Vote> getVotes(Question question) {
        List<Vote> votes = getVoteService().getVotes0(question);
        int maxVoters = getPollService().getMaxVoters(question.getPoll());
        if (maxVoters > 0 && votes.size() > maxVoters) {
            votes = votes.subList(0, maxVoters);
        }
        return votes;
    }

    public ListVoteCountingResultBean getGroupResult(String questionId) {

        Preconditions.checkNotNull(questionId);

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.readResult(question));
        VoterList mainVoterList = getVoterListService().getMainVoterList0(question.getPoll());
        List<Vote> votes = getVotes(question);

        VoteCountingStrategy strategy = getVoteCountingStrategy(question);

        // Create a groupVoter including of the root voters
        ListOfVoter listOfVoter = toListOfVoters(question, mainVoterList, votes);

        ListVoteCountingResult groupVoteCountingResult = strategy.votecount(listOfVoter);


        ListVoteCountingResultBean result = new ListVoteCountingResultBean();
        result.fromResult(groupVoteCountingResult);

        return result;
    }


    /**
     * Collect all vote for all voters of the given poll.
     * <p/>
     * The result (a set of simple voters) are stored in a fictiv group of voter.
     * <p/>
     * <strong>Note:</strong> Even if the poll is a group type, then no of his
     * group are used (just collect simple voters with no group linkage).
     *
     * @param votes votes to scan
     * @return the aggregate group of voter containing simple voters with
     * their votes.
     */
    protected ListOfVoter toSimpleVotersGroup(List<Vote> votes) {

        Preconditions.checkNotNull(votes);

        Set<Voter> voters = Sets.newHashSet();

        for (Vote vote : votes) {

            SimpleVoter simpleVoter = toSimpleVoter(vote);
            voters.add(simpleVoter);
        }

        return ListOfVoter.newVoter(null, 1.0, voters);
    }

    /**
     * Build the group of voter of the question, using his group to build sub
     * group of the question.
     * <p/>
     * <strong>Note:</strong> This method can only be used for a group poll.
     *
     * @param question       the question to scan
     * @param voterList voterList to scan
     * @param votes      votes to scan
     * @return the aggregate group of group of voters with their votes
     */
    protected ListOfVoter toListOfVoters(Question question, VoterList voterList, List<Vote> votes) {

        Preconditions.checkNotNull(question);
        Preconditions.checkNotNull(voterList);
        Preconditions.checkNotNull(votes);
        Preconditions.checkArgument(Polls.isPollRestricted(question.getPoll()),
                                    "Can only use this method for a restricted poll");

        return toListOfVoters(voterList, votes);
    }

    protected ListOfVoter toListOfVoters(VoterList voterList, List<Vote> votes) {

        Set<Voter> voters = Sets.newHashSet();

        List<VoterList> subVoterLists = getVoterListDao().forParentEquals(voterList).findAll();

        for (VoterList subVoterList : subVoterLists) {

            ListOfVoter subListOfVoter = toListOfVoters(subVoterList, votes);
            voters.add(subListOfVoter);
        }

        List<VoterListMember> voterListMembers = getVoterListService().getVoterListMembers0(voterList);
        for (VoterListMember voterListMember : voterListMembers) {

            Optional<Vote> voteOptional = votes.stream()
                    .filter(vote -> vote.getVoterListMember().contains(voterListMember))
                    .findFirst();

            if (voteOptional.isPresent()) {

                SimpleVoter simpleVoter = toSimpleVoter(voteOptional.get());
                simpleVoter.setWeight(voterListMember.getWeight());
                voters.add(simpleVoter);

            }

        }

        return ListOfVoter.newVoter(voterList.getTopiaId(), voterList.getWeight(), voters);
    }

    protected SimpleVoter toSimpleVoter(Vote vote) {

        Set<VoteForChoice> voteForChoices = Sets.newHashSet();

        if (!vote.isVoteToChoiceEmpty()) {

            for (VoteToChoice voteToChoice : vote.getVoteToChoice()) {

                VoteForChoice voteForChoice = VoteForChoice.newVote(
                        voteToChoice.getChoice().getTopiaId(),
                        voteToChoice.getVoteValue());

                voteForChoices.add(voteForChoice);

            }

        }

        return SimpleVoter.newVoter(vote.getVoter().getTopiaId(), vote.getWeight(), voteForChoices);
    }

    protected SimpleVoter toSimpleVoter(VoteBean vote) {

        Set<VoteForChoice> voteForChoices = vote.getChoice()
                .stream()
                .map(voteToChoice -> VoteForChoice.newVote(
                        voteToChoice.getChoiceId().getEntityId() ,
                        voteToChoice.getVoteValue()))
                .collect(Collectors.toSet());

        return SimpleVoter.newVoter(vote.getVoterId().getEntityId(), vote.getWeight(), voteForChoices);
    }

    protected <S extends VoteCountingStrategy<C>, C extends VoteCountingConfig> VoteCounting<S, C> getVoteCounting(Question question) {

        int id = question.getVoteCountingType();

        VoteCountingFactory voteCountingFactory = serviceContext.getVoteCountingFactory();
        VoteCounting<S, C> result = voteCountingFactory.getVoteCounting(id);

        Preconditions.checkNotNull(
                result, "Could not find vote counting for id " + id);

        return result;
    }

    protected <C extends VoteCountingConfig> ErrorMap checkVote(VoteBean vote, VoteCounting<?, C> voteCounting, List<ChoiceBean> choices, C config) {
        ErrorMap errorMap = new ErrorMap();

        SimpleVoter simpleVoter = toSimpleVoter(vote);

        errorMap.addAllErrors(voteCounting.checkVote(simpleVoter, config, getLocale()));
        return errorMap;
    }

}
