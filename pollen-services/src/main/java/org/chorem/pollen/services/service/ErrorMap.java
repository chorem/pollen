package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;

/**
 * Created on 5/7/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class ErrorMap {

    protected final Multimap<String, String> errors;

    public ErrorMap() {

        errors = ArrayListMultimap.create();

    }

    public void addError(String key, String error) {

        errors.put(key, error);

    }

    public void addErrors(String key, Iterable<String> error) {

        errors.putAll(key, error);

    }

    public void addAllErrors(ErrorMap errorMap) {
        errors.putAll(errorMap.getErrors());
    }


    public void addAllErrors(Multimap<String, String> errors) {
        this.errors.putAll(errors);
    }

    public void copyTo(ErrorMap errorMap, String prefix) {

        for (String key : errors.keySet()) {

            Collection<String> errors = this.errors.get(key);

            errorMap.addErrors(prefix + key, errors);
        }

    }

    public boolean isEmpty() {

        return errors.isEmpty();

    }

    public Multimap<String, String> getErrors() {
        return errors;
    }

    public void failIfNotEmpty() throws InvalidFormException {

        if (!errors.isEmpty()) {

            throw new InvalidFormException(errors);
        }
    }
}
