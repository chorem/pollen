package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndContentImpl;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndFeedImpl;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.SyndFeedOutput;
import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.service.security.PollenPermissions;

import java.io.StringReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.0
 */
public class FeedService extends PollenServiceSupport {

    public static final String FEED_TYPE = "atom_1.0";
    public static final String DESCRIPTION_TYPE = "text/plain";

    public void onPollCreated(Poll poll) {
        SyndFeed feed = new SyndFeedImpl();
        feed.setFeedType(FEED_TYPE);

        feed.setTitle(poll.getTitle());
        String voteUrl = getPollVoteUrl(poll);
        feed.setLink(voteUrl);
        feed.setDescription(poll.getDescription());

        SyndEntry entry = new SyndEntryImpl();
        entry.setTitle(t("pollen.service.feed.pollCreated.title", poll.getCreator().getName(), poll.getTitle()));
        entry.setLink(voteUrl);
        entry.setPublishedDate(poll.getTopiaCreateDate());

        SyndContent description = new SyndContentImpl();
        description.setType(DESCRIPTION_TYPE);
        description.setValue(t("pollen.service.feed.pollCreated.description",
                               DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(poll.getTopiaCreateDate()),
                               poll.getCreator().getName(),
                               poll.getTitle()));
        entry.setDescription(description);

        List<SyndEntry> feedEntries = Collections.singletonList(entry);
        feed.setEntries(feedEntries);

        saveFeed(poll, feed);
    }

    public void onPollEdited(Poll poll) {
        addFeedEntry(poll, t("pollen.service.feed.pollEdited.title"));
    }

    public void onPollClosed(Poll poll) {
        addFeedEntry(poll, t("pollen.service.feed.pollClosed.title"));
    }

    public void onPollReopened(Poll poll) {
        addFeedEntry(poll, t("pollen.service.feed.pollReopened.title"));
    }

    public void onChoiceAdded(Question question, Choice choice) {
        addFeedEntry(question.getPoll(), t("pollen.service.feed.choiceAdded.title"));
    }

    public void onVoteAdded(Question question, Vote vote) {
        addVoteFeedEntry(question.getPoll(), vote, n("pollen.service.feed.voteAdded.title"));
    }

    public void onVoteEdited(Question question, Vote vote) {
        addVoteFeedEntry(question.getPoll(), vote, n("pollen.service.feed.voteEdited.title"));
    }

    public void onVoteDeleted(Question question, Vote vote) {
        addVoteFeedEntry(question.getPoll(), vote, n("pollen.service.feed.voteDeleted.title"));
    }

    public void onCommentAdded(Poll poll, Comment comment) {
        addFeedEntry(poll, t("pollen.service.feed.commentAdded.title", comment.getAuthor()));
    }

    public void onCommentEdited(Poll poll, Comment comment) {
        addFeedEntry(poll, t("pollen.service.feed.commentEdited.title", comment.getAuthor()));
    }

    public void onCommentDeleted(Poll poll, Comment comment) {
        addFeedEntry(poll, t("pollen.service.feed.commentDeleted.title", comment.getAuthor()));
    }

    public void onCommentAdded(Question question, Comment comment) {
        //FIXME JC181002 - FeedEntry spécifique pour questions
        addFeedEntry(question.getPoll(), t("pollen.service.feed.commentAdded.title", comment.getAuthor()));
    }

    public void onCommentEdited(Question question, Comment comment) {
        //FIXME JC181002 - FeedEntry spécifique pour questions
        addFeedEntry(question.getPoll(), t("pollen.service.feed.commentEdited.title", comment.getAuthor()));
    }

    public void onCommentDeleted(Question question, Comment comment) {
        //FIXME JC181002 - FeedEntry spécifique pour questions
        addFeedEntry(question.getPoll(), t("pollen.service.feed.commentDeleted.title", comment.getAuthor()));
    }

    public SyndFeed getFeedForPoll(String pollId) {
        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        if (StringUtils.isBlank(poll.getFeedContent())) {
            // uniquement pour les sondages créé avant la génération de flux RSS
            onPollCreated(poll);
        }

        try {
            SyndFeedInput feedInput = new SyndFeedInput(true, getLocale());
            return feedInput.build(new StringReader(poll.getFeedContent()));

        } catch (FeedException e) {
            throw new PollenTechnicalException("Error while reading the feed", e);
        }
    }

    public String getFeedContentForPoll(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        return poll.getFeedContent();
    }

    private void addVoteFeedEntry(Poll poll, Vote vote, String messageKey) {
        String voterName;
        if (vote.isAnonymous()) {
            voterName = t("pollen.service.feed.anonymous");
        } else {
            voterName = vote.getVoter().getName();
        }
        addFeedEntry(poll, t(messageKey, voterName));
    }

    private void addFeedEntry(Poll poll, String title) {
        SyndFeed feed = getFeedForPoll(poll.getTopiaId());
        List<SyndEntry> entries = new ArrayList<>(feed.getEntries());

        SyndEntry entry = new SyndEntryImpl();
        entry.setTitle(title);
        entry.setLink(getPollVoteUrl(poll));
        entry.setPublishedDate(new Date());

        entries.add(entry);
        feed.setEntries(entries);

        saveFeed(poll, feed);
    }

    private void saveFeed(Poll poll, SyndFeed feed) {
        try {
            SyndFeedOutput output = new SyndFeedOutput();
            String feedContent = output.outputString(feed);
            poll.setFeedContent(feedContent);
            commit();

        } catch (FeedException e) {
            throw new PollenTechnicalException("Error while creating the feed", e);
        }
    }
}
