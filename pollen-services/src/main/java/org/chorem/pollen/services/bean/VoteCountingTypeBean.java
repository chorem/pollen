package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.votecounting.model.ChoiceToVoteRenderType;

/**
 * Created on 03/07/14.
 *
 * @author agarandel
 * @since 2.0
 */
public class VoteCountingTypeBean {

    protected int id;

    protected String name;

    protected String helper;

    protected String shortHelper;

    protected String renderType;

    protected Double minimumValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelper() {
        return helper;
    }

    public void setHelper(String helper) {
        this.helper = helper;
    }

    public String getShortHelper() {
        return shortHelper;
    }

    public void setShortHelper(String shortHelper) {
        this.shortHelper = shortHelper;
    }

    public String getRenderType() {
        return renderType;
    }

    public void setRenderType(String renderType) {
        this.renderType = renderType;
    }

    public void setRenderType(ChoiceToVoteRenderType renderType) {
        switch (renderType) {
            case CHECKBOX:
                this.renderType = "checkbox";
                break;

            case TEXTFIELD:
                this.renderType = "text";
                break;

            case SELECT:
                this.renderType = "select";
                break;
        }
    }

    public Double getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(Double minimumValue) {
        this.minimumValue = minimumValue;
    }
}
