package org.chorem.pollen.services.service.security;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;

import java.io.Serializable;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class DefaultPollenSecurityContext implements Serializable, PollenSecurityContext {

    private static final long serialVersionUID = 1L;

    /**
     * Connected user account.
     * <p/>
     * Can be {@code null} if user is not connected.
     */
    protected PollenUser pollenUser;

    /**
     * Main principal (mainly to acquire credentials on a data).
     * <p/>
     * Can be {@code null} if no credentials is required.
     */
    protected PollenPrincipal mainPrincipal;

    @Override
    public PollenUser getPollenUser() {
        return pollenUser;
    }

    @Override
    public void setPollenUser(PollenUser pollenUser) {
        this.pollenUser = pollenUser;
    }

    @Override
    public PollenPrincipal getMainPrincipal() {
        return mainPrincipal;
    }

    @Override
    public boolean isConnected() {
        return getPollenUser() != null;
    }

    @Override
    public boolean isAdmin() {
        return getPollenUser() != null && getPollenUser().isAdministrator();
    }

    @Override
    public void setMainPrincipal(PollenPrincipal mainPrincipal) {
        this.mainPrincipal = mainPrincipal;
    }
}
