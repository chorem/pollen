package org.chorem.pollen.services.bean.export;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.persistence.entity.FavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteListImpl;
import org.chorem.pollen.services.bean.FavoriteListBean;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FavoriteListExport {

    protected String name;

    protected List<ChildFavoriteListExport> children;

    protected List<FavoriteListMemberExport> members;

    public static FavoriteListExport fromEntity(FavoriteList entity) {
        FavoriteListExport export = new FavoriteListExport();

        export.setName(entity.getName());

        return export;
    }

    public FavoriteList toEntity() {

        FavoriteList entity = new FavoriteListImpl();
        entity.setName(getName());

        return entity;

    }

    public FavoriteListBean toBean() {

        FavoriteListBean bean = new FavoriteListBean();
        bean.setName(getName());

        return bean;

    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public List<ChildFavoriteListExport> getChildren() {
        if (children == null) {
            children = Lists.newLinkedList();
        }
        return children;
    }

    public void setChildren(List<ChildFavoriteListExport> children) {
        this.children = children;
    }

    public List<FavoriteListMemberExport> getMembers() {
        if (members == null) {
            members = Lists.newLinkedList();
        }
        return members;
    }

    public void setMembers(List<FavoriteListMemberExport> members) {
        this.members = members;
    }
}
