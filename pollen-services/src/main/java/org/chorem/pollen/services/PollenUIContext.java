package org.chorem.pollen.services;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class PollenUIContext implements Serializable {

    private String uiEndPoint;

    private String userValidateUrl;

    private String pollVoteUrl;

    private String pollVoteEditUrl;

    private String pollEditUrl;

    private String resourceUrl;

    private String resourceDownloadUrl;

    private String profileUrl;

    private String offersUrl;

    private String timeZone;

    public String getUiEndPoint() {
        return uiEndPoint;
    }

    public void setUiEndPoint(String uiEndPoint) {
        this.uiEndPoint = uiEndPoint;
    }

    public String getUserValidateUrl() {
        return userValidateUrl;
    }

    public void setUserValidateUrl(String userValidateUrl) {
        this.userValidateUrl = userValidateUrl;
    }

    public String getPollVoteUrl() {
        return pollVoteUrl;
    }

    public void setPollVoteUrl(String pollVoteUrl) {
        this.pollVoteUrl = pollVoteUrl;
    }

    public String getPollVoteEditUrl() {
        return pollVoteEditUrl;
    }

    public void setPollVoteEditUrl(String pollVoteEditUrl) {
        this.pollVoteEditUrl = pollVoteEditUrl;
    }

    public String getPollEditUrl() {
        return pollEditUrl;
    }

    public void setPollEditUrl(String pollEditUrl) {
        this.pollEditUrl = pollEditUrl;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getResourceDownloadUrl() {
        return resourceDownloadUrl;
    }

    public void setResourceDownloadUrl(String resourceDownloadUrl) {
        this.resourceDownloadUrl = resourceDownloadUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getOffersUrl() {
        return offersUrl;
    }

    public void setOffersUrl(String offersUrl) {
        this.offersUrl = offersUrl;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
