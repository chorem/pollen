package org.chorem.pollen.services.service.mail;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.PollenUser;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Kevin Morin - morin@kevinmorin.net
 * @since 3.0
 */
public class UserAccountEmailValidatedEmail extends PollenMail {

    private PollenUser user;

    private String validateUrl;
    private String pollenUrl;
    private String password;

    protected UserAccountEmailValidatedEmail(Locale locale, TimeZone timeZone) {
        super(locale, timeZone);
    }

    @Override
    public String getSubject() {
        return I18n.l(locale, "pollen.service.mail.UserAccountEmailValidatedEmail.subject", user.getName());
    }

    public PollenUser getUser() {
        return user;
    }

    public void setUser(PollenUser user) {
        this.user = user;
    }

    public String getValidateUrl() {
        return validateUrl;
    }

    public void setValidateUrl(String validateUrl) {
        this.validateUrl = validateUrl;
    }

    public String getPollenUrl() {
        return pollenUrl;
    }

    public void setPollenUrl(String pollenUrl) {
        this.pollenUrl = pollenUrl;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public List<TopiaEntity> getEntitiesKey() {
        return Collections.singletonList(user);
    }
}
