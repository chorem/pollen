package org.chorem.pollen.services.service.mail;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.nuiton.topia.persistence.TopiaEntity;

import jakarta.mail.internet.InternetAddress;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

/**
 * Created on 4/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public abstract class PollenMail {

    public static String formatDate(Date date, Locale locale, TimeZone timeZone) {
        String result = null;
        if (date != null) {
            DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, locale);
            formatter.setTimeZone(timeZone);
            result = formatter.format(date);
        }
        return result;
    }

    protected final Locale locale;

    protected final TimeZone timeZone;

    protected Set<String> tos;

    protected Set<String> bccs;

    protected String signing;

    public PollenMail(Locale locale, TimeZone timeZone){
        this.locale = locale;
        this.timeZone = timeZone;
    }

    public Locale getLocale() {
        return locale;
    }

    public Set<String> getTos() {
        if (tos == null) {
            tos = Sets.newHashSet();
        }
        return tos;
    }

    public void addTo(String email) {
        getTos().add(email);
    }

    public abstract String getSubject();

    public Set<String> getBccs() {
        if (bccs == null) {
            bccs = Sets.newHashSet();
        }
        return bccs;
    }

    public void addBcc(String email) {
        getBccs().add(email);
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public boolean isRecipientProvided() {
        return CollectionUtils.isNotEmpty(getBccs())
                || CollectionUtils.isNotEmpty(getTos());
    }

    protected String formatDate(Date date) {
        return formatDate(date, locale, timeZone);
    }

    public String getSigning() {
        return signing;
    }

    public void setSigning(String signing) {
        this.signing = signing;
    }

    public String getFromName() {
        return null;
    }

    public abstract List<TopiaEntity> getEntitiesKey();

    public Collection<InternetAddress> getReplyTo() {
        return Collections.emptyList();
    }
}
