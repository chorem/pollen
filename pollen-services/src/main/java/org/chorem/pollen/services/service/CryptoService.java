package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.chorem.pollen.services.PollenTechnicalException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;

public class CryptoService extends PollenServiceSupport {

    private static final Log log = LogFactory.getLog(CryptoService.class);

    public static final String PROVIDER = "BC";

    static final SecureRandom SECURE_RANDOM = new SecureRandom();

    static {
        if (Security.getProvider(PROVIDER) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public PGPPublicKey getPublicKey(String email) {

        PGPPublicKey pgpPublicKey = null;

        Optional<String> publicKeyString = getPollenUserEmailAddressDao()
                .forEmailAddressEquals(email)
                .tryFindUnique()
                .toJavaUtil()
                .map(PollenUserEmailAddress::getPgpPublicKey);
        if (publicKeyString.isPresent()) {
            try {
                InputStream publicKeyInput = new ByteArrayInputStream(publicKeyString.get().getBytes());

                pgpPublicKey = readPublicKey(publicKeyInput);

            } catch (IOException | PGPException e) {
                throw new PollenTechnicalException("unable to read PGP public key for email : " + email, e);
            }

        }

        return pgpPublicKey;
    }

    protected PGPPublicKey readPublicKey(InputStream input) throws IOException, PGPException {
        PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(
                PGPUtil.getDecoderStream(input), new JcaKeyFingerprintCalculator());

        //
        // we just loop through the collection till we find a key suitable for encryption, in the real
        // world you would probably want to be a bit smarter about this.
        //

        Iterator keyRingIter = pgpPub.getKeyRings();
        while (keyRingIter.hasNext()) {
            PGPPublicKeyRing keyRing = (PGPPublicKeyRing)keyRingIter.next();

            Iterator keyIter = keyRing.getPublicKeys();
            while (keyIter.hasNext())
            {
                PGPPublicKey key = (PGPPublicKey)keyIter.next();

                if (key.isEncryptionKey())
                {
                    return key;
                }
            }
        }

        throw new IllegalArgumentException("Can't find encryption key in key ring.");
    }

    protected byte[] compressMessage(byte[] message) throws IOException {

        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        OutputStream pOut = null;
        PGPCompressedDataGenerator comData = null;

        // we want to generate compressed data. This might be a user option later,
        // in which case we would pass in bOut.
        try {
            comData = new PGPCompressedDataGenerator(CompressionAlgorithmTags.ZIP);
            try (OutputStream cos = comData.open(bOut)) {
                PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();

                pOut = lData.open(cos, // the compressed output stream
                        PGPLiteralData.BINARY,
                        PGPLiteralDataGenerator.CONSOLE,  // "filename" to store
                        message.length, // length of clear data
                        new Date()  // current time
                );
                pOut.write(message);
            }
        } catch(Exception e) {
            log.error(e);
            throw e;
        } finally {
            if (pOut != null) {
                pOut.close();
            }
            if (comData != null) {
                comData.close();
            }
        }

        return bOut.toByteArray();
    }

    protected byte[] encryptMessage(byte[] message, PGPPublicKey pgpPublicKey) throws IOException, PGPException {

        byte[] compressMessage = compressMessage(message);

        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        OutputStream out = null;
        OutputStream encOut = null;
        
        try {
            out = new ArmoredOutputStream(bout);
            
            PGPEncryptedDataGenerator encryptedDataGenerator = createEncryptedDataGenerator(pgpPublicKey);
    
            encOut = encryptedDataGenerator.open(out, compressMessage.length);
    
            encOut.write(compressMessage);
            
        } catch (Exception e) {
            log.error(e);
            throw e;
        } finally {
            
            if (encOut != null) {
                encOut.close();
            }
            if (out != null) {
                out.close();
            }
        }

        return bout.toByteArray();

    }

    private PGPEncryptedDataGenerator createEncryptedDataGenerator(PGPPublicKey pgpPublicKey) {
        JcePGPDataEncryptorBuilder encryptorBuilder = new JcePGPDataEncryptorBuilder(SymmetricKeyAlgorithmTags.CAST5)
                .setSecureRandom(SECURE_RANDOM)
                .setWithIntegrityPacket(true)
                .setProvider(PROVIDER);

        PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(encryptorBuilder);

        JcePublicKeyKeyEncryptionMethodGenerator method = new JcePublicKeyKeyEncryptionMethodGenerator(pgpPublicKey)
                .setProvider(PROVIDER)
                .setSecureRandom(SECURE_RANDOM);

        encryptedDataGenerator.addMethod(method);

        return encryptedDataGenerator;
    }

    public String encryptMailIfAsKey(String body, String email) {

        PGPPublicKey publicKey = getPublicKey(email);

        if (publicKey != null) {

            try {
                byte[] encryptMessage = encryptMessage(body.getBytes(), publicKey);
                return new String(encryptMessage);
            } catch (IOException | PGPException e) {
                if (log.isErrorEnabled()) {
                    log.error("error on encrypte mail to email", e);
                }
                return body;
            }

        }

        return body;

    }

    public byte[] encryptMessageSymmetric(byte[] message) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", PROVIDER);
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey());
            return cipher.doFinal(message);
        } catch (GeneralSecurityException e) {
            throw new PollenTechnicalException("error on encrypt message", e);
        }
    }

    public byte[] decryptMessageSymmetric(byte[] encryptMessage) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", PROVIDER);
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey());
            return cipher.doFinal(encryptMessage);
        } catch (GeneralSecurityException e) {
            throw new PollenTechnicalException("error on decrypt message", e);
        }
    }

    protected SecretKey getSecretKey() {

        byte[] keyBytes = getPollenServiceConfig().getTokenSecretBytes();

        if (keyBytes.length != 16 && keyBytes.length != 24 && keyBytes.length != 32) {
            throw new PollenTechnicalException("keyBytes wrong length for AES key");
        }

        return new SecretKeySpec(keyBytes, "AES");
    }

}
