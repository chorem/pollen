package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.bean.FeedbackBean;
import org.chorem.pollen.services.bean.PollenUserBean;
import org.chorem.pollen.services.service.mail.EmailService;
import org.chorem.pollen.services.service.mail.FeedbackEmail;

import java.util.List;
import java.util.Locale;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FeedbackService extends PollenServiceSupport {

    public boolean addFeedback(FeedbackBean feedbackBean, String sessionId) {
        List<String> mailsFeedbackList = getPollenServiceConfig().getMailsFeedbackList();
        Locale locale = getPollenServiceConfig().getFeedbackLocale();

        PollenUser user = getConnectedUser();
        PollenUserBean userBean = null;
        if (user != null) {
            userBean = getUserService().toPollenUserBean(user);
        }


        EmailService emailService = getEmailService();
        FeedbackEmail feedbackEMail = emailService.newFeedbackMail(feedbackBean, userBean, sessionId, locale);

        feedbackEMail.getTos().addAll(mailsFeedbackList);
        emailService.send(feedbackEMail);
        commit();

        return true;
    }

}
