package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.entity.FavoriteList;
import org.chorem.pollen.persistence.entity.FavoriteListMember;
import org.chorem.pollen.persistence.entity.FavoriteListMemberImpl;
import org.nuiton.util.StringUtil;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 6/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class FavoriteListImportFromLdap extends PollenServiceSupport implements FavoriteListImport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FavoriteListImportFromLdap.class);

    protected String ldap;

    public void setLdap(String ldap) {
        this.ldap = ldap;
    }

    @Override
    public void doImport(FavoriteList favoriteList, List<FavoriteListMember> existingFavoriteListMembers) throws FavoriteListImportException {

        checkIsConnected();
        checkNotNull(favoriteList);
        checkNotNull(existingFavoriteListMembers);
        checkNotNull(ldap);

        Locale locale = serviceContext.getLocale();

        Set<String> usedName = Sets.newHashSet();
        Set<String> usedEmail = Sets.newHashSet();

        if (CollectionUtils.isNotEmpty(existingFavoriteListMembers)) {
            for (FavoriteListMember member : existingFavoriteListMembers) {
                usedName.add(member.getName());
                usedEmail.add(member.getEmail());
            }
        }

        List<String> errors = Lists.newLinkedList();
        List<FavoriteListMember> membersToAdd = Lists.newLinkedList();

        // Initialisation du contexte
        Properties env = new Properties();

        Pattern loginLdapPattern = Pattern.compile("ldaps?://(([^:]+):([^@]+)@)?.*");
        Matcher matcher = loginLdapPattern.matcher(ldap);
        if (matcher.find() && matcher.group(1) != null) {
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            if (matcher.group(2) != null) {
                String login = null;
                try {
                    login = URLDecoder.decode(matcher.group(2), Charsets.UTF_8.name());
                    env.put(Context.SECURITY_PRINCIPAL, "cn=" + login);
                } catch (UnsupportedEncodingException e) {
                    errors.add(l(locale, "pollen.error.favoriteList.import.ldap.login.urlEncoding", matcher.group(2)));
                }

            }
            if (matcher.group(3) != null) {
                String password = null;
                try {
                    password = URLDecoder.decode(matcher.group(3), Charsets.UTF_8.name());
                    env.put(Context.SECURITY_CREDENTIALS, password);
                } catch (UnsupportedEncodingException e) {
                    errors.add(l(locale, "pollen.error.favoriteList.import.ldap.password.urlEncoding", matcher.group(3)));
                }
            }
            ldap = ldap.replace(matcher.group(1), "");
        }

        if (CollectionUtils.isEmpty(errors)) {

            try {

                DirContext ictx = new InitialDirContext(env);

                // Recherche en profondeur
                SearchControls control = new SearchControls();
                control.setSearchScope(SearchControls.SUBTREE_SCOPE);

                // Création des comptes avec les résultats de la recherche
                NamingEnumeration<SearchResult> e = ictx.search(ldap, null, control);

                while (e.hasMore()) {

                    SearchResult r = e.next();

                    Attribute attrName = r.getAttributes().get("cn");
                    Attribute attrEmail = r.getAttributes().get("mail");

                    if (attrName != null && attrEmail != null) {

                        boolean error = false;

                        String memberName = attrName.get().toString().trim();

                        if (!usedName.add(memberName)) {

                            error = true;
                            // name already exists
                            errors.add(l(locale, "pollen.error.favoriteList.import.ldap.already.used.name", memberName));

                        }

                        String email = getCleanMail(attrEmail.get().toString());

                        if (!usedEmail.add(email)) {

                            error = true;
                            // email already exists
                            errors.add(l(locale, "pollen.error.favoriteList.import.ldap.already.used.email", email));

                        }

                        if (!StringUtil.isEmail(email)) {

                            error = true;
                            // email is not valid
                            errors.add(l(locale, "pollen.error.favoriteList.import.ldap.invalid.email", email));

                        }

                        if (!error) {
                            FavoriteListMember member = new FavoriteListMemberImpl();
                            member.setName(memberName);
                            member.setEmail(email);
                            member.setWeight(1);
                            member.setFavoriteList(favoriteList);
                            membersToAdd.add(member);
                        }

                    }
                    
                }

            } catch (NamingException ex) {
                errors.add(l(locale, "pollen.error.favoriteList.import.ldap.server", ex.getMessage()));
            }
        }

        if (CollectionUtils.isNotEmpty(errors)) {
            throw new FavoriteListImportException(errors);
        }

        for (FavoriteListMember member : membersToAdd) {
            getFavoriteListMemberDao().create(member);

            if (log.isDebugEnabled()) {
                log.debug(String.format("imported member %s / %s", member.getName(), member.getEmail()));
            }
        }

        commit();

    }
}
