package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.ChoiceType;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class ChoiceBean extends PollenBean<Choice> {

    protected String permission;

    protected int choiceOrder;

    protected String choiceValue;

    protected ChoiceType choiceType;

    protected String description;

    protected boolean choiceIsDeletable;

    protected ReportResumeBean report;

    public ChoiceBean() {
        super(Choice.class);
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getChoiceValue() {
        return choiceValue;
    }

    public void setChoiceValue(String value) {
        this.choiceValue = value;
    }

    public ChoiceType getChoiceType() {
        return choiceType;
    }

    public void setChoiceType(ChoiceType choiceType) {
        this.choiceType = choiceType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getChoiceOrder() {
        return choiceOrder;
    }

    public void setChoiceOrder(int choiceOrder) {
        this.choiceOrder = choiceOrder;
    }

    public boolean isChoiceIsDeletable() {
        return choiceIsDeletable;
    }

    public void setChoiceIsDeletable(boolean choiceIsDeletable) {
        this.choiceIsDeletable = choiceIsDeletable;
    }

    public ReportResumeBean getReport() {
        return report;
    }

    public void setReport(ReportResumeBean report) {
        this.report = report;
    }
}
