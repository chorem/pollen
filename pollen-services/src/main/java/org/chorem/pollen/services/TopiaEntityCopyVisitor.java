package org.chorem.pollen.services;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * To copy entities.
 * FIXME-tchemit-2014-05-08 (Could we move this in ToPIA ?).
 * <p/>
 * Created on 5/8/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class TopiaEntityCopyVisitor<T extends TopiaApplicationContext> implements TopiaEntityVisitor {

    /**
     * Stack of entities to copy.
     * <p/>
     * When a entity is started, the previous current entity goes at the top of the stack.
     * <p/>
     * When a entity is ended, the current entity goes is marked as the last,
     * and the last entity of the stack goes back to the current state.
     */
    protected final Deque<TopiaEntity> stack = new ArrayDeque<>();

    /**
     * The current entity (means the last entity entering in the {@link #start(TopiaEntity)} method).
     */
    protected TopiaEntity current;

    /**
     * The last entity (means the last entity entering in the {@link #end(TopiaEntity)} method).
     */
    protected TopiaEntity last;

    /**
     * To get entity concrete class from a entity instance.
     */
    protected final T topiaApplicationContext;

    /**
     * To get and set properties.
     */
    protected final PropertyUtilsBean propertyUtilsBean;

    /**
     * Cache of already explored entities.
     */
    protected final Collection<String> alreadyExplored;

    public static <T extends TopiaApplicationContext, E extends TopiaEntity> E copy(T topiaApplicationContext, E source) {

        Preconditions.checkNotNull(topiaApplicationContext);
        Preconditions.checkNotNull(source);

        TopiaEntityCopyVisitor<T> visitor = new TopiaEntityCopyVisitor<>(topiaApplicationContext);

        try {

            source.accept(visitor);

            return (E) visitor.last;

        } finally {

            visitor.clear();

        }


    }

    public static <T extends TopiaApplicationContext, E extends TopiaEntity> Collection<E> copy(T topiaApplicationContext, Collection<E> sources) {

        Preconditions.checkNotNull(topiaApplicationContext);
        Preconditions.checkNotNull(sources);

        TopiaEntityCopyVisitor<T> visitor = new TopiaEntityCopyVisitor<>(topiaApplicationContext);

        Collection<E> targets = createCollectionInstance(sources);

        for (E source : sources) {

            source.accept(visitor);

            try {

                E target = visitor.copy(source);

                targets.add(target);

            } finally {

                visitor.clear();

            }

        }

        return targets;

    }

    protected TopiaEntityCopyVisitor(T topiaApplicationContext) {
        this.topiaApplicationContext = topiaApplicationContext;
        this.alreadyExplored = new ArrayList<>();
        this.propertyUtilsBean = new PropertyUtilsBean();
    }

    @Override
    public void start(TopiaEntity source) {

        boolean accept = source != null && acceptEntity(source);

        if (accept) {

            if (current != null) {

                stack.push(current);

            }

            try {

                Class<? extends TopiaEntity> entityClass = source.getClass();

                Class<?> implementationClass = topiaApplicationContext.getImplementationClass(entityClass);

                current = (TopiaEntity) implementationClass.newInstance();
                current.setTopiaId(source.getTopiaId());
                current.setTopiaCreateDate(source.getTopiaCreateDate());
                current.setTopiaVersion(source.getTopiaVersion());
                if (source.isDeleted()) {

                    current.notifyDeleted();

                }

            } catch (InstantiationException | IllegalAccessException e) {

                throw new PollenTechnicalException("could not create a new entity", e);

            }

        }

    }

    @Override
    public void end(TopiaEntity source) {

        boolean accept = source != null && acceptEntity(source);

        if (accept) {

            last = current;

            if (!stack.isEmpty()) {

                current = stack.pop();

            }

        }
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object source) {

        Object target = copy(source);

        setPropertyValue(propertyName, target);

    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object sources) {

        if (sources != null) {

            Collection<Object> targets = createCollectionInstance(sources);

            setPropertyValue(propertyName, targets);

            Collection<?> collectionSource = (Collection<?>) sources;

            for (Object source : collectionSource) {

                Object target = copy(source);

                targets.add(target);

            }

        }

    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object source) {

    }

    @Override
    public void clear() {

        stack.clear();
        alreadyExplored.clear();
        last = null;
        current = null;

    }

    protected <O> O copy(O source) {

        O target;

        if (source != null && acceptEntity(source)) {

            ((TopiaEntity) source).accept(this);

            target = (O) last;

        } else {

            target = source;

        }

        return target;

    }

    protected void setPropertyValue(String propertyName, Object valueToCopy) {

        try {

            propertyUtilsBean.setSimpleProperty(current, propertyName, valueToCopy);

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

            throw new PollenTechnicalException("Could not set property " + propertyName, e);

        }

    }

    protected static <E> Collection<E> createCollectionInstance(Object value) {

        Collection<E> valueToCopy;

        if (value instanceof LinkedHashSet) {

            valueToCopy = new LinkedHashSet<>();

        } else if (value instanceof SortedSet) {

            valueToCopy = new TreeSet<>();

        } else if (value instanceof Set) {

            valueToCopy = new HashSet<>();

        } else {

            valueToCopy = new ArrayList<>();

        }

        return valueToCopy;

    }

    protected boolean acceptEntity(Object entity) {

        return entity instanceof TopiaEntity &&
                alreadyExplored.add(((TopiaEntity) entity).getTopiaId());

    }
}
