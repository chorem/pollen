package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Comment;

import java.util.Date;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class CommentBean extends PollenBean<Comment> {

    protected String permission;

    protected String authorName;

    protected String authorAvatar;

    protected String text;

    protected Date postDate;

    protected ReportResumeBean report;

    public CommentBean() {
        super(Comment.class);
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorAvatar() {
        return authorAvatar;
    }

    public void setAuthorAvatar(String authorAvatar) {
        this.authorAvatar = authorAvatar;
    }

    public ReportResumeBean getReport() {
        return report;
    }

    public void setReport(ReportResumeBean report) {
        this.report = report;
    }
}
