package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationOrder;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * Created on 5/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenBeans {

    public static <E extends TopiaEntity, B extends PollenBean<E>> PaginationResultBean<B> toBean(PaginationResult<E> entities, Function<E, B> beanFunction) {

        ImmutableList<B> bs = toBeanList(entities.getElements(), beanFunction);

        PaginationResultBean<B> result = new PaginationResultBean<>();
        result.setElements(bs);
        result.setPageSize(entities.getCurrentPage().getPageSize());
        result.setCurrentPage(entities.getCurrentPage().getPageNumber());
        result.setLastPage(entities.getLastPage().getPageNumber());
        result.setCount(entities.getCount());
        List<PaginationOrder> orderClauses = entities.getCurrentPage().getOrderClauses();
        if (CollectionUtils.isNotEmpty(orderClauses)) {

            // take the first order clause
            PaginationOrder paginationOrder = orderClauses.get(0);
            result.setOrder(paginationOrder.getClause());
            result.setDesc(paginationOrder.isDesc());

        }


        return result;

    }

    public static <E extends TopiaEntity, B extends PollenBean<E>> ImmutableSet<B> toBeanSet(Collection<E> entities, Function<E, B> beanFunction) {

        return entities.stream().map(beanFunction).collect(ImmutableSet.toImmutableSet());

    }

    public static <E extends TopiaEntity, B extends PollenBean<E>> ImmutableList<B> toBeanList(Collection<E> entities, Function<E, B> beanFunction) {

        return entities.stream().map(beanFunction).collect(ImmutableList.toImmutableList());

    }

}
