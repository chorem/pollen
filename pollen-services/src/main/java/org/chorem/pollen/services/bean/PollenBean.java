package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Objects;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public abstract class PollenBean<E extends TopiaEntity> {

    protected final Class<E> entityType;

    protected PollenEntityId<E> id;

    protected PollenBean(Class<E> entityType) {
        this.entityType = entityType;
        this.id = PollenEntityId.newId(entityType);
    }

    public void setId(PollenEntityId<E> id) {
        if (id == null) {
            this.id.setEntityId(null);
            this.id.setReducedId(null);
        } else {
            this.id = id;
        }

    }

    @JsonIgnore
    public boolean isPersisted() {
        return id != null && id.isNotEmpty() && !id.isTemporaryId();
    }

    @JsonIgnore
    public String getEntityId() {
        return id.getEntityId();
    }

    public PollenEntityId<E> getId() {
        return id;
    }

    @JsonIgnore
    public boolean isTemporaryId() {
        return id.isTemporaryId();
    }

    public void setEntityId(String id) {
        this.id.setEntityId(id);
    }

    @Override
    public boolean equals(Object o) {
        return this == o || o instanceof PollenBean && Objects.equals(id, ((PollenBean) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
