package org.chorem.pollen.services.bean.voteCounting.InstantRunoff;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.services.bean.voteCounting.VoteCountingDetailResultBean;
import org.chorem.pollen.votecounting.InstantRunoffDetailResult;
import org.chorem.pollen.votecounting.InstantRunoffRound;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class InstantRunoffDetailResultBean extends VoteCountingDetailResultBean {

    protected List<InstantRunoffRoundBean> rounds;

    public void fromResult(InstantRunoffDetailResult result) {

        for (InstantRunoffRound instantRunoffRound : result.getRounds()) {

            InstantRunoffRoundBean instantRunoffRoundBean = new InstantRunoffRoundBean();
            instantRunoffRoundBean.fromResult(instantRunoffRound);
            getRounds().add(instantRunoffRoundBean);

        }
    }

    public List<InstantRunoffRoundBean> getRounds() {
        if (rounds == null) {
            rounds = Lists.newLinkedList();
        }
        return rounds;
    }

    public void setRounds(List<InstantRunoffRoundBean> rounds) {
        this.rounds = rounds;
    }
}
