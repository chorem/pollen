package org.chorem.pollen.services.bean.voteCounting.condorcet;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.services.bean.voteCounting.VoteCountingDetailResultBean;
import org.chorem.pollen.votecounting.CondorcetBattle;
import org.chorem.pollen.votecounting.CondorcetDetailResult;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CondorcetDetailResultBean extends VoteCountingDetailResultBean {

    protected List<CondorcetBattleBean> battles;

    public void fromResult(CondorcetDetailResult result) {

        for (CondorcetBattle battle : result.getBattles()) {

            CondorcetBattleBean condorcetBattleBean = new CondorcetBattleBean();
            condorcetBattleBean.fromResult(battle);
            getBattles().add(condorcetBattleBean);

        }
    }

    public List<CondorcetBattleBean> getBattles() {
        if (battles == null) {
            battles = Lists.newLinkedList();
        }
        return battles;
    }

    public void setBattles(List<CondorcetBattleBean> battles) {
        this.battles = battles;
    }
}
