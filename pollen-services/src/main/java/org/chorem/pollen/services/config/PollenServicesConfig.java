package org.chorem.pollen.services.config;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.UsersRight;
import org.chorem.pollen.persistence.entity.CommentVisibility;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.ResultVisibility;
import org.chorem.pollen.persistence.entity.VoteVisibility;
import org.chorem.pollen.services.PollenTechnicalException;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

import java.io.File;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenServicesConfig extends GeneratedPollenServicesConfig {

    public static final String VERSION = "pollen.version";
    public static final String BUILD_DATE = "pollen.buildDate";

    private static final Log log = LogFactory.getLog(PollenServicesConfig.class);

    public PollenServicesConfig(String filename) {
        this(filename, null);
    }

    public PollenServicesConfig(String filename, Properties defaultValues) {
        super();
        ApplicationConfig applicationConfig = get();
        applicationConfig.setAppName("pollen");
        applicationConfig.setConfigFileName(filename);
        if (defaultValues != null) {
            for (Map.Entry<Object, Object> entry : defaultValues.entrySet()) {

                applicationConfig.setOption((String) entry.getKey(), (String) entry.getValue());
            }
        }
        try {
            applicationConfig.parse();
        } catch (ArgumentsParserException e) {
            throw new PollenTechnicalException(e);
        }
        if (log.isInfoEnabled()) {
            StringBuilder builder = new StringBuilder("Pollen configuration:");
            builder.append("\nFilename: ").append(filename);
            List<PollenServicesConfigOption> options = Lists.newArrayList(PollenServicesConfigOption.values());
            for (PollenServicesConfigOption option : options) {
                builder.append(String.format("\n%1$-40s = %2$s",
                                             option.getKey(),
                                             applicationConfig.getOption(String.class, option.getKey())));
            }
            log.info(builder.toString());
        }
        validate();
    }

    private void validate() {
        File dataDirectory = getDataDirectory();
        Preconditions.checkState(dataDirectory != null, "missing configuration directive, please set " + PollenServicesConfigOption.DATA_DIRECTORY.getKey() + " in your pollen-rest-api.properties file");
        if (dataDirectory.exists()) {
            Preconditions.checkArgument(dataDirectory.isDirectory(), dataDirectory + " exists but is not a directory");
            Preconditions.checkArgument(dataDirectory.canWrite(), dataDirectory + " directory exists but it is not writable");
        } else {
            if (log.isInfoEnabled()) {
                log.info("will create " + dataDirectory);
            }
            boolean created = dataDirectory.mkdirs();
            Preconditions.checkState(created, "unable to create " + dataDirectory);
        }
    }

    public Map<String, String> getTopiaProperties() {
        Map<String, String> topiaParameters = Maps.newHashMap();
        Properties properties = get().getOptionStartsWith("hibernate");
        properties.putAll(get().getOptionStartsWith("topia"));
        properties.putAll(get().getOptionStartsWith("jakarta"));

        for (Object o : properties.keySet()) {
            String key = String.valueOf(o);
            String value = get().getOption(String.class, key);
            topiaParameters.put(key, value);
        }
        return topiaParameters;
    }

    public boolean isLogConfigurationProvided() {
        return StringUtils.isNotBlank(get().getOption(PollenServicesConfigOption.LOG_CONFIGURATION_FILE.getKey()));
    }

    public String getHashAlgorithmName() {
        return "SHA-512";
    }

    @Override
    public PollType getDefaultPollType() {
        return PollType.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_POLL_TYPE.getKey()));
    }

    @Override
    public VoteVisibility getDefaultVoteVisibility() {
        return VoteVisibility.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_VOTE_VISIBILITY.getKey()));
    }

    @Override
    public CommentVisibility getDefaultCommentVisibility() {
        return CommentVisibility.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_COMMENT_VISIBILITY.getKey()));
    }

    @Override
    public ResultVisibility getDefaultResultVisibility() {
        return ResultVisibility.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_RESULT_VISIBILITY.getKey()));
    }

    @Override
    public Boolean getDefaultContinuousResults() {
        return Boolean.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_CONTINUOUS_RESULTS.getKey()));
    }

    @Override
    public Boolean getDefaultVoteNotification() {
        return Boolean.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_VOTE_NOTIFICATION.getKey()));
    }

    @Override
    public Boolean getDefaultCommentNotification() {
        return Boolean.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_COMMENT_NOTIFICATION.getKey()));
    }

    @Override
    public Boolean getDefaultNewChoiceNotification() {
        return Boolean.valueOf(get().getOption(PollenServicesConfigOption.DEFAULT_NEW_CHOICE_NOTIFICATION.getKey()));
    }

    public List<String> getMailsFeedbackList() {
        return Lists.newArrayList(get().getOption(PollenServicesConfigOption.MAILS_FEEDBACK.getKey()).split(","));
    }

    public Locale getFeedbackLocale() {
        return Locale.forLanguageTag(get().getOption(PollenServicesConfigOption.LOCALE_FEEDBACK.getKey()));
    }

    public TimeZone getDefaultTimeZone() {
        return TimeZone.getTimeZone(get().getOption(PollenServicesConfigOption.DEFAULT_TIME_ZONE_ID.getKey()));
    }

    @Override
    public UsersRight getUsersCanCreatePoll() {
        return UsersRight.valueOf(get().getOption(PollenServicesConfigOption.USERS_CAN_CREATE_POLL.getKey()));
    }

    public String getVersion() {
        return get().getOption(VERSION);
    }

    public String getBuildDate() {
        return get().getOption(BUILD_DATE);
    }

    public byte[] getTokenSecretBytes() {
        String secret = getTokenSecret();
        return Base64.getDecoder().decode(secret);
    }

}
