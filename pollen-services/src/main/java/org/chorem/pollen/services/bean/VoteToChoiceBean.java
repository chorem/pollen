package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.VoteToChoice;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoteToChoiceBean extends PollenBean<VoteToChoice> {

    protected PollenEntityId<Choice> choiceId;

    protected Double voteValue;

    public VoteToChoiceBean() {
        super(VoteToChoice.class);
    }

    public PollenEntityId<Choice> getChoiceId() {
        if (choiceId == null) {
            choiceId = PollenEntityId.newId(Choice.class);
        }
        return choiceId;
    }

    public void setChoiceId(PollenEntityId<Choice> choiceId) {
        this.choiceId = choiceId;
    }

    public Double getVoteValue() {
        return voteValue;
    }

    public void setVoteValue(Double voteValue) {
        this.voteValue = voteValue;
    }

}
