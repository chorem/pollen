package org.chorem.pollen.services.service.mail;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail2.core.EmailException;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Report;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.service.PollenServiceSupport;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaEntity;

import jakarta.mail.Flags;
import jakarta.mail.Folder;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Store;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.search.FlagTerm;
import java.util.Collections;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MailBoxService extends PollenServiceSupport {

    private static final Log log = LogFactory.getLog(MailBoxService.class);

    public void checkMailBox() {
        Store store = null;
        if (StringUtils.isNotBlank(getPollenServiceConfig().getMailBoxHost())) {
            try {
                if (log.isDebugEnabled()) {
                    log.debug("Open session ...");
                }
                store = openSession();
                Folder folder = null;
                try {
                    if (log.isDebugEnabled()) {
                        log.debug("Open folder ...");
                    }
                    folder = openFolder(store);
                    if (log.isDebugEnabled()) {
                        log.debug("Process folder ...");
                    }
                    processFolder(folder);
                    if (log.isDebugEnabled()) {
                        log.debug("Done.");
                    }
                } finally {
                    closeQuietly(folder);
                }
            } catch (MessagingException e) {
                log.error("une erreur est survenue pendant la traitement de la boîte " + getPollenServiceConfig().getMailBoxUser(), e);
            } finally {
                closeQuietly(store);
            }
        }

    }

    private Store openSession() throws MessagingException {
        Properties properties = new Properties();
        properties.put("mail.host", getPollenServiceConfig().getMailBoxHost());
        properties.put("mail.store.protocol", getPollenServiceConfig().getMailBoxProtocol());
        Session session = Session.getInstance(properties);
        Store store = session.getStore();
        store.connect(getPollenServiceConfig().getMailBoxUser(), getPollenServiceConfig().getMailBoxPassword());
        return store;
    }

    private Folder openFolder(Store store) throws MessagingException {
        Folder defaultFolder = store.getDefaultFolder();
        Folder inboxFolder = defaultFolder.getFolder("INBOX");
        inboxFolder.open(Folder.READ_WRITE);
        return inboxFolder;
    }

    protected void processFolder(Folder folder) throws MessagingException {
        Flags flags = new Flags();
        flags.add(Flags.Flag.FLAGGED);

        FlagTerm searchNewMessages = new FlagTerm(flags, false);

        Message[] messages = folder.search(searchNewMessages);  

        if (ArrayUtils.isNotEmpty(messages)) {
            if (log.isDebugEnabled()) {
                log.debug(messages.length + " messages to process ...");
            }

            Stream.of(messages).forEach(this::processMessage);

            folder.setFlags(messages, flags, true);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("No messages.");
            }
        }

    }

    protected void processMessage(Message message) {
        try {
            if (log.isInfoEnabled()) {
                log.info(String.format("Process message \"%1s\" recevied at %tF ...", message.getSubject(), message.getReceivedDate()));
            }

            String[] tos = message.getHeader("To");

            if (ArrayUtils.isNotEmpty(tos)) {
                String mailId = getMailId(tos[0]);
                if (StringUtils.isNotBlank(mailId)) {
                    if (log.isInfoEnabled()) {
                        log.info("Found mail id : " + mailId);
                    }
                    PollenMailReturnContext context = extractMessageContext(message, mailId);
                    context.getMailType().getAction().accept(this, context);
                }
            }
            
        } catch (MessagingException e) {
            if (log.isErrorEnabled()) {
                log.error("error on read message on mail box", e);
            }
        }
    }

    protected PollenMailReturnContext extractMessageContext(Message message, String mailId) {
        if (log.isDebugEnabled()) {
            log.debug("Extract context for mailId : " + mailId + " ...");
        }

        PollenMailReturnContext context = new PollenMailReturnContext();
        context.setMessage(message);

        String[] parts = mailId.split(EmailService.MAIL_ID_SEPARATOR_REGEXP);

        if (parts.length > 0) {

            try {
                PollenMailType pollenEmailType = PollenMailType.valueOf(parts[0]);

                context.setMailType(pollenEmailType);

                String[] ids = ArrayUtils.subarray(parts, 1, parts.length);

                context.setPoll(findEntity(ids, Poll.class));
                context.setChoice(findEntity(ids, Choice.class));
                context.setUser(findEntity(ids, PollenUser.class));
                context.setReport(findEntity(ids, Report.class));
                context.setComment(findEntity(ids, Comment.class));
                context.setPrincipal(findEntity(ids, PollenPrincipal.class));

            } catch (IllegalArgumentException e) {
                if (log.isErrorEnabled()) {
                    log.error("unknown pollen email type " + parts[0], e);
                }
            }
        }
        return context;
    }

    protected <E extends TopiaEntity> E findEntity(String[] ids, Class<E> type) {
        PollenEmailKeyType keyType = PollenEmailKeyType.valueOfType(type);

        return Stream.of(ids)
                .filter(id -> id.startsWith(keyType.name()))
                .findFirst()
                .map(id -> id.replaceFirst(keyType.name(), ""))
                .map(id -> findEntity(type, id))
                .map(type::cast)
                .orElse(null);
    }


    protected void noAction() {};

    protected void forwardToCreatorAction(Poll poll, Message message) {
        String email = poll.getCreator().getEmail();
        if (StringUtils.isNotBlank(email)) {
            if (log.isInfoEnabled()) {
                log.info("Forward to " + email);
            }
            try {
                getEmailService().forward(message,
                        Collections.singleton(email),
                        I18n.l(getLocale(), "pollen.service.mailBox.forward.poll.subject"),
                        I18n.l(getLocale(), "pollen.service.mailBox.forward.poll.body"),
                        getPollenServiceConfig().getSmtpFrom(),
                        true);
            } catch (MessagingException | EmailException e) {
                if (log.isErrorEnabled()) {
                    log.error("Error on forward message", e);
                }
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("No poll creator email for forward message");
            }
        }

    }

    protected void undeliveredFlagPollenPrincipal(PollenPrincipal pollenPrincipal, Message message) {

        if (isUndeliveredMessage(message)) {
            if (log.isInfoEnabled()) {
                log.info("Set undelivered flag to " + pollenPrincipal.getTopiaId());
            }
            pollenPrincipal.setInvalid(true);
            commit();
        }
    }


    protected boolean isUndeliveredMessage(Message message) {
        try {
            return message.getSubject().contains("Undelivered Mail Returned to Sender");
        } catch (MessagingException e) {
            if (log.isErrorEnabled()) {
                log.error("error on read message on mail box", e);
            }
            return false;
        }
    }

    protected String getMailId(String address) {
        String mailId = null;
        String smtpFrom = getPollenServiceConfig().getSmtpFrom();

        InternetAddress fromAddress;
        try {
            fromAddress = new InternetAddress(smtpFrom);
        } catch (AddressException e) {
            throw new PollenTechnicalException("Error on smtp from address", e);
        }

        String[] emailSplit = fromAddress.getAddress().split("@");
        String userFrom = emailSplit[0];
        String domainFrom = emailSplit[1];

        String decodeAddress = address;

        try {
            InternetAddress internetAddress = new InternetAddress(address);
            decodeAddress = internetAddress.getAddress();
        } catch (AddressException e) {
            if (log.isErrorEnabled()) {
                log.error("address parse error : " + address, e);
            }
        }

        if (decodeAddress.startsWith(userFrom + "+") && decodeAddress.endsWith("@" + domainFrom)) {
            mailId = decodeAddress.replace(userFrom + "+", "").replace("@" + domainFrom, "");
        }

        return mailId;
    }



    private void closeQuietly(Store store) {
        if (store != null) {
            try {
                store.close();
            } catch (MessagingException e) {
                if (log.isErrorEnabled()) {
                    log.error("error occured while trying to disconnect from IMAP server", e);
                }
            }
        }
    }

    private void closeQuietly(Folder inboxFolder) {
        if (inboxFolder != null) {
            try {
                inboxFolder.close(false);
            } catch (MessagingException e) {
                if (log.isErrorEnabled()) {
                    log.error("error occured while trying to close folder", e);
                }
            }
        }
    }

}
