package org.chorem.pollen.services.bean.voteCounting.condorcet;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.votecounting.CondorcetBattle;

import java.math.BigDecimal;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CondorcetBattleBean {

    protected PollenEntityId<Choice> opponentId;

    protected PollenEntityId<Choice> runnerId;

    protected BigDecimal score;

    public CondorcetBattleBean() {
        this.opponentId = PollenEntityId.newId(Choice.class);
        this.runnerId = PollenEntityId.newId(Choice.class);;
    }

    public void fromResult(CondorcetBattle result) {

        setOpponentId(result.getOpponentId());
        setRunnerId(result.getRunnerId());
        setScore(result.getScore());

    }

    public PollenEntityId<Choice> getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(PollenEntityId<Choice> opponentId) {
        this.opponentId = opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId.setEntityId(opponentId);
    }

    public PollenEntityId<Choice> getRunnerId() {
        return runnerId;
    }

    public void setRunnerId(PollenEntityId<Choice> runnerId) {
        this.runnerId = runnerId;
    }

    public void setRunnerId(String runnerId) {
        this.runnerId.setEntityId(runnerId);
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }
}
