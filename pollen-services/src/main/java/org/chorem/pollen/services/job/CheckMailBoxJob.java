package org.chorem.pollen.services.job;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.services.service.mail.MailBoxService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionException;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DisallowConcurrentExecution
public class CheckMailBoxJob extends AbstractPollenJob {

    private static final Log log = LogFactory.getLog(CheckMailBoxJob.class);

    @Override
    public void execute() throws JobExecutionException {

        if (log.isDebugEnabled()) {
            log.debug("Start job to delete obsolete Session Tokens");
        }

        MailBoxService mailBoxService = newService(MailBoxService.class);
        mailBoxService.checkMailBox();

    }
}
