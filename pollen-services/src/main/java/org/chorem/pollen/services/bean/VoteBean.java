package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.persistence.entity.VoterListMember;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoteBean extends PollenBean<Vote> {

    protected PollenEntityId<PollenPrincipal> voterId;

    protected Set<PollenEntityId<VoterListMember>> voterListMemberIds;

    protected String voterName;

    protected String voterAvatar;

    protected String permission;

    protected Boolean anonymous;

    protected double weight;

    protected final Set<VoteToChoiceBean> choice = new LinkedHashSet<>();

    private ReportResumeBean report;

    protected boolean ignored;

    public VoteBean() {
        super(Vote.class);
    }

    public String getVoterName() {
        return voterName;
    }

    public void setVoterName(String voterName) {
        this.voterName = voterName;
    }

    public String getVoterAvatar() {
        return voterAvatar;
    }

    public void setVoterAvatar(String voterAvatar) {
        this.voterAvatar = voterAvatar;
    }

    public PollenEntityId<PollenPrincipal> getVoterId() {
        if (voterId == null) {
            voterId = PollenEntityId.newId(PollenPrincipal.class);
        }
        return voterId;
    }

    public void setVoterId(PollenEntityId<PollenPrincipal> voterId) {
        this.voterId = voterId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

    public Set<VoteToChoiceBean> getChoice() {
        return choice;
    }

    public void addChoice(VoteToChoiceBean choice) {
        this.choice.add(choice);
    }

    public boolean isAnonymous() {
        return anonymous != null && anonymous;
    }

    public Set<PollenEntityId<VoterListMember>> getVoterListMemberId() {
        if (voterListMemberIds == null) {
            voterListMemberIds = Sets.newHashSet();
        }
        return voterListMemberIds;
    }

    public void setVoterListMembers(Collection<VoterListMember> voterListMembers) {
        this.voterListMemberIds = voterListMembers.stream().map(member -> {
            PollenEntityId<VoterListMember> memberId = PollenEntityId.newId(VoterListMember.class);
            memberId.setEntityId(member.getTopiaId());
            return memberId;
        }).collect(Collectors.toSet());
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setReport(ReportResumeBean report) {
        this.report = report;
    }

    public ReportResumeBean getReport() {
        return report;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }
}
