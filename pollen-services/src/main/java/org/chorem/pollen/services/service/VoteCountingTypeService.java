package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.services.bean.VoteCountingTypeBean;
import org.chorem.pollen.votecounting.VoteCounting;
import org.chorem.pollen.votecounting.VoteCountingFactory;
import org.chorem.pollen.votecounting.VoteCountingNotFound;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created on 03/07/14.
 *
 * @author agarandel
 * @since 2.0
 */
public class VoteCountingTypeService extends PollenServiceSupport {

    public List<VoteCountingTypeBean> getVoteCountingTypes() {
        checkIsConnectedRequired();

        VoteCountingFactory factory = serviceContext.getVoteCountingFactory();

        List<VoteCountingTypeBean> voteCountingTypes = new ArrayList<>();

        for (VoteCounting voteCounting : factory) {

            voteCountingTypes.add(idToVoteCountingType(voteCounting.getId()));

        }

        return voteCountingTypes;
    }

    public VoteCountingTypeBean getVoteCountingType(int id) {
        checkIsConnectedRequired();
        try {
            return idToVoteCountingType(id);
        } catch (VoteCountingNotFound e) {
            return null;
        }
    }

    protected VoteCountingTypeBean idToVoteCountingType(int id) throws VoteCountingNotFound {
        VoteCountingFactory factory = serviceContext.getVoteCountingFactory();
        Locale l = serviceContext.getLocale();

        VoteCountingTypeBean newVoteCountingType = new VoteCountingTypeBean();

        VoteCounting voteCounting = factory.getVoteCounting(id);

        newVoteCountingType.setId(voteCounting.getId());
        newVoteCountingType.setName(voteCounting.getName(l));
        newVoteCountingType.setHelper(voteCounting.getHelp(l));
        newVoteCountingType.setShortHelper(voteCounting.getShortHelp(l));
        newVoteCountingType.setRenderType(voteCounting.getVoteValueEditorType());
        newVoteCountingType.setMinimumValue(voteCounting.getMinimumValue());

        return newVoteCountingType;
    }
}
