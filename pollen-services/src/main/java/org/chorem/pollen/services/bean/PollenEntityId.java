package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactory;

/**
 * Created on 5/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenEntityId<E extends TopiaEntity> {

    public static final String TEMPORARY_ID_PREFIX = "TEMP";

    protected final Class<E> entityType;

    protected String entityId;

    protected String reducedId;

    public static <E extends TopiaEntity> PollenEntityId<E> newId(Class<E> entityType) {

        return new PollenEntityId<>(entityType);

    }

    public static String encode(TopiaIdFactory topiaIdFactory, String entityId) {

        String randomPart = topiaIdFactory.getRandomPart(entityId);
        return randomPart;

    }

    public PollenEntityId(Class<E> entityType) {
        this.entityType = entityType;
    }

    public boolean isEmpty() {
        return !isNotEmpty();
    }

    public boolean isNotEmpty() {
        return StringUtils.isNotEmpty(reducedId) || StringUtils.isNotEmpty(entityId);
    }

    public String getReducedId() {
        return reducedId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setReducedId(String reducedId) {
        this.reducedId = reducedId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public boolean isTemporaryId() {
        return reducedId != null && reducedId.startsWith(TEMPORARY_ID_PREFIX);
    }

    public void encode(TopiaIdFactory topiaIdFactory) {

        if (entityId != null) {

            String id = encode(topiaIdFactory, entityId);
            setReducedId(id);

        }

    }

    public void decode(TopiaIdFactory topiaIdFactory) {

        if (reducedId != null && !isTemporaryId()) {

            String id = topiaIdFactory.newTopiaId(entityType, reducedId);
            setEntityId(id);

        }

    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof PollenEntityId)) return false;

        PollenEntityId that = (PollenEntityId) o;

        return entityType.equals(that.entityType)
                && (reducedId != null && reducedId.equals(that.reducedId)
                || entityId != null && entityId.equals(that.entityId));

    }

    @Override
    public int hashCode() {

        int result = entityType.hashCode();
        result = 31 * result + (reducedId == null ? 0 : reducedId.hashCode());
        result = 31 * result + (entityId == null ? 0 : entityId.hashCode());
        return result;

    }

    @Override
    public String toString() {
        return reducedId;
    }
}
