package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.CommentVisibility;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.ResultVisibility;
import org.chorem.pollen.persistence.entity.VoteVisibility;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollBean extends PollenBean<Poll> {

    public enum PollStatus {
        CREATED, // when poll is created, but you still can't vote
        VOTING, // when you can vote
        ADDING_CHOICES, // when you can add choice
        CLOSED // when poll is closed
    }

    public PollBean() {
        super(Poll.class);
    }

    public static boolean isPollFree(PollBean poll) {
        return Objects.equals(PollType.FREE, poll.getPollType());
    }

    public static boolean isPollRestricted(PollBean poll) {
        return Objects.equals(PollType.RESTRICTED, poll.getPollType());
    }

    protected String permission;

    protected String creatorName;

    protected String creatorEmail;

    protected String title;

    protected String description;

    protected Date createDate;

    protected Date beginDate;

    protected Date endDate;

    protected boolean anonymousVoteAllowed;

    protected boolean continuousResults;

    protected PollType pollType;

    protected VoteVisibility voteVisibility;

    protected CommentVisibility commentVisibility;

    protected ResultVisibility resultVisibility;

    protected boolean closed;

    protected boolean resultIsVisible;

    protected boolean commentIsVisible;

    protected boolean voteIsVisible;

    protected boolean participantsIsVisible;

    protected boolean canVote;

    protected long commentCount;

    protected Set<String> invalidEmails;

    protected long participantCount;

    protected long participantInvitedCount;

    protected PollStatus status;

    protected int notifyMeHoursBeforePollEnds;

    protected boolean voteNotification;

    protected boolean commentNotification;

    protected boolean newChoiceNotification;

    protected String notificationLocale;

    protected ReportResumeBean report;

    protected boolean gtuValidated;

    protected int maxVoters;

    protected String creatorAvatar;

    protected List<String> emailAddressSuffixes;

    protected List<QuestionBean> questions;

    protected String picture;
    
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(String creatorEmail) {
        this.creatorEmail = creatorEmail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isAnonymousVoteAllowed() {
        return anonymousVoteAllowed;
    }

    public void setAnonymousVoteAllowed(boolean anonymousVoteAllowed) {
        this.anonymousVoteAllowed = anonymousVoteAllowed;
    }

    public boolean isContinuousResults() {
        return continuousResults;
    }

    public void setContinuousResults(boolean continuousResults) {
        this.continuousResults = continuousResults;
    }

    public PollType getPollType() {
        return pollType;
    }

    public void setPollType(PollType pollType) {
        this.pollType = pollType;
    }

    public VoteVisibility getVoteVisibility() {
        return voteVisibility;
    }

    public void setVoteVisibility(VoteVisibility voteVisibility) {
        this.voteVisibility = voteVisibility;
    }

    public CommentVisibility getCommentVisibility() {
        return commentVisibility;
    }

    public void setCommentVisibility(CommentVisibility commentVisibility) {
        this.commentVisibility = commentVisibility;
    }

    public ResultVisibility getResultVisibility() {
        return resultVisibility;
    }

    public void setResultVisibility(ResultVisibility resultVisibility) {
        this.resultVisibility = resultVisibility;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public boolean isResultIsVisible() {
        return resultIsVisible;
    }

    public void setResultIsVisible(boolean resultIsVisible) {
        this.resultIsVisible = resultIsVisible;
    }

    public boolean isCommentIsVisible() {
        return commentIsVisible;
    }

    public void setCommentIsVisible(boolean commentIsVisible) {
        this.commentIsVisible = commentIsVisible;
    }

    public boolean isVoteIsVisible() {
        return voteIsVisible;
    }

    public void setVoteIsVisible(boolean voteIsVisible) {
        this.voteIsVisible = voteIsVisible;
    }

    public boolean isParticipantsIsVisible() {
        return participantsIsVisible;
    }

    public void setParticipantsIsVisible(boolean participantsIsVisible) {
        this.participantsIsVisible = participantsIsVisible;
    }

    public boolean isCanVote() {
        return canVote;
    }

    public void setCanVote(boolean canVote) {
        this.canVote = canVote;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public PollStatus getStatus() {
        return status;
    }

    public void setStatus(PollStatus status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getNotifyMeHoursBeforePollEnds() {
        return notifyMeHoursBeforePollEnds;
    }

    public void setNotifyMeHoursBeforePollEnds(int notifyMeHoursBeforePollEnds) {
        this.notifyMeHoursBeforePollEnds = notifyMeHoursBeforePollEnds;
    }

    public boolean isVoteNotification() {
        return voteNotification;
    }

    public void setVoteNotification(boolean voteNotification) {
        this.voteNotification = voteNotification;
    }

    public boolean isCommentNotification() {
        return commentNotification;
    }

    public void setCommentNotification(boolean commentNotification) {
        this.commentNotification = commentNotification;
    }

    public boolean isNewChoiceNotification() {
        return newChoiceNotification;
    }

    public void setNewChoiceNotification(boolean newChoiceNotification) {
        this.newChoiceNotification = newChoiceNotification;
    }

    public String getNotificationLocale() {
        return notificationLocale;
    }

    public void setNotificationLocale(String notificationLocale) {
        this.notificationLocale = notificationLocale;
    }

    public long getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(long participantCount) {
        this.participantCount = participantCount;
    }

    public long getParticipantInvitedCount() {
        return participantInvitedCount;
    }

    public void setParticipantInvitedCount(long participantInvitedCount) {
        this.participantInvitedCount = participantInvitedCount;
    }

    public ReportResumeBean getReport() {
        return report;
    }

    public void setReport(ReportResumeBean report) {
        this.report = report;
    }

    public boolean isGtuValidated() {
        return gtuValidated;
    }

    public void setGtuValidated(boolean gtuValidated) {
        this.gtuValidated = gtuValidated;
    }

    public int getMaxVoters() {
        return maxVoters;
    }

    public void setMaxVoters(int maxVoters) {
        this.maxVoters = maxVoters;
    }

    public String getCreatorAvatar() {
        return creatorAvatar;
    }

    public void setCreatorAvatar(String creatorAvatar) {
        this.creatorAvatar = creatorAvatar;
    }

    public Set<String> getInvalidEmails() {
        return invalidEmails;
    }

    public void setInvalidEmails(Set<String> invalidEmails) {
        this.invalidEmails = invalidEmails;
    }

    public List<String> getEmailAddressSuffixes() {
        return emailAddressSuffixes;
    }

    public void setEmailAddressSuffixes(List<String> emailAddressSuffixes) {
        this.emailAddressSuffixes = emailAddressSuffixes;
    }

    public List<QuestionBean> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionBean> questions) {
        this.questions = questions;
    }

    public void addQuestion(QuestionBean question) {
        if (null == this.questions){
            this.questions = new ArrayList<>();
        }
        this.questions.add(question);
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
