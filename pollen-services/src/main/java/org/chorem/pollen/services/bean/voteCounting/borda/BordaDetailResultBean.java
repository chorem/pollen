package org.chorem.pollen.services.bean.voteCounting.borda;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.services.bean.voteCounting.VoteCountingDetailResultBean;
import org.chorem.pollen.votecounting.BordaChoiceRank;
import org.chorem.pollen.votecounting.BordaDetailResult;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BordaDetailResultBean extends VoteCountingDetailResultBean {

    protected List<BordaChoiceRankBean> choiceRanks;

    public void fromResult(BordaDetailResult result) {

        for (BordaChoiceRank choiceRank : result.getChoiceRanks()) {

            BordaChoiceRankBean bordaChoiceRankBean = new BordaChoiceRankBean();
            bordaChoiceRankBean.fromResult(choiceRank);
            getChoiceRanks().add(bordaChoiceRankBean);

        }
    }

    public List<BordaChoiceRankBean> getChoiceRanks() {
        if (choiceRanks == null) {
            choiceRanks = Lists.newLinkedList();
        }
        return choiceRanks;
    }

    public void setChoiceRanks(List<BordaChoiceRankBean> choiceRanks) {
        this.choiceRanks = choiceRanks;
    }
}
