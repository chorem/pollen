package org.chorem.pollen.services;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.UnsafeYamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import org.apache.commons.io.IOUtils;
import org.chorem.pollen.persistence.entity.ChoiceImpl;
import org.chorem.pollen.persistence.entity.CommentImpl;
import org.chorem.pollen.persistence.entity.FavoriteListImpl;
import org.chorem.pollen.persistence.entity.FavoriteListMemberImpl;
import org.chorem.pollen.persistence.entity.PollImpl;
import org.chorem.pollen.persistence.entity.PollenPrincipalImpl;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddressImpl;
import org.chorem.pollen.persistence.entity.PollenUserImpl;
import org.chorem.pollen.persistence.entity.QuestionImpl;
import org.chorem.pollen.persistence.entity.VoteImpl;
import org.chorem.pollen.persistence.entity.VoteToChoiceImpl;
import org.chorem.pollen.persistence.entity.VoterListImpl;
import org.chorem.pollen.persistence.entity.VoterListMemberImpl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Map;

public class PollenFixtures {

    public static final String POLL_NORMAL_ID = "poll_normal";
    public static final String POLL_RESTRICTED_ID = "poll_restricted";

    protected Map<String, Object> fixtures;

    public PollenFixtures(String fixturesName) {
        String yamlPath = "/" + fixturesName + ".yaml";
        String yaml;
        try (InputStream inputStream = PollenFixtures.class.getResourceAsStream(yamlPath)) {
            yaml = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalArgumentException(fixturesName + " is not a valid fixtures set name", e);
        }

        //Besoin de la unsafe config pour les anchors (désactivé par défaut sinon) On est sur les tests depuis un fichier sûr donc ça passe.
        try (YamlReader reader = new YamlReader(yaml, new UnsafeYamlConfig())){
            reader.getConfig().setClassTag("poll", PollImpl.class);
            reader.getConfig().setClassTag("question", QuestionImpl.class);
            reader.getConfig().setClassTag("user", PollenUserImpl.class);
            reader.getConfig().setClassTag("favorite-list", FavoriteListImpl.class);
            reader.getConfig().setClassTag("favorite-list-member", FavoriteListMemberImpl.class);
            reader.getConfig().setClassTag("comment", CommentImpl.class);
            reader.getConfig().setClassTag("choice", ChoiceImpl.class);
            reader.getConfig().setClassTag("vote-to-choice", VoteToChoiceImpl.class);
            reader.getConfig().setClassTag("voter-list", VoterListImpl.class);
            reader.getConfig().setClassTag("voter-list-member", VoterListMemberImpl.class);
            reader.getConfig().setClassTag("vote", VoteImpl.class);
            reader.getConfig().setClassTag("pollen-principal", PollenPrincipalImpl.class);
            reader.getConfig().setClassTag("pollenUIContext", PollenUIContext.class);
            reader.getConfig().setClassTag("email-address", PollenUserEmailAddressImpl.class);
            reader.getConfig().setClassTag("array-list", ArrayList.class);
            fixtures = (Map<String, Object>) reader.read();
        } catch (IOException e) {
            throw new PollenTechnicalException("unable to read yaml file", e);
        }
    }

    public <E> E fixture(String id) {
        return (E) fixtures.get(id);
    }
}
