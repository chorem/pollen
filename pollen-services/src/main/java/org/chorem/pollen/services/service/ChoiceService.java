package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.ChoiceImpl;
import org.chorem.pollen.persistence.entity.ChoiceTopiaDao;
import org.chorem.pollen.persistence.entity.ChoiceType;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.ReportResumeBean;
import org.chorem.pollen.services.service.security.PollenPermissions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class ChoiceService extends PollenServiceSupport {


    public ChoiceBean toChoiceBean(Choice entity) {
        ChoiceBean bean = new ChoiceBean();
        bean.setEntityId(entity.getTopiaId());

        if (entity.getCreator() == null
                || entity.getCreator().getPermission() == null
                || isNotPermitted(PollenPermissions.edit(entity))) {
            bean.setPermission(null);
        } else {
            bean.setPermission(entity.getCreator().getPermission().getToken());
        }

        if (entity.getChoiceType() == ChoiceType.RESOURCE) {
            bean.setChoiceValue(getPollenResourceService().getReduceIdByTopiaId(entity.getChoiceValue()));
        } else {
            bean.setChoiceValue(entity.getChoiceValue());
        }

        bean.setDescription(entity.getDescription());
        bean.setChoiceType(entity.getChoiceType());
        bean.setChoiceOrder(entity.getChoiceOrder());

        if (isPermitted(PollenPermissions.edit(entity))) {
            ReportResumeBean report = getReportService().getReport(entity.getTopiaId());
            bean.setReport(report);
        }

        bean.setChoiceIsDeletable(isPermitted(PollenPermissions.delete(entity)));

        return bean;
    };

    public Choice toChoice(ChoiceBean bean) {
        Choice entity = new ChoiceImpl();
        entity.setTopiaId(bean.getEntityId());
        entity.setChoiceValue(bean.getChoiceValue());
        entity.setDescription(bean.getDescription());
        entity.setChoiceType(bean.getChoiceType());
        entity.setChoiceOrder(bean.getChoiceOrder());

        return entity;
    }

    public List<ChoiceBean> getChoices(String questionId) {
        checkIsConnectedRequired();

        checkNotNull(questionId);

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.read(question));

        List<Choice> choices = getChoiceDao().findAll(question);
        return toBeanList(choices, this::toChoiceBean);
    }

    public ChoiceBean getChoice(String questionId, String choiceId) {
        checkIsConnectedRequired();

        checkNotNull(questionId);
        checkNotNull(choiceId);
        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.read(question));

        Choice choice = getChoice(question, choiceId);

        return toChoiceBean(choice);
    }

    public PollenEntityRef<Choice> addChoice(String pollId, String questionId, ChoiceBean choice) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(questionId);
        checkNotNull(choice);
        checkIsNotPersisted(choice);

        // FIXME 01/06/2018 SBavencoff Evite les modifications concurantes en BD du sondage
        synchronized (getLock(questionId)) {

            Poll poll = getPollService().getPoll0(pollId);

            Question question = getQuestionService().getQuestion0(questionId);
            checkPermission(PollenPermissions.addChoice(question));

            Poll questionPoll = question.getPoll();
            if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
                throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
            }

            List<Choice> existingChoices = getChoiceDao().findAll(question);

            ErrorMap errorMap = checkChoice(existingChoices, choice);
            errorMap.failIfNotEmpty();

            Choice result = saveChoice(question, choice);
            commit();

            getNotificationService().onChoiceAdded(question, result);
            getFeedService().onChoiceAdded(question, result);

            return PollenEntityRef.of(result);
        }
    }

    public ChoiceBean editChoice(String questionId, ChoiceBean choice) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(choice);
        checkIsPersisted(choice);

        Question question = getQuestionService().getQuestion0(questionId);
        Choice choiceBd = getChoice(question, choice.getEntityId());
        checkPermission(PollenPermissions.edit(choiceBd));

        List<Choice> existingChoices = getChoiceDao().findAll(question);

        ErrorMap errorMap = checkChoice(existingChoices, choice);
        errorMap.failIfNotEmpty();

        Choice result = saveChoice(question, choice);
        commit();

        getNotificationService().onChoiceEdited(question, result);

        return toChoiceBean(result);
    }

    public void deleteChoice(String questionId, String choiceId) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(questionId);
        checkNotNull(choiceId);

        Question question = getQuestionService().getQuestion0(questionId);
        Choice choice = getChoice(question, choiceId);
        checkPermission(PollenPermissions.delete(choice));

        List<Choice> existingChoices = getChoiceDao().findAll(question);
        if (existingChoices.size() == 1) {

            // can't delete this choice
            // will be the last one
            ErrorMap errors = new ErrorMap();
            errors.addError("choice", l(getLocale(), "pollen.error.poll.choice.mandatory"));
            errors.failIfNotEmpty();
        }

        getChoiceDao().delete(choice);
        commit();

        getNotificationService().onChoiceDeleted(question, choice);


    }

    protected Choice getChoice(Question question, String choiceId) {

        Choice result = getChoiceDao().forTopiaIdEquals(choiceId).findUnique();

        if (!question.equals(result.getQuestion())) {

            throw new InvalidEntityLinkException(Choice.PROPERTY_QUESTION, result, question);

        }

        return result;
    }
    
    protected void saveChoices(Question question, List<ChoiceBean> choicesToSave) {
        boolean questionExists = question.isPersisted();
        
        List<Choice> existingChoices = questionExists ? getChoiceDao().findAll(question) : new ArrayList<>();
        
        if (CollectionUtils.isNotEmpty(choicesToSave)) {
        
            for (ChoiceBean choice : choicesToSave) {
            
                Choice persistedChoice = saveChoice(question, choice);
                existingChoices.remove(persistedChoice);
            
            }
        }
        
        // the remaining choices have been deleted by the user and must be removed
        if (CollectionUtils.isNotEmpty(existingChoices)) {
            getChoiceDao().deleteAll(existingChoices);
        }
        
    }

    protected Choice saveChoice(Question question, ChoiceBean choice) {

        ChoiceTopiaDao choiceDao = getChoiceDao();

        boolean choiceExists = choice.isPersisted();

        Choice toSave;
        if (choiceExists) {

            // get existing choice

            toSave = getChoice(question, choice.getEntityId());

            if (toSave.getChoiceType() == ChoiceType.RESOURCE) {
                // check if new resource then delete old resource
                if (choice.getChoiceType() != ChoiceType.RESOURCE
                        ||
                        !toSave.getChoiceValue().equals(
                                getPollenResourceService().getTopiaIdByReduceId(choice.getChoiceValue())
                        )) {

                    // delete the old resource
                    getPollenResourceDao().delete(getPollenResourceService().getResource0(toSave.getChoiceValue()));
                }
            }

        } else {

            // create a new choice
            toSave = choiceDao.create();
            toSave.setQuestion(question);

        }

        toSave.setChoiceOrder(choice.getChoiceOrder());
        toSave.setChoiceType(choice.getChoiceType());

        if (ChoiceType.RESOURCE.equals(toSave.getChoiceType())) {
            toSave.setChoiceValue(getPollenResourceService().getTopiaIdByReduceId(choice.getChoiceValue()));
        } else {
            toSave.setChoiceValue(choice.getChoiceValue());
        }

        toSave.setDescription(choice.getDescription());
        toSave.setCreator(question.getPoll().getCreator());

        return toSave;
    }

    protected ErrorMap checkChoice(List<Choice> existingChoices, ChoiceBean choice) {

        ErrorMap errors = new ErrorMap();

        boolean choiceExists = choice.isPersisted();

        Set<String> choiceNames = Sets.newHashSet();

        boolean withChoiceType = checkNotNull(errors, "choiceType", choice.getChoiceType(), l(getLocale(), "pollen.error.choice.choiceTypeEmpty"));

        if (CollectionUtils.isNotEmpty(existingChoices)) {

            // get all used names

            for (Choice choice1 : existingChoices) {

                if (choiceExists &&
                        choice1.getTopiaId().equals(choice.getEntityId())) {

                    continue;

                }

                choiceNames.add(choice1.getChoiceValue());

            }
        }

        if (withChoiceType) {

            switch (choice.getChoiceType()) {

                case TEXT:

                    String choiceName = choice.getChoiceValue();
                    boolean nameNotBlank = checkNotBlank(errors,
                                                         "choiceValue",
                                                         choiceName,
                                                         l(getLocale(), "pollen.error.choice.choiceNameEmpty"));

                    if (nameNotBlank) {
                        boolean nameAdded = choiceNames.add(choiceName);
                        check(errors, "choiceValue", nameAdded, l(getLocale(), "pollen.error.choice.choiceNameExist"));
                    }

                    break;

                case DATE:
                case DATETIME:
                    Calendar choiceDate = null;
                    if (choice.getChoiceValue() != null) {
                        try {
                            choiceDate = Calendar.getInstance();
                            choiceDate.setTimeInMillis(Long.parseLong(choice.getChoiceValue()));

                        } catch (NumberFormatException e) {
                            check(errors, "choiceValue", false, l(getLocale(), "pollen.error.choice.choiceDateInvalid", choice.getChoiceValue()));
                        }
                        if (choiceDate != null) {
                            boolean dateAdded = choiceNames.add(String.valueOf(choiceDate.getTime()));
                            check(errors, "choiceValue", dateAdded, l(getLocale(), "pollen.error.choice.choiceDateExist"));
                        }

                    } else {
                        check(errors, "choiceValue", false, l(getLocale(), "pollen.error.choice.choiceDateEmpty"));
                    }
                    break;

                case RESOURCE:

                    boolean hasResource = checkNotBlank(errors,
                                                        "choiceValue",
                                                        choice.getChoiceValue(),
                                                        l(getLocale(), "pollen.error.resource.empty"));

                    if (hasResource) {
                        PollenResourceService resourceService = getPollenResourceService();
                        PollenResource resource = resourceService.getResource0(resourceService.getTopiaIdByReduceId(choice.getChoiceValue()));

                        checkNotBlank(errors,
                                      "choiceValue",
                                      resource.getName(),
                                      l(getLocale(), "pollen.error.resource.notExist"));
                    }

                    break;

                default:
                    throw new UnsupportedOperationException("Unexpected value : " + choice.getChoiceType());

            }
        }

        return errors;
    }

    public long getChoiceCount(Question question) {
        checkIsConnectedRequired();
        return getChoiceDao().forQuestionEquals(question).count();
    }
}
