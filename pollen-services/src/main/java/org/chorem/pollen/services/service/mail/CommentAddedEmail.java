package org.chorem.pollen.services.service.mail;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created on 4/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class CommentAddedEmail extends PollenMail {

    protected Poll poll;

    protected Comment comment;

    protected CommentAddedEmail(Locale locale, TimeZone timeZone) {
        super(locale, timeZone);
    }

    @Override
    public String getSubject() {
        return I18n.l(locale, "pollen.service.mail.CommentAddedEmail.subject", poll.getTitle());
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    @Override
    public List<TopiaEntity> getEntitiesKey() {
        return Lists.newArrayList(poll, comment);
    }
}
