package org.chorem.pollen.services.bean.export;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FavoriteListsExport {

    public static final String EXPORT_VERSION = "1.0.0";

    protected String version;

    protected List<FavoriteListExport> favoriteLists;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<FavoriteListExport> getFavoriteLists() {
        if (favoriteLists == null) {
            favoriteLists = Lists.newLinkedList();
        }
        return favoriteLists;
    }

    public void setFavoriteLists(List<FavoriteListExport> favoriteLists) {
        this.favoriteLists = favoriteLists;
    }
}
