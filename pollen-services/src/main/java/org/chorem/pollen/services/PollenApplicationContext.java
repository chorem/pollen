package org.chorem.pollen.services;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.PollenTopiaApplicationContext;
import org.chorem.pollen.persistence.PollenTopiaPersistenceContext;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.chorem.pollen.votecounting.VoteCountingFactory;

import java.io.Closeable;
import java.util.Locale;

/**
 * Created on 5/4/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public interface PollenApplicationContext extends Closeable {

    PollenTopiaApplicationContext getTopiaApplicationContext();

    PollenServicesConfig getApplicationConfig();

    VoteCountingFactory getVoteCountingFactory();

    PollenTopiaPersistenceContext newPersistenceContext();

    PollenServiceContext newServiceContext(PollenPersistenceContext persistenceContext, Locale locale);

    PollenSecurityContext newSecurityContext(PollenUser user, PollenPrincipal mainPrincipal);

    void init();
}
