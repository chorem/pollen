package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.persistence.entity.ResourceType;
import org.chorem.pollen.services.PollenService;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.UnitHuman;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.resource.AbstractResourceBean;
import org.chorem.pollen.services.bean.resource.GtuMetaBean;
import org.chorem.pollen.services.bean.resource.ResourceFileBean;
import org.chorem.pollen.services.bean.resource.ResourceMetaBean;
import org.chorem.pollen.services.bean.resource.ResourceStreamBean;
import org.nuiton.topia.persistence.TopiaIdFactory;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 10/07/14.
 *
 * @author dralagen
 */
public class PollenResourceService extends PollenServiceSupport implements PollenService {

    public ResourceStreamBean toResourceStreamBean(PollenResource entity) {
        ResourceStreamBean bean = new ResourceStreamBean();
        bean.setEntityId(entity.getTopiaId());

        try {
            bean.setResourceContent(entity.getResourceContent().getBinaryStream());
        } catch (SQLException e) {
            throw new PollenTechnicalException(e);
        }

        bean.setName(entity.getName());
        bean.setSize(entity.getSize());
        bean.setContentType(entity.getContentType());

        return bean;
    }

    public ResourceMetaBean toResourceMetaBean(PollenResource entity) {
        ResourceMetaBean bean = new GtuMetaBean();
        bean.setEntityId(entity.getTopiaId());

        bean.setName(entity.getName());
        bean.setSize(entity.getSize());
        bean.setContentType(entity.getContentType());
        bean.setResourceType(entity.getResourceType());
        bean.setUploadDate(entity.getTopiaCreateDate());

        return bean;
    }

    public ResourceStreamBean getResource(String resourceId) {
        checkIsConnectedRequired();
        checkNotNull(resourceId);

        PollenResource resource = getResource0(resourceId);

        return toResourceStreamBean(resource);
    }

    public ResourceMetaBean getMetaResource(String resourceId) {
        checkIsConnectedRequired();
        checkNotNull(resourceId);

        PollenResource resource = getResource0(resourceId);

        return toResourceMetaBean(resource);
    }

    public ResourceStreamBean getResourcePreview(String resourceId, boolean maxDimension) {
        checkIsConnectedRequired();

        checkNotNull(resourceId);

        PollenResource resource = getResource0(resourceId);

        String contentType;
        BufferedImage source;
        try {
            if (resource.getContentType().matches("image/(jpeg|png|gif)")) {
                source = ImageIO.read(resource.getResourceContent().getBinaryStream());
                contentType = resource.getContentType();
            } else {
                source = ImageIO.read(PollenResourceService.class.getResourceAsStream("/default.jpg"));
                contentType = "image/jpeg";
            }

            int width, height;
            Integer previewWidth;
            Integer previewHeight;

            int previewMax = getPollenServiceConfig().getResourcePreviewMax();

            if (source.getWidth() > source.getHeight()) {
                width = previewMax;
                height = width * source.getHeight() / source.getWidth();
            } else {
                height = previewMax;
                width = height * source.getWidth() / source.getHeight();
            }

            if (maxDimension) {
                previewWidth = previewMax;
                previewHeight = previewMax;
            } else {
                previewWidth = width;
                previewHeight = height;
            }

            // TODO AThimel 02/01/2018 Les preview devraient être stockées plutôt que recalculées à chaque fois

            BufferedImage destination = new BufferedImage(previewWidth, previewHeight, source.getType());
            Graphics2D g = destination.createGraphics();
            g.drawImage(source, (previewWidth - width) / 2, (previewHeight - height) / 2, width, height, null);
            g.dispose();

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ImageIO.write(destination, contentType.split("/")[1], output);

            ResourceStreamBean bean = new ResourceStreamBean();
            bean.setName(resource.getName());
            bean.setContentType(contentType);
            bean.setResourceContent(new ByteArrayInputStream(output.toByteArray()));

            return bean;
        } catch (SQLException | IOException e) {
            throw new PollenTechnicalException("error get preview resource", e);
        }
    }

    public PollenEntityRef<PollenResource> createResource(ResourceFileBean resource) throws InvalidFormException {
        checkIsConnectedRequired();
        checkNotNull(resource);
        checkIsNotPersisted(resource);

        if (ResourceType.GTU.equals(resource.getResourceType())) {
            checkIsAdmin();
        }

        ErrorMap errorMap = checkRessource(resource);
        errorMap.failIfNotEmpty();

        PollenResource savedResource = saveResource(resource);
        commit();

        return PollenEntityRef.of(savedResource);
    }

    public PollenResource createAvatarResource(AbstractResourceBean resource) throws InvalidFormException {
        checkIsConnectedRequired();
        checkNotNull(resource);
        checkIsNotPersisted(resource);

        ErrorMap errorMap = checkRessource(resource);
        errorMap.failIfNotEmpty();

        // commit done by the caller
        return getPollenResourceDao().create(saveResource(resource));
    }

    public void deleteResource(String resourceId) {
        checkIsConnectedRequired();
        checkNotNull(resourceId);
        checkIsAdmin();

        PollenResource resource = getResource0(resourceId);

        if (ResourceType.GTU.equals(resource.getResourceType())) {
            checkIsAdmin();
        }

        getPollenResourceDao().delete(resource);

        commit();
    }

    public ResourceStreamBean getAvatar(String userId) {
        checkIsConnectedRequired();
        PollenResource resource = getPollenResourceDao().findAvatarForUser(userId);
        return toResourceStreamBean(resource);
    }

    protected PollenResource getResource0(String resourceId) {

        return getPollenResourceDao().forTopiaIdEquals(resourceId).findUnique();
    }


    protected PollenResource saveResource(AbstractResourceBean resource) {

        boolean resourceExist = resource.isPersisted();

        PollenResource toSave;

        if (resourceExist) {
            toSave = getResource0(resource.getEntityId());
        } else {
            toSave = getPollenResourceDao().create();
        }

        toSave.setName(resource.getName());
        toSave.setContentType(resource.getContentType());
        toSave.setSize(resource.getSize());
        toSave.setResourceType(resource.getResourceType());
        toSave.setResourceContent(resource.getResourceBlob());

        return toSave;
    }

    protected String getTopiaIdByReduceId(String shortId) {
        PollenEntityId<PollenResource> resourceId = PollenEntityId.newId(PollenResource.class);
        resourceId.setReducedId(shortId);
        TopiaIdFactory topiaIdFactory = serviceContext.getTopiaApplicationContext().getTopiaIdFactory();
        resourceId.decode(topiaIdFactory);

        return resourceId.getEntityId();
    }

    protected String getReduceIdByTopiaId(String topiaId) {
        PollenEntityId<PollenResource> resourceId = PollenEntityId.newId(PollenResource.class);
        resourceId.setEntityId(topiaId);
        TopiaIdFactory topiaIdFactory = serviceContext.getTopiaApplicationContext().getTopiaIdFactory();
        resourceId.encode(topiaIdFactory);

        return resourceId.getReducedId();
    }

    protected ErrorMap checkRessource(AbstractResourceBean resource) {
        ErrorMap errorMap = new ErrorMap();
        if (resource.getSize() > getPollenServiceConfig().getResourceMaxSize()) {

            UnitHuman sizeUnitHuman = UnitHuman.getUnitHuman(resource.getSize());
            UnitHuman maxUnitHuman = UnitHuman.getUnitHuman(getPollenServiceConfig().getResourceMaxSize());
            String message  = l(getLocale(),
                    "pollen.error.resource.maxSize",
                    resource.getName(),
                    sizeUnitHuman.getUnitValue(resource.getSize()),
                    sizeUnitHuman.getUnit("o"),
                    maxUnitHuman.getUnitValue(getPollenServiceConfig().getResourceMaxSize()),
                    maxUnitHuman.getUnit("o"));

            errorMap.addError("size", message);
        }
        checkNotNull(errorMap, PollenResource.PROPERTY_RESOURCE_TYPE, resource.getResourceType(), l(getLocale(),"pollen.error.resource.resourceTypeRequired"));
        return errorMap;
    }
}
