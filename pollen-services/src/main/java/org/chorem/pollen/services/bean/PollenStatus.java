package org.chorem.pollen.services.bean;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollenStatus {

    protected String version;

    protected String buildDate;

    protected String encoding;

    protected boolean persistenceOk;

    protected long nbPolls;

    protected List<String> errors;

    protected boolean allOk;

    protected String memoryAllocated;

    protected String memoryUsed;

    protected String memoryFree;

    protected String memoryMax;

    protected double loadAverage;

    protected long duration;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setPersistenceOk(boolean persistenceOk) {
        this.persistenceOk = persistenceOk;
    }

    public boolean isPersistenceOk() {
        return persistenceOk;
    }

    public void setNbPolls(long nbPolls) {
        this.nbPolls = nbPolls;
    }

    public long getNbPolls() {
        return nbPolls;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setAllOk(boolean allOk) {
        this.allOk = allOk;
    }

    public boolean isAllOk() {
        return allOk;
    }

    public void setMemoryAllocated(String memoryAllocated) {
        this.memoryAllocated = memoryAllocated;
    }

    public String getMemoryAllocated() {
        return memoryAllocated;
    }

    public void setMemoryUsed(String memoryUsed) {
        this.memoryUsed = memoryUsed;
    }

    public String getMemoryUsed() {
        return memoryUsed;
    }

    public void setMemoryFree(String memoryFree) {
        this.memoryFree = memoryFree;
    }

    public String getMemoryFree() {
        return memoryFree;
    }

    public void setMemoryMax(String memoryMax) {
        this.memoryMax = memoryMax;
    }

    public String getMemoryMax() {
        return memoryMax;
    }

    public void setLoadAverage(double loadAverage) {
        this.loadAverage = loadAverage;
    }

    public double getLoadAverage() {
        return loadAverage;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getDuration() {
        return duration;
    }
}
