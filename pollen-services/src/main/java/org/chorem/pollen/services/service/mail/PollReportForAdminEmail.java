package org.chorem.pollen.services.service.mail;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.persistence.entity.Poll;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollReportForAdminEmail extends AbstractReportForAdminEmail<Poll> {

    public PollReportForAdminEmail(Locale locale, TimeZone timeZone) {
        super(locale, timeZone);
    }

    @Override
    public String getSubject() {
        return I18n.l(locale, "pollen.service.mail.PollReportForAdminEmail.subject", poll.getTitle());
    }

    @Override
    public List<TopiaEntity> getEntitiesKey() {
        return Lists.newArrayList(poll, report);
    }

}
