package org.chorem.pollen.services;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public enum UnitHuman {

    UNIT("", 0),
    KILO("k", 3),
    MEGA("M", 6),
    GIGA("G", 9),
    TERA("T", 12),
    PETA("P", 15),
    EXA("E", 18),
    ZETTA("Z", 21),
    YOTTA("Y", 24);

    protected String prefix;

    protected int decimalPower;

    UnitHuman(String prefix, int decimalPower) {
        this.prefix = prefix;
        this.decimalPower = decimalPower;
    }

    public static UnitHuman getUnitHuman(double value) {
        int ordinal = (int) Math.floor(Math.log10(value) / 3);
        ordinal = Math.max(Math.min(ordinal, UnitHuman.values().length - 1), 0);
        return UnitHuman.values()[ordinal];
    }

    public double getUnitValue(double value) {
        return value / Math.pow(10, decimalPower);
    }

    public String getUnit(String unit) {
        return prefix + unit;
    }

    public static String toHumanString(double value, String unit) {
        UnitHuman unitHuman = getUnitHuman(value);
        return String.format("%4.2f %s", unitHuman.getUnitValue(value), unitHuman.getUnit(unit));
    }

}
