package org.chorem.pollen.services.bean;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.ChildFavoriteList;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ChildFavoriteListBean extends PollenBean<ChildFavoriteList> {

    protected FavoriteListBean child;

    protected double weight;

    public ChildFavoriteListBean() {
        super(ChildFavoriteList.class);
    }

    public FavoriteListBean getChild() {
        return child;
    }

    public void setChild(FavoriteListBean child) {
        this.child = child;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
