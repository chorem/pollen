package org.chorem.pollen.services.bean;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;

import java.util.Date;
import java.util.List;

public class QuestionBean extends PollenBean<Question>{

    protected String permission;

    protected Date createDate;

    protected String title;

    protected String description;

    protected Date beginChoiceDate;

    protected Date endChoiceDate;

    protected boolean choiceAddAllowed;

    protected int voteCountingType;

    protected VoteCountingConfig voteCountingConfig;

    protected String poll;

    protected boolean resultIsVisible;

    protected boolean commentIsVisible;

    protected boolean voteIsVisible;

    protected boolean participantsIsVisible;

    protected List<ChoiceBean> choices;

    protected long choiceCount;

    protected long voteCount;

    protected long commentCount;

    protected int questionOrder;

    protected boolean canVote;

    protected ReportResumeBean report;

    public QuestionBean() {
        super(Question.class);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginChoiceDate() {
        return beginChoiceDate;
    }

    public void setBeginChoiceDate(Date beginChoiceDate) {
        this.beginChoiceDate = beginChoiceDate;
    }

    public Date getEndChoiceDate() {
        return endChoiceDate;
    }

    public void setEndChoiceDate(Date endChoiceDate) {
        this.endChoiceDate = endChoiceDate;
    }

    public boolean isChoiceAddAllowed() {
        return choiceAddAllowed;
    }

    public void setChoiceAddAllowed(boolean choiceAddAllowed) {
        this.choiceAddAllowed = choiceAddAllowed;
    }

    public int getVoteCountingType() {
        return voteCountingType;
    }

    public void setVoteCountingType(int voteCountingType) {
        this.voteCountingType = voteCountingType;
    }

    public VoteCountingConfig getVoteCountingConfig() {
        return voteCountingConfig;
    }

    public void setVoteCountingConfig(VoteCountingConfig voteCountingConfig) {
        this.voteCountingConfig = voteCountingConfig;
    }

    public String getPoll() {
        return poll;
    }

    public void setPoll(String poll) {
        this.poll = poll;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isResultIsVisible() {
        return resultIsVisible;
    }

    public void setResultIsVisible(boolean resultIsVisible) {
        this.resultIsVisible = resultIsVisible;
    }

    public boolean isCommentIsVisible() {
        return commentIsVisible;
    }

    public void setCommentIsVisible(boolean commentIsVisible) {
        this.commentIsVisible = commentIsVisible;
    }

    public boolean isVoteIsVisible() {
        return voteIsVisible;
    }

    public void setVoteIsVisible(boolean voteIsVisible) {
        this.voteIsVisible = voteIsVisible;
    }

    public boolean isParticipantsIsVisible() {
        return participantsIsVisible;
    }

    public void setParticipantsIsVisible(boolean participantsIsVisible) {
        this.participantsIsVisible = participantsIsVisible;
    }

    public List<ChoiceBean> getChoices() {
        return choices;
    }

    public void setChoices(List<ChoiceBean> choices) {
        this.choices = choices;
    }

    public long getChoiceCount() {
        return choiceCount;
    }

    public void setChoiceCount(long choiceCount) {
        this.choiceCount = choiceCount;
    }

    public long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(long voteCount) {
        this.voteCount = voteCount;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public int getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(int questionOrder) {
        this.questionOrder = questionOrder;
    }

    public boolean isCanVote() {
        return canVote;
    }

    public void setCanVote(boolean canVote) {
        this.canVote = canVote;
    }

    public ReportResumeBean getReport() {
        return report;
    }

    public void setReport(ReportResumeBean report) {
        this.report = report;
    }
}
