package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.FavoriteList;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Report;
import org.chorem.pollen.persistence.entity.UserCredential;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.persistence.entity.VoterListMember;
import org.chorem.pollen.services.service.mail.ChoiceAddedEmail;
import org.chorem.pollen.services.service.mail.ChoiceReportEmail;
import org.chorem.pollen.services.service.mail.ChoiceReportForAdminEmail;
import org.chorem.pollen.services.service.mail.CommentAddedEmail;
import org.chorem.pollen.services.service.mail.CommentDeletedEmail;
import org.chorem.pollen.services.service.mail.CommentEditedEmail;
import org.chorem.pollen.services.service.mail.CommentReportEmail;
import org.chorem.pollen.services.service.mail.CommentReportForAdminEmail;
import org.chorem.pollen.services.service.mail.EmailService;
import org.chorem.pollen.services.service.mail.ExceedingMaxVotersEmail;
import org.chorem.pollen.services.service.mail.LostPasswordEmail;
import org.chorem.pollen.services.service.mail.PollClosedEmail;
import org.chorem.pollen.services.service.mail.PollCreatedEmail;
import org.chorem.pollen.services.service.mail.PollEndReminderEmail;
import org.chorem.pollen.services.service.mail.PollInvitationEmail;
import org.chorem.pollen.services.service.mail.PollReportEmail;
import org.chorem.pollen.services.service.mail.PollReportForAdminEmail;
import org.chorem.pollen.services.service.mail.ResendValidationEmail;
import org.chorem.pollen.services.service.mail.RestrictedPollInvitationEmail;
import org.chorem.pollen.services.service.mail.UserAccountCreatedEmail;
import org.chorem.pollen.services.service.mail.UserAccountCreatedFromProviderEmail;
import org.chorem.pollen.services.service.mail.UserAccountDeletedEmail;
import org.chorem.pollen.services.service.mail.UserAccountEditedEmail;
import org.chorem.pollen.services.service.mail.UserAccountEmailAddressAddedEmail;
import org.chorem.pollen.services.service.mail.UserAccountEmailValidatedEmail;
import org.chorem.pollen.services.service.mail.UserAccountPasswordChangedEmail;
import org.chorem.pollen.services.service.mail.VoteAddedEmail;
import org.chorem.pollen.services.service.mail.VoteDeletedEmail;
import org.chorem.pollen.services.service.mail.VoteEditedEmail;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created on 4/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class NotificationService extends PollenServiceSupport {

    public void onUserCreated(PollenUser user, String token) {
        if (user.getDefaultEmailAddress() != null) {
            EmailService emailService = getEmailService();
            UserAccountCreatedEmail email = emailService.newUserAccountCreatedEmail(user, token);
            email.addTo(user.getDefaultEmailAddress().getEmailAddress());
            emailService.send(email);
            commit();
        }
    }

    public void onUserCreatedFromProvider(PollenUser user, UserCredential credential) {
        if (user.getDefaultEmailAddress() != null) {
            EmailService emailService = getEmailService();
            UserAccountCreatedFromProviderEmail email = emailService.newUserAccountCreatedFromProviderEmail(user, credential);
            email.addTo(user.getDefaultEmailAddress().getEmailAddress());
            emailService.send(email);
            commit();
        }
    }

    public void onResendValidation(PollenUser user, PollenUserEmailAddress emailAddress, String token) {
        EmailService emailService = getEmailService();
        ResendValidationEmail email = emailService.newUserResendValidationEmail(user, token);
        email.addTo(emailAddress.getEmailAddress());
        emailService.send(email);
        commit();
    }

    public void onUserEdited(PollenUser user) {
        if (user.getDefaultEmailAddress() != null) {
            EmailService emailService = getEmailService();
            UserAccountEditedEmail email = emailService.newUserAccountEditedEmail(user);
            email.addTo(user.getDefaultEmailAddress().getEmailAddress());
            emailService.send(email);
            commit();
        }
    }

    public void onUserDeleted(PollenUser user) {
        if (user.getDefaultEmailAddress() != null) {
            EmailService emailService = getEmailService();
            UserAccountDeletedEmail email = emailService.newUserAccountDeletedEmail(user);
            email.addTo(user.getDefaultEmailAddress().getEmailAddress());
            emailService.send(email);
            commit();
        }
    }

    public void onUserPasswordChanged(PollenUser user) {
        if (user.getDefaultEmailAddress() != null) {
            EmailService emailService = getEmailService();
            UserAccountPasswordChangedEmail email = emailService.newUserAccountPasswordChangedEmail(user);
            email.addTo(user.getDefaultEmailAddress().getEmailAddress());
            emailService.send(email);
            commit();
        }
    }

    public void onUserEmailAddressAdded(PollenUser user, PollenUserEmailAddress emailAddress, String token) {
        EmailService emailService = getEmailService();
        UserAccountEmailAddressAddedEmail email = emailService.newUserAccountEmailAddressAddedEmail(user, token);
        email.addTo(emailAddress.getEmailAddress());
        emailService.send(email);
        commit();
    }

    public void onUserEmailValidated(PollenUser user, PollenUserEmailAddress emailAddress) {
        EmailService emailService = getEmailService();
        UserAccountEmailValidatedEmail email = emailService.newUserAccountEmailValidatedEmail(user);
        email.addTo(emailAddress.getEmailAddress());
        emailService.send(email);
        commit();
    }

    public void onUserLostPasswordAsked(PollenUser user, String newPassword) {
        if (user.getDefaultEmailAddress() != null) {
            EmailService emailService = getEmailService();
            LostPasswordEmail email = emailService.newLostPasswordEmail(user, newPassword);
            email.addTo(user.getDefaultEmailAddress().getEmailAddress());
            emailService.send(email);
            commit();
        }
    }

    public void onFavoriteListAdded(PollenUser user, FavoriteList favoriteList) {
        // TODO
        // 170606 kmorin really ?
        // do nothing for now
    }

    public void onFavoriteListEdited(PollenUser user, FavoriteList favoriteList) {
        // TODO
        // 170606 kmorin really ?
        // do nothing for now
    }

    public void onFavoriteListDeleted(PollenUser user, FavoriteList favoriteList) {
        // TODO
        // 170606 kmorin really ?
        // do nothing for now
    }

    public void onPollCreated(Poll poll) {

        EmailService emailService = getEmailService();

        if (StringUtils.isNotBlank(poll.getCreator().getEmail())) {
            PollCreatedEmail email = emailService.newPollCreatedEmail(poll);

            email.addTo(poll.getCreator().getEmail());

            emailService.send(email);
        }

        commit();
    }

    public void onPollEdited(Poll poll) {
        // do nothing for now
    }

    public void onPollDeleted(Poll poll) {
        // do nothing for now
    }

    public void onPollClosed(Poll poll) {
        EmailService emailService = getEmailService();
        PollClosedEmail email = emailService.newPollClosedEmail(poll);
        //email.addTo(poll.getCreator().getEmail());
        emailService.send(email);
        commit();
    }

    public void onPollReopened(Poll poll) {
        // do nothing for now
    }

    public void onChoiceAdded(Question question, Choice choice) {
        if (question.getPoll().isNewChoiceNotification()) {
            EmailService emailService = getEmailService();
            ChoiceAddedEmail email = emailService.newChoiceAddedEmail(question, choice);
            email.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(email);
            commit();
        }
    }

    public void onChoiceEdited(Question question, Choice choice) {
        // do nothing for now
    }

    public void onChoiceDeleted(Question question, Choice choice) {
        // do nothing for now
    }

    public void onCommentAdded(Poll poll, Comment comment) {
        if (poll.isCommentNotification()) {
            EmailService emailService = getEmailService();
            CommentAddedEmail email = emailService.newCommentAddedEmail(poll, comment);
            email.addTo(poll.getCreator().getEmail());
            emailService.send(email);
            commit();
        }
    }

    public void onCommentEdited(Poll poll, Comment comment) {
        if (poll.isCommentNotification()) {
            EmailService emailService = getEmailService();
            CommentEditedEmail email = emailService.newCommentEditedEmail(poll, comment);
            email.addTo(poll.getCreator().getEmail());
            emailService.send(email);
            commit();
        }
    }

    public void onCommentDeleted(Poll poll, Comment comment) {
        if (poll.isCommentNotification()) {
            EmailService emailService = getEmailService();
            CommentDeletedEmail email = emailService.newCommentDeletedEmail(poll, comment);
            email.addTo(poll.getCreator().getEmail());
            emailService.send(email);
            commit();
        }
    }

    public void onCommentAdded(Question question, Comment comment) {
        if (question.getPoll().isCommentNotification()) {
            EmailService emailService = getEmailService();
            CommentAddedEmail email = emailService.newCommentAddedEmail(question, comment);
            email.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(email);
            commit();
        }
    }

    public void onCommentEdited(Question question, Comment comment) {
        if (question.getPoll().isCommentNotification()) {
            EmailService emailService = getEmailService();
            CommentEditedEmail email = emailService.newCommentEditedEmail(question, comment);
            email.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(email);
            commit();
        }
    }

    public void onCommentDeleted(Question question, Comment comment) {
        if (question.getPoll().isCommentNotification()) {
            EmailService emailService = getEmailService();
            CommentDeletedEmail email = emailService.newCommentDeletedEmail(question, comment);
            email.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(email);
            commit();
        }
    }

    public void onVoteAdded(Question question, Vote vote) {
        if (question.getPoll().isVoteNotification()) {
            EmailService emailService = getEmailService();

            VoteAddedEmail voteAddedEmail = emailService.newVoteAddedEmail(question, vote);
            voteAddedEmail.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(voteAddedEmail);
            commit();
        }
    }

    public void onVoteEdited(Question question, Vote vote) {
        if (question.getPoll().isVoteNotification()) {
            EmailService emailService = getEmailService();

            VoteEditedEmail voteEditedEmail = emailService.newVoteEditedEmail(question, vote);
            voteEditedEmail.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(voteEditedEmail);
            commit();
        }
    }

    public void onVoteDeleted(Question question, Vote vote) {
        if (question.getPoll().isVoteNotification()) {
            EmailService emailService = getEmailService();

            VoteDeletedEmail voteDeletedEmail = emailService.newVoteDeletedEmail(question, vote);
            voteDeletedEmail.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(voteDeletedEmail);
            commit();
        }
    }

    public void sendInvitation(Poll poll, List<VoterListMember> voterListMembers) {
        EmailService emailService = getEmailService();

        voterListMembers.stream()
                .map(VoterListMember::getMember)
                .distinct()
                .forEach(pollenPrincipal -> {
                    RestrictedPollInvitationEmail email = emailService.newRestrictedPollInvitationEmail(poll, pollenPrincipal);
                    email.addTo(pollenPrincipal.getEmail());
                    emailService.send(email);
                });
        commit();

    }

    public void sendPollEndReminder(Poll poll, Locale locale) {
        EmailService emailService = getEmailService();
        PollEndReminderEmail pollEndReminderEmail = emailService.newPollEndReminderEmail(poll, locale);

        pollEndReminderEmail.addTo(poll.getCreator().getEmail());
        emailService.send(pollEndReminderEmail);
        commit();
    }

    public void onAddCommentReport(Poll poll, Comment comment, Report report) {

        EmailService emailService = getEmailService();

        // email to poll creator
        if (StringUtils.isNotBlank(poll.getCreator().getEmail())) {
            CommentReportEmail commentReportEmail = emailService.newCommentReportEmail(poll, comment, report);

            commentReportEmail.addTo(poll.getCreator().getEmail());
            emailService.send(commentReportEmail);
        }

        // email to administrators
        getAdminsToSentReport(poll, comment).forEach(admin -> {
            CommentReportForAdminEmail commentReportForAdminEmail = emailService.newCommentReportForAdminEmail(poll, comment, report, admin);

            commentReportForAdminEmail.addTo(admin.getDefaultEmailAddress().getEmailAddress());
            emailService.send(commentReportForAdminEmail);
        });
        commit();

    }

    public void onAddQuestionCommentReport(Question question, Comment comment, Report report) {

        EmailService emailService = getEmailService();

        //FIXME JC181005 - APRIL - Use proper Email for question comments (now re-using poll one)

        // email to poll creator
        if (StringUtils.isNotBlank(question.getPoll().getCreator().getEmail())) {
            CommentReportEmail commentReportEmail = emailService.newCommentReportEmail(question.getPoll(), comment, report);

            commentReportEmail.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(commentReportEmail);
        }

        // email to administrators
        getAdminsToSentReport(question.getPoll(), comment).forEach(admin -> {
            CommentReportForAdminEmail commentReportForAdminEmail = emailService.newCommentReportForAdminEmail(question.getPoll(), comment, report, admin);

            commentReportForAdminEmail.addTo(admin.getDefaultEmailAddress().getEmailAddress());
            emailService.send(commentReportForAdminEmail);
        });
        commit();

    }

    protected List<PollenUser> getAdminsToSentReport(Poll poll, TopiaEntity target) {
        long score = getReportService().getScore(target);

        List<PollenUser> admins = Collections.emptyList();

        if (StringUtils.isBlank(poll.getCreator().getEmail()) || score >= getPollenServiceConfig().getReportMaxScore()) {

            admins = getPollenUserDao().forAdministratorEquals(true).findAll();

        }

        return admins;
    }

    public void onAddChoiceReport(Question question, Choice choice, Report report) {

        EmailService emailService = getEmailService();

        // email to poll creator
        if (StringUtils.isNotBlank(question.getPoll().getCreator().getEmail())) {
            ChoiceReportEmail choiceReportEmail = emailService.newChoiceReportEmail(question.getPoll(), choice, report);

            choiceReportEmail.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(choiceReportEmail);
        }

        // email to administrators
        getAdminsToSentReport(question.getPoll(), choice).forEach(admin -> {
            ChoiceReportForAdminEmail choiceReportForAdminEmail = emailService.newChoiceReportForAdminEmail(question.getPoll(), choice, report, admin);

            choiceReportForAdminEmail.addTo(admin.getDefaultEmailAddress().getEmailAddress());
            emailService.send(choiceReportForAdminEmail);
        });
        commit();

    }

    public void onAddPollReport(Poll poll, Report report) {

        EmailService emailService = getEmailService();

        // email to poll creator
        if (StringUtils.isNotBlank(poll.getCreator().getEmail())) {
            PollReportEmail pollReportEmail = emailService.newPollReportEmail(poll, report);

            pollReportEmail.addTo(poll.getCreator().getEmail());
            emailService.send(pollReportEmail);
        }

        // email to administrators
        getAdminsToSentReport(poll, poll).forEach(admin -> {
            PollReportForAdminEmail pollReportForAdminEmail = emailService.newPollReportForAdminEmail(poll, report, admin);

            pollReportForAdminEmail.addTo(admin.getDefaultEmailAddress().getEmailAddress());
            emailService.send(pollReportForAdminEmail);
        });
        commit();

    }

    public void onAddQuestionReport(Question question, Report report) {

        EmailService emailService = getEmailService();

        //FIXME JC181005 - APRIL - Use proper email for questions - now re-using poll one

        // email to poll creator
        if (StringUtils.isNotBlank(question.getPoll().getCreator().getEmail())) {
            PollReportEmail pollReportEmail = emailService.newPollReportEmail(question.getPoll(), report);

            pollReportEmail.addTo(question.getPoll().getCreator().getEmail());
            emailService.send(pollReportEmail);
        }

        // email to administrators
        getAdminsToSentReport(question.getPoll(), question.getPoll()).forEach(admin -> {
            PollReportForAdminEmail pollReportForAdminEmail = emailService.newPollReportForAdminEmail(question.getPoll(), report, admin);

            pollReportForAdminEmail.addTo(admin.getDefaultEmailAddress().getEmailAddress());
            emailService.send(pollReportForAdminEmail);
        });
        commit();

    }

    //TODO JC181002 - APRIL - Per question ?
    public void onExceedingMaxVoters(Poll poll, int maxVoters) {
        EmailService emailService = getEmailService();

        ExceedingMaxVotersEmail email = emailService.newExceedingMaxVotersEmail(poll, maxVoters);
        email.addTo(poll.getCreator().getEmail());
        emailService.send(email);
        commit();
    }
}
