package org.chorem.pollen.services.service.mail;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.services.PollenTechnicalException;

import java.util.function.BiConsumer;
import java.util.stream.Stream;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public enum PollenMailType {

    cha(ChoiceAddedEmail.class, (service, context) -> service.noAction()),
    chd(ChoiceDeletedEmail.class, (service, context) -> service.noAction()),
    che(ChoiceEditedEmail.class, (service, context) -> service.noAction()),
    chr(ChoiceReportEmail.class, (service, context) -> service.noAction()),
    chrfa(ChoiceReportForAdminEmail.class, (service, context) -> service.noAction()),
    coa(CommentAddedEmail.class, (service, context) -> service.noAction()),
    cod(CommentDeletedEmail.class, (service, context) -> service.noAction()),
    coe(CommentEditedEmail.class, (service, context) -> service.noAction()),
    cor(CommentReportEmail.class, (service, context) -> service.noAction()),
    corfa(CommentReportForAdminEmail.class, (service, context) -> service.noAction()),
    emv(ExceedingMaxVotersEmail.class, (service, context) -> service.noAction()),
    fb(FeedbackEmail.class, (service, context) -> service.noAction()),
    lp(LostPasswordEmail.class, (service, context) -> service.noAction()),
    pcpe(PollChoicePeriodEndedEmail.class, (service, context) -> service.noAction()),
    pcps(PollChoicePeriodStartedEmail.class, (service, context) -> service.noAction()),
    pcl(PollClosedEmail.class, (service, context) -> service.noAction()),
    pcr(PollCreatedEmail.class, (service, context) -> service.undeliveredFlagPollenPrincipal(context.getPoll().getCreator(), context.getMessage())),
    per(PollEndReminderEmail.class, (service, context) -> service.noAction()),
    pi(PollInvitationEmail.class, (service, context) -> service.forwardToCreatorAction(context.getPoll(), context.getMessage())),
    pr(PollReportEmail.class, (service, context) -> service.noAction()),
    prfa(PollReportForAdminEmail.class, (service, context) -> service.noAction()),
    pvpe(PollVotePeriodEndedEmail.class, (service, context) -> service.noAction()),
    pvps(PollVotePeriodStartedEmail.class, (service, context) -> service.noAction()),
    pvr(PollVoteReminderEmail.class, (service, context) -> service.noAction()),
    rv(ResendValidationEmail.class, (service, context) -> service.noAction()),
    rpi(RestrictedPollInvitationEmail.class, (service, context) -> {
        service.forwardToCreatorAction(context.getPoll(), context.getMessage());
        service.undeliveredFlagPollenPrincipal(context.getPrincipal(), context.getMessage());
    }),
    uac(UserAccountCreatedEmail.class, (service, context) -> service.noAction()),
    uacfp(UserAccountCreatedFromProviderEmail.class, (service, context) -> service.noAction()),
    uad(UserAccountDeletedEmail.class, (service, context) -> service.noAction()),
    uae(UserAccountEditedEmail.class, (service, context) -> service.noAction()),
    uaeaa(UserAccountEmailAddressAddedEmail.class, (service, context) -> service.noAction()),
    uaev(UserAccountEmailValidatedEmail.class, (service, context) -> service.noAction()),
    uapc(UserAccountPasswordChangedEmail.class, (service, context) -> service.noAction()),
    va(VoteAddedEmail.class, (service, context) -> service.noAction()),
    vd(VoteDeletedEmail.class, (service, context) -> service.noAction()),
    ve(VoteEditedEmail.class, (service, context) -> service.noAction())
    ;

    protected final Class<? extends PollenMail> emailType;

    protected final BiConsumer<MailBoxService, PollenMailReturnContext> action;

    PollenMailType(Class<? extends PollenMail> emailType, BiConsumer<MailBoxService, PollenMailReturnContext> action) {
        this.emailType = emailType;
        this.action = action;
    }


    public Class<? extends PollenMail> getEmailType() {
        return emailType;
    }

    public static PollenMailType valueOfMail(PollenMail pollenMail) {
        return Stream.of(values())
                .filter(type -> type.getEmailType().isInstance(pollenMail))
                .findFirst()
                .orElseThrow(() -> new PollenTechnicalException("unknown pollenEmailType of " + pollenMail.getClass()));
    }

    public BiConsumer<MailBoxService, PollenMailReturnContext> getAction() {
        return action;
    }
}
