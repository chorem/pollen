package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.PollenUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenUserBean extends PollenBean<PollenUser> {

    protected String name;

    protected boolean administrator;

    protected String language;

    protected String password;

    protected boolean banned;

    protected boolean emailIsValidate;

    protected boolean withPassword;

    protected List<UserCredentialBean> credentials = new ArrayList<>();

    protected boolean gtuValidated;

    protected String avatar;

    protected Date premiumTo;

    protected boolean premium;

    protected List<PollenUserEmailAddressBean> emailAddresses = new ArrayList<>();

    protected PollenUserEmailAddressBean defaultEmailAddress;

    protected boolean canCreatePoll;

    public PollenUserBean() {
        super(PollenUser.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEmailIsValidate() {
        return emailIsValidate;
    }

    public void setEmailIsValidate(boolean emailIsValidate) {
        this.emailIsValidate = emailIsValidate;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public boolean isWithPassword() {
        return withPassword;
    }

    public void setWithPassword(boolean withPassword) {
        this.withPassword = withPassword;
    }

    public List<UserCredentialBean> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<UserCredentialBean> credentials) {
        this.credentials = credentials;
    }

    public boolean isGtuValidated() {
        return gtuValidated;
    }

    public void setGtuValidated(boolean gtuValidated) {
        this.gtuValidated = gtuValidated;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    public Date getPremiumTo() {
        return premiumTo;
    }

    public void setPremiumTo(Date premiumTo) {
        this.premiumTo = premiumTo;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public List<PollenUserEmailAddressBean> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(List<PollenUserEmailAddressBean> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public PollenUserEmailAddressBean getDefaultEmailAddress() {
        return defaultEmailAddress;
    }

    public void setDefaultEmailAddress(PollenUserEmailAddressBean defaultEmailAddress) {
        this.defaultEmailAddress = defaultEmailAddress;
    }

    public boolean isCanCreatePoll() {
        return canCreatePoll;
    }

    public void setCanCreatePoll(boolean canCreatePoll) {
        this.canCreatePoll = canCreatePoll;
    }
}
