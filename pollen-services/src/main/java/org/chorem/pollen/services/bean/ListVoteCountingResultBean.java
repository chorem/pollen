package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.votecounting.model.ListVoteCountingResult;
import org.chorem.pollen.votecounting.model.VoteCountingResult;

import java.util.ArrayList;
import java.util.List;

/**
 * //FIXME
 * Created on 5/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class ListVoteCountingResultBean {

    protected VoteCountingResultBean mainResult;

    protected final List<VoteCountingResultBean> subListResult;

    public ListVoteCountingResultBean() {
        subListResult = new ArrayList<>();
    }

    public void fromResult(ListVoteCountingResult result) {

        setMainResult(result.getMainResult());

        for (String groupId : result.getGroupIds()) {
            VoteCountingResultBean voteCountingResult = new VoteCountingResultBean();
            voteCountingResult.fromResult(result.getListResult(groupId));
            subListResult.add(voteCountingResult);
        }

    }

    private void setMainResult(VoteCountingResult mainResult) {
        this.mainResult = new VoteCountingResultBean();
        this.mainResult.fromResult(mainResult);
    }

    public VoteCountingResultBean getMainResult() {
        return mainResult;
    }

}
