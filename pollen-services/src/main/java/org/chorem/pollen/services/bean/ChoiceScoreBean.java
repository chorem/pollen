package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.votecounting.model.ChoiceScore;

import java.math.BigDecimal;

/**
 * Created on 5/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class ChoiceScoreBean {

    /** Id of the choice. */
    protected PollenEntityId<Choice> choiceId;

    /**
     * Global score for this choice (in some vote count it does not mean a
     * lot (and it should be improved to take account of the notion of round)).
     */
    protected BigDecimal scoreValue;

    /** Order of this score (0 is winner, 1 is second,...). */
    protected int scoreOrder;

    public ChoiceScoreBean() {
        this.choiceId = PollenEntityId.newId(Choice.class);
    }

    public void fromResult(ChoiceScore result) {

        setChoiceId(result.getChoiceId());
        setScoreOrder(result.getScoreOrder());
        setScoreValue(result.getScoreValue());

    }

    public PollenEntityId<Choice> getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(String choiceId) {

        Preconditions.checkNotNull("choiceId can not be null", choiceId);
        this.choiceId.setEntityId(choiceId);
    }


    public void setChoiceId(PollenEntityId<Choice> choiceId) {
        Preconditions.checkNotNull("choiceId can not be null", choiceId);
        this.choiceId = choiceId;
    }


    public BigDecimal getScoreValue() {
        return scoreValue;
    }

    public void setScoreValue(BigDecimal scoreValue) {
        this.scoreValue = scoreValue;
    }

    public int getScoreOrder() {
        return scoreOrder;
    }

    public void setScoreOrder(int scoreOrder) {
        this.scoreOrder = scoreOrder;
    }
}
