package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.util.pagination.PaginationParameter;

import jakarta.ws.rs.QueryParam;

/**
 * Created on 5/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PaginationParameterBean {

    /**
     * 0-based page number
     */
    protected int pageNumber;

    /**
     * The size of each page. Value can be -1 (for infinite pageSize) or greater than 0
     */
    protected int pageSize;

    protected String order;

    protected boolean desc;

    public static PaginationParameterBean of(int pageNumber, int pageSize) {
        return of(pageNumber, pageSize, null, false);
    }

    public static PaginationParameterBean of(int pageNumber, int pageSize, String order, boolean desc) {
        PaginationParameterBean result = new PaginationParameterBean();
        result.setPageNumber(pageNumber);
        result.setPageSize(pageSize);
        result.setOrder(order);
        result.setDesc(desc);
        return result;
    }

    public PaginationParameter toPaginationParameter() {
        PaginationParameter paginationParameter;

        //XXX JC191126 - Hack to prevent value overflowing
//        if (pageSize == -1) {
//            pageSize = Integer.MAX_VALUE-1;
//        }

        if (order != null) {
            paginationParameter = PaginationParameter.of(pageNumber, pageSize, order, desc);
        } else {
            paginationParameter = PaginationParameter.of(pageNumber, pageSize);
        }

        return paginationParameter;

    }

    public int getPageNumber() {
        return pageNumber;
    }

    @QueryParam("pageNumber")
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    @QueryParam("pageSize")
    public void setPageSize(int pageSize) {
        //XXX JC191126 - Hack to prevent value overflowing
        if (pageSize == -1) {
            this.pageSize = Integer.MAX_VALUE-1;
        } else {
            this.pageSize = pageSize;
        }
    }

    public String getOrder() {
        return order;
    }

    @QueryParam("order")
    public void setOrder(String order) {
        this.order = order;
    }

    public boolean isDesc() {
        return desc;
    }

    @QueryParam("desc")
    public void setDesc(boolean desc) {
        this.desc = desc;
    }
}
