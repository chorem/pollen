package org.chorem.pollen.services.service.mail;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Report;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.services.PollenTechnicalException;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.stream.Stream;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public enum PollenEmailKeyType {

    P(Poll.class),
    C(Choice.class),
    V(Vote.class),
    U(PollenUser.class),
    R(Report.class),
    O(Comment.class),
    E(PollenPrincipal.class);

    protected final Class<? extends TopiaEntity> type;


    PollenEmailKeyType(Class<? extends TopiaEntity> type) {
        this.type = type;
    }

    public Class<? extends TopiaEntity> getType() {
        return type;
    }

    public static PollenEmailKeyType valueOfType(Class<? extends TopiaEntity> typeClass) {
        return Stream.of(values())
                .filter(type -> type.getType().isAssignableFrom(typeClass))
                .findFirst()
                .orElseThrow(() -> new PollenTechnicalException("unknown pollenEmailKeyType of " + typeClass));
    }

    public static PollenEmailKeyType valueOfEntity(TopiaEntity entity) {
        return valueOfType(entity.getClass());
    }


}
