package org.chorem.pollen.services.service.security;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Longs;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.entity.*;
import org.chorem.pollen.services.PollenServiceContext;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.persistence.UsersRight;
import org.chorem.pollen.services.service.PollService;
import org.chorem.pollen.services.service.PollenServiceSupport;
import org.nuiton.topia.persistence.TopiaNoResultException;

import java.nio.ByteBuffer;
import java.time.Clock;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

/**
 * SecurityService
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class SecurityService extends PollenServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SecurityService.class);

    @Override
    public void setServiceContext(PollenServiceContext serviceContext) {
        super.setServiceContext(serviceContext);
    }

    @Override
    public void checkIsConnected() {

        PollenSecurityContext securityContext = getSecurityContext();
        if (!securityContext.isConnected()) {
            throw new PollenUnauthorizedException("connected");
        }

    }

    @Override
    public void checkIsConnectedRequired() {
        if (getPollenServiceConfig().isUserConnectedRequired()) {
            checkIsConnected();
        }
    }

    @Override
    public void checkIsConnected(String userId) {

        PollenSecurityContext securityContext = getSecurityContext();
        if (!securityContext.isConnected() || !securityContext.getPollenUser().getTopiaId().equals(userId)) {
            throw new PollenUnauthorizedException("connected");
        }

    }

    @Override
    public void checkIsAdmin() {

        PollenSecurityContext securityContext = getSecurityContext();
        if (!securityContext.isAdmin()) {
            throw new PollenUnauthorizedException("administrator");
        }

    }

    public PollenEntityRef<PollenUser> login(String login, String password, Boolean rememberMe)
            throws PollenAuthenticationException, PollenEmailNotValidatedException, PollenUserBannedException {

        if (StringUtils.isBlank(password)) {
            throw new PollenAuthenticationException();
        }

        PollenUser user = getPollenUserDao().findUserWithEmailAddressOrNull(login);
        if (user == null) {
            throw new PollenAuthenticationException();
        }

        try {
            checkUserPassword(user, password);
        } catch (PollenInvalidPasswordException e) {
            //Erreur au login, on essaye tous les providers enregistrés, au cas où
            boolean valid = false;
            for (UserCredential credential : user.getUserCredential()) {
                valid = valid || getUserCredentialDao().isCredentialValid(credential.getProvider(),
                        credential.getUserId(),
                        user.getTopiaId(),
                        login);
            }
            if (!valid) {
                throw new PollenAuthenticationException(e);
            }
        }

        if (!user.isEmailValidated()) {
            throw new PollenEmailNotValidatedException();
        }
        if (user.isBanned()) {
            throw new PollenUserBannedException();
        }

        this.getSecurityContext().setPollenUser(user);
        return PollenEntityRef.of(user);

    }

    public void logout() {

        // Remove the session token from security context
        getSecurityContext().setPollenUser(null);
    }

    public void lostPassword(String login) throws PollenUserUnknownException, PollenEmailNotValidatedException {

        Preconditions.checkNotNull(login);

        PollenUser user = getPollenUserDao().findUserWithEmailAddressOrNull(login);

        if (user == null) {
            throw new PollenUserUnknownException();
        }

        if (!user.isEmailValidated()) {
            throw new PollenEmailNotValidatedException();
        }

        String newPassword = serviceContext.generatePassword();

        getSecurityService().setUserPassword(user, newPassword);
        commit();

        getNotificationService().onUserLostPasswordAsked(user, newPassword);

    }

    public void deleteObsoleteSessionTokens() {

        Set<SessionToken> sessionTokens = getSessionTokenDao().findAllBeforeEndDate(getNow());

        if (log.isDebugEnabled()) {
            sessionTokens.forEach(sessionToken ->
                log.debug(String.format("SessionToken %s delete expired : %s",
                        sessionToken.getPollenToken().getToken(),
                        sessionToken.getPollenToken().getEndDate()))
            );
        }

        getSessionTokenDao().deleteAll(sessionTokens);
        commit();

    }

    public String getToken() {
        String token = "";
        if (isConnected()) {
            token = getToken(getConnectedUser());
        }
        return token;
    }


    public String getToken(PollenUser user) {

        Date now = getNow();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.SECOND, getPollenServiceConfig().getTokenTimeout());
        Date expireDate = calendar.getTime();

        String token = JWT.create()
                .withIssuer(getPollenServiceConfig().getTokenIssue())
                .withIssuedAt(now)
                .withExpiresAt(expireDate)
                .withSubject(getReduceId(user))
                .sign(getAlgorithm());

        return token;
    }

    public PollenUser getUserFromToken(String token) throws PollenInvalidSessionTokenException {
        PollenUser user = null;

        if (StringUtils.isNotBlank(token)) {

            try {
                JWTVerifier.BaseVerification verification =
                        (JWTVerifier.BaseVerification) JWT.require(getAlgorithm())
                        .withIssuer(getPollenServiceConfig().getTokenIssue());

                Clock now = Clock.fixed(this.getNow().toInstant(), ZoneId.systemDefault());

                JWTVerifier verifier = verification.build(now);
                DecodedJWT jwt = verifier.verify(token);
                String userId = jwt.getSubject();

                user = findEntity(PollenUser.class, userId);

            } catch (JWTVerificationException e) {
                if (log.isInfoEnabled()) {
                    log.info("Invalid Token : " + e.getMessage() + " - " + token);
                }
            } catch (TopiaNoResultException e) {
                if (log.isErrorEnabled()) {
                    log.error("Not find user from token : " + token);
                }
            }
        }
        return user;
    }

    protected Algorithm getAlgorithm() {
        byte[] secret = getPollenServiceConfig().getTokenSecretBytes();
        return Algorithm.HMAC256(secret);
    }

    public String generateEmailToken(PollenUserEmailAddress email) {
        long expireDate = getNow().getTime() + getPollenServiceConfig().getTokenTimeout() * 1000L;
        byte[] bytesEmail = email.getEmailAddress().getBytes();


        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES + bytesEmail.length);
        buffer.putLong(expireDate);
        buffer.put(bytesEmail);
        byte[] message = buffer.array();

        byte[] encryptMessage = getCryptoService().encryptMessageSymmetric(message);

        String token = Base64.getUrlEncoder().encodeToString(encryptMessage);
        return token;
    }

    public PollenUserEmailAddress getEmailAdresseFromToken(String token) {
        PollenUserEmailAddress pollenUserEmailAddress = null;

        if (StringUtils.isNotBlank(token)) {

            byte[] encryptMessage = Base64.getUrlDecoder().decode(token);

            byte[] message = getCryptoService().decryptMessageSymmetric(encryptMessage);

            byte[] bytesExpireDate = Arrays.copyOfRange(message, 0, Long.BYTES);
            byte[] bytesEmail = Arrays.copyOfRange(message, Long.BYTES, message.length);

            long expireDate = Longs.fromByteArray(bytesExpireDate);
            String emailAdresse = new String(bytesEmail);

            if (getNow().getTime() <= expireDate) {
                pollenUserEmailAddress = getPollenUserEmailAddressDao().forEmailAddressEquals(emailAdresse).findUniqueOrNull();

            }

        }
        return pollenUserEmailAddress;
    }

    public PollenToken generateNewToken() {

        // Generate token
        String token = serviceContext.generateToken();

        // Create instance
        PollenTokenTopiaDao dao = getPersistenceContext().getPollenTokenDao();
        PollenToken pollenToken = dao.createByNaturalId(token);
        pollenToken.setCreationDate(serviceContext.getNow());

        return pollenToken;

    }

    public PollenPrincipal generatePollenPrincipal() {

        PollenPrincipalTopiaDao dao = getPersistenceContext().getPollenPrincipalDao();

        // Create instance
        PollenPrincipal principal = dao.create();

        // Generate token
        PollenToken token = generateNewToken();
        principal.setPermission(token);

        return principal;

    }

    public PollenPrincipal getPollenPrincipalByPermissionToken(String principalId) {

        PollenPrincipal principal = null;
        if (principalId != null) {
            principal = getPollenPrincipalDao().findByPermissionToken(principalId);
        }
        return principal;

    }

    public void setUserPassword(PollenUser user, String newPassword) {

        String salt = serviceContext.generateSalt();

        String encodedPassword = serviceContext.encodePassword(salt, newPassword);

        user.setSalt(salt);
        user.setPassword(encodedPassword);
    }

    public void checkUserPassword(PollenUser user, String password) throws PollenInvalidPasswordException {

        String encodedPassword = null;
        //Si pas de salt, c'est qu'on est sur un provider (google/facebook...)
        if (password != null && user.getSalt() != null) {
            encodedPassword = serviceContext.encodePassword(user.getSalt(), password);
        }
        boolean valid = Objects.equals(encodedPassword, user.getPassword());

        //Si pas loggué ici, on essaye tous les providers enregistrés
        if (!valid) {
            for (UserCredential credential : user.getUserCredential()) {
                valid = valid || getUserCredentialDao().isCredentialValid(credential.getProvider(),
                        credential.getUserId(),
                        user.getTopiaId(),
                        credential.getEmail());
            }
        }

        //Là on a vraiment tout tenté, donc si on a pas réussi à logguer c'est que c'est foutu on n'y arrivera pas autrement.
        if (user.isBanned() || !valid) {
            throw new PollenInvalidPasswordException();
        }

    }

    protected boolean matchPrincipal(PollenPrincipal principal) {
        return principal != null
                && (principal.equals(getSecurityContext().getMainPrincipal())
                    || principal.getPollenUser() != null && principal.getPollenUser().equals(getSecurityContext().getPollenUser()));
    }

    public boolean canAddPoll() {
        UsersRight usersCanCreatePoll = getPollenServiceConfig().getUsersCanCreatePoll();
        return UsersRight.ALL_USERS.equals(usersCanCreatePoll)
                || (UsersRight.USERS_CONNECTED.equals(usersCanCreatePoll) && isConnected())
                || (UsersRight.USERS_SELECTED.equals(usersCanCreatePoll) && isCanCreatePoll());

    }

    protected boolean isConnected() {
        return getSecurityContext().isConnected();
    }

    protected boolean isCanCreatePoll() {
        PollenSecurityContext context = getSecurityContext();
        return context.isConnected() && context.getPollenUser() != null && context.getPollenUser().isCanCreatePoll();
    }

    protected boolean isAdmin() {
        return getSecurityContext().isAdmin();
    }

    public boolean canRead(Poll poll) {
        return isAdmin()
                || Polls.isPollFree(poll)
                || matchPrincipal(poll.getCreator())
                || Polls.isPollRestricted(poll) && isInvited(poll)
                || Polls.isPollRegistered(poll) && isRegisteredAndAccept(poll);
    }

    protected boolean isInvited(Poll poll) {
        return getSecurityContext().getMainPrincipal() != null
                && getVoterListMemberDao()
                .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                .addEquals(VoterListMember.PROPERTY_MEMBER, getSecurityContext().getMainPrincipal())
                .exists()
            || getSecurityContext().isConnected()
                && getVoterListMemberDao()
                .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                .addEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_POLLEN_USER, getSecurityContext().getPollenUser())
                .exists();
    }

    protected boolean isRegisteredAndAccept(Poll poll) {
        String suffixes = poll.getEmailAddressSuffixes();
        return getSecurityContext().isConnected() && (StringUtils.isBlank(suffixes) ||
                Stream.of(suffixes.split(PollService.EMAIL_SUFFIX_SEPARATOR))
                .anyMatch(this::isConnectedUserAcceptEmailSuffix)
        );
    }

    protected boolean isConnectedUserAcceptEmailSuffix(String suffix) {
        return getConnectedUser().getEmailAddresses().stream()
                .filter(PollenUserEmailAddress::isValidated) // email Validate
                .map(PollenUserEmailAddress::getEmailAddress)
                .anyMatch(email -> email.endsWith(suffix));
    }

    protected boolean hasVoted(Question question) {
        return (getSecurityContext().getMainPrincipal() != null
                && getVoteDao()
                .forQuestionEquals(question)
                .addEquals(Vote.PROPERTY_VOTER, getSecurityContext().getMainPrincipal())
                .exists())
                || (getSecurityContext().isConnected()
                && getVoteDao()
                .forQuestionEquals(question)
                .addEquals(Vote.PROPERTY_VOTER + "." + PollenPrincipal.PROPERTY_POLLEN_USER, getSecurityContext().getPollenUser())
                .exists());
    }

    protected boolean hasVoted(Poll poll) {
        boolean hasVoted = false;
        //FIXME JC181004 - APRIL - Should use a single db request to perform this operation (perf issue).
        List<Question> questions = getQuestionService().getQuestions(poll);
        for (Question question:questions){
            hasVoted = hasVoted || hasVoted(question);
        }
        return hasVoted;
    }

    protected boolean isVoter(Poll poll) {

        boolean hasVotedToOneQuestion = false;

        List<Question> questions = getQuestionService().getQuestions(poll);

        for (Question question:questions) {
            hasVotedToOneQuestion = hasVotedToOneQuestion || hasVoted(question);
        }

        return isAdmin() || hasVotedToOneQuestion || (Polls.isPollRestricted(poll) && isInvited(poll));
    }

    protected boolean isVoter(Question question) {
        return isAdmin() || hasVoted(question) || (Polls.isPollRestricted(question.getPoll()) && isInvited(question.getPoll()));
    }

    protected boolean isCreator(Poll poll) {
        return isAdmin() || matchPrincipal(poll.getCreator());
    }

    public boolean canReadComments(Poll poll) {
        return canRead(poll)
                && (isAdmin()
                || CommentVisibility.EVERYBODY.equals(poll.getCommentVisibility())
                || CommentVisibility.VOTER.equals(poll.getCommentVisibility()) && (isVoter(poll) || isCreator(poll))
                || CommentVisibility.CREATOR.equals(poll.getCommentVisibility()) && isCreator(poll));
    }

    public boolean canReadComments(Question question) {
        return canRead(question)
                && (isAdmin()
                || CommentVisibility.EVERYBODY.equals(question.getPoll().getCommentVisibility())
                || CommentVisibility.VOTER.equals(question.getPoll().getCommentVisibility()) && (isVoter(question) || isCreator(question.getPoll()))
                || CommentVisibility.CREATOR.equals(question.getPoll().getCommentVisibility()) && isCreator(question.getPoll()));
    }

    public boolean canReadVotes(Question question) {
        return canRead(question)
                && (VoteVisibility.EVERYBODY.equals(question.getPoll().getVoteVisibility())
                || VoteVisibility.VOTER.equals(question.getPoll().getVoteVisibility()) && (isVoter(question) || isCreator(question.getPoll()))
                || VoteVisibility.CREATOR.equals(question.getPoll().getVoteVisibility()) && isCreator(question.getPoll()));
    }

    public boolean canReadVotes(Poll poll) {
        return canRead(poll)
                && (VoteVisibility.EVERYBODY.equals(poll.getVoteVisibility())
                || VoteVisibility.VOTER.equals(poll.getVoteVisibility()) && (isVoter(poll) || isCreator(poll))
                || VoteVisibility.CREATOR.equals(poll.getVoteVisibility()) && isCreator(poll));
    }

    public boolean canReadParticipants(Poll poll) {
        return Polls.isPollRestricted(poll) && canRead(poll)
                && (VoteVisibility.ANONYMOUS.equals(poll.getVoteVisibility())
                || Polls.isFinished(poll, getNow())
                || !poll.isContinuousResults());
    }

    public boolean canReadParticipants(Question question) {
        return Polls.isPollRestricted(question.getPoll()) && canRead(question)
                && (VoteVisibility.ANONYMOUS.equals(question.getPoll().getVoteVisibility())
                || Polls.isFinished(question.getPoll(), getNow())
                || !question.getPoll().isContinuousResults());
    }

    public boolean canRead(Vote vote) {
        return matchPrincipal(vote.getVoter())
                || canReadVotes(vote.getQuestion());
    }

    public boolean canRead(Question question) {
        return isAdmin()
                || Polls.isPollFree(question.getPoll())
                || matchPrincipal(question.getPoll().getCreator())
                || Polls.isPollRestricted(question.getPoll()) && isInvited(question.getPoll())
                || Polls.isPollRegistered(question.getPoll()) && isRegisteredAndAccept(question.getPoll());
    }

    public boolean canReadResult(Question question) {
        return (Polls.isFinished(question.getPoll(), getNow()) || question.getPoll().isContinuousResults())
                && canRead(question)
                && (ResultVisibility.EVERYBODY.equals(question.getPoll().getResultVisibility())
                || ResultVisibility.VOTER.equals(question.getPoll().getResultVisibility()) && (isVoter(question) || isCreator(question.getPoll()))
                || ResultVisibility.CREATOR.equals(question.getPoll().getResultVisibility()) && isCreator(question.getPoll()));
    }

    public boolean canReadResult(Poll poll) {
        return (Polls.isFinished(poll, getNow()) || poll.isContinuousResults())
                && canRead(poll)
                && (ResultVisibility.EVERYBODY.equals(poll.getResultVisibility())
                || ResultVisibility.VOTER.equals(poll.getResultVisibility()) && (isVoter(poll) || isCreator(poll))
                || ResultVisibility.CREATOR.equals(poll.getResultVisibility()) && isCreator(poll));
    }

    public boolean canAddChoice(Question question) {
        return !Polls.isFinished(question.getPoll(), getNow()) && canEdit(question.getPoll())
               || Polls.isAddChoiceRunning(question, getNow()) && canRead(question);
    }

    public boolean canAddComment(Poll poll) {
        return canRead(poll);
    }

    public boolean canAddComment(Question question) {
        return canRead(question);
    }

    public boolean canAddVote(Question question) {
        return Polls.isRunning(question.getPoll(), getNow())
                && (Polls.isPollFree(question.getPoll())
                || Polls.isPollRestricted(question.getPoll()) && isInvited(question.getPoll())  && !hasVoted(question)
                || Polls.isPollRegistered(question.getPoll()) && isRegisteredAndAccept(question.getPoll())  && !hasVoted(question)
                );
    }

    public boolean canAddVote(Poll poll) {
        return Polls.isRunning(poll, getNow())
                && (Polls.isPollFree(poll)
                || Polls.isPollRestricted(poll) && isInvited(poll)  && !hasVoted(poll)
                || Polls.isPollRegistered(poll) && isRegisteredAndAccept(poll)  && !hasVoted(poll)
        );
    }

    public boolean canEdit(Poll poll) {
        return matchPrincipal(poll.getCreator());
    }

    public boolean canEdit(Question question) {
        return matchPrincipal(question.getPoll().getCreator());
    }

    public boolean canEdit(Choice choice) {
        return !Polls.isFinished(choice.getQuestion().getPoll(), getNow()) && canEdit(choice.getQuestion().getPoll())
               || matchPrincipal(choice.getCreator());
    }

    public boolean canEdit(Comment comment) {
        return matchPrincipal(comment.getAuthor());
    }

    public boolean canEdit(Vote vote) {
        return matchPrincipal(vote.getVoter());
    }

    public boolean canDelete(Poll poll) {
        return isAdmin() || canEdit(poll);
    }

    public boolean canDelete(Choice choice) {
        return isAdmin() || canEdit(choice);
    }

    public boolean canDelete(Comment comment) {
        return isAdmin() || canEdit(comment.getPoll()) || matchPrincipal(comment.getAuthor());
    }

    public boolean canDelete(Vote vote) {
        return isAdmin() || canEdit(vote.getQuestion().getPoll()) || matchPrincipal(vote.getVoter());
    }

    public boolean canClose(Poll poll) {
        return canAddPoll() && canEdit(poll);
    }

    public boolean canClone(Poll poll) {
        return canAddPoll() && canEdit(poll);
    }

    public boolean canExport(Poll poll) {
        return canAddPoll() && canEdit(poll);
    }

}
