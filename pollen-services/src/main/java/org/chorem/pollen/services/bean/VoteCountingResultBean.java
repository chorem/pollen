package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.services.bean.voteCounting.VoteCountingDetailResultBean;
import org.chorem.pollen.votecounting.model.ChoiceScore;
import org.chorem.pollen.votecounting.model.VoteCountingResult;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 5/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoteCountingResultBean {

    /**
     * Results for each choice.
     * <p/>
     * <strong>Note:</strong> Natural order is used to describe choice scores
     * (first is winner,...).
     */
    protected List<ChoiceScoreBean> scores;

    protected VoteCountingDetailResultBean detail;

    protected int nbVotants;

    public VoteCountingResultBean() {
        scores = new LinkedList<>();
    }

    public void fromResult(VoteCountingResult result) {

        scores.clear();

        for (ChoiceScore choiceScore : result.getScores()) {

            ChoiceScoreBean choiceScoreBean = new ChoiceScoreBean();
            choiceScoreBean.fromResult(choiceScore);
            scores.add(choiceScoreBean);

        }
        setDetail(VoteCountingDetailResultBean.toBean(result.getDetailResult()));

        setNbVotants(result.getNbVotants());
    }

    public List<ChoiceScoreBean> getScores() {
        return scores;
    }

    public void setScores(List<ChoiceScoreBean> scores) {
        this.scores = scores;
    }

    public int getNbVotants() {
        return nbVotants;
    }

    public void setNbVotants(int nbVotants) {
        this.nbVotants = nbVotants;
    }

    public VoteCountingDetailResultBean getDetail() {
        return detail;
    }

    public void setDetail(VoteCountingDetailResultBean detail) {
        this.detail = detail;
    }
}
