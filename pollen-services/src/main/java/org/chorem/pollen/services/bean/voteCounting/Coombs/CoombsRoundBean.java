package org.chorem.pollen.services.bean.voteCounting.Coombs;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.votecounting.CoombsRound;
import org.chorem.pollen.votecounting.CoombsRoundChoice;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CoombsRoundBean {

    protected List<CoombsRoundChoiceBean> roundChoices;

    protected List<PollenEntityId<Choice>> choiceIdsExclude;

    public void fromResult(CoombsRound result) {

        for (CoombsRoundChoice coombsRoundChoice : result.getRoundChoices()) {

            CoombsRoundChoiceBean coombsRoundChoiceBean = new CoombsRoundChoiceBean();
            coombsRoundChoiceBean.fromResult(coombsRoundChoice);
            getRoundChoices().add(coombsRoundChoiceBean);

        }

        for (String choiceIdExclude : result.getChoiceIdsExclude()) {

            PollenEntityId<Choice> choiceId = PollenEntityId.newId(Choice.class);
            choiceId.setEntityId(choiceIdExclude);
            getChoiceIdsExclude().add(choiceId);

        }

    }


    public List<CoombsRoundChoiceBean> getRoundChoices() {
        if (roundChoices == null) {
            roundChoices = Lists.newLinkedList();
        }
        return roundChoices;
    }

    public void setRoundChoices(List<CoombsRoundChoiceBean> roundChoices) {
        this.roundChoices = roundChoices;
    }

    public List<PollenEntityId<Choice>> getChoiceIdsExclude() {
        if (choiceIdsExclude == null) {
            choiceIdsExclude = Lists.newLinkedList();
        }
        return choiceIdsExclude;
    }

    public void setChoiceIdsExclude(List<PollenEntityId<Choice>> choiceIdsExclude) {
        this.choiceIdsExclude = choiceIdsExclude;
    }
}
