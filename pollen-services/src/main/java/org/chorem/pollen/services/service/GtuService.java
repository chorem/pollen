package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.collections4.CollectionUtils;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.ResourceType;
import org.chorem.pollen.services.bean.resource.GtuMetaBean;
import org.chorem.pollen.services.bean.resource.ResourceStreamBean;
import org.nuiton.util.pagination.PaginationOrder;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class GtuService extends PollenServiceSupport {

    public GtuMetaBean toGtuMetaBean(PollenResource entity) {
        GtuMetaBean bean = new GtuMetaBean();
        bean.setEntityId(entity.getTopiaId());

        bean.setName(entity.getName());
        bean.setSize(entity.getSize());
        bean.setContentType(entity.getContentType());
        bean.setResourceType(entity.getResourceType());
        bean.setUploadDate(entity.getTopiaCreateDate());

        return bean;
    }

    public ResourceStreamBean getCurrentGtu() {

        return getCurrentGtu0()
                .map(getPollenResourceService()::toResourceStreamBean)
                .orElse(null);
    }

    public List<GtuMetaBean> getAllGtus() {
        checkIsAdmin();

        List<PollenResource> gtus = getPollenResourceDao()
                .forResourceTypeEquals(ResourceType.GTU)
                .setOrderByArguments(PollenResource.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();

        String currentId = CollectionUtils.isNotEmpty(gtus) ?  Iterables.getLast(gtus).getTopiaId() : null;

        ImmutableList<GtuMetaBean> gtuMetaBeans = toBeanList(gtus, this::toGtuMetaBean);

        if (CollectionUtils.isNotEmpty(gtus)) {
            Iterables.getLast(gtuMetaBeans).setCurrent(true);
        }

        return gtuMetaBeans;

    }

    public boolean isGtu()  {
        return getPollenResourceDao()
                .forResourceTypeEquals(ResourceType.GTU)
                .exists();
    }

    protected Optional<PollenResource> getCurrentGtu0() {
        PaginationOrder order = new PaginationOrder(PollenResource.PROPERTY_TOPIA_CREATE_DATE, true);
        return getPollenResourceDao()
                .forResourceTypeEquals(ResourceType.GTU)
                .setOrderByArguments(Collections.singleton(order))
                .tryFindFirst()
                .toJavaUtil();
    }

    public boolean isGtuValidated(PollenUser user) {
        checkIsConnectedRequired();
        return isGtuValidatedForDate(user.getGtuValidationDate());
    }

    protected boolean isGtuValidatedForDate(Date validationDate) {
        return getCurrentGtu0()
                .map(PollenResource::getTopiaCreateDate)
                .map(date -> validationDate != null && date.before(validationDate))
                .orElse(true);
    }

    public boolean isGtuValidated(Poll poll) {
        checkIsConnectedRequired();
        boolean validated;

        PollenUser user = poll.getCreator().getPollenUser();
        if (user != null) {
            validated = isGtuValidated(user);
        } else {
            validated = isGtuValidatedForDate(poll.getGtuValidationDate());
        }

        return validated;

    }

    public void validateGtu() {
        PollenUser connectedUser = checkAndGetConnectedUser();
        connectedUser.setGtuValidationDate(getNow());
        commit();
    }

}
