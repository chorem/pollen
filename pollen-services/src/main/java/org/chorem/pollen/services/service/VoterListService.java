package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.persistence.entity.VoterListMember;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.VoterListBean;
import org.chorem.pollen.services.bean.VoterListMemberBean;
import org.chorem.pollen.services.bean.export.ExportBean;
import org.chorem.pollen.services.service.security.PollenPermissions;
import org.nuiton.csv.Exporter;
import org.nuiton.csv.ExporterBuilder;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoterListService extends PollenServiceSupport {

    public VoterListMemberBean toVoterListMemberBean(VoterListMember entity) {

        VoterListMemberBean bean = new VoterListMemberBean();

        bean.setEntityId(entity.getTopiaId());
        PollenPrincipal pollenPrincipal = entity.getMember();
        if (pollenPrincipal != null) {
            bean.setName(pollenPrincipal.getName());
            bean.setEmail(pollenPrincipal.getEmail());
            if (pollenPrincipal.getPollenUser() != null && pollenPrincipal.getPollenUser().getAvatar() != null) {
                bean.setAvatar(getPollenResourceService().getReduceIdByTopiaId(pollenPrincipal.getPollenUser().getAvatar().getTopiaId()));
            }
        }
        bean.setWeight(entity.getWeight());
        bean.getVoterListId().setEntityId(entity.getVoterList().getTopiaId());
        bean.setInvitationSent(entity.isInvitationSent());

        boolean voting = getVoteDao().forVoterListMemberContains(entity).exists();
        bean.setVoting(voting);
        if (pollenPrincipal != null) {
            bean.setInvalidEmail(pollenPrincipal.isInvalid());
        }

        return bean;
    }

    /**
     * Same as {@link this.toVoterListMemberBean()} with only name, avatar and voting
     * @param entity
     * @return
     */
    public VoterListMemberBean toVoterListMemberLightBean(VoterListMember entity) {

        VoterListMemberBean bean = new VoterListMemberBean();

        PollenPrincipal pollenPrincipal = entity.getMember();
        if (pollenPrincipal != null) {
            bean.setName(pollenPrincipal.getName());
            if (pollenPrincipal.getPollenUser() != null && pollenPrincipal.getPollenUser().getAvatar() != null) {
                bean.setAvatar(getPollenResourceService().getReduceIdByTopiaId(pollenPrincipal.getPollenUser().getAvatar().getTopiaId()));
            }
        }

        boolean voting = getVoteDao().forVoterListMemberContains(entity).exists();
        bean.setVoting(voting);

        return bean;
    }

    public VoterListBean toVoterListBean(VoterList entity) {
        checkIsConnectedRequired();

        VoterListBean bean = new VoterListBean();
        bean.setEntityId(entity.getTopiaId());
        bean.setName(entity.getName());
        bean.setWeight(entity.getWeight());
        if (entity.getParent() != null) {
            bean.getParentId().setEntityId(entity.getParent().getTopiaId());
        }

        long countSubLists = getVoterListDao()
                .forParentEquals(entity)
                .count();

        long countMembers = getVoterListMemberDao()
                .forVoterListEquals(entity)
                .count();

        bean.setCountSubLists(countSubLists);
        bean.setCountMembers(countMembers);
        bean.setAllEmails(getVoterListMemberMails(entity));

        return bean;
    }

    public List<VoterListBean> getVoterLists(String pollId, String parentId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        List<VoterList> voterLists = getVoterLists0(poll, parentId);
        return toBeanList(voterLists, this::toVoterListBean);

    }

    public VoterListBean getMainVoterList(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        VoterList voterList = getVoterListDao()
                .forPollEquals(poll)
                .addNull(VoterList.PROPERTY_PARENT)
                .findUniqueOrNull();

        VoterListBean result = null;
        if (voterList != null) {
            result = toVoterListBean(voterList);
        }

        return result;
    }

    public VoterListBean getVoterList(String pollId, String voterListId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(voterListId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        VoterList voterList = getVoterList0(poll, voterListId);
        return toVoterListBean(voterList);

    }

    public PollenEntityRef<VoterList> addVoterList(String pollId,
                                                   VoterListBean voterList,
                                                   List<VoterListMemberBean> members) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(voterList);
        checkIsNotPersisted(voterList);
        saveVoters(pollId,
                Collections.singletonList(voterList),
                members,
                Collections.emptyList(),
                Collections.emptyList(),
                null);

        VoterList result = getVoterListDao().forTopiaIdEquals(voterList.getEntityId()).findUnique();

        return PollenEntityRef.of(result);

    }

    public VoterListBean editVoterList(String pollId, VoterListBean voterList) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(voterList);
        checkIsPersisted(voterList);

        saveVoters(pollId,
                Collections.singletonList(voterList),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                null);

        VoterList result = getVoterListDao().forTopiaIdEquals(voterList.getEntityId()).findUnique();

        //TODO Notify
        return toVoterListBean(result);

    }

    public void deleteVoterList(String pollId, String voterListId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(voterListId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        VoterList voterList = getVoterList0(poll, voterListId);
        getVoterListDao().delete(voterList);
        commit();

    }

    public Set<VoterListMemberBean> getVoterListMembers(String pollId, String voterListId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(voterListId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        VoterList voterList = getVoterList0(poll, voterListId);

        List<VoterListMember> members = getVoterListMembers0(voterList);
        return toBeanSet(members, this::toVoterListMemberBean);

    }

    public PaginationResultBean<VoterListMemberBean> getAllVoterListMembers(String pollId, PaginationParameterBean paginationParameter) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readParticipants(poll));

        PaginationParameter paginationParameter1 =
                PaginationParameter.of(paginationParameter.getPageNumber(),
                        paginationParameter.getPageSize()!=0 ? paginationParameter.getPageSize() : -1,
                        "topiaEntity_." + VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_NAME, // FIXME pourquoi l'alias doit être ajouté ?
                        false);
        PaginationResult<VoterListMember> result = getVoterListMemberDao()
                .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                .findPage(paginationParameter1);

        return toPaginationListBean(result, this::toVoterListMemberLightBean);
    }

    public ExportBean exportAllVoterListMembers(String pollId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.readParticipants(poll));

        List<VoterListMember> members = getVoterListMemberDao()
                .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                .setOrderByArguments(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_NAME)
                .findAll();

        ImmutableList<VoterListMemberBean> memberBeans = toBeanList(members, this::toVoterListMemberLightBean);

        Exporter<VoterListMemberBean> beanExporter = new ExporterBuilder<VoterListMemberBean>()
                .addColumn(PollenPrincipal.PROPERTY_NAME, VoterListMemberBean::getName)
                .addColumn("voting", VoterListMemberBean::isVoting, b -> Boolean.toString(b))
                .build();


        ExportBean exportBean = new ExportBean();

        try {
            exportBean.setContent(beanExporter.toString(memberBeans));
        } catch (IOException e) {
            throw new PollenTechnicalException("erron on export participants", e);
        }
        exportBean.setName(l(getLocale(), "pollen.export.voterListMember", poll.getTitle(), getNow()));
        exportBean.setContentType("text/csv");

        return exportBean;
    }



    public VoterListMemberBean getVoterListMember(String pollId, String voterListId, String memberId) {
        checkIsConnectedRequired();

        checkNotNull(pollId);
        checkNotNull(voterListId);
        checkNotNull(memberId);
        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.read(poll));

        VoterList voterList = getVoterList0(poll, voterListId);

        VoterListMember member = getVoterListMember0(voterList, memberId);
        return toVoterListMemberBean(member);

    }

    public VoterListMemberBean addVoterListMember(String pollId, VoterListMemberBean member) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(member);
        checkIsNotPersisted(member);
        saveVoters(pollId,
                Collections.emptyList(),
                Collections.singletonList(member),
                Collections.emptyList(),
                Collections.emptyList(),
                null);

        VoterListMember result = getVoterListMemberDao().forTopiaIdEquals(member.getEntityId()).findUnique();
        return toVoterListMemberBean(result);

    }

    public VoterListMemberBean editVoterListMember(String pollId, VoterListMemberBean member) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(member);
        checkIsPersisted(member);

        saveVoters(pollId,
                Collections.emptyList(),
                Collections.singletonList(member),
                Collections.emptyList(),
                Collections.emptyList(),
                null);

        VoterListMember result = getVoterListMemberDao().forTopiaIdEquals(member.getEntityId()).findUnique();
        return toVoterListMemberBean(result);

    }


    public void deleteVoterListMember(String pollId, String voterListId, String memberId) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(voterListId);
        checkNotNull(memberId);

        saveVoters(pollId,
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(memberId),
                null);

    }

    protected List<VoterList> getVoterLists0(Poll poll, String parentId) {

        return getVoterListDao()
                .forPollEquals(poll)
                .addEquals(VoterList.PROPERTY_PARENT + "." + VoterList.PROPERTY_TOPIA_ID, parentId)
                .findAll();

    }

    protected VoterList getMainVoterList0(Poll poll) {
        return getVoterListDao()
                .forPollEquals(poll)
                .addNull(VoterList.PROPERTY_PARENT)
                .findUniqueOrNull();
    }

    protected VoterList getVoterList0(Poll poll, String voterListId) {

        VoterList result = getVoterListDao().forTopiaIdEquals(voterListId).findUnique();

        if (!poll.equals(result.getPoll())) {

            throw new InvalidEntityLinkException(VoterList.PROPERTY_POLL, result, poll);

        }

        return result;

    }

    protected VoterList getVoterList0(String pollId, String voterListId) {

        checkNotNull(pollId);
        checkNotNull(voterListId);

        Poll poll = getPollService().getPoll0(pollId);

        return getVoterList0(poll, voterListId);

    }

    protected List<VoterListMember> getVoterListMembers0(VoterList voterList) {

        return getVoterListMemberDao()
                .forVoterListEquals(voterList)
                .setOrderByArguments(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_NAME)
                .findAll();

    }

    protected List<VoterListMember> getVoterListMembers0(String voterListId) {

        return getVoterListMemberDao()
                .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_TOPIA_ID, voterListId)
                .findAll();

    }

    protected VoterListMember getVoterListMember0(VoterList voterList, String memberId) {

        VoterListMember result = getVoterListMemberDao().forTopiaIdEquals(memberId).findUnique();

        if (!voterList.equals(result.getVoterList())) {

            throw new InvalidEntityLinkException(VoterListMember.PROPERTY_VOTER_LIST, voterList, result);

        }

        return result;

    }

    protected VoterList saveVoterList(Poll poll, VoterListBean voterList) {

        boolean voterListExists = voterList.isPersisted();

        VoterList toSave;

        if (voterListExists) {

            toSave = getVoterList0(poll, voterList.getEntityId());

        } else {

            toSave = getVoterListDao().create();
            toSave.setPoll(poll);
            String parentId = voterList.getParentId().getEntityId();
            if (parentId != null) {
                VoterList parent = getVoterList0(poll, parentId);
                toSave.setParent(parent);
            }

        }

        toSave.setName(voterList.getName());
        toSave.setWeight(voterList.getWeight());

        return toSave;

    }

    protected VoterListMember saveVoterListMember(Poll poll, VoterListMemberBean voterListMember) {

        boolean voterListMemberExists = voterListMember.isPersisted();

        VoterListMember toSave;

        VoterList voterList = getVoterList0(poll, voterListMember.getVoterListId().getEntityId());

        if (voterListMemberExists) {

            toSave = getVoterListMember0(voterList, voterListMember.getEntityId());

        } else {

            toSave = getVoterListMemberDao().create();

            toSave.setVoterList(voterList);

        }

        // when create or edit email
        String cleanMail = getCleanMail(voterListMember.getEmail());
        if (!voterListMemberExists || !cleanMail.equals(toSave.getMember().getEmail())) {
            PollenPrincipal member = searchOrCreatePrincipal(poll, cleanMail, voterListMember.getName());
            PollenUser user = getPollenUserDao().findUserWithEmailAddressOrNull(cleanMail);
            if (user != null) {
                member.setPollenUser(user);
                member.setName(user.getName());
            }

            toSave.setMember(member);
        }

        toSave.getMember().setName(voterListMember.getName());
        toSave.setWeight(voterListMember.getWeight());

        return toSave;

    }

    protected PollenPrincipal searchOrCreatePrincipal(Poll poll, String email, String name) {
        PollenPrincipal principal;
        if (poll.getCreator().getEmail() != null && poll.getCreator().getEmail().equals(email)) {
            principal = poll.getCreator();
        } else {
            principal = getVoterListMemberDao().forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                    .addEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_EMAIL, email)
                    .tryFindAny()
                    .transform(VoterListMember::getMember)
                    .orNull();

        }
        if (principal == null) {
            principal = getSecurityService().generatePollenPrincipal();
            principal.setEmail(email);
            principal.setName(name);
        }

        return principal;
    }

    protected ErrorMap checkVoterList(Set<String> sisterNames, VoterListBean voterList, List<VoterListMemberBean> members, List<VoterListBean> subLists) {

        ErrorMap errors = new ErrorMap();

        checkNotBlank(errors, VoterList.PROPERTY_NAME, voterList.getName(), l(getLocale(), "pollen.error.voterList.name.mandatory"));
        check(errors, VoterList.PROPERTY_WEIGHT, voterList.getWeight() > 0, l(getLocale(), "pollen.error.voterList.weight.greaterThan0"));
        
        boolean condition;
        if (subLists.size() == 0 && voterList.getCountMembers() == 0) {
            condition = false;
        } else {
            condition = subLists.stream().noneMatch(voterListBean -> voterListBean.getCountMembers() == 0);
        }
        
        check(errors, "member",
                condition,
                l(getLocale(), "pollen.error.voterList.member.mandatory"));
        
        boolean nameAlreadyUse = sisterNames.contains(voterList.getName());

        check(errors, VoterList.PROPERTY_NAME, !nameAlreadyUse, l(getLocale(), "pollen.error.voterList.name.alreadyUsed"));

        int voterListMemberIndex = 0;

        if (CollectionUtils.isNotEmpty(members)) {

            Set<String> brotherNames = Sets.newHashSet();
            Set<String> brotherEmails = Sets.newHashSet();

            for (VoterListMemberBean voterListMember : members) {

                ErrorMap voterListMemberErrors = checkVoterListMember(brotherNames, brotherEmails, voterListMember);
                voterListMemberErrors.copyTo(errors, "member[" + (voterListMemberIndex++) + "].");
                brotherNames.add(voterListMember.getName());
                brotherEmails.add(voterListMember.getEmail());

            }
        }

        return errors;

    }

    protected ErrorMap checkVoterListMember( Set<String> voterListMemberNames, Set<String> voterListMemberEmails, VoterListMemberBean voterListMember) {

        ErrorMap errors = new ErrorMap();

        String voterListMemberName = voterListMember.getName();
        boolean nameNotblank = checkNotBlank(errors, PollenPrincipal.PROPERTY_NAME, voterListMemberName, l(getLocale(), "pollen.error.voterList.member.name.mandatory"));

        if (nameNotblank) {

            boolean nameAlreadyUsed = voterListMemberNames.contains(voterListMemberName);
            check(errors, PollenPrincipal.PROPERTY_NAME, !nameAlreadyUsed, l(getLocale(), "pollen.error.voterList.member.name.alreadyUsed"));

        }

        String voterListMemberEmail = getCleanMail(voterListMember.getEmail());

        boolean emailNotBlank = checkNotBlank(errors, PollenPrincipal.PROPERTY_EMAIL, voterListMemberEmail, l(getLocale(), "pollen.error.voterList.member.email.mandatory"));

        if (emailNotBlank) {

            checkValidEmail(errors, PollenPrincipal.PROPERTY_EMAIL, voterListMemberEmail, l(getLocale(), "pollen.error.voterList.member.email.invalid"));
            boolean emailAlreadyUsed = voterListMemberEmails.contains(voterListMemberEmail);
            check(errors, PollenPrincipal.PROPERTY_EMAIL, !emailAlreadyUsed, l(getLocale(), "pollen.error.voterList.member.email.alreadyUsed"));

        }

        check(errors, VoterListMember.PROPERTY_WEIGHT, voterListMember.getWeight() > 0, l(getLocale(), "pollen.error.voterList.member.weight.greaterThan0"));

        return errors;

    }

    public void saveVoters(String pollId,
                           List<VoterListBean> listsToSave,
                           List<VoterListMemberBean> membersToSave,
                           List<String> listIdsToDelete,
                           List<String> memberIdsToDelete,
                           PollType pollType) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));
    
        // when reaching this code Poll may no be accurate, if user change the poll type the persisted PollType may have
        // the previous one. Getting the pool type from args force make sure to use the correct one
        // and process the correct validation process
        // see bug ref#249
        PollType forcePollType = pollType != null ? pollType : poll.getPollType();
        if (forcePollType == PollType.RESTRICTED) {
            checkVoters(poll, listsToSave, membersToSave);
        }
        
        checkNotNull(listIdsToDelete);
        checkNotNull(memberIdsToDelete);

        List<VoterList> voterListToDelete = Lists.newLinkedList();
        for (String listIdToDelete : listIdsToDelete) {

            VoterList voterList = getVoterListDao().forTopiaIdEquals(listIdToDelete).findUnique();

            if (!voterList.getPoll().equals(poll)) {

                throw new InvalidEntityLinkException(VoterList.PROPERTY_POLL, voterList, poll);

            }

            voterListToDelete.add(voterList);
        }

        List<VoterListMember> voterListMemberToDelete = Lists.newLinkedList();
        for (String memberIdToDelete : memberIdsToDelete) {

            VoterListMember voterListMember = getVoterListMemberDao().forTopiaIdEquals(memberIdToDelete).findUnique();

            if (!voterListMember.getVoterList().getPoll().equals(poll)) {

                throw new InvalidEntityLinkException(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, voterListMember, poll);

            }

            voterListMemberToDelete.add(voterListMember);
        }


        createVoters(poll, listsToSave, membersToSave);

        for (VoterList voterList : voterListToDelete) {

            getVoterListDao().delete(voterList);

        }

        for (VoterListMember voterListMember : voterListMemberToDelete) {

            getVoterListMemberDao().delete(voterListMember);

        }

        commit();

    }

    protected void checkVoters(Poll poll, List<VoterListBean> listsToSave, List<VoterListMemberBean> membersToSave) throws InvalidFormException {

        checkNotNull(listsToSave);
        checkNotNull(membersToSave);
        // verification des listes de voteurs
        Map<PollenEntityId<VoterList>, Set<String>> listNameByParentId = Maps.newHashMap();
        
        // use to load existing member names
        // do not load the name from the members ids that we want to save otherwise they will be consider as duplicated.
        Set<String> idsToIgnore = listsToSave.stream().map(VoterListBean::getEntityId).collect(Collectors.toSet());

        for (VoterListBean voterList : listsToSave) {
    
            List<VoterListMemberBean> members;
            List<VoterListBean> subLists;
    
            members = membersToSave.stream()
                    .filter(member -> voterList.getId().equals(member.getVoterListId()))
                    .collect(Collectors.toList());
    
            subLists = listsToSave.stream()
                    .filter(list -> voterList.getId().equals(list.getParentId()))
                    .collect(Collectors.toList());
    
            Set<String> sisterNames = listNameByParentId.computeIfAbsent(voterList.getParentId(), parentId -> {
                if (poll == null || parentId.isNotEmpty() || parentId.isTemporaryId()) {
                    return Sets.newHashSet();
                } else {
                    // do not load names of not new list as they will be consider as duplicated
                    List<VoterList> existingVoterLists = getVoterLists0(poll, parentId.getEntityId());
                    return existingVoterLists.stream()
                            .filter(voterList0 -> !idsToIgnore.contains(voterList0.getTopiaId()))
                            .map(VoterList::getName)
                            .collect(Collectors.toSet());
                }
            });

            ErrorMap errorMap = checkVoterList(sisterNames, voterList, members, subLists);
            errorMap.failIfNotEmpty();

            sisterNames.add(voterList.getName());
        }

        // Vérification des membres
        Map<String, Pair<Set<String>, Set<String>>> listNamesEmailsByListId = Maps.newHashMap();


        for (VoterListMemberBean voterListMember : membersToSave) {

            if (voterListMember.getVoterListId().getEntityId() != null && !voterListMember.getVoterListId().isTemporaryId()) {

                Pair<Set<String>, Set<String>> brotherNamesEmails = listNamesEmailsByListId.computeIfAbsent(voterListMember.getVoterListId().getEntityId(), voterListId -> {
                    List<VoterListMember> existingVoterListMembers = getVoterListMembers0(voterListId);
                    Set<String> names = existingVoterListMembers.stream()
                            .map(VoterListMember::getMember)
                            .map(PollenPrincipal::getName)
                            .collect(Collectors.toSet());
                    Set<String> emails = existingVoterListMembers.stream()
                            .map(VoterListMember::getMember)
                            .map(PollenPrincipal::getEmail)
                            .collect(Collectors.toSet());
                    return Pair.of(names, emails);
                });

                Set<String> brotherNames = brotherNamesEmails.getLeft();
                Set<String> brotherEmails = brotherNamesEmails.getLeft();

                if (voterListMember.isPersisted() && poll != null) {

                    VoterList voterList = getVoterList0(poll, voterListMember.getVoterListId().getEntityId());

                    VoterListMember oldMember = getVoterListMember0(voterList, voterListMember.getEntityId());
                    brotherNames.remove(oldMember.getMember().getName());
                    brotherEmails.remove(oldMember.getMember().getEmail());

                }

                ErrorMap errorMap = checkVoterListMember(brotherNames, brotherEmails, voterListMember);
                errorMap.failIfNotEmpty();
                brotherNames.add(voterListMember.getName());
                brotherEmails.add(voterListMember.getEmail());
            }

        }
    }

    protected List<VoterListMember> createVoters(Poll poll, List<VoterListBean> listsToSave, List<VoterListMemberBean> membersToSave) {

        Map<String, VoterListBean> voterListBeanByTempId = listsToSave.stream()
                .filter(list -> list.getId().isEmpty() || list.isTemporaryId())
                .collect(Collectors.toMap(
                        v -> v.getId().getReducedId(),
                        Function.identity()));

        List<VoterListMember> newVoters = Lists.newLinkedList();

        List<PollenPrincipal> principalSentInvitation = getVoterListMemberDao()
                .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                .addEquals(VoterListMember.PROPERTY_INVITATION_SENT, true)
                .findAll()
                .stream()
                .map(VoterListMember::getMember)
                .collect(Collectors.toList());


        for (VoterListBean voterList : listsToSave) {

            if (voterList.getEntityId() == null || voterList.isTemporaryId()) {

                PollenEntityId<VoterList> parentId = voterList.getParentId();
                if (parentId.isNotEmpty() && parentId.isTemporaryId()) {
                    String parentIdString = voterListBeanByTempId.get(parentId.getReducedId()).getEntityId();
                    voterList.getParentId().setEntityId(parentIdString);
                }

                voterList.setEntityId(null);
                VoterList voterListSaved = saveVoterList(poll, voterList);
                voterList.setEntityId(voterListSaved.getTopiaId());

            } else {
                saveVoterList(poll, voterList);
            }
        }

        for (VoterListMemberBean voterListMember : membersToSave) {
            if (!voterListMember.isPersisted()) {

                PollenEntityId<VoterList> voterListId = voterListMember.getVoterListId();
                if (voterListId.isEmpty() || voterListId.isTemporaryId()) {
                    String voterListIdString = voterListBeanByTempId.get(voterListId.getReducedId()).getEntityId();
                    voterListMember.getVoterListId().setEntityId(voterListIdString);
                }

                voterListMember.setEntityId(null);
                VoterListMember voterListMemberSaved = saveVoterListMember(poll, voterListMember);
                voterListMember.setEntityId(voterListMemberSaved.getTopiaId());
                if (principalSentInvitation.contains(voterListMemberSaved.getMember())) {
                    voterListMemberSaved.setInvitationSent(true);
                }

                newVoters.add(voterListMemberSaved);
            } else {
                saveVoterListMember(poll, voterListMember);
            }
        }

        return newVoters;
    }

    public int sendInvitationVoterList(String pollId, String voterListId, boolean allMemberNoVoting) {
        checkIsConnectedRequired();

        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        VoterList voterList = getVoterList0(poll, voterListId);

        List<VoterListMember> members = getVoterListDao().getAllMembers(voterList);

        return sendInvitation(poll, members, allMemberNoVoting);
    }

    public boolean sendInvitationMember(String pollId, String voterListId, String memberId, boolean allMemberNoVoting) {
        checkIsConnectedRequired();
        checkNotNull(pollId);

        Poll poll = getPollService().getPoll0(pollId);
        checkPermission(PollenPermissions.edit(poll));

        VoterList voterList = getVoterList0(poll, voterListId);

        VoterListMember member = getVoterListMember0(voterList, memberId);

        return sendInvitation(poll, Collections.singletonList(member), allMemberNoVoting) == 1;
    }

    protected int sendInvitation(Poll poll, List<VoterListMember> members, boolean allMemberNoVoting) {

        //get all votes for all questions
        List<Question> questions = getQuestionService().getQuestions(poll);
        List<Vote> votes = new ArrayList<>();
        for (Question question:questions) {
            List<Vote> questionVotes = getVoteDao().forQuestionEquals(question).findAll();
            votes.addAll(questionVotes);
        }

        List<VoterListMember> memberToSend;

        if (allMemberNoVoting) {

            final Set<VoterListMember> memberVoting = votes.stream()
                    .map(Vote::getVoterListMember)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());

            memberToSend = members.stream()
                    .filter(member -> !memberVoting.contains(member))
                    .collect(Collectors.toList());
        } else {
            memberToSend = members.stream()
                    .filter(member -> ! member.isInvitationSent())
                    .collect(Collectors.toList());
        }

        getNotificationService().sendInvitation(poll, memberToSend);

        // plusieurs membre dans des listes différentes peuvent avoir le même email
        List<PollenPrincipal> pollenPrincipals = memberToSend.stream().map(VoterListMember::getMember).collect(Collectors.toList());

        getVoterListMemberDao()
                .forProperties(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, poll)
                .addIn(VoterListMember.PROPERTY_MEMBER, pollenPrincipals)
                .findAll()
                .forEach(m -> m.setInvitationSent(true));

        commit();

        return memberToSend.size();
    }

    public long getVoterListMemberCount(Poll poll) {
        return getVoterListMemberDao().countDistinctMembers(poll, false);
    }

    public long getVoterListMemberInvitedCount(Poll poll) {
        return getVoterListMemberDao().countDistinctMembers(poll, true);
    }

    public Set<String> getVoterListMemberMails(VoterList voterList) {

        List<VoterListMember> allMembers = getVoterListDao().getAllMembers(voterList);

        return allMembers.stream()
                .map(VoterListMember::getMember)
                .map(PollenPrincipal::getEmail)
                .collect(Collectors.toSet());
    }
}
