package org.chorem.pollen.services.bean.export;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.FavoriteListMember;
import org.chorem.pollen.persistence.entity.FavoriteListMemberImpl;
import org.chorem.pollen.services.bean.FavoriteListMemberBean;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FavoriteListMemberExport {

    protected String name;

    protected String email;

    protected double weight;

    public static FavoriteListMemberExport fromEntity(FavoriteListMember entity) {
        FavoriteListMemberExport export = new FavoriteListMemberExport();
        export.setName(entity.getName());
        export.setEmail(entity.getEmail());
        export.setWeight(entity.getWeight());

        return export;
    }

    public FavoriteListMember toEntity() {

        FavoriteListMember entity = new FavoriteListMemberImpl();
        entity.setEmail(getEmail());
        entity.setName(getName());
        entity.setWeight(getWeight());

        return entity;

    }

    public FavoriteListMemberBean toBean() {
        FavoriteListMemberBean bean = new FavoriteListMemberBean();
        bean.setEmail(getEmail());
        bean.setName(getName());
        bean.setWeight(getWeight());

        return bean;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


}
