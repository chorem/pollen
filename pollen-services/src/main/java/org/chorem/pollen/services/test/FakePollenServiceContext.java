package org.chorem.pollen.services.test;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.PollenTopiaApplicationContext;
import org.chorem.pollen.persistence.PollenTopiaPersistenceContext;
import org.chorem.pollen.services.DefaultPollenServiceContext;
import org.chorem.pollen.services.PollenUIContext;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.votecounting.VoteCountingFactory;

import java.util.Date;
import java.util.Locale;

public class FakePollenServiceContext extends DefaultPollenServiceContext {

    private static final Log log =
            LogFactory.getLog(FakePollenServiceContext.class);

    protected Date date;


    public static FakePollenServiceContext newServiceContext(Date now,
                                                             Locale locale,
                                                             PollenServicesConfig serviceConfig,
                                                             PollenTopiaApplicationContext applicationcontext,
                                                             PollenTopiaPersistenceContext persistenceContext,
                                                             VoteCountingFactory voteCountingFactory) {
        FakePollenServiceContext serviceContext = new FakePollenServiceContext();
        serviceContext.setPersistenceContext(persistenceContext);
        serviceContext.setPollenServicesConfig(serviceConfig);
        serviceContext.setTopiaApplicationContext(applicationcontext);
        serviceContext.setVoteCountingFactory(voteCountingFactory);
        serviceContext.setSecurityContext(new FakePollenSecurityContext());
        serviceContext.setLocale(locale);
        serviceContext.setDate(now);
        serviceContext.setUIContext(new PollenUIContext());
        return serviceContext;
    }

    @Override
    public Date getNow() {
        Preconditions.checkState(date != null, "you must provide a date before running service test");
        if (log.isTraceEnabled()) {
            log.trace("injecting fake date in service: " + date);
        }
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
}
