package org.chorem.pollen.services;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.PollenPersistenceContext;
import org.chorem.pollen.persistence.PollenTopiaApplicationContext;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.chorem.pollen.votecounting.VoteCountingFactory;

import java.util.Date;
import java.util.Locale;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public interface PollenServiceContext {

    String generateToken();

    Date getNow();

    PollenSecurityContext getSecurityContext();

    void setSecurityContext(PollenSecurityContext securityContext);

    PollenTopiaApplicationContext getTopiaApplicationContext();

    PollenPersistenceContext getPersistenceContext();

    PollenServicesConfig getPollenServicesConfig();

    <E extends PollenService> E newService(Class<E> serviceClass);

    String generatePassword();

    Locale getLocale();

    String getCleanMail(String email);

    String generateSalt();

    String encodePassword(String salt, String password);

    VoteCountingFactory getVoteCountingFactory();

    PollenUIContext getUIContext();

    void setUIContext(PollenUIContext uiContext);
}
