package org.chorem.pollen.services.service;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.services.UnitHuman;
import org.chorem.pollen.services.bean.ConfigurationBean;
import org.chorem.pollen.services.bean.PollenStatus;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TransverseService extends PollenServiceSupport {

    private static final Log log = LogFactory.getLog(TransverseService.class);

    public ConfigurationBean getConfiguration() {
        ConfigurationBean bean = new ConfigurationBean();
        bean.setUserConnectedRequired(getPollenServiceConfig().isUserConnectedRequired());
        bean.setUsersCanCreatePoll(getPollenServiceConfig().getUsersCanCreatePoll());

        return bean;
    }

    public PollenStatus getStatus() {
        long statusStart = System.currentTimeMillis();
        List<String> errors = Lists.newArrayList();


        PollenStatus status = new PollenStatus();
        status.setVersion(getPollenServiceConfig().getVersion());
        status.setBuildDate(getPollenServiceConfig().getBuildDate());
        status.setEncoding(System.getProperty("file.encoding"));

        boolean persistenceOk = false;
        long nbPolls = 0;
        try {
            nbPolls = getPollDao().count();
            persistenceOk = true;
        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Erreur lors de la lecture du nombre de sondage ", eee);
            }
            errors.add(eee.getMessage());
        }
        status.setPersistenceOk(persistenceOk);
        status.setNbPolls(nbPolls);
        status.setErrors(errors);

        boolean allOk = persistenceOk && errors.isEmpty();
        status.setAllOk(allOk);

        // Mémoire : Données brutes
        Runtime runtime = Runtime.getRuntime();
        long freeMemoryOnAllocated = runtime.freeMemory();            // Mémoire libre (par rapport à la mémoire allouée)
        long totalMemory = runtime.totalMemory();                     // Mémoire allouée
        long maxMemory = runtime.maxMemory();                         // Mémoire totale (max)

        // Mémoire : Données déduites
        long usedMemory = totalMemory - freeMemoryOnAllocated;        // Mémoire utilisée (allouée - libre)
        double usedPercent = ((double) usedMemory / maxMemory) * 100d; // Mémoire utilisée en pourcentage du max
        long freeMemory = maxMemory - usedMemory;                     // Mémoire libre (par rapport au max)
        double freePercent = 100d - usedPercent;                      // Mémoire libre en pourcentage du max

        status.setMemoryAllocated(UnitHuman.toHumanString(totalMemory, "o"));
        status.setMemoryUsed(String.format("%s (%.2f%s)", UnitHuman.toHumanString(usedMemory, "o"), usedPercent, "%"));
        status.setMemoryFree(String.format("%s (%.2f%s)", UnitHuman.toHumanString(freeMemory, "o"), freePercent, "%"));
        status.setMemoryMax(UnitHuman.toHumanString(maxMemory, "o"));

        OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
        double systemLoadAverage = os.getSystemLoadAverage();
        status.setLoadAverage(systemLoadAverage);

        long statusEnd = System.currentTimeMillis();

        status.setDuration(statusEnd - statusStart);

        if (log.isDebugEnabled()) {
            log.debug("Status :" + status);
        }

        return status;
    }

}
