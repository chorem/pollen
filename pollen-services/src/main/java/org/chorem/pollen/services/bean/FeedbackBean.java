package org.chorem.pollen.services.bean;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.PollenResource;

import java.awt.Dimension;
import java.util.Locale;


/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FeedbackBean {

    protected String description;

    protected String category;

    protected String browser;

    protected String operatingSystem;

    protected String platform;

    protected Dimension screenResolution;

    protected Dimension windowDimension;

    protected Locale locale;

    protected String location;

    protected String locationTitle;

    protected PollenEntityId<PollenResource> screenShotId;

    protected String consoleHistory;

    protected String userEmail;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Dimension getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(Dimension screenResolution) {
        this.screenResolution = screenResolution;
    }

    public Dimension getWindowDimension() {
        return windowDimension;
    }

    public void setWindowDimension(Dimension windowDimension) {
        this.windowDimension = windowDimension;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationTitle() {
        return locationTitle;
    }

    public void setLocationTitle(String locationTitle) {
        this.locationTitle = locationTitle;
    }

    public PollenEntityId<PollenResource> getScreenShotId() {
        return screenShotId;
    }

    public void setScreenShotId(PollenEntityId<PollenResource> screenShotId) {
        this.screenShotId = screenShotId;
    }

    public String getConsoleHistory() {
        return consoleHistory;
    }

    public void setConsoleHistory(String consoleHistory) {
        this.consoleHistory = consoleHistory;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
