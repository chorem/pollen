package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.PollenUserEmailAddress;
import org.chorem.pollen.persistence.entity.ResourceType;
import org.chorem.pollen.persistence.entity.UserCredential;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.persistence.entity.VoteTopiaDao;
import org.chorem.pollen.services.PollenService;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.PollenUserBean;
import org.chorem.pollen.services.bean.PollenUserEmailAddressBean;
import org.chorem.pollen.services.bean.UserCredentialBean;
import org.chorem.pollen.persistence.UsersRight;
import org.chorem.pollen.services.bean.resource.ResourceFileBean;
import org.chorem.pollen.services.service.security.PollenDefaultEmailAddressException;
import org.chorem.pollen.services.service.security.PollenEmailNotValidatedException;
import org.chorem.pollen.services.service.security.PollenInvalidEmailActivationTokenException;
import org.chorem.pollen.services.service.security.PollenInvalidPasswordException;
import org.chorem.pollen.services.service.security.PollenSecurityContext;
import org.nuiton.util.pagination.PaginationOrder;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenUserService extends PollenServiceSupport implements PollenService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PollenUserService.class);

    public PollenUserBean toPollenUserBean(PollenUser entity) {
        PollenUserBean bean = new PollenUserBean();
        bean.setEntityId(entity.getTopiaId());

        bean.setName(entity.getName());
        bean.setAdministrator(entity.isAdministrator());
        bean.setBanned(entity.isBanned());
        bean.setLanguage(entity.getLanguage());
        bean.setPassword(null);
        bean.setEmailIsValidate(entity.isEmailValidated());
        bean.setWithPassword(entity.getPassword() != null);
        if (entity.getUserCredential() != null) {
            bean.setCredentials(entity.getUserCredential().stream()
                    .map(this::toUserCredentialBean)
                    .collect(Collectors.toList()));
        }
        bean.setPremiumTo(entity.getPremiumTo());
        if (entity.getAvatar() != null) {
            bean.setAvatar(getPollenResourceService().getReduceIdByTopiaId(entity.getAvatar().getTopiaId()));
        }

        bean.setGtuValidated(getGtuService().isGtuValidated(entity));
        bean.setPremium(isPremium(entity));
        bean.setEmailAddresses(entity.getEmailAddresses().stream()
                                       .map(this::toPollenUserEmailAddressBean)
                                       .collect(Collectors.toList()));
        if (entity.getDefaultEmailAddress() != null) {
            bean.setDefaultEmailAddress(toPollenUserEmailAddressBean(entity.getDefaultEmailAddress()));
        }

        bean.setCanCreatePoll(entity.isCanCreatePoll());

        return bean;
    }

    public PollenUserEmailAddressBean toPollenUserEmailAddressBean(PollenUserEmailAddress entity) {
        PollenUserEmailAddressBean bean = new PollenUserEmailAddressBean();
        bean.setEntityId(entity.getTopiaId());
        bean.setEmailAddress(entity.getEmailAddress());
        bean.setValidated(entity.isValidated());
        bean.setPgpPublicKey(entity.getPgpPublicKey());
        return bean;
    }

    public UserCredentialBean toUserCredentialBean(UserCredential entity) {
        UserCredentialBean bean = new UserCredentialBean();
        bean.setEntityId(entity.getTopiaId());
        bean.setProvider(entity.getProvider());
        bean.setUserName(entity.getUserName());
        bean.setEmailAddress(entity.getEmail());
        return bean;
    }

    public PaginationResultBean<PollenUserBean> getUsers(PaginationParameterBean paginationParameter, String search) {

        checkIsAdmin();

        PaginationParameter page = getPaginationParameter(paginationParameter);
        if (CollectionUtils.isEmpty(page.getOrderClauses())) {
            page.getOrderClauses().add(new PaginationOrder(PollenUser.PROPERTY_NAME, false));
        }
        PaginationResult<PollenUser> pollenUsers = getPollenUserDao().findAll(page, search);

        return toPaginationListBean(pollenUsers, this::toPollenUserBean);

    }

    public PollenUserBean getUser() {

        PollenUser pollenUser = checkAndGetConnectedUser();

        return toPollenUserBean(pollenUser);

    }

    public PollenUserBean getUser(String userId) {

        checkNotNull(userId);
        PollenUser pollenUser = checkAndGetConnectedUser();

        if (!userId.equals(pollenUser.getTopiaId())) {
            checkIsAdmin();
            pollenUser = getUser0(userId);
        }
        return toPollenUserBean(pollenUser);

    }

    public PollenEntityRef<PollenUser> createUser(PollenUserBean user) throws InvalidFormException {

        checkNotNull(user);
        checkIsNotPersisted(user);

        ErrorMap errorMap = checkPollenUser(user);
        if (user.getDefaultEmailAddress() == null) {
            errorMap.addError("email", "pollen.error.user.mailEmpty");
        } else {
            checkUserEmailAddress(errorMap, user.getDefaultEmailAddress().getEmailAddress());
        }
        errorMap.failIfNotEmpty();

        PollenUser result = savePollenUser(user);
        commit();

        String token = getSecurityService().generateEmailToken(result.getDefaultEmailAddress());

        getNotificationService().onUserCreated(result, token);

        return PollenEntityRef.of(result);

    }

    public PollenUserBean editUser(PollenUserBean user) throws InvalidFormException {

        checkNotNull(user);
        checkIsPersisted(user);

        checkConnectedUserOrAdmin(user.getEntityId());

        ErrorMap errorMap = checkPollenUser(user);
        errorMap.failIfNotEmpty();

        PollenUser result = savePollenUser(user);
        commit();

        getNotificationService().onUserEdited(result);

        return toPollenUserBean(result);

    }

    public boolean deleteUser(String userId, boolean anonymize) {

        boolean selfDeletion = checkConnectedUserOrAdmin(userId);

        PollenUser user = getUser0(userId);

        if (anonymize) {
            anonymizeUser(user);
        }

        getUserCredentialDao().deleteAll(user.getUserCredential());
        getPollenUserDao().delete(user);
        commit();

        getNotificationService().onUserDeleted(user);

        return selfDeletion;

    }

    public void changePassword(String oldPassword,
                               String newPassword) throws InvalidFormException {

        PollenUser user = checkAndGetConnectedUser();
        checkNotNull(newPassword);
        ErrorMap errorMap = new ErrorMap();

        boolean passwordNotBlank = checkNotBlank(errorMap, "newPassword", newPassword, l(getLocale(), "pollen.error.user.passwordEmpty"));

        if (passwordNotBlank) {
            // check old password
            try {
                getSecurityService().checkUserPassword(user, oldPassword);
            } catch (PollenInvalidPasswordException e) {
                check(errorMap, "oldPassword", false, l(getLocale(), "pollen.error.user.passwordInvalid"));
            }
        }

        errorMap.failIfNotEmpty();

        // set new password
        getSecurityService().setUserPassword(user, newPassword);

        commit();

        getNotificationService().onUserPasswordChanged(user);

    }

    public void validateUserEmail(String userId, String token) throws PollenInvalidEmailActivationTokenException {

        checkNotNull(userId);
        checkNotNull(token);

        PollenUser user = getUser0(userId);
        PollenUserEmailAddress emailAddress = getSecurityService().getEmailAdresseFromToken(token);
        if (emailAddress == null || !user.containsEmailAddresses(emailAddress)) {
            throw new PollenInvalidEmailActivationTokenException();
        }

        // reset token in database
        emailAddress.setValidated(true);

        commit();

        getNotificationService().onUserEmailValidated(user, emailAddress);

    }

    public void validateUserEmailByAdmin(String userId, String emailAddressId) throws PollenInvalidEmailActivationTokenException {

        checkIsAdmin();
        checkNotNull(userId);
        checkNotNull(emailAddressId);

        PollenUser user = getUser0(userId);
        PollenUserEmailAddress emailAddress = getPollenUserEmailAddressDao().forTopiaIdEquals(emailAddressId).findUnique();
        if (emailAddress == null || !user.containsEmailAddresses(emailAddress)) {
            throw new PollenInvalidEmailActivationTokenException();
        }

        // reset token in database
        emailAddress.setValidated(true);

        commit();

        getNotificationService().onUserEmailValidated(user, emailAddress);

    }

    public void resendValidation(String email) {
        checkNotNull(email);

        PollenUserEmailAddress userEmailAddress = getPollenUserEmailAddressDao().forEmailAddressEquals(email).findUniqueOrNull();
        if (userEmailAddress == null) {
            return;
        }
        PollenUser user = getPollenUserDao().forEmailAddressesContains(userEmailAddress).findUnique();

        userEmailAddress.setValidated(false);
        commit();

        String token = getSecurityService().generateEmailToken(userEmailAddress);

        getNotificationService().onResendValidation(user, userEmailAddress, token);

    }

    public void createDefaultUsers() throws InvalidFormException {

        if (getPollenUserDao().count() == 0) {

            PollenUserBean adminBean = new PollenUserBean();

            PollenUserEmailAddressBean emailAddress = new PollenUserEmailAddressBean();
            emailAddress.setEmailAddress("admin@chorem.org");

            adminBean.setDefaultEmailAddress(emailAddress);
            adminBean.setPassword("admin");
            adminBean.setName("admin");

            PollenUser admin = savePollenUser(adminBean);
            admin.setAdministrator(true);
            admin.getDefaultEmailAddress().setValidated(true);
            commit();
        }

    }

    public void deleteAvatar() {
        checkIsConnected();
        PollenUser user = getConnectedUser();
        if (user.getAvatar() != null) {
            getPollenResourceDao().delete(user.getAvatar());
        }
        commit();
    }

    public void deleteAvatar(String userId) {
        checkIsAdmin();
        PollenUser user = getUser0(userId);
        if (user.getAvatar() != null) {
            getPollenResourceDao().delete(user.getAvatar());
        }
        commit();
    }

    public PollenEntityRef<PollenResource> setAvatar(ResourceFileBean resourceBean) throws InvalidFormException {
        checkIsConnected();
        resourceBean.setResourceType(ResourceType.AVATAR);
        PollenResource avatarResource = getPollenResourceService().createAvatarResource(resourceBean);
        PollenUser user = getConnectedUser();
        if (user.getAvatar() != null) {
            getPollenResourceDao().delete(user.getAvatar());
        }
        user.setAvatar(avatarResource);
        commit();
        return PollenEntityRef.of(avatarResource);
    }

    public PollenEntityRef<PollenUserEmailAddress> addEmailAddress(String emailAddress) throws InvalidFormException {
        PollenUser connectedUser = checkAndGetConnectedUser();
        return addEmailAddress(connectedUser.getTopiaId(), emailAddress);
    }

    public PollenEntityRef<PollenUserEmailAddress> addEmailAddress(String userId, String emailAddress) throws InvalidFormException {
        checkConnectedUserOrAdmin(userId);
        return addEmailAddress(getUser0(userId), emailAddress);
    }

    protected PollenEntityRef<PollenUserEmailAddress> addEmailAddress(PollenUser user, String emailAddress) throws InvalidFormException {
        checkNotNull(emailAddress);
        ErrorMap errors = new ErrorMap();
        emailAddress = checkUserEmailAddress(errors, emailAddress);
        errors.failIfNotEmpty();

        PollenUserEmailAddress address = getPollenUserEmailAddressDao().create();
        address.setEmailAddress(emailAddress);
        address.setValidated(false);
        user.addEmailAddresses(address);
        commit();

        String token = getSecurityService().generateEmailToken(address);

        getNotificationService().onUserEmailAddressAdded(user, address, token);

        return PollenEntityRef.of(address);
    }

    protected String checkUserEmailAddress(ErrorMap errors, String emailAddress) {
        emailAddress = getCleanMail(emailAddress);
        boolean emailNotblank = checkNotBlank(errors, "email", emailAddress, l(getLocale(), "pollen.error.user.mailEmpty"));
        if (emailNotblank) {
            checkValidEmail(errors, "email", emailAddress, l(getLocale(), "pollen.error.user.mailInvalid"));
            checkEmailPattern(errors, "email", emailAddress, l(getLocale(), "pollen.error.user.mailUnauthorized"));
            check(errors, "email", !getPollenUserEmailAddressDao().emailExists(emailAddress), l(getLocale(), "pollen.error.user.mailExist"));
        }
        return emailAddress;
    }

    public void setDefaultEmailAddress(String emailAddressId) throws PollenEmailNotValidatedException {
        PollenUser connectedUser = checkAndGetConnectedUser();
        setDefaultEmailAddress(connectedUser, emailAddressId);
    }

    public void setDefaultEmailAddress(String userId, String emailAddressId) throws PollenEmailNotValidatedException {
        checkConnectedUserOrAdmin(userId);
        setDefaultEmailAddress(getUser0(userId), emailAddressId);
    }

    protected void setDefaultEmailAddress(PollenUser user, String emailAddressId) throws PollenEmailNotValidatedException {
        checkNotNull(emailAddressId);
        PollenUserEmailAddress emailAddress = user.getEmailAddressesByTopiaId(emailAddressId);
        checkNotNull(emailAddress);
        if (!emailAddress.isValidated()) {
            throw new PollenEmailNotValidatedException();
        }
        user.setDefaultEmailAddress(emailAddress);
        commit();
    }

    public void removeEmailAddress(String emailAddressId) throws PollenDefaultEmailAddressException {
        PollenUser connectedUser = checkAndGetConnectedUser();
        removeEmailAddressFromUser(connectedUser, emailAddressId);
    }

    public void removeEmailAddress(String userId, String emailAddressId) throws PollenDefaultEmailAddressException {
        checkConnectedUserOrAdmin(userId);
        removeEmailAddressFromUser(getUser0(userId), emailAddressId);
    }

    protected void removeEmailAddressFromUser(PollenUser user, String emailAddressId) throws PollenDefaultEmailAddressException {
        checkNotNull(emailAddressId);
        if (user.getDefaultEmailAddress() != null && user.getDefaultEmailAddress().getTopiaId().equals(emailAddressId)) {
            throw new PollenDefaultEmailAddressException();
        }
        PollenUserEmailAddress emailAddress = user.getEmailAddressesByTopiaId(emailAddressId);
        checkNotNull(emailAddress);
        user.removeEmailAddresses(emailAddress);
        getPollenUserEmailAddressDao().delete(emailAddress);
        commit();
    }

    protected ErrorMap checkPollenUser(PollenUserBean user) {

        ErrorMap errors = new ErrorMap();

        boolean userExists = user.isPersisted();

        checkNotBlank(errors, "name", user.getName(), l(getLocale(), "pollen.error.user.nameEmpty"));

        if (!userExists) {
            checkNotBlank(errors, "password", user.getPassword(), l(getLocale(), "pollen.error.user.passwordEmpty"));
        }

        check(errors, PollenUser.PROPERTY_BANNED,
                !(user.isBanned() && user.isPersisted() && getConnectedUser() != null && user.getEntityId().equals(getConnectedUser().getTopiaId())),
                l(getLocale(), "pollen.error.user.bannedSelf"));

        if (getGtuService().getCurrentGtu0().isPresent()) {
            check(errors, "gtuValidated",
                    user.isPersisted() || user.isGtuValidated(),
                    l(getLocale(), "pollen.error.user.gtuValidation.required"));
        }

        return errors;

    }

    protected PollenUser savePollenUser(PollenUserBean user) {

        boolean userExists = user.isPersisted();

        PollenUser toSave;

        if (userExists) {

            toSave = getUser0(user.getEntityId());

        } else {

            toSave = getPollenUserDao().create();

            PollenUserEmailAddressBean emailAddress = user.getDefaultEmailAddress();
            if (emailAddress != null) {
                PollenUserEmailAddress defaultEmailAddress = getPollenUserEmailAddressDao().create();
                String cleanMail = getCleanMail(emailAddress.getEmailAddress());
                defaultEmailAddress.setEmailAddress(cleanMail);
                defaultEmailAddress.setValidated(false);
                toSave.addEmailAddresses(defaultEmailAddress);
                toSave.setDefaultEmailAddress(defaultEmailAddress);
            }

            if (user.isGtuValidated()) {
                toSave.setGtuValidationDate(getNow());
            }

            getSecurityService().setUserPassword(toSave, user.getPassword());
        }

        PollenSecurityContext securityContext = getSecurityContext();
        if (securityContext.isAdmin()) {
            toSave.setAdministrator(user.isAdministrator());
            toSave.setBanned(user.isBanned());
            toSave.setPremiumTo(user.getPremiumTo());
        }
        toSave.setName(user.getName());
        toSave.setLanguage(user.getLanguage());
        if (UsersRight.USERS_SELECTED.equals(getPollenServiceConfig().getUsersCanCreatePoll())) {
            toSave.setCanCreatePoll(user.isCanCreatePoll());
        }

        return toSave;

    }

    protected PollenUser getUser0(String userId) {

        return getPollenUserDao().forTopiaIdEquals(userId).findUnique();

    }

    protected PaginationParameter getPaginationParameter(PaginationParameterBean paginationParameter) {

        if (paginationParameter == null) {

            paginationParameter = PaginationParameterBean.of(0, PAGE_SIZE_DEFAULT);

        }

        if (StringUtils.isEmpty(paginationParameter.getOrder())) {
            paginationParameter.setOrder(PollenUser.PROPERTY_NAME);
        }

        return paginationParameter.toPaginationParameter();

    }

    /**
     * Anonymize a user (setting name to "?"), and break links between votes and comments so that we cannot trace back
     * the user
     * @param user
     */
    protected void anonymizeUser(PollenUser user) {

        List<Vote> votesUser = getVoteDao().forEquals(Vote.PROPERTY_VOTER + "." + PollenPrincipal.PROPERTY_POLLEN_USER, user).findAll();

        for (Vote vote : votesUser) {
            PollenPrincipal anonymousUser = getSecurityService().generatePollenPrincipal();
            anonymousUser.setName(VoteTopiaDao.ANONYMOUS_NAME);
            vote.setVoter(anonymousUser);
            vote.setAnonymous(true);
        }

        List<Comment> commentsUser = getCommentDao().forEquals(Comment.PROPERTY_AUTHOR + "." + PollenPrincipal.PROPERTY_POLLEN_USER, user).findAll();

        for (Comment comment : commentsUser) {
            PollenPrincipal anonymousUser = getSecurityService().generatePollenPrincipal();
            anonymousUser.setName(VoteTopiaDao.ANONYMOUS_NAME);
            comment.setAuthor(anonymousUser);
        }

        commit();

    }

    public boolean isPremium(PollenUser user) {
        return user != null && isPremium(user.getPremiumTo());
    }

    protected boolean isPremium(Date premiumTo) {
        return premiumTo != null && premiumTo.after(getNow());
    }

    /**
     * @param userId
     * @return true if the user id is the one of the connected user
     */
    protected boolean checkConnectedUserOrAdmin(String userId) {
        checkNotNull(userId);
        PollenUser connectedUser = checkAndGetConnectedUser();
        boolean result = connectedUser.getTopiaId().equals(userId);
        if (!result) {
            checkIsAdmin();
        }
        return result;
    }

    public PollenUserEmailAddressBean editEmailAddress(PollenUserEmailAddressBean emailAddress) throws PollenDefaultEmailAddressException {
        PollenUser connectedUser = checkAndGetConnectedUser();
        return editEmailAddressFromUser(connectedUser, emailAddress);
    }

    public PollenUserEmailAddressBean editEmailAddress(String userId, PollenUserEmailAddressBean emailAddress) throws PollenDefaultEmailAddressException {
        checkConnectedUserOrAdmin(userId);
        return editEmailAddressFromUser(getUser0(userId), emailAddress);
    }


    protected PollenUserEmailAddressBean editEmailAddressFromUser(PollenUser user, PollenUserEmailAddressBean emailAddress) {
        checkIsConnectedRequired();

        checkNotNull(emailAddress);
        checkIsPersisted(emailAddress);

        PollenUserEmailAddress result = getPollenUserEmailAddressDao().forTopiaIdEquals(emailAddress.getEntityId()).findUnique();

        if (!user.containsEmailAddresses(result)) {

            throw new InvalidEntityLinkException(PollenUser.PROPERTY_EMAIL_ADDRESSES, user, result);

        }

        result.setPgpPublicKey(emailAddress.getPgpPublicKey());

        commit();

        return toPollenUserEmailAddressBean(result);

    }
}
