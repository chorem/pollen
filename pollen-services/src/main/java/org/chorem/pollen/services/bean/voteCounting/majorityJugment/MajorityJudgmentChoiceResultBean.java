package org.chorem.pollen.services.bean.voteCounting.majorityJugment;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.votecounting.MajorityJudgmentChoiceResult;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MajorityJudgmentChoiceResultBean {

    protected PollenEntityId<Choice> choiceId;

    protected List<BigDecimal> voteByGrad;

    protected int median;

    public MajorityJudgmentChoiceResultBean() {
        this.choiceId = PollenEntityId.newId(Choice.class);;
    }

    public void fromResult(MajorityJudgmentChoiceResult choiceResult) {

        setChoiceId(choiceResult.getChoiceId());
        setVoteByGrad(choiceResult.getVoteByGrad());
        setMedian(choiceResult.getMedian());

    }

    public PollenEntityId<Choice> getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(PollenEntityId<Choice> choiceId) {
        this.choiceId = choiceId;
    }

    public void setChoiceId(String choiceId) {

        this.choiceId.setEntityId(choiceId);
    }

    public List<BigDecimal> getVoteByGrad() {
        return voteByGrad;
    }

    public void setVoteByGrad(List<BigDecimal> voteByGrad) {
        this.voteByGrad = voteByGrad;
    }

    public int getMedian() {
        return median;
    }

    public void setMedian(int median) {
        this.median = median;
    }

}
