package org.chorem.pollen.services.service.mail;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Report;
import org.nuiton.topia.persistence.TopiaEntity;

import jakarta.mail.Message;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class PollenMailReturnContext {

    protected Message message;

    protected PollenMailType mailType;

    protected Poll poll;

    protected Choice choice;

    protected PollenUser user;

    protected Report report;

    protected Comment comment;

    protected PollenPrincipal principal;

    protected static <E extends TopiaEntity> E findEntity(List<TopiaEntity> entities, Class<E> type) {
        return entities.stream()
                .filter(type::isInstance)
                .map(type::cast)
                .findFirst()
                .orElse(null);
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public PollenMailType getMailType() {
        return mailType;
    }

    public void setMailType(PollenMailType mailType) {
        this.mailType = mailType;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public Choice getChoice() {
        return choice;
    }

    public void setChoice(Choice choice) {
        this.choice = choice;
    }

    public PollenUser getUser() {
        return user;
    }

    public void setUser(PollenUser user) {
        this.user = user;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public PollenPrincipal getPrincipal() {
        return principal;
    }

    public void setPrincipal(PollenPrincipal principal) {
        this.principal = principal;
    }
}
