package org.chorem.pollen.services.service.mail;

/*-
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.Choice;
import org.nuiton.i18n.I18n;

import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ChoiceReportForAdminEmail extends AbstractReportForAdminEmail<Choice> {

    private String choiceValue;

    public ChoiceReportForAdminEmail(Locale locale, TimeZone timeZone) {
        super(locale, timeZone);
    }

    @Override
    public String getSubject() {
        return I18n.l(locale, "pollen.service.mail.ChoiceReportForAdminEmail.subject", poll.getTitle());
    }

    public String getChoiceValue() {
        return choiceValue;
    }

    public void setChoiceValue(String choiceValue) {
        this.choiceValue = choiceValue;
    }

    public String getChoiceCreateDate() {
        return formatDate(target.getTopiaCreateDate());
    }
}
