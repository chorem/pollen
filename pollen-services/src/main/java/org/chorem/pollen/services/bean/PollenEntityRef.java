package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.PollenEntityEnum;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenToken;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.SessionToken;
import org.chorem.pollen.persistence.entity.Vote;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class PollenEntityRef<E extends TopiaEntity> extends PollenEntityId<E> {

    protected String permission;

    public static PollenEntityRef<Poll> of(Poll entity) {

        return of(entity, entity.getCreator().getPermission());

    }

    public static PollenEntityRef<Choice> of(Choice entity) {

        return of(entity, entity.getCreator().getPermission());

    }

    public static PollenEntityRef<Comment> of(Comment entity) {

        return of(entity, entity.getAuthor().getPermission());

    }

    public static PollenEntityRef<Vote> of(Vote entity) {

        return of(entity, entity.getVoter().getPermission());

    }

    public static PollenEntityRef<PollenUser> of(SessionToken result) {

        return of(result.getPollenUser(), result.getPollenToken());

    }

    public static <E extends TopiaEntity> PollenEntityRef<E> of(E result) {

        return new PollenEntityRef<>(result, null);

    }

    public static <E extends TopiaEntity> PollenEntityRef<E> of(E id, PollenToken permission) {

        return new PollenEntityRef<>(id, permission == null ? null : permission.getToken());

    }

    public static <E extends TopiaEntity> PollenEntityRef<E> newRef(Class<E> entityType) {

        return new PollenEntityRef<>(entityType);

    }

    protected PollenEntityRef(E entity, String permission) {

        this(PollenEntityEnum.getContractClass((Class) entity.getClass()));
        setEntityId(entity.getTopiaId());
        setPermission(permission);
    }

    protected PollenEntityRef(Class<E> entityType) {

        super(entityType);
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
