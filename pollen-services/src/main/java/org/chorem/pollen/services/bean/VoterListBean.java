package org.chorem.pollen.services.bean;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.chorem.pollen.persistence.entity.VoterList;

import java.util.Set;

/**
 * Created on 5/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoterListBean extends PollenBean<VoterList> {

    protected String name;

    protected double weight;

    protected PollenEntityId<VoterList> parentId;

    protected long countSubLists;

    protected long countMembers;

    protected Set<String> allEmails;

    public VoterListBean() {
        super(VoterList.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public PollenEntityId<VoterList> getParentId() {
        if (parentId == null) {
            parentId = PollenEntityId.newId(VoterList.class);
        }
        return parentId;
    }

    public void setParentId(PollenEntityId<VoterList> parentId) {
        this.parentId = parentId;
    }

    public long getCountSubLists() {
        return countSubLists;
    }

    public void setCountSubLists(long countSubLists) {
        this.countSubLists = countSubLists;
    }

    public long getCountMembers() {
        return countMembers;
    }

    public void setCountMembers(long countMembers) {
        this.countMembers = countMembers;
    }

    public Set<String> getAllEmails() {
        return allEmails;
    }

    public void setAllEmails(Set<String> allEmails) {
        this.allEmails = allEmails;
    }
}
