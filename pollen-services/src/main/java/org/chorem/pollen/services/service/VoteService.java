package org.chorem.pollen.services.service;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollType;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenPrincipalTopiaDao;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Polls;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.persistence.entity.VoteToChoice;
import org.chorem.pollen.persistence.entity.VoteToChoiceTopiaDao;
import org.chorem.pollen.persistence.entity.VoteToChoices;
import org.chorem.pollen.persistence.entity.VoteTopiaDao;
import org.chorem.pollen.persistence.entity.VoterList;
import org.chorem.pollen.persistence.entity.VoterListMember;
import org.chorem.pollen.services.bean.ChoiceBean;
import org.chorem.pollen.services.bean.PaginationParameterBean;
import org.chorem.pollen.services.bean.PaginationResultBean;
import org.chorem.pollen.services.bean.PollenEntityRef;
import org.chorem.pollen.services.bean.ReportResumeBean;
import org.chorem.pollen.services.bean.VoteBean;
import org.chorem.pollen.services.bean.VoteToChoiceBean;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.security.PollenPermissions;
import org.chorem.pollen.votecounting.VoteCounting;
import org.chorem.pollen.votecounting.model.VoteCountingConfig;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class VoteService extends PollenServiceSupport {

    public VoteBean toVoteBean(Vote entity) {

        VoteBean bean = new VoteBean();

        bean.setEntityId(entity.getTopiaId());

        PollenPrincipal voter = entity.getVoter();

        if (voter == null || voter.getPermission() == null) {

            bean.setPermission(null);

        } else {

            bean.setPermission(voter.getPermission().getToken());

        }

        bean.setAnonymous(entity.isAnonymous());

        if (voter != null) {
            bean.getVoterId().setEntityId(voter.getTopiaId());
            bean.setVoterName(voter.getName());
            if (voter.getPollenUser() != null && voter.getPollenUser().getAvatar() != null) {
                bean.setVoterAvatar(getPollenResourceService().getReduceIdByTopiaId(voter.getPollenUser().getAvatar().getTopiaId()));
            }
        }

        bean.setWeight(entity.getWeight());

        entity.getVoteToChoice().stream()
                .sorted(VoteToChoices.VOTE_TO_CHOICE_COMPARATOR)
                .map(this::toVoteToChoiceBean)
                .forEach(bean::addChoice);


        if (entity.getVoterListMember() != null) {

            bean.setVoterListMembers(entity.getVoterListMember());

        }

        if (isNotPermitted(PollenPermissions.edit(entity))) {
            bean.setPermission(null);
            if (entity.isAnonymous()) {
                bean.setVoterName(null);
                bean.setVoterAvatar(null);
            }

            int maxVoters = getPollService().getMaxVoters(entity.getQuestion().getPoll());
            if (maxVoters > 0 && getVoteDao().getVoteIndex(entity) >= maxVoters) {
                bean.setIgnored(true);
                bean.getChoice().forEach(vc -> vc.setVoteValue(null));
            }

        } else {
            ReportResumeBean report = getReportService().getReport(entity.getTopiaId());
            bean.setReport(report);
        }
        return bean;
    }

    public VoteToChoiceBean toVoteToChoiceBean(VoteToChoice entity) {

        VoteToChoiceBean bean = new VoteToChoiceBean();

        bean.setEntityId(entity.getTopiaId());
        bean.setVoteValue(entity.getVoteValue());
        bean.getChoiceId().setEntityId(entity.getChoice().getTopiaId());

        return bean;
    }

    public VoteBean getNewVote(String pollId, String questionId) {
        checkIsConnectedRequired();

        Poll poll = getPollService().getPoll0(pollId);

        Question question = getQuestionService().getQuestion0(questionId);
        checkPermission(PollenPermissions.addVote(question));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        VoteBean voteBean = new VoteBean();
        voteBean.setWeight(1d);

        PollenPrincipal mainPrincipal = getSecurityContext().getMainPrincipal();
        PollenUser connectedUser = getConnectedUser();

        if (question.getPoll().getPollType() == PollType.RESTRICTED) {

            List<VoterListMember> voterListMembers = new ArrayList<>();
            // si si le mainPrincipal a voté
            if (mainPrincipal != null) {
                voterListMembers = getVoterListMemberDao()
                        .forEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_EMAIL, mainPrincipal.getEmail())
                        .addEquals(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL + "." + Poll.PROPERTY_TOPIA_ID, question.getPoll().getTopiaId())
                        .findAll();
            }


            if (CollectionUtils.isNotEmpty(voterListMembers)
                    && !getVoteDao().forQuestionEquals(question).addEquals(Vote.PROPERTY_VOTER, mainPrincipal).exists()
                    && mainPrincipal != null) {
                voteBean.setVoterName(mainPrincipal.getName());
                voteBean.setVoterListMembers(voterListMembers);
            } else if (connectedUser != null) {
                voteBean.setVoterName(connectedUser.getName());
            }

        } else {

            if (mainPrincipal != null) {

                voteBean.setVoterName(mainPrincipal.getName());

                List<VoterListMember> voterListMembers = getVoterListMemberDao()
                        .forEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_EMAIL, mainPrincipal.getEmail())
                        .addEquals(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL + "." + Poll.PROPERTY_TOPIA_ID, question.getPoll().getTopiaId())
                        .findAll();
                if (voterListMembers != null) {
                    voteBean.setVoterListMembers(voterListMembers);
                    // fixme bavencoff  17/05/2017 faut-il toujour garder le poid sur le vote ?? et pas sur le VoterListMember
                    //voteBean.setWeight(voterListMember.getWeight());
                }
            } else if (connectedUser != null) {

                voteBean.setVoterName(connectedUser.getName());

            }
        }

        return voteBean;

    }

    public PaginationResultBean<VoteBean> getVotes(String pollId, String questionId, PaginationParameterBean paginationParameter) {
        checkIsConnectedRequired();

        checkNotNull(questionId);

        Question question = getQuestionService().getQuestion0(questionId);

        Poll poll = getPollService().getPoll0(pollId);

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }
        
        List<Vote> votes = getVotes0(question).stream()
                .filter(vote -> isPermitted(PollenPermissions.read(vote)))
                .collect(Collectors.toList());

        PaginationResult<Vote> votePaginationResult = PaginationResult.fromFullList(votes, getPaginationParameter(paginationParameter));

        return toPaginationListBean(votePaginationResult, this::toVoteBean);

    }

    public VoteBean getVote(String pollId, String questionId, String voteId) {
        checkIsConnectedRequired();

        checkNotNull(voteId);

        Question question = getQuestionService().getQuestion0(questionId);

        Poll poll = getPollService().getPoll0(pollId);

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        Vote vote = getVote(question, voteId);
        checkPermission(PollenPermissions.read(vote));

        return toVoteBean(vote);

    }

    public PollenEntityRef<Vote> addVote(String pollId, String questionId, VoteBean vote) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(questionId);
        checkNotNull(vote);
        checkIsNotPersisted(vote);

        Question question = getQuestionService().getQuestion0(questionId);

        Poll poll = getPollService().getPoll0(pollId);

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        checkPermission(PollenPermissions.addVote(question));

        List<ChoiceBean> choices = getChoiceService().getChoices(questionId);

        ErrorMap errorMap = checkVote(question, vote, choices);
        errorMap.failIfNotEmpty();

        Vote result = saveVote(question, vote);
        commit();

        getNotificationService().onVoteAdded(question, result);
        getFeedService().onVoteAdded(question, result);

        int maxVoters = getPollService().getMaxVoters(question.getPoll());
        if (maxVoters > 0 && getVoteCount(question) > maxVoters) {
            boolean notificationSend = getPollDao().setFlagNotificationMaxVoterSend(question.getPoll());
            if (notificationSend) {
                commit();
                getNotificationService().onExceedingMaxVoters(question.getPoll(), maxVoters);
            }
        }

        return PollenEntityRef.of(result);

    }

    public VoteBean editVote(String pollId, String questionId, VoteBean vote) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(vote);
        checkIsPersisted(vote);

        Poll poll = getPollService().getPoll0(pollId);

        Question question = getQuestionService().getQuestion0(questionId);
        Vote voteBd = getVote(question, vote.getEntityId());
        checkPermission(PollenPermissions.edit(voteBd));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        List<ChoiceBean> choices = getChoiceService().getChoices(questionId);

        ErrorMap errorMap = checkVote(question, vote, choices);
        errorMap.failIfNotEmpty();

        Vote result = saveVote(question, vote);
        commit();

        getNotificationService().onVoteEdited(question, result);
        getFeedService().onVoteEdited(question, result);

        return toVoteBean(result);

    }

    public void deleteVote(String pollId, String questionId, String voteId) throws InvalidFormException {
        checkIsConnectedRequired();

        checkNotNull(questionId);
        checkNotNull(voteId);

        Poll poll = getPollService().getPoll0(pollId);

        Question question = getQuestionService().getQuestion0(questionId);
        Vote vote = getVote(question, voteId);
        checkPermission(PollenPermissions.delete(vote));

        Poll questionPoll = question.getPoll();
        if (!questionPoll.getTopiaId().equals(poll.getTopiaId())){
            throw new InvalidEntityLinkException(Question.PROPERTY_POLL, question, poll);
        }

        ErrorMap errorMap = checkPoll(question.getPoll());
        errorMap.failIfNotEmpty();

        getVoteDao().delete(vote);
        commit();

        getNotificationService().onVoteDeleted(question, vote);
        getFeedService().onVoteDeleted(question, vote);

    }

    protected ErrorMap checkPoll(Poll poll) {

        ErrorMap errors = new ErrorMap();

        Date now = serviceContext.getNow();

        check(errors, "poll", Polls.isStarted(poll, now), l(getLocale(), "pollen.error.vote.poll.notStarted"));
        check(errors, "poll", !Polls.isFinished(poll, now), l(getLocale(), "pollen.error.vote.poll.finished"));

        return errors;
    }

    protected <C extends VoteCountingConfig> ErrorMap checkVote(Question question, VoteBean vote, List<ChoiceBean> choices) {

        ErrorMap errors = new ErrorMap();

        Set<String> voterNames = new HashSet<>();

        errors.addAllErrors(checkPoll(question.getPoll()));

        boolean voteExists = vote.isPersisted();
        boolean voteNameNotBlank = checkNotBlank(errors, "voter.name", vote.getVoterName(), l(getLocale(), "pollen.error.vote.voterName.mandatory"));

        if (voteNameNotBlank) {
            List<Vote> existingVote = getVoteDao().findAll(question);

            if (CollectionUtils.isNotEmpty(existingVote)) {

                // get all voter name

                for (Vote oneVote : existingVote) {
                    if (voteExists &&
                        oneVote.getTopiaId().equals(vote.getEntityId())) {

                        continue;
                    }

                    voterNames.add(oneVote.getVoter().getName());
                }
            }

            boolean voterNameAdded = voterNames.add(vote.getVoterName());
            check(errors, "voter.name", voterNameAdded, l(getLocale(), "pollen.error.vote.voterName.alreadyExist"));

        }

        VoteCounting<?, C> voteCounting = getVoteCountingService().getVoteCounting(question);
        C config = getVoteCountingService().getVoteCountingConfig(question);


        ErrorMap valueErrors = getVoteCountingService().checkVote(vote, voteCounting, choices, config);
        valueErrors.copyTo(errors, "vote.");

        return errors;

    }

    protected Vote saveVote(Question question, VoteBean vote) {

        boolean voteExist = vote.isPersisted();

        Vote toSave;

        //TODO Finish save
        if (voteExist) {

            toSave = getVote(question, vote.getEntityId());

        } else {
            toSave = getVoteDao().create();

            PollenPrincipal mainPrincipal = getSecurityContext().getMainPrincipal();

            if (Polls.isPollRestricted(question.getPoll())) {

                List<VoterListMember> voterListMembers = new ArrayList<>();

                // si si le mainPrincipal a voté
                if (mainPrincipal != null) {
                    voterListMembers = getVoterListMemberDao()
                            .forEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_EMAIL, mainPrincipal.getEmail())
                            .addEquals(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL + "." + TopiaEntity.PROPERTY_TOPIA_ID, question.getPoll().getTopiaId())
                            .findAll();
                }


                if (CollectionUtils.isNotEmpty(voterListMembers) && !getVoteDao().forQuestionEquals(question).addEquals(Vote.PROPERTY_VOTER, mainPrincipal).exists()) {
                    // vote pour le mainPrincipal
                    toSave.setVoterListMember(voterListMembers);
                    toSave.setVoter(mainPrincipal);
                   } else {
                    // vote pour le connectedUser
                    voterListMembers = getVoterListMemberDao()
                            .forEquals(VoterListMember.PROPERTY_MEMBER + "." + PollenPrincipal.PROPERTY_POLLEN_USER, getConnectedUser())
                            .addEquals(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL + "." + TopiaEntity.PROPERTY_TOPIA_ID, question.getPoll().getTopiaId())
                            .findAll();
                    toSave.setVoterListMember(voterListMembers);
                    toSave.setVoter(voterListMembers.get(0).getMember());
                }

            } else {

                if (mainPrincipal != null) {
                    List<VoterListMember> voterListMembers = getVoterListMemberDao().forMemberEquals(mainPrincipal)
                            .addEquals(VoterListMember.PROPERTY_VOTER_LIST + "." + VoterList.PROPERTY_POLL, question.getPoll())
                            .findAll();
                    if (!voterListMembers.isEmpty()) {
                        toSave.setVoterListMember(voterListMembers);

                        // FIXME bavencoff 17/05/2017 ne poids ne devrai plus etre enregistré dans le vote
                        //toSave.setWeight(voterListMember.getWeight());
                        toSave.setVoter(mainPrincipal);
                    }
                }

                // -- author -- //
                if (toSave.getVoter() == null) {
                    toSave.setWeight(1);
                    PollenPrincipal author = getSecurityService().generatePollenPrincipal();
                    toSave.setVoter(author);
                }
            }

            toSave.setQuestion(question);
        }

        toSave.setAnonymous(question.getPoll().isAnonymousVoteAllowed());

        // -- author -- //

        toSave.getVoter().setName(vote.getVoterName());

        PollenUser connectedUser = getConnectedUser();

        if (connectedUser != null) {

            // link to connected user
            toSave.getVoter().setPollenUser(connectedUser);

        }

        // -- List of VoteToChoice -- //

        if (voteExist) {

            //TODO dralagen 31/07/14 : review code for persisted Collection

            Collection<VoteToChoice> choicesToSave = toSave.getVoteToChoice();
            for (VoteToChoiceBean input : vote.getChoice()) {
                if (input.isPersisted()) {
                    for (VoteToChoice voteToChoice : choicesToSave) {
                        if (input.getChoiceId().getEntityId().equals(voteToChoice.getChoice().getTopiaId())) {
                            voteToChoice.setVoteValue(input.getVoteValue());
                        }
                    }
                } else {
                    boolean edited = false;

                    for (VoteToChoice voteToChoice : choicesToSave) {
                        if (input.getChoiceId().getEntityId().equals(voteToChoice.getChoice().getTopiaId())) {
                            //Found a vote for this choice, so update it
                            voteToChoice.setVoteValue(input.getVoteValue());
                            edited = true;
                        }
                    }

                    if (!edited) {
                        //Did not find a vote for this choice, so create it
                        choicesToSave.add(createVoteToChoice(toSave, input));
                    }
                }
            }
        } else {

            Set<VoteToChoice> choicesToSave = new HashSet<>();
            for (VoteToChoiceBean input : vote.getChoice()) {

                VoteToChoice voteToChoice = createVoteToChoice(toSave, input);
                choicesToSave.add(voteToChoice);

            }

            toSave.setVoteToChoice(choicesToSave);
        }

        return toSave;

    }

    protected Vote getVote(Question question, String voteId) {

        Vote result = getVoteDao().forTopiaIdEquals(voteId).findUnique();

        if (!question.equals(result.getQuestion())) {

            throw new InvalidEntityLinkException(Vote.PROPERTY_QUESTION, result, question);

        }

        return result;

    }

    protected List<Vote> getVotes0(Question question) {

        return getVoteDao().findAll(question);

    }

    protected VoteToChoice getVoteToChoice(Vote vote, String voteToChoiceId) {

        VoteToChoice result = getVoteToChoiceDao().forTopiaIdEquals(voteToChoiceId).findUnique();

        if (!vote.containsVoteToChoice(result)) {

            throw new InvalidEntityLinkException(Vote.PROPERTY_VOTE_TO_CHOICE, result, vote);

        }

        return result;

    }


    protected VoteToChoice createVoteToChoice(Vote vote, VoteToChoiceBean source) {

        VoteToChoice result;

        VoteToChoiceTopiaDao voteToChoiceDao = getVoteToChoiceDao();

        result = voteToChoiceDao.create();

        String choiceId = source.getChoiceId().getEntityId();

        Choice choiceLoaded = getChoiceDao().forTopiaIdEquals(choiceId).findUnique();

        result.setChoice(choiceLoaded);

        vote.addVoteToChoice(result);

        result.setVoteValue(source.getVoteValue());

        return result;

    }

    public long getVoteCount(Question question) {
        checkIsConnectedRequired();

        return getVoteDao().forQuestionEquals(question).count();
    }

    public void purgeOldVotes() {

        PollenPrincipalTopiaDao principalDao = getPollenPrincipalDao();

        VoteTopiaDao voteDao = getVoteDao();

        PollenServicesConfig applicationConfig = getPollenServiceConfig();

        int anonymizeOlderVotesAgeSecond = applicationConfig.getAnonymizeOlderVotesAge();
        Date now = serviceContext.getNow();
        Date someTimeAgo = DateUtils.addSeconds(now, -1 * anonymizeOlderVotesAgeSecond);

        List<Vote> votesToAnon = voteDao.findAllOldVotes(someTimeAgo);

        List<PollenPrincipal> principalsToSave = new ArrayList<>();
        List<Vote> votesToSave = new ArrayList<>();

        for(Vote vote:votesToAnon){
            PollenPrincipal principal = vote.getVoter();
            //Ceinture-bretelles, on vérifie bien que aucun user est lié,
            // même si ça devrait jamais être le cas
            if (principal.getPollenUser() != null) {
                principal.setName(VoteTopiaDao.ANONYMOUS_NAME);
                principalsToSave.add(principal);
                vote.setAnonymous(true);
                votesToSave.add(vote);
            }
        }

        //save votes
        voteDao.updateAll(votesToSave);

        //save principals
        principalDao.updateAll(principalsToSave);
    }
}
