package org.chorem.pollen.services.service.mail;

/*
 * #%L
 * Pollen :: Service
 * %%
 * Copyright (C) 2009 - 2017 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheException;
import com.github.mustachejava.MustacheFactory;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail2.jakarta.Email;
import org.apache.commons.mail2.core.EmailException;
import org.apache.commons.mail2.jakarta.MultiPartEmail;
import org.apache.commons.mail2.jakarta.SimpleEmail;
import org.chorem.pollen.persistence.entity.Choice;
import org.chorem.pollen.persistence.entity.Comment;
import org.chorem.pollen.persistence.entity.EmailToResend;
import org.chorem.pollen.persistence.entity.EmailToResendTopiaDao;
import org.chorem.pollen.persistence.entity.Poll;
import org.chorem.pollen.persistence.entity.PollenPrincipal;
import org.chorem.pollen.persistence.entity.PollenResource;
import org.chorem.pollen.persistence.entity.PollenUser;
import org.chorem.pollen.persistence.entity.Question;
import org.chorem.pollen.persistence.entity.Report;
import org.chorem.pollen.persistence.entity.UserCredential;
import org.chorem.pollen.persistence.entity.Vote;
import org.chorem.pollen.services.PollenTechnicalException;
import org.chorem.pollen.services.bean.FeedbackBean;
import org.chorem.pollen.services.bean.PollenEntityId;
import org.chorem.pollen.services.bean.PollenUserBean;
import org.chorem.pollen.services.config.PollenServicesConfig;
import org.chorem.pollen.services.service.PollenServiceSupport;
import org.nuiton.i18n.I18n;

import jakarta.mail.Address;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Part;
import jakarta.mail.SendFailedException;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMultipart;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 4/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class EmailService extends PollenServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EmailService.class);

    public static final String RECIPIENT_SEPARATOR = ";";
    public static final String MAIL_ID_SEPARATOR = "+";
    public static final String MAIL_ID_SEPARATOR_REGEXP = "\\+";

    static {
        n("pollen.service.mail.loginProvider.amazon");
        n("pollen.service.mail.loginProvider.facebook");
        n("pollen.service.mail.loginProvider.flickr");
        n("pollen.service.mail.loginProvider.foursquare");
        n("pollen.service.mail.loginProvider.github");
        n("pollen.service.mail.loginProvider.googleplus");
        n("pollen.service.mail.loginProvider.hotmail");
        n("pollen.service.mail.loginProvider.instagram");
        n("pollen.service.mail.loginProvider.linkedin");
        n("pollen.service.mail.loginProvider.linkedin2");
        n("pollen.service.mail.loginProvider.mendeley");
        n("pollen.service.mail.loginProvider.myspace");
        n("pollen.service.mail.loginProvider.nimble");
        n("pollen.service.mail.loginProvider.runkeeper");
        n("pollen.service.mail.loginProvider.salesforce");
        n("pollen.service.mail.loginProvider.stackexchange");
        n("pollen.service.mail.loginProvider.twitter");
        n("pollen.service.mail.loginProvider.yahoo");
        n("pollen.service.mail.loginProvider.yammer");
    }

    protected TimeZone getTimeZone() {
        TimeZone timeZone = getPollenServiceConfig().getDefaultTimeZone();
        return timeZone;
    }

    public ChoiceAddedEmail newChoiceAddedEmail(Question question, Choice choice) {
        //FIXME JC181002 - APRIL - Modifier l'email pour adapter aux questions
        ChoiceAddedEmail email = new ChoiceAddedEmail(getLocale(), getTimeZone());
        email.setPoll(question.getPoll());
        email.setChoice(choice);
        return email;
    }

    public ChoiceEditedEmail newChoiceEditedEmail(Question question, Choice choice) {
        //FIXME JC181002 - APRIL - Modifier l'email pour adapter aux questions
        ChoiceEditedEmail email = new ChoiceEditedEmail(getLocale(), getTimeZone());
        email.setPoll(question.getPoll());
        email.setChoice(choice);
        return email;
    }

    public ChoiceDeletedEmail newChoiceDeletedEmail(Question question, Choice choice) {
        //FIXME JC181002 - APRIL - Modifier l'email pour adapter aux questions
        ChoiceDeletedEmail email = new ChoiceDeletedEmail(getLocale(), getTimeZone());
        email.setPoll(question.getPoll());
        email.setChoice(choice);
        return email;
    }

    public CommentAddedEmail newCommentAddedEmail(Poll poll, Comment comment) {
        CommentAddedEmail email = new CommentAddedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setComment(comment);
        return email;
    }

    public CommentEditedEmail newCommentEditedEmail(Poll poll, Comment comment) {
        CommentEditedEmail email = new CommentEditedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setComment(comment);
        return email;
    }

    public CommentDeletedEmail newCommentDeletedEmail(Poll poll, Comment comment) {
        CommentDeletedEmail email = new CommentDeletedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setComment(comment);
        return email;
    }

    public CommentAddedEmail newCommentAddedEmail(Question question, Comment comment) {
        //FIXME JC181002 - APRIL - Email spécifique pour les questions ?
        CommentAddedEmail email = new CommentAddedEmail(getLocale(), getTimeZone());
        email.setPoll(question.getPoll());
        email.setComment(comment);
        return email;
    }

    public CommentEditedEmail newCommentEditedEmail(Question question, Comment comment) {
        //FIXME JC181002 - APRIL - Email spécifique pour les questions ?
        CommentEditedEmail email = new CommentEditedEmail(getLocale(), getTimeZone());
        email.setPoll(question.getPoll());
        email.setComment(comment);
        return email;
    }

    public CommentDeletedEmail newCommentDeletedEmail(Question question, Comment comment) {
        //FIXME JC181002 - APRIL - Email spécifique pour les questions ?
        CommentDeletedEmail email = new CommentDeletedEmail(getLocale(), getTimeZone());
        email.setPoll(question.getPoll());
        email.setComment(comment);
        return email;
    }

    public VoteAddedEmail newVoteAddedEmail(Question question, Vote vote) {
        return new VoteAddedEmail(getLocale(), getTimeZone(), question.getPoll(), vote);
    }

    public VoteEditedEmail newVoteEditedEmail(Question question, Vote vote) {
        return new VoteEditedEmail(getLocale(), getTimeZone(), question.getPoll(), vote);
    }

    public VoteDeletedEmail newVoteDeletedEmail(Question question, Vote vote) {
        return new VoteDeletedEmail(getLocale(), getTimeZone(), question.getPoll(), vote);
    }

    public PollCreatedEmail newPollCreatedEmail(Poll poll) {
        PollCreatedEmail email = new PollCreatedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);

        email.setPollenUrl(getUIContext().getUiEndPoint());

        PollenEntityId<Poll> pollId = getPollenEntityId(poll);
        email.setUrl(getPollenUIUrlRenderService().getPollEditUrl(getUIContext().getPollEditUrl(),
                                                                  pollId.getReducedId(),
                                                                  poll.getCreator().getPermission().getToken()));

        return email;
    }

    public PollClosedEmail newPollClosedEmail(Poll poll) {
        PollClosedEmail email = new PollClosedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        return email;
    }

    public PollChoicePeriodStartedEmail newPollChoicePeriodStartedEmail(Poll poll) {
        PollChoicePeriodStartedEmail email = new PollChoicePeriodStartedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        return email;
    }

    public PollChoicePeriodEndedEmail newPollChoicePeriodEndedEmail(Poll poll) {
        PollChoicePeriodEndedEmail email = new PollChoicePeriodEndedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        return email;
    }

    public PollVotePeriodStartedEmail newPollVotePeriodStartedEmail(Poll poll) {
        PollVotePeriodStartedEmail email = new PollVotePeriodStartedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        return email;
    }

    public PollVotePeriodEndedEmail newPollVotePeriodEndedEmail(Poll poll) {
        PollVotePeriodEndedEmail email = new PollVotePeriodEndedEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        return email;
    }

    public PollVoteReminderEmail newPollVoteReminderEmail(Poll poll) {
        PollVoteReminderEmail email = new PollVoteReminderEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        return email;
    }

    public PollInvitationEmail newPollInvitationEmail(Poll poll) {
        PollInvitationEmail email = new PollInvitationEmail(getLocale(), getTimeZone());

        List<Question> questions = getQuestionService().getQuestions(poll);

        email.setPoll(poll);
        email.setQuestions(questions);
        email.setVoteUrl(getPollVoteUrl(poll));

        return email;
    }

    public RestrictedPollInvitationEmail newRestrictedPollInvitationEmail(Poll poll, PollenPrincipal principal) {
        RestrictedPollInvitationEmail email = new RestrictedPollInvitationEmail(getLocale(), getTimeZone());

        List<Question> questions = getQuestionService().getQuestions(poll);

        email.setPoll(poll);
        email.setQuestions(questions);
        email.setPrincipal(principal);
        email.setVoteUrl(getPollVoteUrl(poll, principal.getPermission().getToken()));

        email.setToken(principal.getPermission().getToken());

        return email;
    }

    public UserAccountCreatedEmail newUserAccountCreatedEmail(PollenUser user, String token) {
        UserAccountCreatedEmail email = new UserAccountCreatedEmail(getLocale(), getTimeZone());
        email.setUser(user);

        PollenEntityId<PollenUser> userId = getPollenEntityId(user);
        email.setValidateUrl(getPollenUIUrlRenderService().getUserValidateUrl(getUIContext().getUserValidateUrl(),
                                                                              userId.getReducedId(),
                                                                              token));

        return email;
    }

    public UserAccountCreatedFromProviderEmail newUserAccountCreatedFromProviderEmail(PollenUser user,
                                                                                      UserCredential credential) {
        UserAccountCreatedFromProviderEmail email = new UserAccountCreatedFromProviderEmail(getLocale(), getTimeZone());
        email.setUser(user);
        email.setProvider(t("pollen.service.mail.loginProvider." + credential.getProvider()));
        email.setProfileUrl(getUIContext().getProfileUrl());
        return email;
    }

    public UserAccountEditedEmail newUserAccountEditedEmail(PollenUser user) {
        UserAccountEditedEmail email = new UserAccountEditedEmail(getLocale(), getTimeZone());
        email.setUser(user);
        return email;
    }

    public UserAccountDeletedEmail newUserAccountDeletedEmail(PollenUser user) {
        UserAccountDeletedEmail email = new UserAccountDeletedEmail(getLocale(), getTimeZone());
        email.setUser(user);
        return email;
    }

    public UserAccountEmailAddressAddedEmail newUserAccountEmailAddressAddedEmail(PollenUser user,
                                                                                  String token) {
        UserAccountEmailAddressAddedEmail email = new UserAccountEmailAddressAddedEmail(getLocale(), getTimeZone());
        email.setUser(user);
        PollenEntityId<PollenUser> userId = getPollenEntityId(user);
        email.setValidateUrl(getPollenUIUrlRenderService().getUserValidateUrl(getUIContext().getUserValidateUrl(),
                                                                              userId.getReducedId(),
                                                                              token));
        return email;
    }

    public UserAccountEmailValidatedEmail newUserAccountEmailValidatedEmail(PollenUser user) {
        UserAccountEmailValidatedEmail email = new UserAccountEmailValidatedEmail(getLocale(), getTimeZone());
        email.setUser(user);
        email.setPollenUrl(getUIContext().getUiEndPoint());
        return email;
    }

    public ResendValidationEmail newUserResendValidationEmail(PollenUser user, String token) {
        ResendValidationEmail email = new ResendValidationEmail(getLocale(), getTimeZone());
        email.setUser(user);

        PollenEntityId<PollenUser> userId = getPollenEntityId(user);
        email.setValidateUrl(getPollenUIUrlRenderService().getUserValidateUrl(getUIContext().getUserValidateUrl(),
                                                                              userId.getReducedId(),
                                                                              token));

        return email;
    }

    public UserAccountPasswordChangedEmail newUserAccountPasswordChangedEmail(PollenUser user) {
        UserAccountPasswordChangedEmail email = new UserAccountPasswordChangedEmail(getLocale(), getTimeZone());
        email.setUser(user);
        return email;
    }

    public LostPasswordEmail newLostPasswordEmail(PollenUser user, String password) {
        LostPasswordEmail email = new LostPasswordEmail(getLocale(), getTimeZone());
        email.setUser(user);
        email.setPassword(password);
        return email;
    }

    public PollEndReminderEmail newPollEndReminderEmail(Poll poll) {
        return newPollEndReminderEmail(poll, getLocale());
    }

    public PollEndReminderEmail newPollEndReminderEmail(Poll poll, Locale locale) {
        PollEndReminderEmail email = new PollEndReminderEmail(locale, getTimeZone());
        email.setPoll(poll);
        return email;
    }

    public void send(PollenMail mail) {
        doSend(mail);
    }

    public void resendEmails() {

        EmailToResendTopiaDao emailToResendDao = getEmailToResendDao();
        List<EmailToResend> allEmailsToResend = emailToResendDao.findAll();

        if (log.isDebugEnabled()) {
            log.debug("Send " + allEmailsToResend.size() + " emails ...");
        }

        for (EmailToResend emailToResend : allEmailsToResend) {
            try {
                String to = emailToResend.getTos();
                Collection<String> tos = new HashSet<>();
                if (StringUtils.isNotBlank(to)) {
                    tos.addAll(Arrays.asList(to.split(RECIPIENT_SEPARATOR)));
                }
                String bcc = emailToResend.getBccs();
                Collection<String> bccs = new HashSet<>();
                if (StringUtils.isNotBlank(bcc)) {
                    bccs.addAll(Arrays.asList(bcc.split(RECIPIENT_SEPARATOR)));
                }

                if (log.isDebugEnabled()) {
                    log.debug("Will send mail: to = '" + to + "', bcc = '" + bcc + "'");
                }

                Collection<InternetAddress> replyTo = null;
                if (StringUtils.isNotEmpty(emailToResend.getReplyTo())) {
                    try {
                        replyTo = Arrays.asList(InternetAddress.parse(emailToResend.getReplyTo()));
                    } catch (AddressException e) {
                        throw new PollenTechnicalException("error parse replyTo", e);
                    }
                }

                doSend(emailToResend.getSubject(), emailToResend.getBody(), emailToResend.getAdrFrom(), replyTo, tos, bccs);
                emailToResendDao.delete(emailToResend);
                commit();

            } catch (EmailException e) {
                if (SendFailedException.class.isInstance(e.getCause())) {
                    SendFailedException sendFailedException = SendFailedException.class.cast(e.getCause());
                    if (ArrayUtils.isNotEmpty(sendFailedException.getInvalidAddresses())) {
                        List<String> emails = Stream.of(sendFailedException.getInvalidAddresses())
                                .map(Address::toString)
                                .collect(Collectors.toList());
                        getPollenPrincipalDao()
                                .forEmailIn(emails)
                                .findAll()
                                .forEach(pollenPrincipal -> pollenPrincipal.setInvalid(true));
                        commit();
                    }
                } else {
                    if (log.isErrorEnabled()) {
                        log.error("Error while resending an email, keep it and try again", e);
                    }
                }
            }
            
            try {
                TimeUnit.MILLISECONDS.sleep(getPollenServiceConfig().getSmtpWait());
            } catch (InterruptedException e) {
                if (log.isErrorEnabled()) {
                    log.error("Error while resending an email, Interrupted sleeping", e);
                }

                // Restore the interrupted status
                Thread.currentThread().interrupt();
            }

        }
    }

    protected void doSend(PollenMail mail) {

        if (mail.isRecipientProvided()) {

            String subject = mail.getSubject();
            String body = getBody(mail);
            Set<String> tos = mail.getTos();
            Set<String> bccs = mail.getBccs();

            String smtpFrom = getPollenServiceConfig().getSmtpFrom();

            InternetAddress fromAddress = null;
            try {
                fromAddress = new InternetAddress(smtpFrom);

                if (StringUtils.isNotBlank(mail.getFromName())) {
                    String personal = I18n.l(getLocale(),"pollen.service.mail.from.via", mail.getFromName(), fromAddress.getPersonal());
                    fromAddress.setPersonal(personal);
                }
            } catch (AddressException | UnsupportedEncodingException e) {
                throw new PollenTechnicalException("Error on smtp from adresse", e);
            }

            String[] emailSplit = fromAddress.getAddress().split("@");
            String userFrom = emailSplit[0];
            String domainFrom = emailSplit[1];
            String mailId = getMailId(mail);
            fromAddress.setAddress(userFrom + "+" + mailId +  "@" + domainFrom);

            String from = fromAddress.toUnicodeString();
            Collection<InternetAddress> replyTo = mail.getReplyTo();

            EmailToResend emailToResend = getEmailToResendDao().create();
            emailToResend.setSubject(subject);
            emailToResend.setBody(body);
            emailToResend.setAdrFrom(from);
            emailToResend.setReplyTo(replyTo.stream()
                    .map(InternetAddress::toUnicodeString)
                    .collect(Collectors.joining(", ")));
            emailToResend.setTos(StringUtils.join(tos, RECIPIENT_SEPARATOR));
            emailToResend.setBccs(StringUtils.join(bccs, RECIPIENT_SEPARATOR));

            if (log.isInfoEnabled()) {
                log.info("record email after send : " + mailId);
            }

        } else {
            if (log.isErrorEnabled()) {
                log.error("email has no recipient, won't be sent " + mail);
            }
        }
    }

    protected void doSend(String subject, String body,
                          String from, Collection<InternetAddress> replyTo,
                          Collection<String> tos, Collection<String> bccs) throws EmailException {

        if (log.isDebugEnabled()) {
            log.debug("doSend: tos = '" + tos + "', bccs = '" + bccs + "'");
        }

        PollenServicesConfig applicationConfig = getPollenServiceConfig();

        if (applicationConfig.getSmtpHost() == null) {
            if (log.isInfoEnabled()) {
                log.info("an email should have been sent if not in devMode: tos = " +
                         tos + ". subject = '" + subject + "'. body = \n" + body);
            }
            if (log.isWarnEnabled()) {
                if ((tos == null || tos.isEmpty()) && (bccs == null || bccs.isEmpty())) {
                    log.warn("email has no recipient, would not have been sent");
                }
            }

        } else {

            Email newEmail = new SimpleEmail();
            if (log.isDebugEnabled()) {
                newEmail.setDebug(true);
            }
            newEmail.setHostName(applicationConfig.getSmtpHost());
            newEmail.setSmtpPort(applicationConfig.getSmtpPort());
            newEmail.setCharset(Charsets.UTF_8.name());
            newEmail.setSubject(subject);
            if (log.isDebugEnabled()) {
                log.debug("doSend: created email: host = '" + newEmail.getHostName() + "', port = '" + newEmail.getSmtpPort() + "'");
            }

            try {
                InternetAddress fromAdr = new InternetAddress(from);
                newEmail.setFrom(fromAdr.getAddress(), fromAdr.getPersonal());
            } catch (AddressException e) {
                log.warn("Could not parse address");
            }

            if (CollectionUtils.isNotEmpty(replyTo)) {
                newEmail.setReplyTo(replyTo);
            }

            for (String to : tos) {
                newEmail.addTo(to);
            }
            for (String bcc : bccs) {
                newEmail.addBcc(bcc);
            }
            newEmail.setMsg(body);
            try {
                newEmail.send();
            } catch (Throwable e) {
                throw new PollenTechnicalException("newEmail.send() failed: " + e);
            }
            if (log.isInfoEnabled()) {
                log.info("send email to : " + tos.stream().collect(Collectors.joining(", ")) + " - from " + from);
            }
        }
    }

    protected String getBody(PollenMail mail) {

        Mustache mustache = getMustache(mail);

        StringWriter stringWriter = new StringWriter();

        mustache.execute(stringWriter, mail);

        String body = stringWriter.toString() + mail.getSigning();

        if (mail.getTos().size() == 1) {

            body = getCryptoService().encryptMailIfAsKey(body, IterableUtils.get(mail.getTos(), 0));

        }

        return body;

    }

    protected Mustache getMustache(PollenMail mail) {

        MustacheFactory mustacheFactory = new DefaultMustacheFactory("email/");

        Locale locale = mail.getLocale();
        String signingTemplateName = "signing_" + locale.getLanguage() + ".mustache";
        String templateName = mail.getClass().getSimpleName() + "_" + locale.getLanguage() + ".mustache";

        Mustache mustache;
        try {
            mail.setSigning(getSigning(mustacheFactory, signingTemplateName));
            mustache = mustacheFactory.compile(templateName);

        } catch (MustacheException e) {

            // fallback with no locale
            signingTemplateName = "signing.mustache";
            mail.setSigning(getSigning(mustacheFactory, signingTemplateName));

            templateName = mail.getClass().getSimpleName() + ".mustache";
            mustache = mustacheFactory.compile(templateName);
        }

        return mustache;
    }

    protected String getSigning(MustacheFactory mustacheFactory, String signingTemplateName) {
        StringWriter stringWriter = new StringWriter();
        Mustache mustache = mustacheFactory.compile(signingTemplateName);
        mustache.execute(stringWriter, getUIContext());
        return stringWriter.toString();
    }

    public CommentReportEmail newCommentReportEmail(Poll poll, Comment comment, Report report) {
        // FIXME BAVENCOFF 15/06/2017   la local du mail devrait etre celle du destinataire
        CommentReportEmail email = new CommentReportEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setTarget(comment);

        comment.getAuthor().getName(); //FIXME BAVENCOFF 14/06/2017 pb de lazy hibernate

        email.setReport(report);

        PollenEntityId<Poll> pollId = getPollenEntityId(poll);
        String pollUrl = getPollenUIUrlRenderService().getPollVoteUrl(
                getUIContext().getPollVoteUrl(),
                pollId.getReducedId(),
                poll.getCreator().getPermission().getToken());
        email.setUrl(pollUrl);
        
        return email;
    }

    public CommentReportForAdminEmail newCommentReportForAdminEmail(Poll poll, Comment comment, Report report, PollenUser admin) {
        // FIXME BAVENCOFF 15/06/2017   la local du mail devrait etre celle du destinataire
        CommentReportForAdminEmail email = new CommentReportForAdminEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setTarget(comment);

        comment.getAuthor().getName(); //FIXME BAVENCOFF 14/06/2017 pb de lazy hibernate

        email.setReport(report);

        PollenEntityId<Poll> pollId = getPollenEntityId(poll);
        String pollUrl = getPollenUIUrlRenderService().getPollVoteUrl(
                getUIContext().getPollVoteUrl(),
                pollId.getReducedId(),
                null);
        email.setUrl(pollUrl);

        email.setAdministrator(admin);
        email.setReports(getReportTopiaDao().forTargetIdEquals(comment.getTopiaId()).findAll());
        email.setResume(getReportTopiaDao().getReportResume(comment.getTopiaId()));

        return email;
    }

    public ChoiceReportEmail newChoiceReportEmail(Poll poll, Choice choice, Report report) {
        // FIXME BAVENCOFF 15/06/2017   la local du mail devrait etre celle du destinataire
        ChoiceReportEmail email = new ChoiceReportEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setTarget(choice);
        email.setReport(report);
        email.setChoiceValue(getChoiceValue(choice));

        PollenEntityId<Poll> pollId = getPollenEntityId(poll);
        String pollUrl = getPollenUIUrlRenderService().getPollVoteUrl(
                getUIContext().getPollVoteUrl(),
                pollId.getReducedId(),
                poll.getCreator().getPermission().getToken());
        email.setUrl(pollUrl);

        return email;
    }

    public ChoiceReportForAdminEmail newChoiceReportForAdminEmail(Poll poll, Choice choice, Report report, PollenUser admin) {
        // FIXME BAVENCOFF 15/06/2017   la local du mail devrait etre celle du destinataire
        ChoiceReportForAdminEmail email = new ChoiceReportForAdminEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setTarget(choice);
        email.setReport(report);
        email.setChoiceValue(getChoiceValue(choice));

        PollenEntityId<Poll> pollId = getPollenEntityId(poll);
        String pollUrl = getPollenUIUrlRenderService().getPollVoteUrl(
                getUIContext().getPollVoteUrl(),
                pollId.getReducedId(),
                null);
        email.setUrl(pollUrl);

        email.setAdministrator(admin);
        email.setReports(getReportTopiaDao().forTargetIdEquals(choice.getTopiaId()).findAll());
        email.setResume(getReportTopiaDao().getReportResume(choice.getTopiaId()));

        return email;
    }

    public PollReportEmail newPollReportEmail(Poll poll, Report report) {
        // FIXME BAVENCOFF 15/06/2017   la local du mail devrait etre celle du destinataire
        PollReportEmail email = new PollReportEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setTarget(poll);
        email.setReport(report);

        PollenEntityId<Poll> pollId = getPollenEntityId(poll);
        String pollUrl = getPollenUIUrlRenderService().getPollVoteUrl(
                getUIContext().getPollVoteUrl(),
                pollId.getReducedId(),
                poll.getCreator().getPermission().getToken());
        email.setUrl(pollUrl);

        return email;
    }

    public PollReportForAdminEmail newPollReportForAdminEmail(Poll poll, Report report, PollenUser admin) {
        // FIXME BAVENCOFF 15/06/2017   la local du mail devrait etre celle du destinataire
        PollReportForAdminEmail email = new PollReportForAdminEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setTarget(poll);
        email.setReport(report);

        PollenEntityId<Poll> pollId = getPollenEntityId(poll);
        String pollUrl = getPollenUIUrlRenderService().getPollVoteUrl(
                getUIContext().getPollVoteUrl(),
                pollId.getReducedId(),
                null);
        email.setUrl(pollUrl);

        email.setAdministrator(admin);
        email.setReports(getReportTopiaDao().forTargetIdEquals(poll.getTopiaId()).findAll());
        email.setResume(getReportTopiaDao().getReportResume(poll.getTopiaId()));

        return email;
    }

    protected String getChoiceValue(Choice choice) {
        String value;
        switch (choice.getChoiceType()) {
            case DATE:
            case DATETIME:
                Date date = new Date(Long.parseLong(choice.getChoiceValue()));
                value = PollenMail.formatDate(date, getLocale(), getTimeZone());
                break;
            case RESOURCE:
                PollenResource resource = getPollenResourceDao().forTopiaIdEquals(choice.getChoiceValue()).findUnique();
                PollenEntityId<PollenResource> resourceId = getPollenEntityId(resource);
                value = getPollenUIUrlRenderService().getResourceUrl(
                        getUIContext().getResourceUrl(),
                        resourceId.getReducedId());
                break;
            default:
                value = choice.getChoiceValue();
        }
        return value;
    }

    public FeedbackEmail newFeedbackMail(FeedbackBean feedbackBean, PollenUserBean user, String sessionId, Locale locale) {
        FeedbackEmail email = new FeedbackEmail(locale, getTimeZone());
        email.setUser(user);
        email.setFeedback(feedbackBean);
        email.setSessionId(sessionId);
        email.setFeedbackDate(getNow());
        email.setUserEmail(feedbackBean.getUserEmail());
        if (feedbackBean.getScreenShotId() != null) {
            String screenShotUrl = getPollenUIUrlRenderService().getResourceUrl(
                    getUIContext().getResourceUrl(),
                    feedbackBean.getScreenShotId().getReducedId());
            email.setScreenShotUrl(screenShotUrl);
        }

        String version = getClass().getPackage().getImplementationVersion();
        email.setApplicationVersion(version);
        return email;
    }

    public ExceedingMaxVotersEmail newExceedingMaxVotersEmail(Poll poll, int maxVoters) {
        ExceedingMaxVotersEmail email = new ExceedingMaxVotersEmail(getLocale(), getTimeZone());
        email.setPoll(poll);
        email.setMaxVoters(maxVoters);
        email.setOffersUrl(getUIContext().getOffersUrl());
        return email;
    }

    protected String getMailId(PollenMail mail) {
        PollenMailType pollenEmailType = PollenMailType.valueOfMail(mail);
        return pollenEmailType.name() + MAIL_ID_SEPARATOR +
                mail.getEntitiesKey().stream()
                .map(entity -> PollenEmailKeyType.valueOfEntity(entity).name() + getReduceId(entity))
                .collect(Collectors.joining(MAIL_ID_SEPARATOR));
    }

    /**
     * Fait suivre le message donnée en paramètre aux adresses données en paramètre.
     * <p>
     * http://www.oracle.com/technetwork/java/faq-135477.html#forward
     */
    void forward(Message messageToForward, Set<String> tos, String subject, String body, String from, boolean preventAutomaticResponse) throws EmailException, MessagingException {
        Preconditions.checkArgument(!tos.isEmpty(), "il faut préciser au moins un destinataire à qui faire suivre le message " + messageToForward);

        PollenServicesConfig applicationConfig = getPollenServiceConfig();

        // create a body part to contain whatever text you want to include
        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setText(body);

        // create another body part to contain the message to be forwarded
        MimeBodyPart messageToForwardAsAttachment = new MimeBodyPart();
        messageToForwardAsAttachment.setContent(messageToForward, "message/rfc822");
        messageToForwardAsAttachment.setDisposition(Part.ATTACHMENT);

        Email newEmail = new MultiPartEmail();

        newEmail.setHostName(applicationConfig.getSmtpHost());
        newEmail.setSmtpPort(applicationConfig.getSmtpPort());
        newEmail.setCharset(Charsets.UTF_8.name());
        newEmail.setSubject(subject);
        newEmail.setFrom(from);

        for (String to : tos) {
            newEmail.addTo(to);
        }
        newEmail.setMsg(body);

        if (preventAutomaticResponse) {
            newEmail.addHeader("Return-Path", "<>");
        }
        MimeMultipart multipart = new MimeMultipart();
        multipart.addBodyPart(bodyPart);
        multipart.addBodyPart(messageToForwardAsAttachment);
        newEmail.setContent(multipart);
        try {
            newEmail.send();
        } catch (Throwable e) {
            throw new PollenTechnicalException("newEmail.send() failed: " + e);
        }
    }

}
